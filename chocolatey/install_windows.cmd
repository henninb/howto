@echo off

SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin

echo must run as admin
echo In the choco.exe Properties window -> Compatibility tab checkbox "Run this program as an administrator".
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
echo In the choco.exe Properties window -> Compatibility tab checkbox "Run this program as an administrator".
pause
