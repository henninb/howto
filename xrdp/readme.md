https://www.systutorials.com/4054/setting-mate-as-default-desktop-environment-for-startx-on-fedora/
http://www.elinuxbook.com/configure-xrdp-remote-desktop-server-in-linux/
https://www.centos.org/forums/viewtopic.php?t=64788
https://askubuntu.com/questions/234856/unable-to-do-remote-desktop-using-xrdp

## RHEL/CentOS 7 64-Bit ##
# wget http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
# rpm -ivh epel-release-latest-7.noarch.rpm


# vnc
https://www.tecmint.com/install-and-configure-vnc-server-in-centos-7/


#not sure if block is require
echo "mate-session" > ~/.Xclients
chmod a+x ~/.Xclients
####################

sudo yum install -y epel-release
sudo yum -y install xrdp tigervnc-server


Windows RDP (Choose Color depth of remote session)
High quality - 32bit

vncpasswd henninb

[henninb@centos7 log]$ vncpasswd
Password:
Verify:
Would you like to enter a view-only password (y/n)? y
Password:
Verify:

sudo cp /lib/systemd/system/vncserver@.service  /etc/systemd/system/vncserver@:1.service
sudo vi /etc/systemd/system/vncserver@\:1.service
[Unit]
Description=Remote desktop service (VNC)
After=syslog.target network.target
[Service]
Type=forking
ExecStartPre=/bin/sh -c '/usr/bin/vncserver -kill %i > /dev/null 2>&1 || :'
ExecStart=/sbin/runuser -l henninb -c "/usr/bin/vncserver %i -geometry 1920x1080"
PIDFile=/home/henninb/.vnc/%H%i.pid
ExecStop=/bin/sh -c '/usr/bin/vncserver -kill %i > /dev/null 2>&1 || :'
[Install]
WantedBy=multi-user.target


sudo systemctl daemon-reload
sudo systemctl start vncserver@:1
sudo systemctl status vncserver@:1
sudo systemctl enable vncserver@:1



#remove vnc
sudo rm -rf /tmp/.X*
sudo yum install xrdp tightvnc-server
echo exec mate-session > $HOME/.xinitrc
chmod 755 $HOME/.xinitrc

echo exec mate-session > $HOME/.xsession
chmod 755 $HOME/.xsession

echo exec mate-session > $HOME/.Xclients
chmod 755 $HOME/.Xclients

sudo firewall-cmd --zone=public --add-port=3389/tcp --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --list-ports

sudo firewall-cmd --zone=public --add-port=5901/tcp --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --list-ports

sudo systemctl start xrdp
sudo systemctl enable xrdp

#sudo chcon --type=bin_t /usr/sbin/xrdp
#sudo chcon --type=bin_t /usr/sbin/xrdp-sesman

sudo netstat -antup | grep xrdp
sudo netstat -antup | grep vnc
sudo netstat -tulpn | grep vnc

# not sure if required
sudo cp /etc/xrdp/xrdp.ini
/etc/xrdp/xrdp.ini 
GLOBAL
address=0.0.0.0
###################

sudo vi /etc/xrdp/xrdp.ini
[Xvnc]
name=Xvnc
lib=libvnc.so
username=ask
password=ask
ip=127.0.0.1
port=5901    #was -1

sudo systemctl restart xrdp


xfce4-session
yum install epel-release -y
sudo yum groupinstall -y "Xfce"
