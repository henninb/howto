#!/bin/sh

sudo apt-get install tightvncserver xrdp

#echo "exec xfce4-session" > ~/.Xclients
#chmod a+x ~/.Xclients

vncpasswd

sudo cp vncserver_mint.service /etc/systemd/system/vncserver@.service
sudo systemctl daemon-reload
sudo systemctl enable vncserver@1.service
sudo systemctl start vncserver@1

sudo sed -i "s/port=-1/port=5901/g" /etc/xrdp/xrdp.ini
sudo systemctl restart xrdp

exit 0