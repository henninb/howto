#!/bin/sh

sudo yum -y install xrdp tigervnc-server
sudo yum groupinstall -y "Xfce"
echo install xrdp on centos7 using mate-session
sudo yum groupinstall -y "X Window system"

echo "exec xfce4-session" > ~/.Xclients
chmod a+x ~/.Xclients

vncpasswd

sudo cp vncserver.service /etc/systemd/system/vncserver@\:1.service

sudo systemctl daemon-reload
sudo systemctl start vncserver@:1
sudo systemctl status vncserver@:1
sudo systemctl enable vncserver@:1

#remove vnc
#sudo rm -rf /tmp/.X*

#echo exec mate-session > $HOME/.xinitrc
#chmod 755 $HOME/.xinitrc
#echo exec mate-session > $HOME/.xsession
#chmod 755 $HOME/.xsession
#echo exec mate-session > $HOME/.Xclients
#chmod 755 $HOME/.Xclients

sudo firewall-cmd --zone=public --add-port=3389/tcp --permanent
sudo firewall-cmd --zone=public --add-port=5901/tcp --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --list-ports

sudo systemctl start xrdp
sudo systemctl enable xrdp

#sudo chcon --type=bin_t /usr/sbin/xrdp
#sudo chcon --type=bin_t /usr/sbin/xrdp-sesman

sudo sed -i "s/port=-1/port=5901/g" /etc/xrdp/xrdp.ini

sudo systemctl restart xrdp

#xfce4-session
#yum install epel-release -y
#sudo yum groupinstall -y "Xfce"

sudo netstat -antup | grep xrdp
#sudo netstat -antup | grep vnc
sudo netstat -tulpn | grep vnc

echo Windows RDP Choose Color depth of remote session High quality 32bit

xrdp --version

exit 0
