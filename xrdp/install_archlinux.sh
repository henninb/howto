#!/bin/sh

mkdir -p $HOME/projects

sudo pacman -S patch autoconf automake pkg-config fakeroot

cd $HOME/projects
git clone https://aur.archlinux.org/xrdp.git
cd xrdp

makepkg -si

sudo systemctl start xrdp
sudo systemctl status xrdp

cd $HOME/projects
git clone https://aur.archlinux.org/tightvnc.git
cd tightvnc

cd $HOME/projects
git clone https://aur.archlinux.org/xorgxrdp-git.git
cd xorgxrdp-git
makepkg -si

exit 0

setpriv --no-new-privs Xorg :10 -auth .Xauthority -config xrdp/xorg.conf -noreset -nolisten tcp -logfile /tmp/xorgxrdp.%s.log


[henninb@archlinux ~]$ ps -ef| grep sesman
root       967     1  0 10:42 ?        00:00:00 /usr/bin/xrdp-sesman

sudo kill -9 $(pgrep -f xrdp-sesman)
sudo xrdp-sesman -n &


sudo mv /etc/xrdp/xrdp.ini /etc/xrdp/xrdp.ini.bak
sudo su
echo "
[xrdp8]
name=Vino-Session
lib=libvnc.so
username=ask
password=ask
ip=127.0.0.1
port=5900
" >> "/etc/xrdp/xrdp.ini"


sudo mv /etc/xrdp/xrdp.ini.bak /etc/xrdp/xrdp.ini