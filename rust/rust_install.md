# install rustup to install version of rust
``
curl -sSf https://sh.rustup.rs | sh
curl -sSf https://static.rust-lang.org/rustup.sh | sh
``

#/usr/local/bin/rustc

rustup show

rustup install nightly
rustup default nightly
rustup update
