sudo netstat -anp | grep sshd

sudo adduser joeuser
sudo passwd joeuser

sudo mkdir -p /SECURE/Jail
sudo chown root:root /SECURE/Jail
sudo chmod 755 /SECURE/Jail

sudo mkdir -p /SECURE/Jail/dev/
sudo mknod -m 666 /SECURE/Jail/dev/null c 1 3
sudo mknod -m 666 /SECURE/Jail/dev/tty c 5 0
sudo mknod -m 666 /SECURE/Jail/dev/zero c 1 5
sudo mknod -m 666 /SECURE/Jail/dev/random c 1 8

cd /SECURE/Jail/
ldd /bin/bash

mkdir -p /SECURE/Jail/bin
sudo cp -v /bin/bash /SECURE/Jail/bin

sudo mkdir -p /SECURE/Jail/lib64/
sudo cp /usr/lib64/libtinfo.so.5 /SECURE/Jail/lib64/
sudo cp /usr/lib64/ld-linux-x86-64.so.2 /SECURE/Jail/lib64/
sudo cp /usr/lib64/libdl.so.2 /SECURE/Jail/lib64/
sudo cp /usr/lib64/libc.so.6 /SECURE/Jail/lib64/


sudo mkdir -p /SECURE/Jail/etc/
sudo cp -vf /etc/{passwd,group} /SECURE/Jail/etc/


vi /etc/ssh/sshd_config

Match User joeuser
ChrootDirectory /SECURE/Jail
ForceCommand internal-sftp

sudo systemctl restart sshd.service