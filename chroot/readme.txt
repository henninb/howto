# http://danieldandrada.blogspot.com/2006/09/ubuntu-chroot-environments.html

chroot enables you to run a shell having a different filesystem root directory
than the real one. e.g. running a shell using the directory
/opt/my_sandbox_root as its root directory.
n that case /opt/my_sandbox_root/usr/bin would be seen by this "chrooted"
shell as /usr/bin.

Putting all the necessary files into this fake root directory (like bash, libc,
debian-utils, gcc, etc) is tricky and laborious.
To save you from that hassle there is a clever tool called debootstrap,
which downloads and installs all the necessary packages to run a minimal
distro in your chrooted environment.

http://packages.ubuntu.com/

http://archive.ubuntu.com/ubuntu/dists/dapper/

sudo debootstrap --arch i386 dapper /opt/dapper_root
sudo debootstrap --arch i386 dapper /opt/dapper_root

---
debootstrap --arch=amd64 trusty /opt/trusty http://mirror.internode.on.net/pub/ubuntu/ubuntu/


sudo cp /etc/passwd /opt/trusty/etc/
sudo cp /etc/shadow /opt/trusty/etc/
sudo cp /etc/group /opt/trusty/etc/
sudo cp /etc/sudoers /opt/trusty/etc/
sudo cp /etc/hosts /opt/trusty/etc/
---

/tmp /home/ddtc/dapper_root/tmp none bind 0 0 (you won't be able to run X apps from your chroot environment without it)
/dev /home/ddtc/dapper_root/dev none bind 0 0
/proc /home/ddtc/dapper_root/proc proc defaults 0 0

sudo chroot /opt/trusty
