https://www.cyberciti.biz/tips/chroot-apache-under-rhel-fedora-centos-linux.html

sudo mkdir /httpdjail

sudo mkdir -p /httpdjail/var/run
sudo chown -R root.root /httpdjail/var/run
sudo mkdir -p /httpdjail/home/httpd
sudo mkdir -p /httpdjail/var/www/html
sudo mkdir -p /httpdjail/tmp
sudo chmod 1777 /httpdjail/tmp
sudo mkdir -p /httpdjail/var/lib/php/session
sudo chown root.apache /httpdjail/var/lib/php/session

wget http://core.segfault.pl/~hobbit/mod_chroot/dist/mod_chroot-0.5.tar.gz
tar -zxvf mod_chroot-0.5.tar.gz

sudo yum install httpd-devel

cd mod_chroot-0.5
sudo apxs -cia mod_chroot.c


/etc/httpd/conf/httpd.conf