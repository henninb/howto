rip dvd with dd

dd if=/dev/cdrom of=/path/to/cdcopy.iso
dd --list

\\.\Volume{e24717c2-ddd7-11dd-9a8a-806d6172696f}\
  link to \\?\Device\CdRom0
  CD-ROM
  Mounted on \\.\z:

@echo off
set PATH=c:\tools;%PATH%
dd if=\\.\z: of=file.iso
pause
