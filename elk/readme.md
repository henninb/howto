# install centos 7
sudo rpm --import http://packages.elastic.co/GPG-KEY-elasticsearch

echo '[elasticsearch-2.x]
name=Elasticsearch repository for 2.x packages
baseurl=http://packages.elastic.co/elasticsearch/2.x/centos
gpgcheck=1
gpgkey=http://packages.elastic.co/GPG-KEY-elasticsearch
enabled=1
' | sudo tee /etc/yum.repos.d/elasticsearch.repo


sudo yum -y install elasticsearch

sudo vi /etc/elasticsearch/elasticsearch.yml
network.host: 0.0.0.0
http.port: 9200


sudo systemctl start elasticsearch


echo '[kibana-4.4]
name=Kibana repository for 4.4.x packages
baseurl=http://packages.elastic.co/kibana/4.4/centos
gpgcheck=1
gpgkey=http://packages.elastic.co/GPG-KEY-elasticsearch
enabled=1' | sudo tee /etc/yum.repos.d/kibana.repo


sudo yum -y install kibana

server.port: 5601
server.host: "0.0.0.0"

sudo systemctl start kibana
sudo chkconfig kibana on



echo '[logstash-2.2]
name=logstash repository for 2.2 packages
baseurl=http://packages.elasticsearch.org/logstash/2.2/centos
gpgcheck=1
gpgkey=http://packages.elasticsearch.org/GPG-KEY-elasticsearch
enabled=1' | sudo tee /etc/yum.repos.d/logstash.repo

sudo yum -y install logstash
