---

situation - There is a jar of gum balls on my desk
potential outcomes - it has 2 potential outcomes -- Either there are an even number of gum balls or odd number of gum balls.
claim - The the number of gum balls is even

question - if I reject the claim the number of gum balls is even then does that mean i believe the number is odd?
answer - NO
---

belief - acceptance of a proposition that is true or likely true
knowledge - expression of of confidence ( really really believe it )
we act on what we believe (what we accept to be true)
---

default position
certainty

Should I not believe proposition?

There is a possibly unicorns exists

arguments for a proposition

---

rejection for claims (a-unicorns)
negation for claims (claim requires burdon of proof)

I reject the claim which is not saying that x is true or false (I don't know)

---
Statements, hypotheses, or theories have falsifiability or refutability if there is the inherent possibility that they can be proven false

---

alternative hypothisis
null hypothisis
what is the default position?

"null hypothesis" is a general statement or default position that there is no relationship between two measured phenomena
The null hypothesis is generally assumed to be true until evidence indicates otherwise.
In this case the null hypothesis is rejected and an alternative hypothesis is accepted in its place
we cannot prove unicorns do not exist

---

An assertion is just a point of view, an opinion. An argument goes further. An argument is a point of view supported by reasons that demonstrate the view is a good one.

---

Sound argument - if and only if it is both valid, and all of its premises are actually true.
valid agrument - An argument form is valid if and only if whenever the premises are all true, then conclusion is true.

# sound and valid (accept)
Bobby plays either Baseball or Football.
Bobby does not play Football.
Therefore, Bobby plays Baseball.

# sound but not valid (reject)
All kids are from California.
All people from California play football.
Therefore all kids from California play football.

---

Logical Falacies
https://www.webpages.uidaho.edu/eng207-td/Logic%20and%20Analysis/most_common_logical_fallacies.htm

---

I do not accept x ( i do not know)
i think x is false (an assertion)

---

Therefore, inductive reasoning moves from specific instances into a generalized conclusion, 
while deductive reasoning moves from generalized principles that are known to be true to a true and specific conclusion.
