@echo off

wget https://github.com/coreybutler/nvm-windows/releases/download/1.1.6/nvm-setup.zip

nvm install 4.4.0
nvm use 4.4.0
nvm list

sudo npm install -g create-react-app
create-react-app my-app
cd my-app
npm start

pause
