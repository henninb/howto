#!/bin/sh

sudo yum install --enablerepo=epel-testing npm nodejs
sudo npm install -g react
sudo npm install -g create-react-app
sudo npm install -g gatsby

# node version manager
sudo npm install -g nvm


cd $HOME/projects
create-react-app my-app
cd my-app
npm start

# gatsby based
cd $HOME/projects
gatsby new my-app1
cd my-app
gatsby develop
#Note that the development build is not optimized.
#To create a production build, use gatsby build
echo https://medium.freecodecamp.org/how-to-build-a-react-and-gatsby-powered-blog-in-about-10-minutes-625c35c06481

exit 0
