#!/bin/sh

sudo apt install vagrant
wget https://releases.hashicorp.com/vagrant/2.1.1/vagrant_2.1.1_x86_64.deb
sudo dpkg -i vagrant_2.1.1_x86_64.deb 

sudo apt-get build-dep vagrant ruby-libvirt
sudo apt install -y qemu libvirt-bin ebtables dnsmasq
sudo apt install -y libxslt-dev libxml2-dev libvirt-dev zlib1g-dev ruby-dev

vagrant plugin install vagrant-libvirt

exit 0
