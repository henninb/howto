sudo yum -y install https://releases.hashicorp.com/vagrant/1.9.6/vagrant_1.9.6_x86_64.rpm
vagrant plugin install vagrant-libvirt

vagrant add https://vagrantcloud.com/centos/boxes/7/versions/1804.02/providers/virtualbox.box
vagrant add https://vagrantcloud.com/freebsd/boxes/FreeBSD-11.0-STABLE/versions/2017.05.11.2/providers/virtualbox.box

vagrant init openbsd53
vagrant up

vagrant init freebsd/FreeBSD-11.0-STABLE
vagrant up

https://app.vagrantup.com/boxes/search

# for vagrant
sudo yum install -y qemu libvirt libvirt-devel ruby-devel gcc qemu-kvm
vagrant plugin install vagrant-libvirt

vagrant init centos/7
vagrant up
vagrant up --provider=libvirt
vagrant up --provider=virtualbox

Error while creating domain: Error saving the server: Call to virDomainDefineXML failed: invalid argument: could not find capabilities for domaintype=kvm
lsmod | grep kvm
egrep '(vmx|svm)' /proc/cpuinfo

sudo yum install libvirt-devel
