evil

.emacs
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)


In Emacs, "M-x command" means press M-x, then type the name of the command, and then press Enter.

  M-x package-refresh-contents
  M-x package-install RET evil
