#!/bin/sh

#wget https://www.rabbitmq.com/releases/rabbitmq-server/v3.6.15/rabbitmq-server-3.6.15-1.el7.noarch.rpm
#sudo yum install rabbitmq-server-3.6.15-1.el7.noarch.rpm

sudo rpm --import https://www.rabbitmq.com/rabbitmq-signing-key-public.asc
sudo yum install -y rabbitmq-server


sudo firewall-cmd --zone=public --permanent --add-port=4369/tcp --add-port=25672/tcp --add-port=5671-5672/tcp --add-port=15672/tcp  --add-port=61613-61614/tcp --add-port=1883/tcp --add-port=8883/tcp
sudo firewall-cmd --reload
sudo firewall-cmd --list-ports

#sudo firewall-cmd --permanent --add-port=4369/tcp --permanent
#sudo firewall-cmd --reload
#sudo firewall-cmd --list-ports

sudo chown -R rabbitmq:rabbitmq /var/lib/rabbitmq/

sudo systemctl start rabbitmq-server

exit 0
