ssh generate public key (id_rsa.pub) from pre-existing private key (id_rsa)
```
ssh-keygen -y -f id_rsa > id_rsa.pub
```

Usage of an alternative port
```
ssh user@192.168.1.1 -p 2222
```

Display the fingerprint of the key
```
ssh-keygen -lf  ~/.ssh/id_rsa.pub
ssh-keygen -lf  ~/.ssh/id_rsa
```

Display the fingerprint of the key (md5)
```
ssh-keygen -l -E md5 -f  ~/.ssh/id_rsa.pub
ssh-keygen -l -E md5 -f  ~/.ssh/id_rsa
```
