 select
   b.logon_time,
   c.owner,
   c.object_name,
   c.object_type,
   b.sid,
   b.serial#,
   b.status,
   b.osuser,
   b.machine
from
   v$locked_object a,
   v$session b,
   dba_objects c
where
   b.sid = a.session_id
and
   a.object_id = c.object_id order by b.sid;

SELECT * FROM v$locked_object a, dba_objects b WHERE a.object_id = b.OBJECT_id;


SELECT
          a.osuser || ':' || a.username   UserID
       , a.sid || '/' || a.serial#       usercode
     , b.lock_type Type, b.mode_held   Hold
   , c.owner || '.' || c.object_name Object
       , a.program                       Program
       , ROUND(d.seconds_in_wait/60,2)   WaitMin
     FROM
         v$session       a
      , dba_locks   b
      , dba_objects c
      , v$session_wait  d
 WHERE
         a.sid        =  b.session_id
    AND b.lock_type  IN ('DML','DDL')
      AND b.lock_id1   =  c.object_id
      AND b.session_id  =  d.sid;
