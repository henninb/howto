ALTER TABLE
   pick_task
ADD
   (
   pub_f CHAR(1) DEFAULT 'N' NOT NULL,  
   pub_retry_i NUMBER DEFAULT 0 NOT NULL
);

ALTER TABLE
   ship
ADD
   (
   pub_f CHAR(1) DEFAULT 'N' NOT NULL,  
   pub_retry_i NUMBER DEFAULT 0 NOT NULL
);
