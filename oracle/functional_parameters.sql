SET serveroutput on;
DECLARE
   out1 VARCHAR2(100);
   out2 NUMBER(3);
   out3 NUMBER(2);
   out4 NUMBER(4);
   out5 VARCHAR2(100);
BEGIN
   some_sp('030243692045', out1, out2, out3, out4, out5, 'G');
   --EXECUTE some_sp('12345', out1, out2, out3, out4, out5, 'G');
   dbms_output.put_line('OutParam1: ' || out1);
   dbms_output.put_line('OutParam1: ' || out2);
   dbms_output.put_line('OutParam1: ' || out3);
   dbms_output.put_line('OutParam1: ' || out4);
END;
