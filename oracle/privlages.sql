--granting access in dev
GRANT SELECT, INSERT, UPDATE, DELETE ON DIST.DC_ITEM_CODE_VAL TO oprint1;
GRANT SELECT, INSERT, UPDATE, DELETE ON DIST.DC_SIZE_CHAR TO oprint1;
GRANT SELECT, INSERT, UPDATE, DELETE ON DIST.DC_ITEM_CMNT TO oprint1;

select * from SYS.DBA_TAB_PRIVS where table_name='DC_SIZE_CHAR';

--should have access to the profile and not individual user

select * from DBA_TAB_PRIVS where 1=0;
select * from DBA_TAB_PRIVS where TABLE_NAME='PICK_TASK';

select username, account_status, lock_date from dba_users where lock_date is not null;

select * from DBA_ROLE_PRIVS where 1=0;

select * from DBA_ROLE_PRIVS where  grantee='CUSER';

select * from DBA_ROLE_PRIVS where granted_role IN (SELECT grantee from DBA_TAB_PRIVS where TABLE_NAME='PICK_TASK') AND grantee = 'CUSER';

select * from DBA_ROLE_PRIVS where granted_role IN (SELECT grantee from DBA_TAB_PRIVS where TABLE_NAME='APPT') AND grantee = 'CUSER';
