SELECT host_name FROM v$instance;

SELECT sys_context('USERENV','SERVICE_NAME') AS service_name FROM dual;
  
SELECT TO_DATE (TO_CHAR (SYSTIMESTAMP, 'YYYY-MON-DD HH24:MI:SS'), 'YYYY-MON-DD HH24:MI:SS') AS my_date FROM DUAL;
  
SELECT * FROM table1 WHERE modf_ts >= to_timestamp('2019-01-01', 'YYYY-MM-DD');

#Yesterday
SELECT SYSDATE-1 FROM DUAL;

SELECT * FROM table1 WHERE modf_ts >= to_timestamp('2019-01-01', 'YYYY-MM-DD');

SELECT * FROM table1 WHERE TRUNC(modf_ts) = trunc(SYSDATE-1);
