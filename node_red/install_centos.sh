#!/bin/sh

sudo yum install -y net-tools psmisc
sudo yum install -y epel-release gcc gcc-c++
sudo yum install -y --enablerepo=epel-testing npm nodejs
sudo npm install -g --unsafe-perm node-red

sudo cp node-red.service /etc/systemd/system/node-red.service
sudo systemctl daemon-reload

sudo systemctl enable node-red
sudo systemctl start node-red

sudo firewall-cmd --zone=public --add-port=1880/tcp --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --list-ports

netstat -na | grep LISTEN | grep tcp | grep 1880
netstat -tulp | grep node-red
sudo fuser 1880/tcp

#sudo vi /usr/lib/node_modules/node-red/setting.js
#sudo vi /root/.node-red/settings.js

exit 0
