# find pattern within file type
find . -type f -name '*.c' -print0 | xargs -0 grep --color=always -n PutToQueue
find . -type f -name "*.pc" -print0 | xargs -0 grep --color=always -n PutToQueue

find /home/cloud-user/Source -type f -name "*.pc" -print0 | xargs -0 grep --color=always -n PutToQueue

find . -type f -name ('*.pc' -o '*.c') -print0 | xargs -0 grep --color=always -n PutToQueue

find . -print | grep "golang_" | grep Makefile
find . -print | grep "groovy_" | grep run.sh
:n next file

find . -name "*.txt" | xargs bash -c '</dev/tty vim "$@"' ignoreme

find . -name "*.txt" | xargs bash -c '</dev/tty vim "$@"' ignoreme

vim $(find . -print | grep "groovy_" | grep run.sh)

vim $(find . -print | grep "sml_" | grep Makefile)

# find golang Makefiles with example as the text
find -type f -name Makefile | grep "golang_" | xargs grep example

# remove executables
find . -type f -perm /u=x,g=x,o=x | xargs rm

# find by size
find . -type f -printf "%s\t%p\n" | sort -n

# 100k
find /path -size +102400

# 10k
find . -size +10240

# list files 3 days old or less
find . -type f -mtime -3 -exec ls -l {} \;

# list files 1 day old or less
find . -type f -mtime -1 -exec ls -l {} \;

# list files 1 day old or more
find . -type f -mtime +1 -exec ls -1 *.pfa {} \; > list

# list files 365 days old or more
find . -type f -mtime +365
find . -type f -mtime +7 -exec rm {} \;

du --max-depth=5 /opt/3d/* | sort -rn

du -shx /opt/3d/* | sort -rh | head

df -H /opt/3d/
du -shx /opt/3d/dc*/var/* | sort -rh | head
du -shx /opt/3d/dc*/var/log/* | sort -rh | head
du -shx /opt/3d/dc*/var/log/rolled/* | sort -rh | head
du -shx /opt/3d/dc*/var/backups/* | sort -rh | head


for ELEMENT in $(cat list); do
  echo $ELEMENT
  rm $ELEMENT
done

# directories 1 deep
find . -type d -maxdepth 1

# find without permission denied
find / -name foo.bar -print 2>/dev/null

# find permissions with other write
find . -perm +o=w -exec ls -l {} \;
find /opt/data -perm +o=w -exec ls -l {} \;
find /opt/data -perm +o=r -exec ls -l {} \;
find /opt/data -perm +o=x -exec ls -l {} \;

# good
find /opt/data -perm -o+w -exec ls -l {} \;
find /opt/data -perm +o+r -exec ls -l {} \;
find /opt/data -perm -o+r -exec ls -l {} \;

# other writable
find /opt/data -perm -220 -exec ls -l {} \;
find /opt/data -perm -007 -exec ls -l {} \;
find /opt/data -perm -775 -exec ls -l {} \;
find /opt/data -perm -555 -exec ls -l {} \;

# good
find /opt/data -perm -006 -exec ls -l {} \;

find /opt/data -perm -770 -exec ls -l {} \;

find /opt/data -perm -2 -exec ls -l {} \;
find /opt/data -perm -1 -exec ls -l {} \;
find /opt/data -perm -4 -exec ls -l {} \;
find /opt/data -type d -maxdepth 1 -exec ls -ld {} \;

find /opt/data -type f \( -perm -004 -o -perm -044 -o -perm -444 -o -perm -040 -o -perm -440 -o -perm -400 -o -perm -404 \) -exec ls -l {} \;
find /opt/data -type f \( -perm -004 -o -perm -044 -o -perm -444 -o -perm -404 \) -exec ls -l {} \;
find /opt/data -type f \( -perm -001 -o -perm -011 -o -perm -111 -o -perm -101 \) -exec ls -l {} \;
find /opt/data -type f \( -perm -002 -o -perm -022 -o -perm -222 -o -perm -202 \) -exec ls -l {} \;

find /opt/data -type f \( -perm -002 -o -perm -022 -o -perm -222 -o -perm -202 -perm -004 -o -perm -044 -o -perm -444 -o -perm -404 -perm -001 -o -perm -011 -o -perm -111 -o -perm -101 \) -exec ls -l {} \;

# find all files with executable permissions
find /opt/data -perm 111 -exec ls -l {} \;
find /opt/data -perm 333 -exec ls -l {} \;

find /opt/data -perm 777 -exec ls -l {} \;
find /opt/data -perm 664 -exec ls -l {} \;
find /opt/data -perm 666 -exec ls -l {} \;
find /opt/data -perm --6 -exec ls -l {} \;
find /opt/data -perm --x -exec ls -l {} \;
find /opt/data -perm --x -exec ls -l {} \;

find /opt/data -perm o=rw -exec ls -l {} \;

find /usr -type d -maxdepth 1 -exec du -hc {} \;
find /opt/stdata/???? -type d -maxdepth 0 -exec du {} \;
find . -name *.bar -maxdepth 2 -print

find . -size +10000 -mtime -1 -exec ls -l {} \;

find /usr/src -not \( -name "*,v" -o -name ".*,v" \) '{}' \; -print

# find files modified within the past 24 hours
find . -mtime 0

# list files by size
ls -l | sort +4 -nr | less

# separate path by newline
echo $PATH | tr ':' '\n'

# polls the date every 2 seconds
watch date

# find cat
find . -name "*TAR2009*" -exec cat {} \; > ../TAR20090330CA
