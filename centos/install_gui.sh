#!/bin/sh

sudo yum groupinstall -y "Development Tools"
sudo yum install -y epel-release kernel-headers kernel-devel
sudo yum groupinstall -y "X Window system"
sudo yum groupinstall -y "MATE Desktop"

sudo systemctl isolate graphical.target
sudo systemctl set-default graphical.target
sudo rm '/etc/systemd/system/default.target'
sudo ln -s '/usr/lib/systemd/system/graphical.target' '/etc/systemd/system/default.target'
echo "exec mate-session" > $HOME/.xinitrc

sudo systemctl enable lightdm
#sudo service lightdm enable

sudo yum -y install http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm
sudo yum -y install vlc

echo install vm additions
sudo /usr/lib64/VBoxGuestAdditions/vboxadd setup
sudo usermod -a -G vboxsf $(whoami)

exit 0
