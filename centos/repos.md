[henninb@centos7 ~]$ yum repolist
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos.unixheads.org
 * epel: mirror.uic.edu
 * extras: centos.mbni.med.umich.edu
 * nux-dextop: mirror.li.nux.ro
 * updates: mirrors.cmich.edu
repo id                                                                repo name                                                                         status
base/7/x86_64                                                          CentOS-7 - Base                                                                    9,363
bintray--jfrog-artifactory-rpms                                        bintray--jfrog-artifactory-rpms                                                      109
cloudera-cdh5                                                          Cloudera's Distribution for Hadoop, Version 5                                        146
docker-ce-edge/x86_64                                                  Docker CE Edge - x86_64                                                                8
docker-ce-stable/x86_64                                                Docker CE Stable - x86_64                                                              9
epel/x86_64                                                            Extra Packages for Enterprise Linux 7 - x86_64                                    11,898
extras/7/x86_64                                                        CentOS-7 - Extras                                                                    451
google-chrome                                                          google-chrome                                                                          3
kubernetes                                                             Kubernetes                                                                            49
mongodb                                                                MongoDB repo                                                                         279
nux-dextop/x86_64                                                      Nux.Ro RPMs for general desktop use                                                2,489
packages-microsoft-com-mssql-server                                    packages-microsoft-com-mssql-server                                                   32
packages-microsoft-com-prod                                            packages-microsoft-com-prod                                                           37
updates/7/x86_64                                                       CentOS-7 - Updates                                                                 2,146
repolist: 27,019


zip -r myfiles.zip mydir
