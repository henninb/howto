sudo fallocate -l 2G /swapfile
sudo dd if=/dev/zero of=/swapfile count=2048 bs=1MiB

sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
