https://www.happyassassin.net/2015/01/14/trusting-additional-cas-in-fedora-rhel-centos-dont-append-to-etcpkitlscertsca-bundle-crt-or-etcpkitlscert-pem/

sudo yum install httpd
sudo yum install mod_ssl
sudo yum install openssl

sudo su
cd /root

> openssl genrsa -out ca.key 4096

openssl genrsa -out ca.key 2048
openssl req -new -key ca.key -out ca.csr
openssl x509 -req -days 365 -in ca.csr -signkey ca.key -out ca.crt

subject=/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=centos7/CN=centos7

mkdir -p /etc/pki/tls/certs
mkdir -p /etc/pki/tls/private
cp /root/ca.crt /etc/pki/tls/certs
cp /root/ca.key /etc/pki/tls/private
cp /root/ca.csr /etc/pki/tls/private

cp /root/ca.crt /tmp/
chmod 777 /tmp/ca.crt

sudo sed -i "s/SSLCertificateFile \/etc\/pki\/tls\/certs\/localhost.crt/SSLCertificateFile \/etc\/pki\/tls\/certs\/ca.crt/g" /etc/httpd/conf.d/ssl.conf
sudo sed -i "s/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/localhost.key/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/ca.key/g" /etc/httpd/conf.d/ssl.conf

vi /etc/httpd/conf.d/ssl.conf
<VirtualHost *:443>
  SSLEngine on
  SSLCertificateFile /etc/pki/tls/certs/ca.crt
  SSLCertificateKeyFile /etc/pki/tls/private/ca.key
  ServerName centos7
  Documentroot /var/www/html
</VirtualHost>

firewall-cmd --permanent --add-service=https
firewall-cmd --permanent --add-port=443/tcp
firewall-cmd --reload

sudo systemctl enable httpd.service

# import the ca.cert into the browser


# to verify
curl "<tareget URL>" --cert ./<KEYSTORE PUBLIC CERT> --key <PRIVATE KEY> --cacert <Truststore cert> -v
curl https://fbsd --cert /etc/pki/tls/certs/ca.crt --key /etc/pki/tls/private/ca.key  --cacert /etc/pki/tls/certs/ca.crt -v


https://letsencrypt.org/


security/ca_root_nss


/usr/local/share/certs/ca-root-nss.crt to /usr/local/openssl/cert.pem
https://github.com/freebsd/freebsd-ports/blob/master/security/ca_root_nss/Makefile


/etc/ssl/cert.pem

openssl s_client -connect fbsd:443 -CAfile /etc/ssl/cert.pem

/usr/local/share/certs/ca-root-nss.crt
