wget https://bintray.com/jfrog/artifactory-rpms/rpm -O bintray-jfrog-artifactory-rpms.repo
sudo mv bintray-jfrog-artifactory-rpms.repo /etc/yum.repos.d/
sudo yum install jfrog-artifactory-oss




The installation of Artifactory has completed successfully.

PLEASE NOTE: It is highly recommended to use Artifactory in conjunction with MySQL. You can easily configure this setup using '/opt/jfrog/artifactory/bin/configure.mysql.sh'.

You can activate artifactory with:
> sudo systemctl start artifactory.service

Then check the status with:
> systemctl status artifactory.service
  Verifying  : jfrog-artifactory-oss-5.4.6-50406900.noarch                                                                                                 1/1

Installed:
  jfrog-artifactory-oss.noarch 0:5.4.6-50406900

Complete!

/etc/opt/jfrog/artifactory/

/var/opt/jfrog/artifactory/logs/request.log


http://localhost:8081/artifactory


admin password
set

sudo firewall-cmd --zone=public --permanent --add-port=8081/tcp
sudo firewall-cmd --list-ports


/etc/opt/jfrog/artifactory/

docker pull docker.bintray.io/jfrog/artifactory-pro:latest
docker pull docker.bintray.io/jfrog/artifactory-oss:latest
docker run --name artifactory -d -p 8081:8081 docker.bintray.io/jfrog/artifactory-pro:latest
docker run --name artifactory -d -p 8081:8081 docker.bintray.io/jfrog/artifactory-oss:latest



[henninb@centos7 ~]$ ssh localhost -p 1339
The authenticity of host '[localhost]:1339 ([::1]:1339)' can't be established.
RSA key fingerprint is 84:97:6e:2d:d9:33:ec:84:e9:93:42:93:7e:1d:6b:43.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '[localhost]:1339' (RSA) to the list of known hosts.
Permission denied (publickey).
[henninb@centos7 ~]$ ssh localhost -p 1339
Permission denied (publickey).
[henninb@centos7 ~]$ 


curl -fL https://getcli.jfrog.io | sh

https://www.jfrog.com/getcli/
jfrog client for windows linux



C:\Users\z037640\Desktop>jfrog rt u froggy.tgz my-local-repo
The CLI commands require the Artifactory URL and authentication details
Configuring JFrog CLI with these parameters now will save you having to include
them as command options.
You can also configure these parameters later using the 'config' command.
Configure now? (y/n): y
Artifactory URL: https://localhost
Artifactory server ID:
API key (leave empty for basic authentication):
User: z037640
Password: [Info] Encrypting password...
[Error]
Artifactory response: 404 Not Found

C:\Users\z037640\Desktop>jfrog rt u froggy.tgz my-local-repo


 curl https://localhost/artifactory/api/application.wadl > f.xml
 
C:\Users\z037640\Desktop>jfrog rt u froggy.tgz my-local-repo
The CLI commands require the Artifactory URL and authentication details
Configuring JFrog CLI with these parameters now will save you having to include
them as command options.
You can also configure these parameters later using the 'config' command.
Configure now? (y/n): y
Artifactory URL: https://localhost/artifactory
Artifactory server ID:
API key (leave empty for basic authentication):
User: z037640
Password: [Info] Encrypting password...
[Info] Done encrypting password.
[Error] Path does not exist: froggy.tgz


curl -uadmin:APBqJw6aEYvUx2NV -T <PATH_TO_FILE> "http://10.0.2.15:8081/artifactory/my-repo/<TARGET_FILE_PATH>"
curl -uadmin:APBqJw6aEYvUx2NV -O "http://10.0.2.15:8081/artifactory/my-repo/<TARGET_FILE_PATH>"

[henninb@centos7 ~]$ ./jfrog rt u file.txt lib-snapshot-local
[henninb@centos7 ~]$ ./jfrog rt u file.txt my-local-repo
The CLI commands require the Artifactory URL and authentication details
Configuring JFrog CLI with these parameters now will save you having to include them as command options.
You can also configure these parameters later using the 'config' command.
Configure now? (y/n): y
Artifactory URL: http://localhost:8081/artifactory
Artifactory server ID:
API key (leave empty for basic authentication):
User: admin
Password: [Info] Encrypting password...
[Info] Done encrypting password.
[Info] [Thread 2] Uploading artifact: file.txt
[Error] [Thread 2] Artifactory response: 403 Forbidden
{
  "errors": [
    {
      "status": 403,
      "message": ""
    }
  ]
}
[Error] [Thread 2] Artifactory response: 403 Forbidden

[Info] Uploaded 0 artifacts.
[Error] Failed uploading 1 artifacts.
