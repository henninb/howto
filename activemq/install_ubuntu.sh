#!/bin/sh

sudo apt install net-tools psmisc wget curl java-1.8.0-openjdk

if [ ! -f apache-activemq-5.15.0-bin.tar.gz ]; then
  #wget https://archive.apache.org/dist/activemq/5.15.0/apache-activemq-5.15.0-bin.tar.gz
  curl https://archive.apache.org/dist/activemq/5.15.0/apache-activemq-5.15.0-bin.tar.gz --output apache-activemq-5.15.0-bin.tar.gz
fi
sudo tar -zxvf apache-activemq-5.15.0-bin.tar.gz -C /opt
sudo ln -s /opt/apache-activemq-5.15.0 /opt/activemq

cat > activemq.service <<'EOF'
[Unit]
Description=activemq message queue
After=network.target
[Service]
PIDFile=/opt/activemq/data/activemq.pid
ExecStart=/opt/activemq/bin/activemq start
ExecStop=/opt/activemq/bin/activemq stop
User=root
Group=root
[Install]
WantedBy=multi-user.target
EOF

ls -l /etc/systemd/system/*
ls -l /run/systemd/system/*
ls -l /lib/systemd/system/*
sudo cp activemq.service /lib/systemd/system/activemq.service

sudo systemctl daemon-reload

sudo systemctl enable activemq
sudo systemctl start activemq
sudo systemctl status activemq

sleep 10

curl http://localhost:8161 --output index.html

netstat -na | grep tcp | grep LIST | grep 8161
netstat -na | grep tcp | grep LIST | grep 61616
netstat -na | grep tcp | grep LIST | grep 61613

sudo fuser 8161/tcp
sudo fuser 61616/tcp
sudo fuser 61613/tcp

#echo "admin:admin"

exit 0
