@echo off

set PATH=C:\Program Files\Oracle\VirtualBox

VBoxManage controlvm  "centos7" savestate
VBoxManage modifyvm "centos7" --natpf1 "activemq http,tcp,127.0.0.1,8161,,8161"
VBoxManage modifyvm "centos7" --natpf1 "activemq,tcp,127.0.0.1,61616,,61616"
VBoxManage startvm "centos7" --type headless

pause
