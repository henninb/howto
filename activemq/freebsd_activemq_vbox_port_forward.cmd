@echo off

set PATH=C:\Program Files\Oracle\VirtualBox

VBoxManage controlvm  "freebsd" savestate
VBoxManage modifyvm "freebsd" --natpf1 "activemq http,tcp,127.0.0.1,8161,,8161"
VBoxManage modifyvm "freebsd" --natpf1 "activemq,tcp,127.0.0.1,61616,,61616"
VBoxManage startvm "freebsd" --type headless

pause
