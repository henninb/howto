#!/bin/sh

sudo pkg install activemq

sudo sysrc activemq_enable="YES"

sudo service activemq restart

netstat -na | grep tcp | grep LIST | grep 8161
netstat -na | grep tcp | grep LIST | grep 61616

curl http://localhost:8161

exit 0
