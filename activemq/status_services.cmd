@echo off

set PATH=C:\opscode\chefdk\embedded\bin;%PATH%

echo "activemq http on 8161"
netstat -na | grep LISTEN | grep TCP | grep 8161

echo "activemq on 61616" 
netstat -na | grep LISTEN | grep TCP | grep 61616

pause
