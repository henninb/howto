#!/bin/sh

if [ $# -ne 1 ]; then
    echo "Usage: $0 <server>"
    exit 1
fi

SERVER_NAME=$1

## Create the broker keystore
keytool -genkey -noprompt -alias amq-server -keyalg RSA -keysize 2048 -validity 365 -keystore amq-server.ks -dname "CN=${SERVER_NAME}, OU=${SERVER_NAME}, O=Target, L=Minneapolis, ST=MN, C=US" -storepass monday1 -keypass monday1

## Export the broker certificate from the keystore
keytool -export -alias amq-server -keystore amq-server.ks -file amq-server_cert

## Create the CLIENT keystore
keytool -genkey -noprompt -alias amq-client -keyalg RSA -keysize 2048 -validity 365 -keystore amq-client.ks -dname "CN=${SERVER_NAME}, OU=${SERVER_NAME}, O=Target, L=Minneapolis, ST=MN, C=US" -storepass monday1 -keypass monday1

## Import the previous exported broker's certificate into a CLIENT truststore
keytool -import -alias amq-server -keystore amq-client.ts -file amq-server_cert

## If you want to make trusted also the client, you must export the client's certificate from the keystore
keytool -export -alias amq-client -keystore amq-client.ks -file amq-client_cert

## Import the client's exported certificate into a broker SERVER truststore
keytool -import -alias amq-client -keystore amq-server.ts -file amq-client_cert

keytool -importkeystore -srckeystore amq-server.ks -destkeystore amq-server.p12 -srcalias amq-server -srcstoretype jks -deststoretype pkcs12
openssl pkcs12 -in amq-server.p12 -out amq-server.pem

keytool -importkeystore -srckeystore amq-client.ks -destkeystore amq-client.p12 -srcalias amq-client -srcstoretype jks -deststoretype pkcs12
openssl pkcs12 -in amq-client.p12 -out amq-client.pem

echo sudo cp amq-server.ks amq-server.ts /opt/activemq/conf
echo sudo systemctl restart activemq

echo openssl s_client -connect ${SERVER_NAME}:61617 -CAfile amq-client.pem
echo openssl s_client -connect ${SERVER_NAME}:61617 -CAfile amq-client.pem -state -debug

exit 0

 <broker xmlns="http://activemq.apache.org/schema/core" brokerName="archlinux" dataDirectory="${activemq.data}">
 
        <transportConnectors>
            <transportConnector name="ssl" uri="ssl://0.0.0.0:61617?trace=true&amp;needClientAuth=true"/>
            <transportConnector name="stomp+ssl" uri="stomp+ssl://0.0.0.0:61612"/>
        </transportConnectors>
        <sslContext>
            <sslContext
            keyStore="amq-server.ks" keyStorePassword="monday1"
            trustStore="amq-client.ks" trustStorePassword="monday1"/>
        </sslContext>

