#!/bin/sh

sudo apt-get install postfix mailutils

sudo vi /etc/postfix/sasl_passwd
[smtp.gmail.com]:587    henninb08@gmail.com:password

sudo chmod 600 /etc/postfix/sasl_passwd

sudo vi /etc/postfix/main.cf
relayhost = [smtp.gmail.com]:587
smtp_use_tls = yes
smtp_sasl_auth_enable = yes
smtp_sasl_security_options =
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt


sudo postmap /etc/postfix/sasl_passwd
sudo systemctl restart postfix

echo test | mail -s "Test subject" henninb@gmail.com

netstat -na | grep 25

exit 0
