#!/bin/sh

sudo yum install -y postfix mutt mailx cyrus-sasl-plain

sudo systemctl enable postfix
sudo systemctl reload postfix

echo "[smtp.gmail.com]:587    henninb08@gmail.com:password" | sudo tee /etc/postfix/sasl_passwd

#sudo chmod 600 /etc/postfix/sasl_passwd

sudo chown root:postfix /etc/postfix/sasl_passwd*
sudo chmod 640 /etc/postfix/sasl_passwd*

sudo cp main_centos.cf /etc/postfix/main.cf
#sudo vi /etc/postfix/main.cf
#relayhost = [smtp.gmail.com]:587
#smtp_use_tls = yes
#smtp_sasl_auth_enable = yes
#smtp_sasl_security_options =
#smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
#smtp_tls_CAfile = /etc/ssl/certs/ca-bundle.crt

sudo postmap /etc/postfix/sasl_passwd
sudo systemctl restart postfix

echo test | mail -s "Test subject - Centos" henninb@gmail.com

#sudo tail -f /var/log/maillog

netstat -na | grep LISTEN | grep 25

#To find out what SASL implementations are compiled into Postfix, use the following commands:
# postconf -a (SASL support in the SMTP server)
# postconf -A (SASL support in the SMTP+LMTP client)

exit 0
