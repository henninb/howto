#!/bin/sh

#To create a quick self-signed certificate for the server, use the following OpenSSL command:

openssl req -new -text -out server.req -days 365 -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=centos7/CN=centos7" -passin pass:monday1 -passout pass:monday1
#Fill out the information that openssl asks for. Make sure you enter the local host name as "Common Name"; the challenge password can be left blank. The program will generate a key that is passphrase protected; it will not accept a passphrase that is less than four characters long. To remove the passphrase (as you must if you want automatic start-up of the server), run the commands:

openssl rsa -in privkey.pem -out server.key -passin pass:monday1
#rm privkey.pem
#Enter the old passphrase to unlock the existing key. Now do:

openssl req -x509 -in server.req -text -key server.key -out server.crt
#to turn the certificate into a self-signed certificate and to copy the key and certificate to where the server will look for them. Finally do:

chmod og-rwx server.key

exit 0
