@echo off

set PATH=C:\Program Files\Oracle\VirtualBox

VBoxManage modifyvm "centos7" --natpf1 "Postges,tcp,127.0.0.1,5432,,5432"
rem VBoxManage modifyvm "centos7" --natpf1 "Https,tcp,127.0.0.1,443,,443"
rem VBoxManage modifyvm "centos7" --natpf1 "Kafka,tcp,127.0.0.1,9092,,9092"
rem VBoxManage modifyvm "centos7" --natpf1 "Zookeeper,tcp,127.0.0.1,2181,,2181"

pause
