#!/bin/sh

sudo pkg install -y postgresql96-server
sudo sysrc postgresql_enable="YES"
#sudo rm /var/db/postgres/data96/
sudo /usr/local/etc/rc.d/postgresql initdb

sudo cp pg_hba.conf /var/db/postgres/data96/pg_hba.conf
#sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '0.0.0.0'/g" /var/db/postgres/data96/postgresql.conf
cp default_commands.sql /tmp
sudo -u postgres sh -c 'cd /tmp && psql postgres -U postgres < /tmp/default_commands.sql'

echo sudo -i -u postgres pwd
echo pg_ctl

sudo service postgresql start

netstat -na | grep 5432

exit 0
