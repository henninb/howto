#!/bin/sh

sudo apt install postgresql-9.5

#sudo rm -rf /var/lib/pgsql/data/
sudo postgresql-setup initdb

sudo systemctl enable postgresql
sudo systemctl start postgresql
sudo systemctl status postgresql

cp default_commands.sql /tmp
#sudo -H -u postgres bash -c 'whoami'
#sudo -u postgres sh -c 'psql postgres'
sudo -u postgres sh -c 'cd /tmp && psql postgres -U postgres < /tmp/default_commands.sql'

#echo sudo -i -u postgres pwd
echo sudo su - postgres
echo psql psql postgres -U henninb
echo psql psql postgres -U postgres

sudo cp pg_hba.conf /etc/postgresql/9.5/main/pg_hba.conf

#sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '0.0.0.0'/g" /var/lib/pgsql/data/postgresql.conf
sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/g" /etc/postgresql/9.5/main/postgresql.conf

sudo systemctl restart postgresql

netstat -na | grep 5432
sudo fuser 5432/tcp
echo pg_ctl

exit 0
