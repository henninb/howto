#!/bin/sh

sudo yum install -y net-tools psmisc

sudo yum install -y https://s3-us-west-2.amazonaws.com/grafana-releases/release/grafana-5.0.1-1.x86_64.rpm

#sudo service grafana-server start
sudo systemctl start grafana-server
sudo systemctl enable grafana-server.service

sudo firewall-cmd --zone=public --add-port=3000/tcp --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --list-ports

echo "admin:admin"

netstat -na | grep LISTEN | grep tcp | grep 3000
netstat -tulp
sudo fuser 3000/tcp

exit 0
