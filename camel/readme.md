Spring XML DSL (Domain Specific Language) or Blueprint XML DSL?

org.apache.sshd.common.KeyPairProvider

<plugin>
  <groupId>org.apache.camel</groupId>
  <artifactId>camel-maven-plugin</artifactId>
  <!-- optional, default value: org.apache.camel.spring.Main -->
  <configuration>
    <mainClass>mypackage.boot.camel.CamelStartup</mainClass>
  </configuration>
</plugin>

http://www.kevinboone.net/mavencameltest.html

camel-maven-plugin

cd examples/camel-example-spring
mvn install
mvn camel:run

install camel-maven


mvn archetype:generate -DarchetypeGroupId=org.apache.camel.archetypes -DarchetypeArtifactId=camel-archetype-java -DarchetypeVersion=2.19.1 -DgroupId=camel_example



mvn archetype:generate 
  -DarchetypeGroupId=org.apache.camel.archetypes   
  -DarchetypeArtifactId=camel-archetype-spring    
  -DarchetypeVersion=2.12.1              
  -DgroupId=myGroupId                  
  -DartifactId=myArtifactId 
  

  
// works great
mvn archetype:generate                            \
  -DarchetypeGroupId=org.apache.camel.archetypes  \
  -DarchetypeArtifactId=camel-archetype-spring    \
  -DarchetypeVersion=2.19.1                       \
  -DgroupId=camel-example-file                \
  -DartifactId=camel-example-file                       \
  -DinteractiveMode=false

mvn install
mvn camel:run




Here is the promised configuration for Blueprint Admin service. 

blueprint.xml: 

<blueprint> 
... 
    <!-- Load in application properties reference --> 
    <cm:property-placeholder persistent-id="etb.esb.config"> 
        <cm:default-properties> 
            <cm:property name="foo.conf.value" value="foo"/> 
            <cm:property name="bar.conf.value" value="bar"/> 
        </cm:default-properties> 
    </cm:property-placeholder> 
... 
</blueprint>
