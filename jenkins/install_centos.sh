#!bin/sh

sudo yum install -y net-tools psmisc

#https://www.vultr.com/docs/how-to-install-jenkins-on-centos-7

sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo
sudo rpm --import http://pkg.jenkins-ci.org/redhat-stable/jenkins-ci.org.key
sudo yum install -y jenkins

sudo systemctl start jenkins
sudo systemctl enable jenkins
sudo systemctl enable jenkins.service

sudo firewall-cmd --zone=public --add-port=8080/tcp --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --list-ports

netstat -na | grep LISTEN | grep tcp | grep 8080
netstat -tulp
sudo fuser 8080/tcp

#sudo usermod -aG docker

#sudo vi /etc/sysconfig/jenkins

# Port Jenkins is listening on.
# Set to -1 to disable
#
#JENKINS_PORT="8080"

exit 0
