1. Minute - Minutes after the hour (0-59).
2. Hour - 24-hour format (0-23).
3. Day - Day of the month (1-31).
4. Month - Month of the year (1-12).
5. Weekday - Day of the week. (0-6, where 0 indicates Sunday).


*     *   *   *    *  command to be executed
-     -    -    -    -
|     |     |     |     |
|     |     |     |     +----- day of week (0 - 6) (Sunday=0)
|     |     |     +------- month (1 - 12)
|     |     +--------- day of month (1 - 31)
|     +----------- hour (0 - 23)
+------------- min (0 - 59)

# edits
crontab -e

# lists
crontab -l

# list by user
crontab -u henninb -l

# 0 = Sunday
# Min   Hour    Day     Mon     Day
# Of    Of      Of      Of      Of
# Day   Day     Mon     Year    Week    Command
# ----  ----    ----    ----    ----    -----------------
# run every 15 minutes M-F
  */15  *       *       *       1-5     stock_command > /dev/null 2>&1

# run every 15 minutes every day
  */15  *       *       *       *       ntpdate us.pool.ntp.org > /dev/null 2>&1
  
  
# runs every hour 
  *    1       *       *       *       ntpdate us.pool.ntp.org > /dev/null 2>&1
  
0     0-23      *       *       *       /bin/sh /home/opr0558/lsof_deleted.sh > /dev/null 2>&1

https://crontab.guru/#0_1/1_*_*_*
