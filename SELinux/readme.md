Security-Enhanced Linux (SELinux) is a Linux kernel security module that provides a mechanism for supporting access control security policies, including United States Department of Defense–style mandatory access controls (MAC).

# disable temporarily (as root) run the following command
sudo setenforce 0

#check status
sestatus

[henninb@centos7 activemq_data]$ cat /etc/sysconfig/selinux

# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=enforcing
# SELINUXTYPE= can take one of three two values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected.
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted


docker
svirt_sandbox_file_t


ls -Z /usr/bin/ssh

curl -fsSL https://get.docker.com/ | sh


semodule -v -e docker
docker info | grep 'Security Options'


chcon -t docker_exec_t /usr/bin/docker*


chcon -Rt svirt_sandbox_file_t /path/to/volume

docker run -v /var/db:/var/db:z rhel7 /bin/sh
