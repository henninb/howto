#!/bin/sh

sudo yum install -y net-tools psmisc sqlite-devel wget

wget downloads.drone.io/master/drone.rpm -O drone.rpm

sudo yum localinstall -y drone.rpm

sudo systemctl start drone
sudo systemctl enable drone.service

sudo systemctl status drone
sudo chkconfig drone on

sudo firewall-cmd --zone=public --add-port=8082/tcp --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --list-ports

netstat -na | grep LISTEN | grep tcp | grep 8082
netstat -tulp
sudo fuser 8082/tcp

#port=":80"
#port=":8082"
#sudo vi /etc/drone/drone.toml

exit 0
