drone server
---

It uses SQlite3 database server for storing its data and information by default. It will automatically create a database file named drone.sqlite under /var/lib/drone/ which will handle database schema setup and migration. To setup SQlite3 drivers, we'll need to follow the below steps.


sudo yum install sqlite-devel

# wget downloads.drone.io/master/drone.rpm

# sudo yum localinstall drone.rpm

After the installation is completed, we'll gonna configure drone to make it workable. The configuration of drone is inside /etc/drone/drone.toml file. By default, drone web interface is exposed under port 80 which is the default port of http, if we wanna change it, we can change it by replacing the value under server block as shown below.

[server] port=":80"



drone client
---
http://docs.drone.io/cli-installation/


Add to path
c:\drone

curl -L https://github.com/drone/drone-cli/releases/download/v0.7.0/drone_linux_amd64.tar.gz | tar zx
sudo install -t /usr/local/bin drone


export DRONE_SERVER=https://drone.local
export DRONE_TOKEN=<token>
