#!/bin/sh

sudo pacman -S httpd mod_ssl openssl curl openssl-perl ca-certificates-utils net-tools psmisc wget

#cd $HOME
# Generate private key
openssl genrsa -out $HOME/ca.key.pem 4096

# Generate CSR 
openssl req -new -key $HOME/ca.key.pem -out $HOME/ca.csr -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=archlinux/CN=archlinux"
openssl req -new -key $HOME/ca.key.pem -out $HOME/archlinux_apache.csr.pem -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=archlinux/CN=archlinux"

# Generate Self Signed Key
openssl x509 -req -days 365 -in $HOME/ca.csr -signkey $HOME/ca.key.pem -out /$HOME/ca.crt.pem
openssl x509 -req -days 365 -in $HOME/archlinux_apache.csr.pem -signkey $HOME/ca.key.pem -out $HOME/archlinux_apache.crt.pem

sudo mkdir -p /etc/pki/tls/certs
sudo mkdir -p /etc/pki/tls/private

sudo cp -v $HOME/ca.crt.pem /etc/pki/tls/certs
sudo cp -v $HOME/archlinux_apache.crt.pem /etc/pki/tls/certs
sudo cp -v $HOME/ca.key.pem /etc/pki/tls/private

echo "not sure if this is required"
#sudo restorecon -RvF /etc/pki

cp -v $HOME/archlinux_apache.crt.pem /tmp/
chmod 777 /tmp/archlinux_apache.crt.pem

#one of these sed command is not accurate
echo one of these sed command is not accurate
sudo cp -v /etc/httpd/conf/extra/httpd-ssl.conf /etc/httpd/conf/ssl.conf
sudo sed -i "s/SSLCertificateFile \/etc\/pki\/tls\/certs\/localhost.crt/SSLCertificateFile \/etc\/pki\/tls\/certs\/archlinux_apache.crt.pem/g" /etc/httpd/conf/ssl.conf
sudo sed -i "s/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/localhost.key/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/ca.key.pem/g" /etc/httpd/conf/ssl.conf

sudo sed -i "s/SSLCertificateFile \/etc\/pki\/tls\/certs\/ca.crt/SSLCertificateFile \/etc\/pki\/tls\/certs\/archlinux_apache.crt.pem/g" /etc/httpd/conf/ssl.conf
sudo sed -i "s/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/ca.key/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/ca.key.pem/g" /etc/httpd/conf/ssl.conf

#not sure if this is required
#sudo update-ca-trust force-enable
#sudo cp -v $HOME/archlinux_apache.crt.pem /etc/pki/ca-trust/source/anchors/
#sudo update-ca-trust extract
#sudo cp -v $HOME/archlinux_apache.crt.pem /usr/local/share/ca-certificates/
#sudo chmod a+r /usr/local/share/ca-certificates/archlinux_apache.crt.pem
#sudo update-ca-certificates
trust anchor $HOME/archlinux_apache.crt.pem
ls -l /etc/ssl/certs/ca-certificates.crt

sudo systemctl start httpd
sudo systemctl restart httpd
sudo systemctl enable httpd

sudo cp -v index.html /usr/share/httpd/

curl https://archlinux
curl --cacert $HOME/archlinux_apache.crt.pem https://archlinux
openssl s_client -connect archlinux:443 -CAfile $HOME/archlinux_apache.crt.pem

netstat -na | grep LIST| grep tcp | grep 80
netstat -na | grep LIST| grep tcp | grep 443

sudo fuser 443/tcp
sudo fuser 80/tcp

#ls -l /etc/pki/tls/openssl.cnf

exit 0
