#!/bin/sh

sudo yum install -y httpd mod_ssl openssl curl openssl-perl

echo not sure which syntax for the firewall is best
sudo firewall-cmd --zone=public --add-port=80/tcp --permanent
sudo firewall-cmd --reload

sudo firewall-cmd --permanent --add-service=https
sudo firewall-cmd --permanent --add-port=443/tcp
sudo firewall-cmd --reload

cd $HOME
# Generate private key
openssl genrsa -out ca.key.pem 4096

# Generate CSR 
openssl req -new -key ca.key -out ca.csr -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=centos7/CN=centos7"

# Generate Self Signed Key
openssl x509 -req -days 365 -in ca.csr -signkey ca.key -out ca.crt

sudo mkdir -p /etc/pki/tls/certs
sudo mkdir -p /etc/pki/tls/private

cd $HOME
sudo cp $HOME/ca.crt /etc/pki/tls/certs
sudo cp $HOME/ca.key /etc/pki/tls/private
sudo cp $HOME/ca.csr /etc/pki/tls/private

echo "not sure if this is required"
sudo restorecon -RvF /etc/pki

cp $HOME/ca.crt /tmp/
chmod 777 /tmp/ca.crt

#sudo vi /etc/httpd/conf.d/ssl.conf
#<VirtualHost *:443>
#  SSLEngine on
#  SSLCertificateFile /etc/pki/tls/certs/ca.crt
#  SSLCertificateKeyFile /etc/pki/tls/private/ca.key
#  ServerName centos7
#  Documentroot /var/www/html
#</VirtualHost>

sudo sed -i "s/SSLCertificateFile \/etc\/pki\/tls\/certs\/localhost.crt/SSLCertificateFile \/etc\/pki\/tls\/certs\/ca.crt/g" /etc/httpd/conf.d/ssl.conf
sudo sed -i "s/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/localhost.key/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/ca.key/g" /etc/httpd/conf.d/ssl.conf

sudo update-ca-trust force-enable
sudo cp $HOME/ca.crt /etc/pki/ca-trust/source/anchors/
sudo update-ca-trust extract

sudo systemctl start httpd
sudo systemctl restart httpd
sudo systemctl enable httpd

echo this file is the CA trust /etc/pki/ca-trust/extracted/openssl/ca-bundle.trust.crt 
echo this is the path to the system certs /etc/ssl/certs/

sudo cp index.html /var/www/html

curl https://centos7
curl --cacert $HOME/ca.crt https://centos7
openssl s_client -connect centos7:443 -CAfile $HOME/ca.crt

netstat -na | grep LIST| grep tcp | grep 80
netstat -na | grep LIST| grep tcp | grep 443

ls -l /etc/pki/tls/openssl.cnf

exit 0
