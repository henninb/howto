#!/bin/sh

sudo yum install -y httpd mod_ssl openssl curl openssl-perl
sudo yum install -y net-tools psmisc wget

sudo firewall-cmd --zone=public --add-port=80/tcp --permanent
#sudo firewall-cmd --permanent --add-service=https
sudo firewall-cmd --permanent --add-port=443/tcp --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --list-ports

# Generate private key
openssl genrsa -out $HOME/ca.key.pem 4096

# Generate CSR 
openssl req -new -key $HOME/ca.key.pem -out $HOME/ca.csr -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=centos7/CN=centos7"
openssl req -new -key $HOME/ca.key.pem -out $HOME/centos_apache.csr.pem -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=centos7/CN=centos7"

# Generate Self Signed Key
openssl x509 -req -days 365 -in $HOME/ca.csr -signkey $HOME/ca.key.pem -out /$HOME/ca.crt.pem
openssl x509 -req -days 365 -in $HOME/centos_apache.csr.pem -signkey $HOME/ca.key.pem -out $HOME/centos_apache.crt.pem

sudo mkdir -p /etc/pki/tls/certs
sudo mkdir -p /etc/pki/tls/private

#cd $HOME
sudo cp -v $HOME/ca.crt.pem /etc/pki/tls/certs
sudo cp -v $HOME/centos_apache.crt.pem /etc/pki/tls/certs
sudo cp -v $HOME/ca.key.pem /etc/pki/tls/private

echo "not sure if this is required"
sudo restorecon -RvF /etc/pki

cp -v $HOME/centos_apache.crt.pem /tmp/
chmod 777 /tmp/centos_apache.crt.pem

#one of these sed command is not accurate
echo one of these sed command is not accurate

sudo sed -i "s/SSLCertificateFile \/etc\/pki\/tls\/certs\/localhost.crt/SSLCertificateFile \/etc\/pki\/tls\/certs\/centos_apache.crt.pem/g" /etc/httpd/conf.d/ssl.conf

sudo sed -i "s/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/localhost.key/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/ca.key.pem/g" /etc/httpd/conf.d/ssl.conf

sudo sed -i "s/SSLCertificateFile \/etc\/pki\/tls\/certs\/ca.crt/SSLCertificateFile \/etc\/pki\/tls\/certs\/centos_apache.crt.pem/g" /etc/httpd/conf.d/ssl.conf

sudo sed -i "s/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/ca.key/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/ca.key.pem/g" /etc/httpd/conf.d/ssl.conf

#not sure if this is required
sudo update-ca-trust force-enable
sudo cp $HOME/centos_apache.crt.pem /etc/pki/ca-trust/source/anchors/
sudo update-ca-trust extract

sudo systemctl start httpd
sudo systemctl restart httpd
sudo systemctl enable httpd

echo this file is the CA trust /etc/pki/ca-trust/extracted/openssl/ca-bundle.trust.crt 
echo this is the path to the system certs /etc/ssl/certs/

sudo cp -v index.html /var/www/html

curl https://centos7
curl --cacert $HOME/centos_apache.crt.pem https://centos7
openssl s_client -connect centos7:443 -CAfile $HOME/centos_apache.crt.pem

netstat -na | grep LIST| grep tcp | grep 80
netstat -na | grep LIST| grep tcp | grep 443

sudo fuser 443/tcp
sudo fuser 80/tcp

#ls -l /etc/pki/tls/openssl.cnf

exit 0
