@echo off

set PATH=C:\Program Files\Oracle\VirtualBox

VBoxManage controlvm  "centos7" savestate
VBoxManage modifyvm "centos7" --natpf1 "http,tcp,127.0.0.1,80,,80"
VBoxManage modifyvm "centos7" --natpf1 "https,tcp,127.0.0.1,443,,443"
VBoxManage startvm "centos7" --type headless

pause
