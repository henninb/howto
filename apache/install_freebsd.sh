#!/bin/sh

sudo pkg install -y apache24 curl openssl ca_root_nss

sudo sysrc apache24_enable="YES"

cd $HOME
# Generate private key
openssl genrsa -out ca.key.pem 4096

# Generate CSR 
#openssl req -new -key ca.key.pem -out ca.csr -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=freebsd11/CN=freebsd11"
openssl req -new -key ca.key.pem -out freebsd_apache.csr.pem -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=freebsd11/CN=freebsd11"

# Generate Self Signed Key
#openssl x509 -req -days 365 -in ca.csr -signkey ca.key.pem -out ca.crt
openssl x509 -req -days 365 -in freebsd_apache.csr.pem -signkey ca.key.pem -out freebsd_apache.crt.pem

sudo mkdir -p /etc/pki/tls/certs
sudo mkdir -p /etc/pki/tls/private

cd $HOME
sudo cp $HOME/freebsd_apache.crt.pem /etc/pki/tls/certs
sudo cp $HOME/ca.key.pem /etc/pki/tls/private

echo "not sure if this is required"
sudo restorecon -RvF /etc/pki

cp $HOME/ca.crt.pem /tmp/
chmod 777 /tmp/freebsd_apache.crt.pem

#sudo vi /etc/httpd/conf.d/ssl.conf
<VirtualHost *:443>
  SSLEngine on
  SSLCertificateFile /etc/pki/tls/certs/ca.crt
  SSLCertificateKeyFile /etc/pki/tls/private/ca.key
  ServerName freebsd11
  Documentroot /usr/local/www/apache24/data/
</VirtualHost>

sudo vi /usr/local/etc/apache24/httpd.conf
LoadModule ssl_module libexec/apache24/mod_ssl.so
Include etc/apache24/ssl.conf

sudo sed -i "s/SSLCertificateFile \/etc\/pki\/tls\/certs\/localhost.crt/SSLCertificateFile \/etc\/pki\/tls\/certs\/freebsd_apache.crt.pem/g" /usr/local/etc/apache24/ssl.conf
sudo sed -i "s/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/localhost.key/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/ca.key.pem/g" /usr/local/etc/apache24/ssl.conf

sudo sed -i "s/SSLCertificateFile \/etc\/pki\/tls\/certs\/ca.crt/SSLCertificateFile \/etc\/pki\/tls\/certs\/freebsd_apache.crt.pem/g" /usr/local/etc/apache24/ssl.conf
sudo sed -i "s/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/ca.key/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/ca.key.pem/g" /usr/local/etc/apache24/ssl.conf

#not sure if this is required
sudo update-ca-trust force-enable
sudo cp $HOME/freebsd_apache.crt.pem /etc/pki/ca-trust/source/anchors/
sudo update-ca-trust extract

sudo systemctl start httpd
sudo systemctl restart httpd
sudo systemctl enable httpd

echo this file is the CA trust /etc/pki/ca-trust/extracted/openssl/ca-bundle.trust.crt 
echo this is the path to the system certs /etc/ssl/certs/

sudo cp index.html /var/www/html

curl https://freebsd11
curl --cacert $HOME/freebsd_apache.crt.pem https://freebsd11
openssl s_client -connect freebsd11:443 -CAfile $HOME/freebsd_apache.crt.pem

#extract the cert text add install it in the store
#sudo openssl x509 -in $HOME/freebsd_apache.crt.pem -text >> /etc/ssl/cert.pem
sudo openssl x509 -in $HOME/freebsd_apache.crt.pem -text >> /usr/local/share/certs/ca-root-nss.crt

netstat -na | grep LIST| grep tcp | grep 80
netstat -na | grep LIST| grep tcp | grep 443

#ls -l /etc/ssl/openssl.cnf
ls -l /usr/src/crypto/openssl/certs

curl-config --ca
#/usr/local/share/certs/ca-root-nss.crt

exit 0
