backup
------
ssh froto "tar czvfC - /usr/local/www data-dist" | dd of=httproot.tar.gz
ssh froto "tar czvfC - /usr/local/www data-dist" | dd of=httproot_20060315.tar.gz

install
pkg install apache24

sudo sysrc apache24_enable="YES"

/usr/sbin/apachectl start

/usr/local/etc/apache/httpd.conf
/var/www/conf/httpd.conf
apachectl restart
apachectl status


/usr/local/sbin/apachectl start
/usr/local/sbin/apachectl stop
/usr/local/sbin/apachectl configtest

password
--------
htpasswd -c /usr/local/etc/apache/htpasswd.users henninb
htpasswd -c /usr/local/etc/apache/htpasswd.henning henning

<Directory />
  Options FollowSymLinks
  AllowOverride None
  AuthName "Main Page"
  AuthType Basic
  AuthUserFile /usr/local/etc/apache/htpasswd.users
  require valid-user
</Directory>

mod_php4
--------
cd /usr/ports/www/mod_php4 && make install && make clean

#LoadModule php4_module libexec/apache2/libphp4.so

Make sure index.php is part of your DirectoryIndex.

You should add the following to your Apache configuration file:

AddType application/x-httpd-php .php
AddType application/x-httpd-php-source .phps



The following line has been added to your /usr/local/etc/php/extensions.ini
configuration file to automatically load the installed extension:

extension=mysql.so


cd /usr/ports/databases/php4-mysql && make && make install
cd /usr/ports/databases/php4-mssql && make && make install

cd /usr/local/etc && cp php.ini-recommended php.ini

mod_mp3
-------
apachectl configtest

# only works in 13
Edit your apache.conf or httpd.conf to enable and setup this
module. Have a look at the files in
${PREFIX}/share/doc/mod_mp3 for information on how to
configure it etc.

Then do this to make it work effective:

# apachectl configtest (see if there are any config errors)
# apachectl restart

<Directory "/home/henninb/mp3">
    Options Indexes FollowSymLinks MultiViews
    AllowOverride None
    Order allow,deny
    Allow from all
</Directory>

# mp3's are located at /home/henninb/mp3
<IfModule mod_mp3.c>
   Listen 80
   Listen 8001
  <VirtualHost 192.168.0.23:8001>
    DocumentRoot /home/henninb/mp3
    MP3Engine On
    MP3CastName "mod_mp3"
#    MP3Genre "Open Source"
    MP3 /home/henninb/mp3
    MP3Random On
    Timeout 5000
    ErrorLog /home/henninb/mod_mp3_err.txt
  </VirtualHost>
</IfModule>




ln -s /d/duplicate_from_windows/g_backup/mp3 mp3

"You don't have permission to access"


It's probably not actually a permissions problem, at a guess. Apache
doesn't follow symlinks by default. If you want it to, you need to have
"FollowSymLinks" in an appropriate Options line in access.conf.



# not needed for mp3
find /d/duplicate_from_windows/g_backup/mp3 | grep -i "\.mp3" > /usr/local/www/data/playlist

winamp
add url: 192.168.0.23

# config
--------
/usr/local/etc/apache
httpd.conf

http root
---------
/var/www/htdocs

php support
-----------
pkg_add -v http://www.tux.org/pub/bsd/openbsd/3.7/packages/i386/php4-core-4.4.0.tgz
To finish the install, enable the php4 module with:
    /usr/local/sbin/phpxs -s

To enable parsing of PHP scripts, add the following to
/var/www/conf/httpd.conf

    AddType application/x-httpd-php .php
apachectl restart

Copy the config file below into /var/www/conf/php.ini
/usr/local/share/examples/php4/php.ini-recommended

Don't forget that the default OpenBSD httpd is chrooted
into /var/www by default, so you may need to create support
directories such as /var/www/tmp for PHP to work correctly.
