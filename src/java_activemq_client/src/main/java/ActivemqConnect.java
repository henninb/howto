import java.lang.*;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

public class ActivemqConnect {
  private static final String url = "jdbc:postgresql://localhost/finance_db";
  private static final String username = "admin";
  private static final String password = "monday1";
  
  //private Connection connection;
  //private Session session;
  //private MessageProducer messageProducer;

  public ActivemqConnect() {
  }

  public static void connect() {

    try {
      // create a Connection Factory
      ConnectionFactory connectionFactory =  new ActiveMQConnectionFactory("tcp://localhost:61616");
      
      // create a Connection
      Connection connection = connectionFactory.createConnection();
      
      // create a Session
      Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
      
      // create the Destination to which messages will be sent
      Destination destination = session.createQueue("queueName");
      
      // create a Message Producer for sending messages
      MessageProducer messageProducer = session.createProducer(destination);

      // create a JMS TextMessage
      TextMessage textMessage = session.createTextMessage("text");
      
      // send the message to the queue destination
      messageProducer.send(textMessage);

      connection.close();

    } catch (Exception e) {
      System.out.println(e.getMessage());
    }

  }

  public static void main( String[] args ) {
    if( args.length != 0 ) {
      System.err.println( "Usage: java -jar PostgresqlConnect.jar <noargs>");
      System.exit(1);
    }

    connect();
  }
}
