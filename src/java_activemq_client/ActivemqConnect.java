import java.lang.*;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQSslConnectionFactory;

public class ActivemqConnect {
  public ActivemqConnect() {
  }

  public static void connect() {

    try {
      ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://hornsup:61616");
      Connection connection = connectionFactory.createConnection();
      System.out.println("connected");
      Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
      Destination destination = session.createQueue("queueName");
      MessageProducer messageProducer = session.createProducer(destination);
      TextMessage textMessage = session.createTextMessage("text");
      messageProducer.send(textMessage);
      connection.close();
      System.out.println("plain text connection is closed.");

      ActiveMQSslConnectionFactory connectionFactory1 = new ActiveMQSslConnectionFactory("ssl://hornsup:61617");
      connectionFactory1.setKeyStore("amq-client_windows.ks");
      connectionFactory1.setKeyStorePassword("monday1");
      connectionFactory1.setTrustStore("amq-client_windows.ts");
      connectionFactory1.setTrustStorePassword("monday1");
      //connectionFactory.setBrokerURL(amqBrokerUrl);
      //connectionFactory1.setUserName("admin");
      //connectionFactory1.setPassword("admin");
      connection = connectionFactory1.createConnection();
      System.out.println("connected");
      session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
      destination = session.createQueue("queueNameSsl");
      messageProducer = session.createProducer(destination);
      textMessage = session.createTextMessage("text");
      messageProducer.send(textMessage);

      connection.close();
      System.out.println("ssl connection is closed.");
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  public static void main( String[] args ) {
    if( args.length != 0 ) {
      System.err.println( "Usage: java -jar ActivemqConnect.jar <noargs>");
      System.exit(1);
    }

    connect();
  }
}
