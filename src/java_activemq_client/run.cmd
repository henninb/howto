@echo off

set PATH=C:\Java\jre\bin
set OS=Windows_NT

call build N
if exist ActivemqConnectManifest.java (
    java -jar ActivemqConnectManifest.jar
) else (
    if NOT exist ActivemqConnect.java (
        echo copy Activemq_connect.
        copy "..\..\Activemq_connect\java_Activemq_connect\ActivemqConnect.java" "ActivemqConnect.java"
    )
)

java -cp .;activemq-all-5.15.0.jar ActivemqConnect

pause
