using System;
using System.IO;
using System.Text.RegularExpressions;
//using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

public class Example {
  [STAThread]
  public static void Main( string[] args ) {
    if( args.Length != 0 ) {
      Console.Error.WriteLine( "Usage: " + Environment.GetCommandLineArgs()[0] + " <noargs>" );
      Environment.Exit(1);
    }


    print_clipboard();
  }
  
  public static void fileWrite( string fname, string data ) {
    StreamWriter streamWriter = null;
    FileStream fileStream = null;

    try {
      //fileStream = new FileStream(fname, FileMode.Create, FileAccess.Write);
      fileStream = new FileStream(fname, FileMode.Append, FileAccess.Write);
      streamWriter = new StreamWriter(fileStream);
      streamWriter.Write(data);
      streamWriter.Close();
      fileStream.Close();
    }

    catch( Exception e ) {
      Console.WriteLine( e.Message );
    }
  }

  public static void print_clipboard() {
    string clipboard_text = string.Empty;
    string fileText = string.Empty;
    for( int idx_i = 0; idx_i < 10; idx_i++ ) {
      clipboard_text = Clipboard.GetText(TextDataFormat.Text);
      clipboard_text = Regex.Replace(clipboard_text, "\r", string.Empty);
      clipboard_text = clipboard_text.Replace(" \n#...3762\t", "\t");
      clipboard_text = Regex.Replace(clipboard_text, "\n#[0-9]+[^\t]+\t", "\t");
      //bcu
      clipboard_text = clipboard_text.Replace("Change payment to Chase Credit Cards ", string.Empty);
      clipboard_text = Regex.Replace(clipboard_text, "Cancel payment to Chase Credit Cards .*$", "||");
      clipboard_text = clipboard_text.Replace("View payment details to ", string.Empty);
      clipboard_text = clipboard_text.Replace("Cancel payment to ", string.Empty);
      clipboard_text = clipboard_text.Replace("Change payment to ", string.Empty);
      clipboard_text = clipboard_text.Replace(" in the amount of", "\t");
      clipboard_text = clipboard_text.Replace(" on ", "\t");

      String[] lineItems = Regex.Split(clipboard_text, "\n");
      if( lineItems != null ) {
        foreach (var lineItem in lineItems) {
          String[] elements = Regex.Split(lineItem, "\t");
          if( elements != null ) {
            if( elements.Length == 3 ) {
              fileText = fileText + elements[0] + "\t" + elements[1] + "\t\t" + elements[2] + "\t0\n";
            } else if( elements.Length == 5 ) {
              fileText = fileText + elements[0] + "\t" + elements[2] + "\t\t" + elements[3] + "\t0\n";
            } else {
            }
          }
        }
      }
      Console.WriteLine(fileText);
      fileWrite("output", fileText);
      Clipboard.SetText(clipboard_text, TextDataFormat.Text);
      Thread.Sleep(3000);
    }
  }
}
