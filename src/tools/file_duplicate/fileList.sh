#!/bin/sh

if [ $# -ne 0 ]; then
    echo "Usage: $0 <noargs>"
    exit 1
fi

PROGRAM=$(basename $(pwd))
WD=$(pwd)

#find . -name "*.sh" | xargs chmod 744

echo > updates.sql

for ELEMENT in $(find . -mindepth 1 -maxdepth 1 -type f -print | sort); do
  MD5_SUM=$(md5sum ${ELEMENT} | awk '{print $1;}') 
  echo "$MD5_SUM $(basename ${ELEMENT})"
  echo "INSERT INTO t_media(hash, fname) VALUES('$MD5_SUM', '$(basename ${ELEMENT})');" >> updates.sql
done

exit 0
