--set client_min_messages = warning;

--DROP DATABASE finance_db;
--CREATE DATABASE finance_db;

DROP TABLE t_media;

DROP SEQUENCE t_media_media_id_seq CASCADE;
CREATE SEQUENCE t_media_media_id_seq START WITH 1001; 

CREATE TABLE t_media(
  media_id INTEGER DEFAULT nextval('t_media_media_id_seq') NOT NULL,
  hash CHAR(60) NOT NULL,
  fname CHAR(200) NOT NULL,
  fname_master CHAR(200),
  exclude CHAR(10),
  date_updated TIMESTAMP,
  date_added TIMESTAMP
);

CREATE OR REPLACE FUNCTION fn_upd_ts_media() RETURNS TRIGGER AS 
$$
BEGIN
  NEW.date_updated := CURRENT_TIMESTAMP;
   --OLD.date_updated := CURRENT_TIMESTAMP;
  RETURN NEW;
END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER tr_upd_ts_media
BEFORE UPDATE ON t_media FOR EACH ROW EXECUTE PROCEDURE fn_upd_ts_media();
