@echo off

rem need to escape space to work with mingw32-make
rem set PATH=C:\Program^ Files\Java\jdk1.8.0_51\bin
rem set PATH=C:\Program^ Files\Java\jdk1.8.0_144\bin;%PATH%
set PATH=C:\Java\jdk\bin
set PATH=C:\TDM-GCC-64\bin;%PATH%
set OS=win32

mingw32-make win32

pause
