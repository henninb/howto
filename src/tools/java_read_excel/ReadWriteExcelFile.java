import java.io.*;
import java.lang.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import org.apache.poi.*;
import org.apache.poi.hssf.*;
import org.apache.poi.xssf.*;

import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

//import org.apache.poi.hssf.usermodel.HSSFCell;
//import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

//import org.apache.poi.xssf.usermodel.XSSFCell;
//import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.apache.poi.poifs.crypt.Decryptor;
import org.apache.poi.poifs.crypt.EncryptionInfo;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.hssf.record.crypto.*;

public class ReadWriteExcelFile {
  public static void readXLSFile() throws IOException {
    InputStream ExcelFileToRead = new FileInputStream("Test.xls");
    HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);

    HSSFSheet sheet=wb.getSheetAt(0);
    Row row;
    Cell cell;

    Iterator<Row> rows = sheet.rowIterator();

    while( rows.hasNext() ) {
        row= rows.next();
        Iterator<Cell> cells = row.cellIterator();

        while (cells.hasNext()) {
            cell= cells.next();

            if (cell.getCellTypeEnum() == CellType.STRING) {
                System.out.print(cell.getStringCellValue()+" ");
            } else if(cell.getCellTypeEnum() == CellType.NUMERIC) {
                System.out.print(cell.getNumericCellValue()+" ");
            } else {
                //U Can Handel Boolean, Formula, Errors
            }
        }
        System.out.println();
    }
  }

    public static void writeXLSFile() throws IOException {
        String excelFileName = "Test.xls";//name of excel file
        String sheetName = "Sheet1";//name of sheet

        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet(sheetName) ;

        //iterating r number of rows
        for (int r=0;r < 5; r++ ) {
            Row row = sheet.createRow(r);
            //iterating c number of columns
            for (int c=0;c < 5; c++ ) {
                Cell cell = row.createCell(c);
                cell.setCellValue("Cell "+r+" "+c);
            }
        }

        FileOutputStream fileOut = new FileOutputStream(excelFileName);

        //write this workbook to an Outputstream.
        wb.write(fileOut);
        fileOut.flush();
        fileOut.close();
    }

    public static void readXLSXFile() throws IOException {
        InputStream ExcelFileToRead = new FileInputStream("Test.xlsx");
        XSSFWorkbook  wb = new XSSFWorkbook(ExcelFileToRead);
        XSSFWorkbook test = new XSSFWorkbook();

        XSSFSheet sheet = wb.getSheetAt(0);
        Row row;
        Cell cell;

        Iterator<Row> rows = sheet.rowIterator();

        while (rows.hasNext()) {
            row = rows.next();
            Iterator<Cell> cells = row.cellIterator();
            while (cells.hasNext()) {
                cell = cells.next();
                if (cell.getCellTypeEnum() == CellType.STRING) {
                    System.out.print(cell.getStringCellValue()+" ");
                } else if(cell.getCellTypeEnum() == CellType.NUMERIC) {
                    System.out.print(cell.getNumericCellValue()+" ");
                } else {
                    //U Can Handel Boolean, Formula, Errors
                }
            }
            System.out.println();
        }
    }

    public static void writeXLSXFile() throws IOException {
        String excelFileName = "Test.xlsx";//name of excel file

        String sheetName = "Sheet1";//name of sheet

        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet sheet = wb.createSheet(sheetName);

        //iterating r number of rows
        for (int r=0;r < 5; r++ ) {
            Row row = sheet.createRow(r);

            //iterating c number of columns
            for (int c=0;c < 5; c++ ) {
                Cell cell = row.createCell(c);
                cell.setCellValue("Cell "+r+" "+c);
            }
        }

        FileOutputStream fileOut = new FileOutputStream(excelFileName);

        //write this workbook to an Outputstream.
        wb.write(fileOut);
        fileOut.flush();
        fileOut.close();
    }

    public static void readProtectedBinFile() {
      try {
      //org.apache.poi.hssf.record.crypto.Biff8EncryptionKey.setCurrentUserPassword("Monday1");
        //Biff8EncryptionKey.setCurrentUserPassword("Monday1");
        InputStream inp = new FileInputStream("file.xlsx");
        //InputStream inp = new FileInputStream("C:\\usr\\finance_db_master.xlsm");

        Workbook wb;
        wb = WorkbookFactory.create(inp, "Monday1");
        //wb = WorkbookFactory.create(inp, "");

        // Write the output to a file
        FileOutputStream fileOut;
        fileOut = new FileOutputStream("unprotectedworkbook.xlsx");
        wb.write(fileOut);
        fileOut.close();
      } catch (EncryptedDocumentException e) {
          e.printStackTrace();
      } catch (InvalidFormatException e) {
          e.printStackTrace();
      } catch (FileNotFoundException e) {
          e.printStackTrace();
      } catch (IOException e) {
          e.printStackTrace();
      }
    }

    public static void main(String[] args) throws IOException {
      System.out.println(System.getProperty("java.class.path"));
        writeXLSFile();
        readXLSFile();

        writeXLSXFile();
        readXLSXFile();
        readProtectedBinFile();

//File input = new File("password-protected.xlsx");
//String password = "nice and secure";
//Workbook wb = WorkbookFactory.create(input, password);

        // InputStream excelFileToRead = new FileInputStream("file.xlsx");
        // POIFSFileSystem fs = new POIFSFileSystem(excelFileToRead);
        // EncryptionInfo encryptionInfo = new EncryptionInfo(fs);
        // Decryptor decryptor = Decryptor.getInstance(encryptionInfo);
    }
}
