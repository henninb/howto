XLConnect

See http://poi.apache.org/encryption.html - if you're using a recent enough copy of Apache POI (eg 3.8) then encrypted .xls files (HSSF) and .xlsx files (XSSF) can be decrypted (proving you have the password!)

At the moment you can't write out encrypted excel files though, only un-encrypted ones



     compile 'org.apache.poi:poi:'+poiVersion
     compile 'org.apache.poi:poi-ooxml:'+poiVersion
     compile 'org.apache.poi:poi-ooxml-schemas:'+poiVersion