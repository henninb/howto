#!/usr/bin/env python3
"""example python3
"""
import sys
import math
import serial
from serial import SerialException
import socket
from flask import Flask
from flask_restful import Resource, Api

#comment
def example_function(value_n):
    """
    Return the square of value_n.
    """
    local_x = 0

    local_x = (value_n + value_n - value_n) * (value_n / 1)
    local_x = local_x * math.floor(1.25)

    if local_x < 0:
        local_x = abs(local_x)
    return local_x

def serial_write(serial_port, message):
    try:
        ser = serial.Serial(serial_port)  # open first serial port
        print(ser.portstr)       # check which port was really used
        ser.write(message)      # write a string
        ser.close()             # close port
    except serial.SerialException:
        print("Serial Exception.");
    except:
        print("The serial logic failed.")

def serial_read(serial_port):
    try:
        ser = serial.Serial(serial_port)  # open first serial port
        print(ser.portstr)       # check which port was really used
        ser.write(message)      # write a string
        ser.close()             # close port
    except serial.SerialException:
        print("Serial Exception.");
    except:
        print("The serial logic failed.")

def client_communication():
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(("127.0.0.1", 5003))
        sock.send('GET / HTTP/1.1'.encode())
        print("bruh")
        data = sock.recv(16)
        sock.close()
        print("received data:", data)
    except ConnectionRefusedError:
        print("ConnectionRefusedError")
    except:
        print("error")
   

def new_client():
    try:
        message = 'This is the message.  It will be repeated.'
        print (sys.stderr, 'sending "%s"' % message)
        sock.sendall(message)
    
        # Look for the response
        amount_received = 0
        amount_expected = len(message)
        
        while amount_received < amount_expected:
            data = sock.recv(16)
            amount_received += len(data)
            print >>sys.stderr, 'received "%s"' % data
    
    finally:
        print (sys.stderr, 'closing socket')
        sock.close()

def main():
    """
    Entry Point.
    """
    if len(sys.argv) != 1:
        print("Usage: %s <noargs>" % sys.argv[0])
        sys.exit(1)
    for idx_i in range(1, 10):
        print("example_function(%d)=<%d>" % (idx_i, example_function(idx_i)))
    serial_write("pps_t01", "message")
    client_communication()
    new_client()
    sys.exit(0)

main()
