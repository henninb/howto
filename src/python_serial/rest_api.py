#!/usr/bin/env python3
"""example python3
"""
from flask import Flask
from flask_restful import Resource, Api
import jsonify
import json

app = Flask(__name__)
api = Api(app)

class HelloWorld(Resource):
    def get(self):
        result = {
    "president": {
        "name": "unknown",
        "year": "2018"
    }
}
        return json.dumps(result)
#        return {'hello': 'world'}

api.add_resource(HelloWorld, '/')

if __name__ == '__main__':
#    app.run(debug=True)
    app.run(port='5003')

#https://github.com/sagaragarwal94/python_rest_flask
