#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>


#include "json.h"
#include "json_tokener.h"
#include "json_visit.h"

static json_c_visit_userfunc emit_object;
static double total = 0.0;


static void remove_char(char *str, char garbage) {

    char *src, *dst;
    for (src = dst = str; *src != '\0'; src++) {
        *dst = *src;
        if (*dst != garbage) dst++;
    }
    *dst = '\0';
}

static void test_read_valid_with_fd(const char *filename) {
    //const char *filename = "./valid.json";

    int d = open(filename, O_RDONLY, 0);
    if (d < 0) {
        fprintf(stderr, "FAIL: unable to open %s\n", filename);
        exit(EXIT_FAILURE);
    }
    json_object *jso = json_object_from_fd(d);
    if (jso != NULL) {
       //printf("OK: json_object_from_fd(%s)=%s\n", filename, json_object_to_json_string(jso));

            int rc;
            rc = json_c_visit(jso, 0, emit_object, NULL);
            //printf("json_c_visit(emit_object)=<%d>\n", rc);
            //printf("================================\n\n");
                //json_object_put(jso);
        } else {
            fprintf(stderr, "FAIL: unable to parse contents of %s: %s\n", filename, json_util_get_last_err());
        }
        close(d);
}


int main( int argc, char *argv[] ) {
  if( argc != 2 ) {
    fprintf(stderr, "Usage: %s <json_fname>\n", argv[0]);
    exit(1);
  }
  test_read_valid_with_fd(argv[1]);
  printf("total=<%.2f>\n", total);
}


static int emit_object(json_object *jso, int flags,
                     json_object *parent_jso,
                     const char *jso_key,
                     size_t *jso_index, void *userarg)
{
        char *jso_value = NULL;
        if( jso_key != NULL ) { 
            if( strncmp(jso_key, "amount", 6) == 0 ) {
                jso_value = json_object_to_json_string(jso);
                remove_char(jso_value, '"');
                //printf("amount=<%s>\n", jso_value);
                total = total + atof(jso_value);
            }
	    //printf("key: %s, index: %ld, value: %s\n", (jso_key ? jso_key : "(null)"), (jso_index ? (long)*jso_index : -1L), json_object_to_json_string(jso));
        }
	return JSON_C_VISIT_RETURN_CONTINUE;
}
