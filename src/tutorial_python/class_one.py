#comment
import sys
#import getopt
import os

def main():
  names = []

  if len(sys.argv) != 1:
    print("Usage: %s <noargs>" % sys.argv[0])
    sys.exit()

  main_menu(names)
  sys.exit(), end

def main_menu(names):
  cmd = ""
  
  while cmd.lower() != "q" and cmd.lower() != "s" and cmd.lower() != "n":
    os.system('cls')
    print("##################################")
    print("")
    print("## n: add to name list          ##")
    print("## s: show name list            ##")
    print("## q: quit program              ##")
    print("")
    print("##################################")
    cmd = input(">>>")
  print(cmd)
  if cmd == "n":
    print("add to name list")
    add_names(names)
  elif cmd == "s":
    print("show names")
    print_names(names)
  else:
    print("quit")
    sys.exit(), end

def add_names(names):
  # Start with an empty list. You can 'seed' the list with
  #  some predefined values if you like.
  #names = []
  
  # Set new_name to something other than 'quit'.
  new_name = ''
  
  # Start a loop that will run until the user enters 'quit'.
  while new_name.lower() != 'b':
      # Ask the user for a name.
      new_name = input("Please enter a name or press 'b' for main menu: ")
      # Add the new name to our list.
      if new_name.lower() != "b":
        names.append(new_name)
  
  # Show that the name has been added to the list.
  #print(names)
  main_menu(names)

def print_names(names):
  print(names);
  value = input("Press enter to continue.");
  main_menu(names)
  
main()
