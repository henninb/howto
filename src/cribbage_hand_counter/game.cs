using System;

public class Game {
public Game() {
  Player player1 = new Player( "Brian", false, false );
  Player player2 = new Player( "George", false, false );

  Deck deck = new Deck();
  Card player1_card = deck.getTopCard();
  Card player2_card = deck.getTopCard();

  int x = player1_card.getDeckValue();
  int y = player2_card.getDeckValue();

  if( x < y ) {
    player1.Dealer = true;
    player2.Dealer = false;
  } else {
    player2.Dealer = true;
    player1.Dealer = false;
  }

  while( playRound( player1, player2 )) {
  }

  if( player1.Score > 120 ) {
    Console.WriteLine( player1.Name + " " + player1.Score );
    Console.WriteLine( player2.Score );
  } else {
    Console.WriteLine( player2.Name + " " + player2.Score );
    Console.WriteLine( player1.Score );
  }
}

public bool playRound( Player player1, Player player2 ) {
  Deck deck = null;
  //Card card = null;

  player1.Hand = new Card[4];
  player2.Hand = new Card[4];
  Card[] cribCards = new Card[4];
  Card cutCard = null;

  deck = new Deck();
  for( int idx = 0; idx < 4; idx++ ) {
    player1.Hand[idx] = deck.getTopCard();
    player2.Hand[idx] = deck.getTopCard();
    cribCards[idx] = deck.getTopCard();
  }
  cutCard = deck.getTopCard();

  int p1_score = Hand.countHand( player1.Hand, cutCard, false );
  int p2_score = Hand.countHand( player2.Hand, cutCard, false );
  int crib_score = Hand.countHand( cribCards, cutCard, true );

  if( player1.Dealer == true ) {
    player2.Score += p2_score;
    if( player2.Score > 120 ) {
      return false;
    }
    player1.Score += p1_score;
    player1.Score += crib_score;
    if( player1.Score > 120 ) {
      return false;
    }
    player2.Dealer = true;
    player1.Dealer = false;
  } else {
    player1.Score += p1_score;
    if( player1.Score > 120 ) {
      return false;
    }
    player2.Score += p2_score;
    player2.Score += crib_score;
    if( player2.Score > 120 ) {
      return false;
    }
    player1.Dealer = true;
    player2.Dealer = false;
  }

  return true;

  //build crib
  //build hand1
  //build hand2
  //count pon hand check for winner
  //count hand check for winner
  //count crib check for winner
}

public static void Main() {
  new Game();
}
}
