using System;

public class Player {
private string name = string.Empty;
private int score = 0;
private bool dealer = false;
private bool isHuman = false;
private Card[] hand = null;

public Player( string name, bool dealer, bool isHuman ) {
  Name = name;
  Dealer = dealer;
  IsHuman = isHuman;
  Score = 0;
}

public string Name {
  get {
    return name;
  }

  set {
    this.name = value;
  }
}

public int Score {
  get {
    return score;
  }

  set {
    this.score = value;
  }
}

public bool Dealer {
  get {
    return dealer;
  }

  set {
    this.dealer = value;
  }
}

public bool IsHuman {
  get {
    return isHuman;
  }

  set {
    this.isHuman = value;
  }
}

public Card[] Hand {
  get {
    return hand;
  }

  set {
    this.hand = value;
  }
}
}
