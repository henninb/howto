using System;

public class Card {
public static readonly char[] rankChars = {
  'A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K'
};

public static readonly int[] cardValues = {
  1, 2, 3, 4, 5, 6, 7, 8, 8, 10, 10, 10, 10
};

public static readonly char[] suitChars = {
  'H', 'D', 'C', 'S'
};

public Card( int deckValue, bool isCutCard ) {
  this.isCutCard = isCutCard;
  this.deckValue = deckValue;
  this.rank = setRank( deckValue );
  this.suit = setSuit( deckValue );
  this.rankIndex = setRankIndex( deckValue );
  this.value = setValue( deckValue );
  this.resourceName = (suit.ToString() + rank.ToString()).ToLower() + "";
}

private char setSuit( int card ) {
  int suit = card % 4;

  if( suit == 0 ) {
    return 'H';
  } else if( suit == 1 ) {
    return 'D';
  } else if( suit == 2 ) {
    return 'C';
  } else if( suit == 3 ) {
    return 'S';
  } else {
    throw new Exception( "WARN: card is out of range (0-51)." );
  }
}

private char setRank( int card ) {
  int rank = card / 4 + 1;

  if((rank > 1) && (rank < 10)) {
    //return rank.ToString();
    return char.Parse( rank.ToString());
  }
  if((card > -1) && (card < 4)) {
    return 'A';
  } else if((card > 35) && (card < 40)) {
    return 'T';
  } else if((card > 39) && (card < 44)) {
    return 'J';
  } else if((card > 43) && (card < 48)) {
    return 'Q';
  } else if((card > 47) && (card < 52)) {
    return 'K';
  } else {
    throw new Exception( "WARN: card is out of range (0-51)." );
  }
}

  public int getValue() {
    return deckValue / 4;
  }

  private int setValue( int card ) {
    int val = card / 4 + 1;
  
    if( val > 10 ) {
      val = 10;
    }
  
    return val;
  }

private int setRankIndex( int card ) {
  int rank = card / 4;

  return rank;
}

public string getResourceName() {
  return resourceName;
}

public char getSuit() {
  return this.suit;
}

public char getRank() {
  return this.rank;
}

public int getRankIndex() {
  return rankIndex;
}

public int getDeckValue() {
  return this.deckValue;
}

public bool getIsCutCard() {
  return isCutCard;
}

private int deckValue = -1;
private string resourceName = string.Empty;
private char rank;
private char suit;

private int rankIndex = -1;

public bool isCutCard = false;
public string cardOwner = string.Empty;
public int value = -1;
}
