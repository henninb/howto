using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Resources;
using System.Reflection;

public class CribbageGame : Form {
  private const int FIRST_CARD = 0;
  private const int SECOND_CARD = 1;
  private const int THIRD_CARD = 2;
  private const int FORTH_CARD = 3;
  private const int FIFTH_CARD = 4;
  private const int SIXTH_CARD = 5;

  private const int DISTANCE = 80;

  private Label[] cardList = null;
  private Label cutCard = null;
  private Button btnDiscard = null;
  private Button btnQuit = null;
  private Button btnDeal = null;
  private Button btnCountHand = null;

  private Label lblKnobsCountValue = null;
  private Label lblPairsCountValue = null;
  private Label lblFlushesCountValue = null;
  private Label lblFifteensCountValue = null;

  private Container components = null;
  private ResourceManager resource = null;

  private ArrayList cribCards = null;

  int clientSizeWidth = 0;
  int clientSizeHeight = 0;
  //Console.WriteLine(this.Width + " " +  this.ClientSize.Width);
  //Console.WriteLine(this.Height + " " +  this.ClientSize.Height);
  
  public static void Main( string[] args ) {
    Application.Run(new CribbageGame());
  }

  public CribbageGame() {
    InitializeComponent();
  }
  
  protected override void Dispose( bool disposing ) {
    if( disposing ) {
      if( components != null ) {
        components.Dispose();
      }
    }
    base.Dispose( disposing );
  }

private void InitializeComponent() {
  this.cardList = null;
  this.cutCard = null;
  this.btnDiscard = null;
  this.btnQuit = null;
  this.btnDeal = null;
  this.btnCountHand = null;
  this.lblKnobsCountValue = null;
  this.lblPairsCountValue = null;
  this.lblFlushesCountValue = null;
  this.lblFifteensCountValue = null;
  this.components = null;
  this.resource = null;
  this.cribCards = null;

  this.resource = new ResourceManager("cribbage_main.Properties.Resources", Assembly.GetExecutingAssembly());
    //this.resource = new ResourceManager()
  //this.resource = new ResourceManager(typeof(CribbageGame));
  //this.resource.BaseName
  this.btnQuit = new Button();
  this.btnDeal = new Button();
  this.btnDiscard = new Button();
  this.btnCountHand = new Button();
  cardList = new Label[6];
  this.cardList[FIRST_CARD] = new Label();
  this.cardList[SECOND_CARD] = new Label();
  this.cardList[THIRD_CARD] = new Label();
  this.cardList[FORTH_CARD] = new Label();
  this.cardList[FIFTH_CARD] = new Label();
  this.cardList[SIXTH_CARD] = new Label();

  this.cutCard = new Label();

  this.lblKnobsCountValue = new Label();
  this.lblPairsCountValue = new Label();
  this.lblFlushesCountValue = new Label();
  this.lblFifteensCountValue = new Label();

  this.SuspendLayout();
  
  for( int idx_i = 0; idx_i < 6; idx_i++ ) {
    this.cardList[idx_i].BorderStyle = BorderStyle.None;
    this.cardList[idx_i].Image = (Bitmap)resource.GetObject("b");
    this.cardList[idx_i].Location = this.getCardPosition(idx_i);
    this.cardList[idx_i].Size = new Size(73, 97);
    this.cardList[idx_i].Tag = null;
    this.cardList[idx_i].Click += new EventHandler(clickedCardImage);
    this.cardList[idx_i].Show();
  }
  ////
  //// FIRST_CARD
  ////
  //this.cardList[FIRST_CARD].BorderStyle = BorderStyle.None;
  //this.cardList[FIRST_CARD].Image = (Bitmap)resource.GetObject("b");
  //this.cardList[FIRST_CARD].Location = this.getCardPosition( FIRST_CARD );
  //this.cardList[FIRST_CARD].Size = new Size(73, 97);
  //this.cardList[FIRST_CARD].Tag = null;
  //this.cardList[FIRST_CARD].Click += new EventHandler( clickedCardImage );
  ////
  //// SECOND_CARD
  ////
  //this.cardList[SECOND_CARD].BorderStyle = BorderStyle.None;
  //this.cardList[SECOND_CARD].Image = (Bitmap)resource.GetObject("b");
  //this.cardList[SECOND_CARD].Location = this.getCardPosition( SECOND_CARD );
  //this.cardList[SECOND_CARD].Size = new Size(73, 97);
  //this.cardList[SECOND_CARD].Tag = null;
  //this.cardList[SECOND_CARD].Click += new EventHandler( clickedCardImage );
  ////
  //// THIRD_CARD
  ////
  //this.cardList[THIRD_CARD].BorderStyle = BorderStyle.None;
  //this.cardList[THIRD_CARD].Image = (Bitmap)resource.GetObject("b");
  //this.cardList[THIRD_CARD].Location = this.getCardPosition( THIRD_CARD );
  //this.cardList[THIRD_CARD].Size = new Size(73, 97);
  //this.cardList[THIRD_CARD].Tag = null;
  //this.cardList[THIRD_CARD].Click += new EventHandler( clickedCardImage );
  ////
  //// FORTH_CARD
  ////
  //this.cardList[FORTH_CARD].BorderStyle = BorderStyle.None;
  //this.cardList[FORTH_CARD].Image = (Bitmap)resource.GetObject("b");
  //this.cardList[FORTH_CARD].Location = this.getCardPosition( FORTH_CARD );
  //this.cardList[FORTH_CARD].Size = new Size(73, 97);
  //this.cardList[FORTH_CARD].Tag = null;
  //this.cardList[FORTH_CARD].Click += new EventHandler( clickedCardImage );
  ////
  //// FIFTH_CARD
  ////
  //this.cardList[FIFTH_CARD].BorderStyle = BorderStyle.None;
  //this.cardList[FIFTH_CARD].Image = (Bitmap)resource.GetObject("b");
  //this.cardList[FIFTH_CARD].Location = this.getCardPosition( FIFTH_CARD );
  //this.cardList[FIFTH_CARD].Size = new Size(73, 97);
  //this.cardList[FIFTH_CARD].Tag = null;
  //this.cardList[FIFTH_CARD].Click += new EventHandler( clickedCardImage );
  ////
  //// SIXTH_CARD
  ////
  //this.cardList[SIXTH_CARD].BorderStyle = BorderStyle.None;
  //this.cardList[SIXTH_CARD].Image = (Bitmap)resource.GetObject("b");
  //this.cardList[SIXTH_CARD].Location = this.getCardPosition( SIXTH_CARD );
  //this.cardList[SIXTH_CARD].Size = new Size(73, 97);
  //this.cardList[SIXTH_CARD].Tag = null;
  //this.cardList[SIXTH_CARD].Click += new EventHandler( clickedCardImage );

  //
  // cutCard
  //
  //this.cutCard.BorderStyle = BorderStyle.None;
  this.cutCard.Image = (Bitmap)resource.GetObject("b");
  this.cutCard.Location = new Point(550, 125);
  this.cutCard.Size = new Size(73, 97);
  this.cutCard.Tag = null;
  this.cutCard.Show();
  //
  // btnCountHand
  //
  this.btnCountHand.Anchor = AnchorStyles.None;
  this.btnCountHand.Location = new Point(10, 350);
  this.btnCountHand.Size = new Size(80, 24);
  this.btnCountHand.Text = "Count Hand";
  this.btnCountHand.Hide();
  this.btnCountHand.Click += new EventHandler(clickedCountHandButton);
  //
  // btnDiscard
  //
  this.btnDiscard.Anchor = AnchorStyles.None;
  this.btnDiscard.Location = new Point( 10, 350 );
  this.btnDiscard.Size = new Size( 80, 24 );
  this.btnDiscard.Text = "Discard";
  this.btnDiscard.Hide();
  this.btnDiscard.Click += new EventHandler( clickedDiscardButton );
  //
  // btnDeal
  //
  this.btnDeal.Anchor = AnchorStyles.None;
  this.btnDeal.Location = new Point(550, 296);
  this.btnDeal.Size = new Size( 80, 24 );
  this.btnDeal.Text = "Deal";
  this.btnDeal.Click += new EventHandler(clickedDealButton);
  this.btnDeal.Show();
  //
  // btnQuit
  //
  this.btnQuit.Anchor = AnchorStyles.None;
  this.btnQuit.Location = new Point( 550, 328 );
  this.btnQuit.Size = new Size( 80, 24 );
  this.btnQuit.Text = "Quit";
  this.btnQuit.Click += new System.EventHandler(this.clickedQuitButton);
  this.btnQuit.Show();
  //
  // CribbageGame
  //
  this.Controls.Add(this.cardList[FIRST_CARD]);
  this.Controls.Add(this.cardList[SECOND_CARD]);
  this.Controls.Add(this.cardList[THIRD_CARD]);
  this.Controls.Add(this.cardList[FORTH_CARD]);
  this.Controls.Add(this.cardList[FIFTH_CARD]);
  this.Controls.Add(this.cardList[SIXTH_CARD]);
  this.Controls.Add(this.btnCountHand);
  this.Controls.Add(this.btnQuit);
  this.Controls.Add(this.btnDeal);
  this.Controls.Add(this.btnDiscard);
  this.Controls.Add(this.lblKnobsCountValue);
  this.Controls.Add(this.lblPairsCountValue);
  this.Controls.Add(this.lblFlushesCountValue);
  this.Controls.Add(this.lblFifteensCountValue);
  this.Controls.Add(this.cutCard);

  //this.AutoScaleBaseSize = new Size( 5, 13 );
  this.StartPosition = FormStartPosition.CenterScreen;
  this.FormBorderStyle = FormBorderStyle.FixedDialog;
  this.MaximizeBox = false;
  this.MinimizeBox = true;
  this.TopMost = false;
  
  //Console.WriteLine(this.Width + " " +  this.ClientSize.Width);
  //Console.WriteLine(this.Height + " " +  this.ClientSize.Height);
  int BorderWidth = (this.Width - this.ClientSize.Width) /2;
  int TitlebarHeight = this.Height - this.ClientSize.Height - 2 * BorderWidth;
  
  //this.clientSizeWidth = this.ClientSize.Width;
  //this.clientSizeHeight = this.ClientSize.Height;
  //this.ClientSize = new Size(BorderWidth, TitlebarHeight);
  this.ClientSize = new Size(850, 480);
  //Screen.FromControl(this).GetWorkingArea();
  this.Text = "Cribbage Game";
  this.ResumeLayout( false );
}

private void clickedQuitButton( object sender, EventArgs e ) {
  Application.Exit();
}

private void clickedCountHandButton( object sender, EventArgs e ) {
  //compute
  Card cutCard = (Card) this.cutCard.Tag;
  Card[] hand = new Card[4];
  for( int idx = 0; idx < 4; idx++ ) {
    hand[idx] = (Card) this.cardList[idx].Tag;
  }

  int count = Hand.countHand( hand, cutCard, false );
  MessageBox.Show("Hand count: " + count.ToString());

  this.btnDeal.Show();
  this.btnCountHand.Hide();

/*
  for( int idx_i = 0; idx_i < 6; idx_i++ ) {
    this.cardList[idx_i].BorderStyle = BorderStyle.None;
    this.cardList[idx_i].Image = (Bitmap)resource.GetObject("b");
    this.cardList[idx_i].Location = this.getCardPosition(idx_i);
    this.cardList[idx_i].Size = new Size(73, 97);
    this.cardList[idx_i].Tag = null;
    this.cardList[idx_i].Click += new EventHandler(clickedCardImage);
    this.cardList[idx_i].Show();
  }
  
  //this.cutCard.BorderStyle = BorderStyle.None;
  this.cutCard.Image = (Bitmap)resource.GetObject("b");
  this.cutCard.Location = new Point(550, 125);
  this.cutCard.Size = new Size(73, 97);
  this.cutCard.Tag = null;
  this.cutCard.Show();
  
  cribCards = null;
*/
  InitializeComponent();
}

private void clickedDiscardButton( object sender, EventArgs e ) {
  int Y_value = 0;
  Card card = null;

  Y_value = this.getCardPosition( 0 ).Y;
  for( int idx = 0; idx < 4; idx++ ) {
    if( this.cardList[idx].Location.Y != Y_value ) {
      if( this.cardList[FIFTH_CARD].Location.Y == Y_value ) {
        card = (Card) this.cardList[4].Tag;
        this.cardList[idx].Location = this.getCardPosition( idx );
        this.cardList[idx].Tag = card;
        this.cardList[idx].Image = (Bitmap)resource.GetObject(card.getResourceName());
        this.cardList[FIFTH_CARD].Location = new Point(10 + (FIFTH_CARD * DISTANCE), 265 - 10);
        this.cardList[FIFTH_CARD].Hide();
      } else if( this.cardList[SIXTH_CARD].Location.Y == Y_value ) {
        card = (Card) this.cardList[5].Tag;
        this.cardList[idx].Location = this.getCardPosition( idx );
        this.cardList[idx].Tag = card;
        this.cardList[idx].Image = (Bitmap)resource.GetObject(card.getResourceName());
        this.cardList[SIXTH_CARD].Location = new Point(10 + (SIXTH_CARD * DISTANCE), 265 - 10);
        this.cardList[SIXTH_CARD].Hide();
      } else {
        MessageBox.Show("WARN: Problem");
      }
    }
  }

  this.cardList[FIFTH_CARD].Hide();
  this.cardList[SIXTH_CARD].Hide();

  card = (Card)cutCard.Tag;
  cutCard.Image = (Bitmap)resource.GetObject(card.getResourceName());

  this.btnDiscard.Hide();
  this.btnCountHand.Show();

#if (TEST)
  MessageBox.Show(
    ((Card)cardList[FIRST_CARD].Tag).getResourceName() + " " +
    ((Card)cardList[SECOND_CARD].Tag).getResourceName() + " " +
    ((Card)cardList[THIRD_CARD].Tag).getResourceName() + " " +
    ((Card)cardList[FORTH_CARD].Tag).getResourceName() + " "
    //((Card) cardList[FIFTH_CARD].Tag).getResourceName() + " " +
    //((Card) cardList[SIXTH_CARD].Tag).getResourceName() + " " +
    );
#endif
}

private void clickedDealButton( object sender, EventArgs e ) {
  Deck deck = null;
  Card card = null;
  Card swapCard;

  //MessageBox.Show("this.clientSizeWidth: " + this.clientSizeWidth);
  //MessageBox.Show("this.clientSizeHeight: " + this.clientSizeHeight);

  this.cribCards = new ArrayList(2);
  deck = new Deck();
  for( int idx = 0; idx < 6; idx++ ) {
    card = deck.getTopCard();
    if( idx > 0 ) {
      swapCard = (Card)(this.cardList[idx - 1].Tag);
      if( card.getValue() < swapCard.getValue() ) {
        //MessageBox.Show("sort: should swap: " + card.getValue());
        this.cardList[idx-1].Location = this.getCardPosition(idx-1);
        this.cardList[idx-1].Image = (Bitmap)resource.GetObject(card.getResourceName());
        this.cardList[idx-1].Tag = card;
        this.cardList[idx-1].Show();
        this.cardList[idx].Location = this.getCardPosition(idx);
        this.cardList[idx].Image = (Bitmap)resource.GetObject(swapCard.getResourceName());
        this.cardList[idx].Tag = swapCard;
        this.cardList[idx].Show();
      } else  {
        //MessageBox.Show("no swap: " + card.getValue());
        this.cardList[idx].Location = this.getCardPosition(idx);
        this.cardList[idx].Image = (Bitmap)resource.GetObject(card.getResourceName());
        this.cardList[idx].Tag = card;
        this.cardList[idx].Show();
      } 
    } else {
      this.cardList[idx].Location = this.getCardPosition(idx);
      this.cardList[idx].Image = (Bitmap)resource.GetObject(card.getResourceName());
      this.cardList[idx].Tag = card;
      this.cardList[idx].Show();
    }
  }
  
  card = deck.getTopCard();
  card.isCutCard = true;
  this.cutCard.Tag = card;
  this.cutCard.Image = (Bitmap)resource.GetObject("b");
  this.btnDeal.Hide();

#if (TEST)
  MessageBox.Show(
    ((Card)cardList[FIRST_CARD].Tag).getResourceName() + " " +
    ((Card)cardList[SECOND_CARD].Tag).getResourceName() + " " +
    ((Card)cardList[THIRD_CARD].Tag).getResourceName() + " " +
    ((Card)cardList[FORTH_CARD].Tag).getResourceName() + " " +
    ((Card)cardList[FIFTH_CARD].Tag).getResourceName() + " " +
    ((Card)cardList[SIXTH_CARD].Tag).getResourceName() + " "
    );
#endif
}

private void clickedCardImage( object sender, EventArgs e ) {
  Point point;
  Label currCardLabel = null;
  Card currCard = null;
  bool discard_flag = false;

  currCardLabel = (Label)sender;
  currCard = (Card)currCardLabel.Tag;

  if( currCard != null && this.cribCards != null ) {
    foreach( object cribCard in this.cribCards ) {
      if( currCard.Equals(cribCard) == true ) {
        discard_flag = true;
      }
    }

    if( discard_flag == true ) {
      cribCards.Remove(currCard);
      point = currCardLabel.Location;
      point.Y += 10;
      currCardLabel.Location = point;
      this.btnDiscard.Hide();
    }
    if( discard_flag == false && this.cribCards.Count < 2 ) {
      cribCards.Add(currCard);
      point = currCardLabel.Location;
      point.Y -= 10;
      currCardLabel.Location = point;
    }
    if ( this.cribCards.Count == 2 ) {
      this.btnDiscard.Show();
    }
    //MessageBox.Show("clickedCardImage: " + this.cribCards.Count);
  }
}

  private Point getCardPosition( int cardIndex ) {
    return new Point( 10 + (cardIndex * DISTANCE), 265 );
  }
}
