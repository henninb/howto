using System;

public class Hand {
public static int countHand( Card[] hand, Card cutCard, bool isCrib ) {
  int[] rankList = new int[13];
  int knobsCount = 0;
  int flushesCount = 0;
  int pairsCount = 0;
  int runsCount = 0;
  int fifteensCount = 0;
  char suit;
  //char rank;

  // count his nobs (jack)
  for( int idx = 0; idx < hand.Length; idx++ ) {
    if( hand[idx].getRank() == 'J' ) {
      if( cutCard.getSuit() == hand[idx].getSuit()) {
        knobsCount++;
      }
    }
  }

  // count flushes
  // Four card flush are not counted in the crib.
  suit = hand[0].getSuit();
  if( suit == hand[1].getSuit() && suit == hand[2].getSuit() && suit == hand[3].getSuit()) {
    if( isCrib ) {
      if( cutCard.getSuit() == suit ) {
        flushesCount += 5;
      }
    } else {
      flushesCount += 4;
      if( cutCard.getSuit() == suit ) {
        flushesCount++;
      }
    }
  }

  // count pairs
  for( int idx_i = 0; idx_i < hand.Length; idx_i++ ) {
    rankList[hand[idx_i].getRankIndex()]++;
  }
  rankList[cutCard.getRankIndex()]++;

  for( int idx_i = 0; idx_i < rankList.Length; idx_i++ ) {
    // i(i-1)
    pairsCount += (rankList[idx_i] * (rankList[idx_i] - 1));
  }

  // count runs
  for( int idx_i = 0; idx_i < rankList.Length; idx_i++ ) {
    int next = idx_i;
    int runLength = 0;
    int multiple = 1;

    runsCount = 0;
    while( next < rankList.Length && rankList[next] > 0 ) {
      if( rankList[next] > 1 ) {
        multiple *= rankList[next];
      }
      next++;
    }

    runLength = next - idx_i;
    if( runLength > 2 ) {
      runsCount = runLength * multiple;
      break;
    }
  }

  // count fifteens
  for( int idx_i = 0; idx_i < (int)Math.Pow( 2, 5 ); idx_i++ ) {
    int sum = 0;

    for( int idx_j = 0; idx_j < 5; idx_j++ ) {
      int mask = (int)Math.Pow( 2, idx_j );
      int value = idx_i;
      int bitVal = value & mask;

      if( bitVal == mask ) {
        if( idx_j == 4 ) {
          sum += cutCard.value;
        } else {
          sum += hand[idx_j].value;
        }
      }
    }

    if( sum == 15 ) {
      fifteensCount += 2;
    }
  }

  return knobsCount + flushesCount + pairsCount + runsCount + fifteensCount;
}
}
