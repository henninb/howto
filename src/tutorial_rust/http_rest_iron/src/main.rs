extern crate iron;
extern crate rustc_serialize;

use iron::prelude::*;
use iron::status;
use rustc_serialize::json;

#[derive(RustcEncodable)]
struct Greeting {
    fname: String,
    lname: String
}

fn main() {
    fn hello_world(_: &mut Request) -> IronResult<Response> {
        let greeting = Greeting { fname: "first".to_string(), lname: "last".to_string() };
        let payload = json::encode(&greeting).unwrap();
        println!("request made");
        Ok(Response::with((status::Ok, payload)))
    }

    println!("Will listen on 0.0.0.0:8888");
    Iron::new(hello_world).http("0.0.0.0:8888").unwrap();
    println!("does not get here");
}
