#include <stdio.h>

extern void process();
extern void process_mq();

int main() {
  printf("hello world\n");
  process();
  process_mq();
  return 0;
}
