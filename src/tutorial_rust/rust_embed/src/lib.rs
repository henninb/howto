extern crate tokio;
extern crate tokio_stomp;
extern crate futures;

use std::time::Duration;

use tokio_stomp::*;
use tokio::executor::current_thread::{run, spawn};
use futures::future::ok;
use futures::prelude;
use futures::Stream;
use futures::Future;

use std::thread;

#[no_mangle]
pub extern fn process() {
    let handles: Vec<_> = (0..10).map(|_| {
        thread::spawn(|| {
            let mut x = 0;
            for _ in 0..5_000_000 {
                x += 1
            }
        x
        })
    }).collect();

    for h in handles {
        println!("Thread finished with count={}",
        h.join().map_err(|_| "Could not join a thread!").unwrap());
    }
    println!("done!");
}

#[no_mangle]
pub extern fn process_mq() {
//fn main() {
    let (fut, tx) = tokio_stomp::connect("127.0.0.1:61613".into(), None, None).unwrap();

    // Sender thread
    std::thread::spawn(move || {
        std::thread::sleep(Duration::from_secs(1));
        tx.unbounded_send(ClientMsg::Subscribe {
            destination: "rusty".into(),
            id: "myid".into(),
            ack: None,
        }).unwrap();
        println!("Subscribe sent");

        std::thread::sleep(Duration::from_secs(1));
        tx.unbounded_send(ClientMsg::Send {
            destination: "rusty".into(),
            transaction: None,
            body: Some(b"Hello there rustaceans!".to_vec()),
        }).unwrap();
        println!("Message sent");

        std::thread::sleep(Duration::from_secs(1));
        tx.unbounded_send(ClientMsg::Unsubscribe { id: "myid".into() })
            .unwrap();
        println!("Unsubscribe sent");

        std::thread::sleep(Duration::from_secs(1));
        tx.unbounded_send(ClientMsg::Disconnect { receipt: None })
            .unwrap();
        println!("Disconnect sent");

        std::thread::sleep(Duration::from_secs(1));
    });

    // Listen from the main thread. Once the Disconnect message is sent from
    // the sender thread, the server will disconnect the client and the future
    // will resolve, ending the program
    let fut = fut.for_each(|item| {
        if let ServerMsg::Message { body, .. } = item.content {
            println!(
                "Message received: {:?}",
                String::from_utf8_lossy(&body.unwrap())
            );
        } else {
            println!("{:?}", item);
        }
        ok(())
    }).map_err(|e| eprintln!("{}", e));

    run(|_| spawn(fut));
}
