#![feature(plugin)]
#![plugin(rocket_codegen)]

#[macro_use] extern crate serde_derive;
//extern crate serde_derive;
extern crate serde_xml_rs;
extern crate rocket;
extern crate serde;

use serde_xml_rs::deserialize;
//use serde_xml_rs::serialize;
use rocket::State;
use rocket::Request;

struct MyValue(usize);

#[derive(Debug, Deserialize)]
struct Item {
    pub name: String,
    pub source: String
}

#[derive(Debug, Deserialize)]
struct Project {
    pub name: String,

    #[serde(rename = "Item", default)]
    pub items: Vec<Item>
}

#[get("/")]
fn hello() -> &'static str {
    it_works();
    "Looks good"
    
}

#[get("/test")]
fn myfun() -> &'static str {
    "my fun"
}

#[get("/")]
fn index(state: State<MyValue>) -> String {
    format!("The stateful value is: {}", state.0)
}

#[error(500)]
fn internal_error() -> &'static str {
    "Whoops! Looks like we messed up."
}

#[error(404)]
fn not_found(req: &Request) -> String {
    format!("I couldn't find '{}'. Try something else?", req.uri())
}

fn it_works() {
    let s = r##"
        <Project name="my_project">
            <Item name="two" source="two.rs" />
            <Item name="one" source="one.rs" />
        </Project>
    "##;
    let project: Project = deserialize(s.as_bytes()).unwrap();
    println!("{:#?}", project);
}

fn main() {
        let _rocket = rocket::ignite().catch(errors![internal_error, not_found])
        .mount("/", routes![hello])
        .mount("/test", routes![hello]).launch();
/*
    rocket::ignite()
        .mount("/", routes![index])
        .manage(MyValue(10))
        .launch();
*/
}


/*
fn main() {
    rocket::ignite()
        .mount("/", routes![index])
        .manage(MyValue(10))
        .launch();
}
*/
