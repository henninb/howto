#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define         ROOT_DIRECTORY          "/"
#define PID_INVALID                                     -1
#define PIDFILE_GARBAGE                                 -2
#define FD_INVALID                                      -1
#define PIDFILE_ALREADY_RUNNING               10005
#ifndef TRUE
#  define TRUE 1
#endif
#ifndef FALSE
#  define FALSE 0
#endif

static int program_loc = 0;

void file_pointer_issue();
void processResults();
int daemon_init();

int main( int argc, char *argv[] ) {
  //file_pointer_issue();
  //processResults();
  daemon_init();
  return 0;
}

void file_pointer_issue() {
  FILE *ifp = NULL;
  fclose(ifp);
}

void processResults() {
  FILE *fpCommandResult = NULL;
  char scCommand[2000 + 1];
  char scInputMsg[2000 + 1];
  int iStatus = 0;

  memset(scCommand, '\0', 2000 + 1);
  memset(scInputMsg, '\0', 2000 + 1);
  //sprintf(scCommand, "%s", "ps -e -o pid -o cmd  | grep -v grep | grep dpp00");
  sprintf(scCommand, "%s", "ps -e -o pid -o cmd  | grep -v grep | grep java");
  fpCommandResult = popen(scCommand, "r");
  if( fpCommandResult == NULL ) {
    printf("failure\n");
  }

  if(errno != 0) {
    printf("errno: %d\n", errno);
  }

  if(fgets(scInputMsg, 2000, fpCommandResult) != NULL) {
    printf("scInputMsg=<%s>\n", scInputMsg);
  }
  iStatus = pclose(fpCommandResult);
  if(iStatus != 0) {
    printf("failure1\n");
  }
}

int daemon_init() {
    static const char   FUNC_NAME[] = "daemon_init";
    int                 pid;
    int                 i;
    int                 iStatus;
    int                 return_status = 0;
    char                scErr[500];

    if ((pid = fork()) < 0)
    {
        sprintf(scErr, "\n%s-%s: Error Fork failed with a return code=<%d> errno=<%d> ", "name", "function", pid, errno);
        return_status = 1;
    }
    else if (pid != 0)  /*  if you are the parent   */
    {
        exit(0);    /*  then you must die   */
    }

    if (return_status == 0)
    {
        iStatus = 0;
        /*  if the program reaches here, then it must be the child  */
        iStatus = setsid();         /* become session leader    */
        if(iStatus == -1)           /* We got some type of error */
        {
            sprintf(scErr, "\n%s-%s: ERR creating a new session, errno=<%d> ",  "name", "function", errno);
            return(2);
        }
        sprintf(scErr, "\nclosing the files is in progress\n");

        for (i = 0; i < FOPEN_MAX; i++) {
            close(i);
        }

        chdir(ROOT_DIRECTORY);  /*      change working directory        */
        umask(0);               /*      clear file mode creation mask   */
    }

    return(return_status);
}  /* end daemon_init */


int spawn_process(char *command_str, char *argv[])
{

    int     pid;
    int     return_status;


    if ((pid = fork()) < 0)
        return_status = pid;
    else if (pid != 0)      /* the parent returns OK */
        return_status = pid;
    else
    {
        /**********************************************************/
        /* If the program reaches here, then it must be the child */
        /* and so it transforms into new task.                    */
        /**********************************************************/
        if (execv(command_str, argv) < 0)
        {
            exit(0);        /* the task trasnformation failed */
        }
    }

    return(return_status);
}  /* end spawn_process */


/* obtain a pid/lock file */
int pidfile_lock(const char *pgmname, const char *sid, const char *role, const int options) {
    const char *pidfname = NULL;
    pid_t read_pid = PID_INVALID;
    int pidfile = FD_INVALID;
    int pidfile_created = FALSE;

    assert(NULL != pgmname);

    fprintf(stdout,"pidfile_lock() called.  pgmname=%s sid=%s role=%s options=%d\n", pgmname, sid, role, options);

    /* generate the pidfile name */
    if( NULL == (pidfname = _gen_pidfile_name(pgmname, sid, role)) ) {
        return 1;
    }

    /* open/create the pidfile.  */
    if( FD_INVALID == (pidfile = _open_pidfile(pidfname, &pidfile_created)) ) {
        return 2;
    }
    fprintf(stdout, "pidfile %s %s\n", pidfname, pidfile_created ? "was created" : "already exists");

    /* if we didn't create the pidfile, read in the pid */
    if( ! pidfile_created ) {
        read_pid = _read_pidfile(pidfile);
        if( PID_INVALID == read_pid ) {
            if( 0 != _close_pidfile(pidfile) ) {
            }
            return 3;
        }
        else if( PIDFILE_GARBAGE == read_pid ) {
            fprintf(stdout, "pidfile contains garbage; ignoring\n");
            if( 0 != unlink(pidfname) ) {
                return _update_pidfile(pidfile, getpid());
            }
        }
        else {
            fprintf(stdout, "pidfile %s contains value %ld\n", pidfname, read_pid);
        }
    }

    /* if we didn't create the pidfile, we have to check if another process is still alive */
    if( ! pidfile_created ) {
        if( _check_pid_alive(read_pid) ) {
            fprintf(stdout, "process %d (read from pidfile %s) is still alive", read_pid, pidfname);
            program_loc = PIDFILE_ALREADY_RUNNING;
            (void)_close_pidfile(pidfile);
            return 4;
        }
    }

    return _update_pidfile(pidfile, getpid());
}

