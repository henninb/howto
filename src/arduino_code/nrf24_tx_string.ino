#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>

/*
This sketch sends a string to a corresponding Arduino
with nrf24 attached.  It appends a specific value 
(2 in this case) to the end to signify the end of the
message.
*/

int msg[1];
RF24 TX_radio(9,10);
const uint64_t pipe = 0xE8E8F0F0E1LL;

void setup() {
  Serial.begin(9600);
  TX_radio.begin();
  TX_radio.openWritingPipe(pipe);
}

void loop() {
  String theMessage = "Hello there!";
  int messageSize = theMessage.length();
  for( int idx_i = 0; idx_i < messageSize; idx_i++ ) {
    int charToSend[1];
    charToSend[0] = theMessage.charAt(idx_i);
    TX_radio.write(charToSend, 1);
    radio.powerDown();
    delay(200);
    radio.powerUp();
  }
//send the 'terminate string' value...  
  msg[0] = 2; 
  TX_radio.write(msg, 1);
/*delay sending for a short period of time.  TX_radio.powerDown()/TX_radio.powerupp
//with a delay in between have worked well for this purpose(just using delay seems to
//interrupt the transmission start). However, this method could still be improved
as I still get the first character 'cut-off' sometimes. I have a 'checksum' function
on the receiver to verify the message was successfully sent.
*/
  TX_radio.powerDown(); 
  delay(1000);
  TX_radio.powerUp();
}
