//#include <TinyWireM.h>
//#include <SoftwareSerial.h>
#include <DHT.h>

//ATtiny Pin 5 = SDA
// ATtiny Pin 7 = SCK

//const int rx=3;
//const int tx=1;

//SoftwareSerial mySerial(rx,tx);

#define DHTPIN 4        // GPIO 2 on the ESP8266-01, D2 = 4 on the wemos, gpio12 = Pin 6 on the ESP-12E
DHT dht(DHTPIN, DHT22, 30);

void setup() {
  //int x = scan_port();
    dht.begin();
  delay(100);
}

void loop() {
  float humidity = 0.0;
  float celsius = 0.0;
  float farenheit = 0.0;
  
  delay(500); // 5 seconds

  humidity = dht.readHumidity();
  celsius = dht.readTemperature();
  farenheit = dht.readTemperature(true);
}

/*
int scan_port() {
  pinMode(rx,INPUT);
  pinMode(tx,OUTPUT);
  mySerial.begin(9600);

  mySerial.println ("I2C scanner. Scanning ...");

  TinyWireM.begin();
  for (byte i = 1; i < 127; i++) {
    TinyWireM.beginTransmission (i);
    if (TinyWireM.endTransmission () == 0) {
      mySerial.print ("Found address: ");
      mySerial.print (i, DEC);
      mySerial.print (" (0x");
      mySerial.print (i, HEX);
      mySerial.println (")");
      return i;
      delay(1);
      }
  }
  return 0;
}
*/
