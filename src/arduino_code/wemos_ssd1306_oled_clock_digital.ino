#include <SNTPtime.h>
#include <Wire.h>  // Include Wire if you're using I2C
#include <SFE_MicroOLED.h>  // Include the SFE_MicroOLED library
//#include <credentials.h>


#define PIN_RESET 255  //
#define DC_JUMPER 0  // I2C Addres: 0 - 0x3C, 1 - 0x3D

//////////////////////////////////
// MicroOLED Object Declaration //
//////////////////////////////////

MicroOLED oled(PIN_RESET, DC_JUMPER);  // I2C Example
SNTPtime NTPch("pool.ntp.org");

// Use these variables to set the initial time
int hours = 11;
int minutes = 50;
int seconds = 30;

// How fast do you want the clock to spin? Set this to 1 for fun.
// Set this to 1000 to get _about_ 1 second timing.
const int CLOCK_SPEED = 1000;

// Global variables to help draw the clock face:
const int MIDDLE_Y = oled.getLCDHeight() / 2;
const int MIDDLE_X = oled.getLCDWidth() / 2;

char txtBuf[9];
int CLOCK_RADIUS;
int POS_12_X, POS_12_Y;
int POS_3_X, POS_3_Y;
int POS_6_X, POS_6_Y;
int POS_9_X, POS_9_Y;
int S_LENGTH;
int M_LENGTH;
int H_LENGTH;

unsigned long lastDraw = 0;

/*
   The structure contains following fields:
   struct strDateTime
   {
   byte hour;
   byte minute;
   byte second;
   int year;
   byte month;
   byte day;
   byte dayofWeek;
   boolean valid;
   };
*/
strDateTime dateTime;


void initClockVariables()
{
  // Calculate constants for clock face component positions:
  oled.setFontType(0);
  if (MIDDLE_X>MIDDLE_Y) CLOCK_RADIUS = MIDDLE_Y;
  else CLOCK_RADIUS = MIDDLE_X;
  CLOCK_RADIUS = CLOCK_RADIUS - 1;
  POS_12_X = MIDDLE_X - oled.getFontWidth();
  POS_12_Y = MIDDLE_Y - CLOCK_RADIUS + 2;
  POS_3_X  = MIDDLE_X + CLOCK_RADIUS - oled.getFontWidth() - 1;
  POS_3_Y  = MIDDLE_Y - oled.getFontHeight() / 2;
  POS_6_X  = MIDDLE_X - oled.getFontWidth() / 2;
  POS_6_Y  = MIDDLE_Y + CLOCK_RADIUS - oled.getFontHeight() - 1;
  POS_9_X  = MIDDLE_X - CLOCK_RADIUS + oled.getFontWidth() - 2;
  POS_9_Y  = MIDDLE_Y - oled.getFontHeight() / 2;

  // Calculate clock arm lengths
  S_LENGTH = CLOCK_RADIUS - 2;
  M_LENGTH = S_LENGTH * 0.7;
  H_LENGTH = S_LENGTH * 0.5;
}

void setup()
{

  Serial.begin(115200);
  Serial.println();
  Serial.println("Booted");
  Serial.println("Connecting to Wi-Fi");

  WiFi.mode(WIFI_STA);
  WiFi.begin ("ssid", "password");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.println("WiFi connected");

  while (!NTPch.setSNTPtime()) Serial.print("!"); // set internal clock
  Serial.println();
  Serial.println("Time set");

  //based on time zone
  dateTime = NTPch.getTime(-5.0, 2); // get time from internal clock
  NTPch.printDateTime(dateTime);


  oled.begin();     // Initialize the OLED
  oled.clear(PAGE); // Clear the display's internal memory
  oled.clear(ALL);  // Clear the library's display buffer
  oled.display();   // Display what's in the buffer (splashscreen)

  initClockVariables();

  oled.clear(ALL);
  sprintf(txtBuf, "%02d:%02d:%02d", hours, minutes, seconds);
  oled.setCursor(5, 5);
  oled.print(txtBuf);
  //drawFace();
  //drawArms(hours, minutes, seconds);
  oled.display(); // display the memory buffer drawn
}

void loop()
{

  // Check if we need to update seconds, minutes, hours:
  if (lastDraw + CLOCK_SPEED < millis())
  {
    lastDraw = millis();
    // Add a second, update minutes/hours if necessary:
    updateTimeNet();

    // Draw the clock:
    oled.clear(PAGE);  // Clear the buffer
    //drawFace();  // Draw the face to the buffer
    //drawArms(hours, minutes, seconds);  // Draw arms to the buffer
    sprintf(txtBuf, "%02d:%02d:%02d", hours, minutes, seconds);
    oled.setCursor(5, 5);
    oled.print(txtBuf);
    oled.display(); // Draw the memory buffer
  }
}

// Simple function to increment seconds and then increment minutes
// and hours if necessary.
void updateTime()
{
  seconds++;  // Increment seconds
  if (seconds >= 60)  // If seconds overflows (>=60)
  {
    seconds = 0;  // Set seconds back to 0
    minutes++;    // Increment minutes
    if (minutes >= 60)  // If minutes overflows (>=60)
    {
      minutes = 0;  // Set minutes back to 0
      hours++;      // Increment hours
      if (hours >= 12)  // If hours overflows (>=12)
      {
        hours = 0;  // Set hours back to 0
      }
    }
  }
}

void updateTimeNet() {
  dateTime = NTPch.getTime(-6.0, 2); // get time from internal clock
  hours = dateTime.hour;
  minutes = dateTime.minute;
  seconds = dateTime.second;
}
