#include <TinyGPS++.h>
//#include <SoftwareSerial.h>
#include <AltSoftSerial.h>

//gy-neo6mv2

#define ARDUINO_RX_PIN 8
#define ARDUINO_TX_PIN 9

//AltSoftSerial:Setup info: begin
//AltSoftSerial:USE TIMER1
//AltSoftSerial:PIN: 8 RX
//AltSoftSerial:PIN: 9 TX
//AltSoftSerial:PIN:10 (unused PWM)
//AltSoftSerial:Setup info: end


//GPS_Vcc > Arduino 5 V (Red)
//GPS_Ground > BreadBoard_Ground > 10k oHm  > GPS_TX_PIN
//10k oHm > 4.7K oHm >  Pin 8 (ARDUINO_RX_PIN)
//-BreadBoard_Ground > Arduino_Ground (Black)
//-GPS_RX_PIN > Pin 9 (ARDUINO_TX_PIN)

AltSoftSerial ss;
//SoftwareSerial ss(ARDUINO_RX_PIN, ARDUINO_TX_PIN); 
TinyGPSPlus gps;

void setup()
{
  Serial.begin(9600);
  //ss.begin(4800);
  //Serial.begin(9600);
  ss.begin(9600);
  Serial.println("GPS Start...");
}

void loop()
{
  while( ss.available() ) {
    //This feeds the serial NMEA data into the library one char at a time
    gps.encode(ss.read());
  }

  Serial.println("Attempting...");
  smartDelay(1000);
  if( gps.location.isUpdated() ) {
    Serial.print("Satellite Count: ");
    Serial.println(gps.satellites.value());
    Serial.print("Latitude: ");
    Serial.println(gps.location.lat(), 6);
    Serial.print("Longitude: ");
    Serial.println(gps.location.lng(), 6);
    Serial.print("Speed MPH: ");
    Serial.println(gps.speed.mph());
    //Serial.print("Altitude Feet:");
    //Serial.println(gps.altitude.feet());
    Serial.println("*****");

  if (gps.date.isValid()) {
    Serial.print("Date: ");
    Serial.print(gps.date.month());
    Serial.print("/");
    Serial.print(gps.date.day());
    Serial.print("/");
    Serial.println(gps.date.year());
  }

  if (gps.time.isValid()) {
    Serial.print("Time: ");
    if (gps.time.hour() < 10) Serial.print("0");
    Serial.print(gps.time.hour());
    Serial.print(":");
    if (gps.time.minute() < 10) Serial.print("0");
    Serial.print(gps.time.minute());
    Serial.print(":");
    if (gps.time.second() < 10) Serial.print("0");
    Serial.print(gps.time.second());
  }

    //smartDelay(5000);
    Serial.println("");
  
  }
}

static void smartDelay(unsigned long ms)
{
  unsigned long start = millis();
  do 
  {
    while (ss.available()) {
      gps.encode(ss.read());
    }
  } while (millis() - start < ms);
}

