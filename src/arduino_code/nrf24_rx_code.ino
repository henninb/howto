#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(9, 10);
const uint64_t pipe = 0xE8E8F0F0E1LL;

void setup() {
  Serial.begin(115200);
  radio.begin();
  radio.openReadingPipe(1, pipe);
  radio.startListening();
  Serial.begin(115200);
  Serial.println("INFO: setup() - completed");
  pinMode(7, OUTPUT);
  delay(1000);
}

void loop() {
  unsigned long prime = 0;

  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  if (radio.available()) {
    radio.read(&prime, sizeof(unsigned long));
    //radio.read(&index, sizeof(int));
    Serial.println("Prime : " + String(prime));
    digitalWrite(7, HIGH);
    delay(1000);
    digitalWrite(7, LOW);
  } else {
    //Serial.println("INFO: loop() - completed");
    digitalWrite(7, LOW);
  }
}

