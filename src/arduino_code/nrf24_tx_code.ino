#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(9, 10);
const uint64_t pipe = 0xE8E8F0F0E1LL;
unsigned long prime = 0;

void setup() {
  Serial.begin(115200);
  radio.begin();
  radio.openWritingPipe(pipe);
  Serial.println("INFO: setup() TX completed.");
}

void loop() {
  prime++;
  prime = (prime % 4294967277);
  //prime = (prime % 10000);
  if( IsPrime(prime) == 1 ) {
    Serial.println("INFO: prime number sent: " + String(prime));
    radio.write(&prime, sizeof(unsigned long));
    radio.powerDown();
    delay(1000);
    radio.powerUp();
  } else {
    //Serial.println("WARN: not a prime number: " + String(prime));
  }
}

int IsPrime( unsigned long number ) {
    unsigned long i;
    for ( i = 2; i < number; i++ ) {
        if (number % i == 0 && i != number) {
          return 0;
        }
    }
    return 1;
}
