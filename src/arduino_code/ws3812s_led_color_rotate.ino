#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN            10

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      16

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_RGB + NEO_KHZ800);

int delayval = 75; // delay for half a second

void setup() {
  pixels.begin(); // This initializes the NeoPixel library.
  pixels.clear();
}

void loop() {
  for(int idx_i = 0; idx_i < NUMPIXELS; idx_i++ ) {
    pixels.setPixelColor(idx_i, pixels.Color(45,30,200));
    pixels.show(); // This sends the updated pixel color to the hardware.
    delay(delayval);
  }
  for(int idx_i = 0; idx_i < NUMPIXELS; idx_i++ ) {  
    pixels.setPixelColor(idx_i, pixels.Color(200,30,68));
    pixels.show(); // This sends the updated pixel color to the hardware.
    delay(delayval);
  }
  for(int idx_i = 0; idx_i < NUMPIXELS; idx_i++ ) {
    // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
    pixels.setPixelColor(idx_i, pixels.Color(25,150,25));
    pixels.show(); // This sends the updated pixel color to the hardware.
    delay(delayval);
  }
  for(int idx_i = 0; idx_i < NUMPIXELS; idx_i++ ) {
    pixels.setPixelColor(idx_i, pixels.Color(25,45,75));
    pixels.show(); // This sends the updated pixel color to the hardware.
    delay(delayval); // Delay for a period of time (in milliseconds).
  }
  
  for(int idx_i = 0; idx_i < NUMPIXELS; idx_i++ ) {
    pixels.setPixelColor(idx_i, pixels.Color(75,60,40));
    pixels.show(); // This sends the updated pixel color to the hardware.
    delay(delayval); // Delay for a period of time (in milliseconds).
  }
  pixels.clear();
}
