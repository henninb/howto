@echo off

set PATH=C:\Arduino\hardware\tools\avr\bin
rem need to escape space to work with mingw32-make
set PATH=%PATH%;C:\TDM-GCC-64\bin

set OS=win32

mingw32-make -f Makefile

pause
