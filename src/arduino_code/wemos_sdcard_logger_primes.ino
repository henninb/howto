/*
 * Micro SD Shield - Datalogger
 *
 * to an SD card using the SD library.
 *
 * The WeMos Micro SD Shield uses:
 * D5, D6, D7, D8, 3V3 and G
 *
 * The shield uses SPI bus pins:
 * D5 = CLK
 * D6 = MISO
 * D7 = MOSI
 * D8 = CS
 *
 * The WeMos D1 Mini has one analog pin A0.
 * The SD card library uses 8.3 format filenames and is case-insensitive.
 */

#include <SdFat.h>
#include <SPI.h>
//#include <SD.h>

//const int chipSelect = D8;

#define FILE_BASE_NAME "temp"

SdFat sd;
SdBaseFile binFile;
char binName[13] = FILE_BASE_NAME "00.bin";
StdioStream csvStream;
unsigned long idx = 0;

void setup()
{
  Serial.begin(115200);

  Serial.print("INFO: Initializing SD card.");

   //see if the card is present and can be initialized:
  //if ( !sd.begin(chipSelect) ) {
  //  Serial.println("ABORT: card failed or not present.");
  //  return;
  //}

  Serial.println("INFO: card initialized.");
}

void loop()
{
  idx++;
  idx = (idx % 4294967277);
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  //File dataFile = SD.open("datalog.txt", FILE_WRITE);
  //File dataFile = SD.open("datalog.txt", FILE_APPEND);
  //File dataFile = FileSystem.open("GPSdatalog.txt", FILE_APPEND);
  String dataString = "test";

  if( IsPrime(idx) == 1 ) {
    dataString = "Prime: " + String(idx);
  } else {
    dataString = "Is Not Prime: " + String(idx);
  }
  
  if (!csvStream.fopen(binName, "a")) {
    Serial.println(F("ABORT: binName not found."));
    return;
  }
  csvStream.print(F("some data"));
  csvStream.fclose();
  delay(10000);
}

int IsPrime( unsigned long number ) {
  long idx;

  for ( idx = 2; idx < number; idx++ ) {
    if (number % idx == 0 && idx != number) {
      return 0;
    }
  }
  return 1;
}
