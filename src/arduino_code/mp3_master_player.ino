#include <SPI.h>
#include <SdFat.h>
#include <SdFatUtil.h> 
#include <SFEMP3Shield.h>
//#include <SD.h>

SFEMP3Shield MP3player;
SdFat sd;
SdFile file;
//SdFile file;

byte temp;
byte result;
int left_ch = 0x3A;   // equals 58 of 254  ; lower numbers are louder and 0x0 is mute
int right_ch = 0x3A;   // equals 58 of 254 ; lower numbers are louder and 0x0 is mute
//const uint8_t SD_CHIP_SELECT = 10;
//File root;

void setup() {
  Serial.begin(115200);
   result = sd.begin(SD_SEL, SPI_HALF_SPEED);
  if(result != 0) {
    Serial.println("WARN: sd.begin() = " + result);
  }
  
  result = MP3player.begin();
  if(result != 0) {
    Serial.println("WARN: MP3player.begin() = " + result);
  }
  
  Serial.println("INFO: Vol = " + MP3player.getVolume());
  //MP3player.setVolume(75, 75);   //0x0000 and total silence is 0xFEFE for the or 0x7D, 0x7D
  MP3player.setVolume(left_ch, right_ch);   //0x0000 and total silence is 0xFEFE or FE=254, FE=254 
  Serial.println("INFO: Vol = " + MP3player.getVolume());
  
  //void pauseMusic();
  //bool resumeMusic();
  //uint8_t playTrack(uint8_t);
}

void loop() {
  Serial.println("At the begin of loop.");

  //sd.ls(&Serial, LS_R);
  ListFiles2(LS_R);
  result = MP3player.playMP3("track001.mp3");
  while( MP3player.isPlaying() ) {
    delay(5000);
    Serial.println("INFO: MP3player.isPlaying() = " + result);
  }  result = MP3player.playMP3("track002.mp3");
  while( MP3player.isPlaying() ) {
    delay(5000);
    Serial.println("INFO: MP3player.isPlaying() = " + result);
  }
  result = MP3player.playMP3("track003.mp3");
  while( MP3player.isPlaying() ) {
    delay(5000);
    Serial.println("INFO: MP3player.isPlaying() = " + result);
  }    
  result = MP3player.playMP3("track004.mp3");
  while( MP3player.isPlaying() ) {
    delay(5000);
    Serial.println("INFO: MP3player.isPlaying() = " + result);
  }  
  result = MP3player.playMP3("track005.mp3");
  while( MP3player.isPlaying() ) {
    delay(5000);
    Serial.println("INFO: MP3player.isPlaying() = " + result);
  }  
  result = MP3player.playMP3("track006.mp3");
  while( MP3player.isPlaying() ) {
    delay(5000);
    Serial.println("INFO: MP3player.isPlaying() = " + result);
  }
  result = MP3player.playMP3("track007.mp3");
  while( MP3player.isPlaying() ) {
    delay(5000);
    Serial.println("INFO: MP3player.isPlaying() = " + result);
  }  
  result = MP3player.playMP3("track008.mp3");
  while( MP3player.isPlaying() ) {
    delay(5000);
    Serial.println("INFO: MP3player.isPlaying() = " + result);
  }
  result = MP3player.playMP3("track009.mp3");
  while( MP3player.isPlaying() ) {
    delay(5000);
    Serial.println("INFO: MP3player.isPlaying() = " + result);
  }
  result = MP3player.playMP3("track010.mp3");
  while( MP3player.isPlaying() ) {
    delay(5000);
    Serial.println("INFO: MP3player.isPlaying() = " + result);
  }
  result = MP3player.playMP3("track011.mp3");
  while( MP3player.isPlaying() ) {
    delay(5000);
    Serial.println("INFO: MP3player.isPlaying() = " + result);
  }
  result = MP3player.playMP3("track012.mp3");
  while( MP3player.isPlaying() ) {
    delay(5000);
    Serial.println("INFO: MP3player.isPlaying() = " + result);
  }
  result = MP3player.playMP3("track013.mp3");
  while( MP3player.isPlaying() ) {
    delay(5000);
    Serial.println("INFO: MP3player.isPlaying() = " + result);
  }
  Serial.println("At the end of loop.");
}

/*
void ls( char *path ) {
  SdBaseFile dir;
  //char *x;
  
  if (!dir.open(path, O_READ) || !dir.isDir()) {
    Serial.println("bad dir");
    return;
  }
  dir.ls(&Serial, LS_R);
}
*/

/*
void printDirectory(File dir, int numTabs) {
  while (true) {

    File entry =  dir.openNextFile();
    if (! entry) {
      // no more files
      break;
    }
    for (uint8_t i = 0; i < numTabs; i++) {
      Serial.print('\t');
    }
    Serial.print(entry.name());
    if (entry.isDirectory()) {
      Serial.println("/");
      printDirectory(entry, numTabs + 1);
    } else {
      // files have sizes, directories do not
      Serial.print("\t\t");
      Serial.println(entry.size(), DEC);
    }
    entry.close();
  }
}
*/

void ListFiles2(uint8_t flags) {
  // This code is just copied from SdFile.cpp in the SDFat library
  // and tweaked to print to the serial output in html!
  dir_t p;

  sd.vwd()->rewind();
  Serial.println("start of function()");
  while (sd.vwd()->readDir(&p) > 0) {
    // done if past last used entry
    if (p.name[0] == DIR_NAME_FREE) break;

    // skip deleted entry and entries for . and  ..
    if (p.name[0] == DIR_NAME_DELETED || p.name[0] == '.') continue;

    // only list subdirectories and files
    if (!DIR_IS_FILE_OR_SUBDIR(&p)) continue;

    if ((p.attributes & DIR_ATT_HIDDEN) != DIR_ATT_HIDDEN) continue;

    // print any indent spaces
    Serial.print(F("  "));
    for (uint8_t i = 0; i < 11; i++) {
      if (p.name[i] == ' ') continue;
      if (i == 8) {
        Serial.print('.');
      }
      Serial.print((char)p.name[i]);
    }
    Serial.print(F(" "));

    // print file name with possible blank fill
    for (uint8_t i = 0; i < 11; i++) {
      if (p.name[i] == ' ') continue;
      if (i == 8) {
        Serial.print('.');
      }
      Serial.print((char)p.name[i]);
      if (DIR_IS_LONG_NAME(&p)) {
        Serial.print(F(" long fn"));
      }
    }

    if (DIR_IS_SUBDIR(&p)) {
      Serial.print('/');
    }

    // print modify date/time if requested
    if (flags & LS_DATE) {
      sd.vwd()->printFatDate(p.lastWriteDate);
      Serial.print(' ');
      sd.vwd()->printFatTime(p.lastWriteTime);
    }
    // print size if requested
    if (!DIR_IS_SUBDIR(&p) && (flags & LS_SIZE)) {
      Serial.print(' ');
      Serial.print(p.fileSize);
    }
    Serial.println();
  }
  Serial.println();
}
