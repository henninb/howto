#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>

/*
This sketch receives strings from sending unit via nrf24 
and prints them out via serial.  The sketch waits until
it receives a specific value (2 in this case), then it 
prints the complete message and clears the message buffer.
*/

int led = 13;
int msg[1];
RF24 RX_radio(9, 10);
const uint64_t pipe = 0xE8E8F0F0E1LL;
int lastmsg = 1;
String theMessage = "";

void setup() {
  Serial.begin(9600);
  RX_radio.begin();
  RX_radio.openReadingPipe(1,pipe);
  RX_radio.startListening();
}

void loop() {
  if (RX_radio.available()){
    RX_radio.read(msg, 1); 
    char theChar = msg[0];
    if (msg[0] != 2) {
      theMessage.concat(theChar);
    } else {
      Serial.println(theMessage);
      theMessage= "";
      digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
      delay(1000);               // wait for a second
      digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
      delay(1000);               // wait for a second
    }
  }
}
