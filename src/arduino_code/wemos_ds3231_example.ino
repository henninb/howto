//****************************************************************************************
//                         OLED NTP Server CLOCK with AutoConect
//                      WeMos D1 R2 WiFi Board, 0.96" OLED, Soft RTC
//                             Adrian Jones, March 2016
//
//****************************************************************************************
 
// Build 3
//   r1 160221 - initial build with soft RTC and NTP server
//   r2 160222 - addition of WiFi scanner
//   r3 160314 - addition of WiFi AutoConnect from https://github.com/tzapu/WiFiManager

//*****************************************************************************************
#define bld  3
#define rev  3
//*****************************************************************************************


#include <ESP8266WiFi.h>                  // https://github.com/esp8266/Arduino
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>                  // https://github.com/tzapu/WiFiManager
#include <WiFiUdp.h>

unsigned int localPort = 2390;            // local port to listen for UDP packets
IPAddress timeServer(129, 6, 15, 28);     // time.nist.gov NTP server
const int NTP_PACKET_SIZE = 48;           // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[ NTP_PACKET_SIZE];      // buffer to hold incoming and outgoing packets

WiFiUDP udp;                              // A UDP instance to let us send and receive packets over UDP
boolean doNTP=false;

#include <Wire.h>
// LCD handler
#include <OzOLED.h>                       // OLED Library
#define Brightness 255                    // display brightness [0-255]
boolean doOLED=true;
#define ops    OzOled.printString         // shortcuts for OLED
#define osc    OzOled.setCursorXY
#define opn    OzOled.printNumber
#define opc    OzOled.printChar

// RTC handler
#include <RTClib.h>                       // RTC-Library
RTC_Millis RTC;                           // RTC (soft)
DateTime now;                             // current time
int ch,cm,cs,os,cdy,cmo,cyr,cdw;          // current time & date variables
int nh,nm,ns,ndy,nmo,nyr,ndw;             // NTP-based time & date variables

#define min(a,b) ((a)<(b)?(a):(b))        // recreate the min function

#include <EEPROM.h>
String esid;
String epass;                        // eprom stored network

boolean doSerial=true;      
#define sb    Serial.begin(115200)        // shortcuts for serial output
#define sp    Serial.print
#define spf   Serial.printf
#define spln  Serial.println

//*****************************************************************************************
//                                    SUBROUTINES
//*****************************************************************************************

// sendNTPpacket(): send an NTP request to the time server at the given address
unsigned long sendNTPpacket(IPAddress& address) {
  if(doSerial) spln("Sending UDP NTP packet request");         

  memset(packetBuffer, 0, NTP_PACKET_SIZE); // set all bytes in the buffer to 0
  // Initialize values needed to form NTP request
  packetBuffer[0] = 0b11100011;             // LI, Version, Mode
  packetBuffer[1] = 0;                      // Stratum, or type of clock
  packetBuffer[2] = 6;                      // Polling Interval
  packetBuffer[3] = 0xEC;                   // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  udp.beginPacket(address, 123);             // NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);  // all NTP fields have values, send UDP packet requesting a timestamp\
  udp.endPacket();
}

//*****************************************************************************************
//                                 TIME ROUTINES
//*****************************************************************************************
// getTime(): get current time from RTC (soft or hard)
void getTime() { 
  now = RTC.now();  
  ch = min(24,now.hour()); if(ch == 0) ch=24; // hours 1-24
  cm = min(59,now.minute()); 
  cs = min(59,now.second());
  cdy= min(31,now.day()); 
  cmo= min(now.month(),12); 
  cyr= min(99,now.year()-2000); 
  cdw =now.dayOfTheWeek();
}

// IsDST(): returns true if during DST, false otherwise
boolean IsDST(int mo, int dy, int dw) {
  if (mo < 3 || mo > 11) { return false; }                // January, February, and December are out.
  if (mo > 3 && mo < 11) { return true;  }                // April to October are in
  int previousSunday = dy - dw;                               
  if (mo == 3) { return previousSunday >= 8; }            // In March, we are DST if our previous Sunday was on or after the 8th.
  return previousSunday <= 0;                             // In November we must be before the first Sunday to be DST. That means the previous Sunday must be before the 1st.
}

// doClearOLED(start,end): Clear OLED lines from [start] to [end] of characters
void doClearOLED(int st,int en) {
  for(int x=st;x<en;x++) { osc(0,x); ops("                "); }
}



//*****************************************************************************************
//                                      Initial Setup
//*****************************************************************************************
void setup() {
  sb; delay(1000); sb;                    // start serial o/p
  
  if(doOLED) {
    OzOled.init();                        // initialze Oscar OLED display
    OzOled.setNormalDisplay();
    OzOled.setHorizontalMode();           // set addressing mode to Horizontal Mode
    OzOled.setBrightness(Brightness);     // set brightness
    OzOled.clearDisplay();                // clear the screen and set start position to top left corner
    doClearOLED(0,7);                     // clear excess chars at end of line
    for(int x=0; x<8; x++) ops(" ",16,x); // clear excess chars at end of line
    ops("OLED NTP CLOCK",1,0); 
    osc(1,2);                             // set to line 2
    ops(" [Build "); opn((long) bld); ops("."); opn((long) rev); ops("]");
    ops("Go 192.168.4.1",1,4);    
  }

  if(doSerial) {
    spln();
    spln(F("*********************************")); 
    spln(F("OLED NTP-Synchronized Soft CLOCK")); 
    spln(F("Adrian Jones, February 2016"));
    sp(F("Build ")); sp(bld);  sp(F(".")); spln(rev);
    spln(F("WiFi configuration at 192.168.4.1"));    
    spln(F("*********************************"));
    spln();
  }
    
    WiFiManager wifiManager;                       // WiFiManager: Local intialization. Once its business is done, there is no need to keep it around
    // wifiManager.resetSettings();                 // reset saved settings   
    // wifiManager.setAPConfig(IPAddress(10,0,1,1), IPAddress(10,0,1,1), IPAddress(255,255,255,0));  //set custom ip for portal
    // wifiManager.autoConnect();                   // use this for auto generated name ESP + ChipID       
    wifiManager.autoConnect("NTP_SynClock_Connect");   // fetches ssid and pass from eeprom and tries to connect. 
    // If does not connect with stored information, start an access point with this name and stay in blocking loop awaiting configuration

  if(doSerial) spln(F("Connected to WiFi"));       // if you get here you have connected to the WiFi
  if(doOLED)   ops("WiFi connected",1,6);    

  delay(2000);
  doClearOLED(1,6);

  Wire.begin();
  RTC.begin(DateTime(F(__DATE__), F(__TIME__)));    // initially set to compile date & time
  udp.begin(localPort);
  if(doSerial) sp(F("Starting UDP with Local Port ")); spln(udp.localPort());
  if(doOLED)   ops("Start UDP",1,4); opn((long)udp.localPort());
  delay(2000);
  doClearOLED(1,6);
  doNTP=true;                                       // get NTP timestamp immediately

}

//*****************************************************************************************
//                                      MAIN LOOP
//*****************************************************************************************
void loop(void) {

  getTime();
  if(cm%60==0) doNTP=true;                 // set flag every hour

  // ****************************** Execute every second ***********************
  if(cs != os && !doNTP) {                              
    if(doSerial) {
      sp(F("Time: "));   sp(ch);  sp(F(":")); if(cm <10) sp("0");  sp(cm);  sp(F(":")); if(cs <10) sp("0");  sp(cs);
      sp(F("\tDate: ")); sp(cyr); sp(F("/")); if(cmo <10) sp("0"); sp(cmo); sp(F("/")); if(cdy <10) sp("0"); sp(cdy);
      spln("");
    }
  
    if(doOLED) {
      ops("Date: ",1,2); if(cyr<10) ops("0"); opn((long)cyr); ops("/"); if(cmo<10) ops("0"); opn((long)cmo); ops("/"); if(cdy<10) ops("0"); opn((long)cdy);
      ops("Time: ",1,4); if(ch <10) ops(" "); opn((long)ch);  ops(":"); if(cm <10) ops("0"); opn((long)cm);  ops(":"); if(cs <10) ops("0"); opn((long)cs);
    }   
    os = cs;
  }
  // *************************************************************************

  // ****************************** Execute every hour ***********************
  if(doNTP) {
    sendNTPpacket(timeServer);            // send an NTP packet to a time server
    delay(1000);                          // wait to see if a reply is available
  
    int cb = udp.parsePacket();           // get packet (if available)
    if (!cb) {
      if(doSerial) spln(F("... no packet yet"));
      if(doOLED)   ops("No packet yet",1,4);
      
    } else {
      if(doSerial) sp(F("... NTP packet received with ")); sp(cb); spln(F(" bytes"));     // We've received a packet, read the data from it
      udp.read(packetBuffer, NTP_PACKET_SIZE);                            // read the packet into the buffer

      unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);  // timestamp starts at byte 40 of packet. It is 2 words (4 bytes) long
      unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);   // Extract each word and...
      unsigned long secsSince1900 = highWord << 16 | lowWord;             // ... combine into long: NTP time (seconds since Jan 1 1900):

      const unsigned long seventyYears = 2208988800UL;                    // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
      unsigned long epoch = secsSince1900 - seventyYears;                 // subtract seventy years to get to 1 Jan. 1900:

      if(doSerial) {
        // sp("Seconds since Jan 1 1900 = " );  spln(secsSince1900);
        // sp("Unix time = ");  spln(epoch);                       // print Unix time:
     }
      
      int tz = 5;                                            // adjust for EST time zone
      DateTime gt(epoch - (tz*60*60));                       // obtain date & time based on NTP-derived epoch...
      tz = IsDST(gt.month(), gt.day(), gt.dayOfTheWeek())?4:5;  // if in DST correct for GMT-4 hours else GMT-5
      DateTime ntime(epoch - (tz*60*60));                    // if in DST correct for GMT-4 hours else GMT-5
      RTC.adjust(ntime);                                     // and set RTC to correct local time   
      nyr = ntime.year()-2000;                       
      nmo = ntime.month();
      ndy = ntime.day();    
      nh  = ntime.hour(); if(nh==0) nh=24;                   // adjust to 1-24            
      nm  = ntime.minute();                     
      ns  = ntime.second();                     

      sp(F("... NTP packet local time: [GMT - ")); sp(tz); sp(F("]: "));       // Local time at Greenwich Meridian (GMT) - offset  
      if(nh < 10) sp(F(" ")); sp(nh);  sp(F(":"));          // print the hour 
      if(nm < 10) sp(F("0")); sp(nm);  sp(F(":"));          // print the minute
      if(ns < 10) sp(F("0")); sp(ns);                       // print the second

      sp(F(" on "));                                        // Local date
      if(nyr < 10) sp(F("0")); sp(nyr);  sp(F("/"));        // print the year 
      if(nmo < 10) sp(F("0")); sp(nmo);  sp(F("/"));        // print the month
      if(ndy < 10) sp(F("0")); spln(ndy);                   // print the day
      spln();

      ops("NTP:  ",1,6);                                    // NTP (and adjusted) time
      if(nh <10) ops(" "); opn((long)nh); ops(":");   
      if(nm <10) ops("0"); opn((long)nm); ops(":"); 
      if(ns <10) ops("0"); opn((long)ns);

      doNTP=false;
     // *************************************************************************
    }
  }   
}
