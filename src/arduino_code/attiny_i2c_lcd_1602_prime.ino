#include <TinyWireM.h>
#include <LiquidCrystal_attiny.h>
#include <SoftwareSerial.h>

//ATtiny Pin 5 = SDA
// ATtiny Pin 7 = SCK

const int rx=3;
const int tx=1;

SoftwareSerial mySerial(rx,tx);

//#define GPIO_ADDR 0x27
#define GPIO_ADDR 0x3f
LiquidCrystal_I2C lcd(GPIO_ADDR, 16, 2); // set address & 16 chars / 2 lines
//max unsigned long 4294967295 
unsigned long idx;

void setup() {
  lcd.init();                           // initialize the lcd
  lcd.backlight();                      // Print a message to the LCD.
  lcd.setCursor(0, 0);
  idx = 0;
  // to 0 - 4,294,967,295 (2^32 - 1).   - 4294967295 
  //String x = String(sizeof(unsigned long));
  int x = scan_port();
}

void loop() {
  idx++;
  idx = (idx % 4294967277);
  String counter = String(idx);

  if( IsPrime(idx) == 1 ) {
    lcd.clear();
    printString(counter, 0);
    printString("is prime", 1);
    lcd.blink();
    delay(1500);
  } else {
    lcd.clear();
    printString(counter, 0);
    printString("is NOT prime", 1);
    lcd.blink();
    delay(1000);
  }
}

void printString( String s , int row ) {
  for (int i = 0; i < s.length(); i++) {
    lcd.setCursor(i, row);
    lcd.print(s[i]);
  }
}

void printBars(int bars) {
  if (bars > 15) bars = 15;
  for (int i = 0; i < 15; i++) {
    lcd.setCursor(i, 1);
    if (i <= bars) {
      lcd.print("*");
    } else {
      lcd.print(" ");
    }
  }
}

int IsPrime(unsigned long number) {
    int i;
    for (i=2; i<number; i++) {
        if (number % i == 0 && i != number) {
          return 0;
        }
    }
    return 1;
}


int scan_port() {
  pinMode(rx,INPUT);
  pinMode(tx,OUTPUT);
  mySerial.begin(9600);

  mySerial.println ("I2C scanner. Scanning ...");

  TinyWireM.begin();
  for (byte i = 1; i < 127; i++) {
    TinyWireM.beginTransmission (i);
    if (TinyWireM.endTransmission () == 0) {
      mySerial.print ("Found address: ");
      mySerial.print (i, DEC);
      mySerial.print (" (0x");
      mySerial.print (i, HEX);
      mySerial.println (")");
      return i;
      delay(1);
      }
  }
  return 0;
}
