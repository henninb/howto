#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN            10

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      16

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_RGB + NEO_KHZ800);

int delayval = 75 * 4; // delay

void setup() {
  pixels.begin(); // This initializes the NeoPixel library.
  pixels.clear();
}

void loop() {
  pixels.setPixelColor(01, pixels.Color(66, 100, 244));
  pixels.setPixelColor(03, pixels.Color(66, 100, 244));
  pixels.setPixelColor(07, pixels.Color(66, 100, 244));
  pixels.setPixelColor(11, pixels.Color(66, 100, 244));
  pixels.setPixelColor(13, pixels.Color(66, 100, 244));
  pixels.setPixelColor(15, pixels.Color(66, 100, 244));
  pixels.show();
  delay(delayval);
  pixels.clear();
  
  pixels.setPixelColor(00, pixels.Color(66, 100, 244));
  pixels.setPixelColor(02, pixels.Color(66, 100, 244));
  pixels.setPixelColor(04, pixels.Color(66, 100, 244));
  pixels.setPixelColor(08, pixels.Color(66, 100, 244));
  pixels.setPixelColor(12, pixels.Color(66, 100, 244));
  pixels.setPixelColor(14, pixels.Color(66, 100, 244));
  pixels.show();
  delay(delayval);
  pixels.clear();
}
