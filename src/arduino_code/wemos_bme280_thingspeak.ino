#include <ESP8266WiFi.h>
//#include <DHT.h>
#include <SparkFunBME280.h>
#include <Wire.h>
#include <PubSubClient.h>
#include <aJSON.h>
#include <WiFiUdp.h>

//D0 - 16 for deepSleep wakeup D0 to the reset (wemos)
//D2 - 04 for DHT22 or DHT11
//D1 - 05 for SCL (i2c)
//D2 - 04 for SDA (i2c)
#define PIN_SCL 5
#define PIN_SDA 4
//#define DHTPIN 12        // ESP-01 GPIO Pin 2; Wemos: D2 = 4; ESP-12: GPIO-12 = (Pin 12 Arduino IDE)
//#define DHTTYPE DHT22   // DHT22 or DHT11
//#define DHTTYPE DHT11   // DHT22 or DHT11

#define WIFI_DELAY 300
#define USE_SERIAL_PRINT_FLAG 1
#define USE_DEEP_SLEEP_FLAG 1
#define USE_SENSOR_FLAG 1
#define UDP_LOCAL_PORT 2390
#define MQTT_PORT 1883
#define NTP_PACKET_SIZE 48
#define ESP_DEEP_SLEEP 600  //10 minutes
//#define ESP_DEEP_SLEEP 10 //10 seconds for testing

const char* ssid = "";
const char* password = "";
const char* thingspeak_host = "api.thingspeak.com";
const char* sparkfun_host = "data.sparkfun.com";
const char* mqttServerName = "192.168.100.134";
//const char* ntpServerName = "time.nist.gov";
const char* ntpServerName = "pool.ntp.org";

//mosquitto_sub -d -t home/lower_bathroom/temperature

String sparkfun_publicKey = "";
String sparkfun_privateKey = "";

String location = "default";
String mqtt_channel = "default";
String THINGSPEAK_API_KEY_LOWER_BATHROOM = ""; //Write API Key
String THINGSPEAK_API_KEY_OFFICE = ""; //Write API Key
String THINGSPEAK_API_KEY_MASTER_BEDROOM = ""; //Write API Key
String THINGSPEAK_API_KEY_GARAGE = ""; //Write API Key
String THINGSPEAK_API_KEY_LAUNDRY_ROOM = ""; //Write API Key
String THINGSPEAK_API_KEY = "default";

BME280 bme280;
//DHT dht(DHTPIN, DHTTYPE, 30);  // 30 is for cpu clock of esp8266 80Mhz
ADC_MODE(ADC_VCC);

IPAddress timeServerIP;
WiFiUDP udp;
byte packetBuffer[NTP_PACKET_SIZE];

//mac address and epoch time
byte macAddressByteArray[6];
char macAddressCharArray[18];

void setup() {
  //Wemos Pins, need to test
  //pinMode(D0, OUTPUT);  //Save Power by writing all Digital IO LOW
  //pinMode(D1, OUTPUT);  //Save Power by writing all Digital IO LOW
  //pinMode(D2, OUTPUT);  //Save Power by writing all Digital IO LOW
  //pinMode(D3, OUTPUT);  //Save Power by writing all Digital IO LOW
  //pinMode(D4, OUTPUT);  //Save Power by writing all Digital IO LOW
  //pinMode(D5, OUTPUT);  //Save Power by writing all Digital IO LOW
  //pinMode(D6, OUTPUT);  //Save Power by writing all Digital IO LOW
  //pinMode(D7, OUTPUT);  //Save Power by writing all Digital IO LOW
  //pinMode(D8, OUTPUT);  //Save Power by writing all Digital IO LOW
#ifdef USE_DEEP_SLEEP_FLAG
  pinMode(D0, WAKEUP_PULLUP);
#endif
  setLocation("office");

#ifdef USE_SERIAL_PRINT_FLAG
  Serial.begin(115200);
#endif

#ifdef USE_SERIAL_PRINT_FLAG
  Serial.println("\nINFO: ESP8266 Connecting to " + String(ssid));
  Serial.print("Location: ");
  Serial.println(location);
  Serial.println("API Key: " + String(THINGSPEAK_API_KEY));
#endif
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  
  while( WiFi.status() != WL_CONNECTED ) {
    delay(WIFI_DELAY);
#ifdef USE_SERIAL_PRINT_FLAG
    Serial.print(".");
#endif
  }

  while (WiFi.localIP() == INADDR_NONE) {
    delay(300);
  }
  
#ifdef USE_SERIAL_PRINT_FLAG
  Serial.println("\nWiFi connected");  
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
#endif

#ifdef USE_SENSOR_FLAG
  Wire.begin(PIN_SDA, PIN_SCL); //required for i2c
  bme280.settings.commInterface = I2C_MODE;
  bme280.settings.I2CAddress = 0x76;
  bme280.settings.runMode = 3; //Normal mode
  bme280.settings.tStandby = 0;
  bme280.settings.filter = 0;
  bme280.settings.tempOverSample = 1;
  bme280.settings.pressOverSample = 1;
  bme280.settings.humidOverSample = 1;
  delay(100);  //Make sure sensor had enough time to turn on. BME280 requires 2ms to start up.
  bme280.begin();
  //dht.begin();
  //delay(100);
#endif

//SPIFFS.begin();
//File ifp = SPIFFS.open("data.dat", "a");
//if(!ifp) {
//  Serial.println("ABORT: cound not open file for appending.");
//  return;
//}

  WiFi.macAddress(macAddressByteArray);
  for ( int i = 0; i < sizeof(macAddressByteArray); i++ ) {
    if( i == (sizeof(macAddressByteArray) - 1)) { 
      sprintf(macAddressCharArray, "%s%02x", macAddressCharArray, macAddressByteArray[i]);
    } else {
      sprintf(macAddressCharArray, "%s%02x:", macAddressCharArray, macAddressByteArray[i]);
    }
  } 

#ifdef USE_SERIAL_PRINT_FLAG
  Serial.println("INFO: setup() complete.");
#endif
}

void callback( char* topic, byte* payload, unsigned int length ) {
  Serial.print("Received message for topic ");
  Serial.print(topic);
  Serial.print("with length ");
  Serial.println(length);
  Serial.println("Message:");
  Serial.write(payload, length);
  Serial.println();
}

void setLocation(String myLocation) {
  location = myLocation;
  
  if( myLocation == "office") {
    THINGSPEAK_API_KEY = THINGSPEAK_API_KEY_OFFICE;
    mqtt_channel = "home/office/temperature";
  } else if (myLocation == "garage" ) {
    THINGSPEAK_API_KEY = THINGSPEAK_API_KEY_GARAGE;
    mqtt_channel = "home/garage/temperature";
  } else if (myLocation == "lower_bathroom" ) {
    THINGSPEAK_API_KEY = THINGSPEAK_API_KEY_LOWER_BATHROOM;
    mqtt_channel = "home/lower_bathroom/temperature";
  } else if (myLocation == "master_bedroom" ) {
    THINGSPEAK_API_KEY = THINGSPEAK_API_KEY_MASTER_BEDROOM;
    mqtt_channel = "home/master_bedroom/temperature";
  } else if (myLocation == "laundry_room" ) {
    THINGSPEAK_API_KEY = THINGSPEAK_API_KEY_LAUNDRY_ROOM;
    mqtt_channel = "home/laundry_room/temperature";
  } else {
    THINGSPEAK_API_KEY = "";
  }  
}

aJsonObject *createJson( double fahrenheit, double humidity, String macAddress, long epoch ) {
  aJsonObject *msg = aJson.createObject();
  
  aJson.addItemToObject(msg, "fahrenheit", aJson.createItem(fahrenheit));
  aJson.addItemToObject(msg, "humidity", aJson.createItem(humidity));
  aJson.addItemToObject(msg, "macAddress", aJson.createItem(macAddress));
  aJson.addItemToObject(msg, "epoch", aJson.createItem(String(epoch)));
  return msg;
}

void publishMQTT(const char *hostname, double fahrenheit, double humidity, String macAddress, long epoch, String location) {
  WiFiClient client1;
  aJsonObject *json;
  char *jsonStr;
  String json_str;
  PubSubClient client(hostname, MQTT_PORT, callback, client1);

  json = createJson(fahrenheit, humidity, macAddress, epoch);
  //jsonStr = aJson.print(json);
  json_str = "{\"fahrenheit\":" + String(fahrenheit) + ",\"humidity\":" + String(humidity) + ",\"macAddress\":\"" + macAddress + "\",\"epoch\":" + String(epoch) + "}";
  
  // Reconnect if the connection was lost
  if (!client.connected()) {
    Serial.println("WARN: publishMQTT() disconnected, reconnecting to publish.");
    if (!client.connect(mqtt_channel.c_str())) {
      Serial.println("WARN: publishMQTT() connection failed");
    }
  }

  if (client.publish(mqtt_channel.c_str(), json_str.c_str())) {
  //if (client.publish(mqtt_channel.c_str(), jsonStr)) {
    Serial.println("INFO: publishMQTT() Publish success");
  } else {
    Serial.println("WARN: publishMQTT() publish failed");
  }
}

void postSparkfunMetrics(const char *hostname, String privateKey, String publicKey, String fahrenheit, String humidity, String location) {
  WiFiClient client1;
  String url;
  unsigned long timeout;
  String line;

  if( !client1.connect(hostname, 80) ) {
#ifdef USE_SERIAL_PRINT_FLAG
    Serial.println("WARN: connection failed");
#endif
    return;
  }

  url = "/input/";
  url += sparkfun_publicKey;
  url += "?private_key=";
  url += sparkfun_privateKey;
  url += "&temp=";
  url += fahrenheit;
  url += "&humidity=";
  url += humidity;
  url += "&loc=";
  url += location;

  client1.print(String("GET ") + url + " HTTP/1.1\r\n" + "Host: " + hostname + "\r\n" +   "Connection: close\r\n\r\n");
  delay(10);

  timeout = millis();
  while ( client1.available() == 0 ) {
    if (millis() - timeout > 5000) {
#ifdef USE_SERIAL_PRINT_FLAG
      Serial.println("WARN: postSparkfunMetrics() - client Timeout on sparkfun.");
#endif
      client1.stop();
      return;
    }
  }
  
  // Read all the lines of the reply from server and print them to Serial
  while( client1.available() ) {
    line = client1.readStringUntil('\r');
  }
#ifdef USE_SERIAL_PRINT_FLAG
    Serial.println("INFO: postSparkfunMetrics() - server responded successfully.");
#endif
}

void postThingspeakMetrics(const char *hostname, String api_key, String fahrenheit, String humidity) {
  WiFiClient client1;
  String url;
  unsigned long timeout;
  String line;

  if( !client1.connect(hostname, 80) ) {
#ifdef USE_SERIAL_PRINT_FLAG
    Serial.println("WARN: postThingspeakMetrics() - connection failed.");
#endif
    return;
  }

  url = "/update?api_key=";
  url += api_key;
  url += "&field1=";
  url += String(fahrenheit);
  url += "&field2=";
  url += String(humidity);

  client1.print(String("GET ") + url + " HTTP/1.1\r\n" + "Host: " + hostname + "\r\n" +   "Connection: close\r\n\r\n");
  delay(10);

  timeout = millis();
  while ( client1.available() == 0 ) {
    if (millis() - timeout > 5000) {
#ifdef USE_SERIAL_PRINT_FLAG
      Serial.println("WARN: postThingspeakMetrics() - client timeout.");
#endif
      client1.stop();
      return;
    }
  }
  
  // Read all the lines of the reply from server and print them to Serial
  while( client1.available() ) {
    line = client1.readStringUntil('\r');
  }
#ifdef USE_SERIAL_PRINT_FLAG
    Serial.print("INFO: postThingspeakMetrics() - server responded successfully.");
#endif
}

void loop() {
  double humidity = 0.0;
  double celsius = 0.0;
  double fahrenheit = 0.0;
  double pressure = 0.0;
  String macAddress = String(macAddressCharArray);
  long epoch = 0;
  //uint32_t vcc = ESP.getVcc();
  //double vcc = ESP.getVcc();
  double vcc = ESP.getVcc() / 1000.0;
  
  delay(5000); // 5 seconds delay

  // Reading temperature or humidity takes about 250 milliseconds for the dht22
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)

#ifdef USE_SERIAL_PRINT_FLAG
  Serial.println("INFO: loop() start.");
#endif

#ifdef USE_SENSOR_FLAG
  //humidity = dht.readHumidity();
  //celsius = dht.readTemperature();
  //fahrenheit = dht.readTemperature(true);

  humidity = bme280.readFloatHumidity();
  celsius = bme280.readTempC();
  fahrenheit = bme280.readTempF();
#endif  

#ifdef USE_SERIAL_PRINT_FLAG
  Serial.println("Humidity: " + String(humidity) + "\t" + "Celsius: " + String(celsius) + "\t" + "fahrenheit: " + String(fahrenheit));
  //Serial.println("host: " + String(sparkfun_host));
#endif

  publishMQTT(mqttServerName, fahrenheit, humidity, macAddress, epoch, location);
#ifdef USE_SERIAL_PRINT_FLAG  
  Serial.println("\nINFO: mqtt connection completed.");
#endif

  postThingspeakMetrics(thingspeak_host, THINGSPEAK_API_KEY, String(fahrenheit), String(humidity));
#ifdef USE_SERIAL_PRINT_FLAG  
  Serial.println("\nINFO: http thingspeak connection completed.");
#endif
  //postSparkfunMetrics(sparkfun_host, sparkfun_privateKey, sparkfun_publicKey, String(fahrenheit), String(humidity), location);
  
#ifdef USE_SERIAL_PRINT_FLAG  
  Serial.println("\nINFO: http sparkfun connection completed.");
#endif

#ifdef USE_DEEP_SLEEP_FLAG
  ESP.deepSleep(ESP_DEEP_SLEEP * 1000000);
#endif
}

unsigned long getEpoch() {
  int cb = 0;
  unsigned long highWord;
  unsigned long lowWord;
  unsigned long secsSince1900;
  unsigned long epoch;
  unsigned long seventyYears;
  
  udp.begin(UDP_LOCAL_PORT);
  
  WiFi.hostByName(ntpServerName, timeServerIP); 

  do {
    sendNTPpacket(timeServerIP);
    delay(10000);
  } while ((cb = udp.parsePacket()) == 0 );

  udp.read(packetBuffer, NTP_PACKET_SIZE);

  highWord = word(packetBuffer[40], packetBuffer[41]);
  lowWord = word(packetBuffer[42], packetBuffer[43]);
  secsSince1900 = highWord << 16 | lowWord;
  seventyYears = 2208988800UL;
  epoch = secsSince1900 - seventyYears;
  return epoch;
}

unsigned long sendNTPpacket( IPAddress& address ) {
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}
