// Henry's Bench
//  1.44"  128 * 128  SPI  V1.1 Display Tutorial

#include <SPI.h>
#include <Adafruit_GFX.h>
#include <TFT_ILI9163C.h>


// Color definitions
#define  BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0  
#define WHITE   0xFFFF

/*
Your Connections to an Uno (Through a Level Shifter)

 LED to 3.3V
 SCK to D13
 SDA to D11
 A0 to D8
 RST to D9
 CS to D10
 GND to GND
 VCC to 3.3V 
 */
 
#define CS 10
#define DC 9

// Declare an instance of the ILI9163
TFT_ILI9163C tft = TFT_ILI9163C(CS, 8, DC);  

//unsigned long idx = 4294967295 - 50;
unsigned long idx = 0;

void setup() {
  tft.begin();
  tft.fillScreen();
}

void loop(){
  idx++;
  testText(idx);
  delay(1000);
}

unsigned long testText( unsigned long x ) {
  idx = (idx % 4294967277);
  String x_to_s = String(idx);
  
  if (IsPrime(x) == 1) {
    tft.clearScreen();
    tft.setCursor(0, 5);
    tft.setTextColor(GREEN);  
    tft.setTextSize(2);
    tft.println(x_to_s);
  } else {
    tft.clearScreen();
    tft.setCursor(0, 5);
    tft.setTextColor(RED);  
    tft.setTextSize(2);
    tft.println(x_to_s);
    tft.setTextSize(1);
    tft.println("Is not PRIME.");
  }
}

int IsPrime(unsigned long number) {
    long i;
    for ( i=2; i<number; i++ ) {
        if (number % i == 0 && i != number) return 0;
    }
    return 1;
}

