#include <stdint.h>
#include <SparkFunBME280.h>
//Library allows either I2C or SPI, so include both.
#include <Wire.h>

//Global sensor object
BME280 bme280;

void setup() {
    bme280.settings.commInterface = I2C_MODE;
    bme280.settings.I2CAddress = 0x76;
    
    //renMode can be:
    //  0, Sleep mode
    //  1 or 2, Forced mode
    //  3, Normal mode
    bme280.settings.runMode = 3; //Normal mode
    
    //tStandby can be:
    //  0, 0.5ms
    //  1, 62.5ms
    //  2, 125ms
    //  3, 250ms
    //  4, 500ms
    //  5, 1000ms
    //  6, 10ms
    //  7, 20ms
    bme280.settings.tStandby = 0;
    
    //filter can be off or number of FIR coefficients to use:
    //  0, filter off
    //  1, coefficients = 2
    //  2, coefficients = 4
    //  3, coefficients = 8
    //  4, coefficients = 16
    bme280.settings.filter = 0;
    
    //tempOverSample can be:
    //  0, skipped
    //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
    bme280.settings.tempOverSample = 1;

    //pressOverSample can be:
    //  0, skipped
    //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
    bme280.settings.pressOverSample = 1;
    
    //humidOverSample can be:
    //  0, skipped
    //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
    bme280.settings.humidOverSample = 1;
    
  Serial.begin(9600);
    Serial.print("Program Started\n");
    Serial.print("Starting BME280... result of .begin(): 0x");
    
    //Calling .begin() causes the settings to be loaded
    delay(10);  //Make sure sensor had enough time to turn on. BME280 requires 2ms to start up.
    Serial.println(bme280.begin(), HEX);
}

void loop() {
    //Each loop, take a reading.
    //Start with temperature, as that data is needed for accurate compensation.
    //Reading the temperature updates the compensators of the other functions
    //in the background.

    Serial.print("Temperature: ");
    Serial.print(bme280.readTempC(), 2);
    Serial.println(" degrees C");

    Serial.print("Temperature: ");
    Serial.print(bme280.readTempF(), 2);
    Serial.println(" degrees F");

    Serial.print("Pressure: ");
    Serial.print(bme280.readFloatPressure(), 2);
    Serial.println(" Pa");

    Serial.print("Altitude: ");
    Serial.print(bme280.readFloatAltitudeMeters(), 2);
    Serial.println(" m");

    Serial.print("Altitude: ");
    Serial.print(bme280.readFloatAltitudeFeet(), 2);
    Serial.println(" ft");    

    Serial.print("Humidity: ");
    Serial.print(bme280.readFloatHumidity(), 2);
    Serial.println(" %");
    
    Serial.println();
    
    delay(9000);
}
