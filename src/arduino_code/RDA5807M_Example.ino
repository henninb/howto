/*
* RDA5807M Example Sketch
*
* This example sketch illustrates how to use some of the basic commands in the
* RDA5807M Library. The sketch will start the RDA5807M in West-FM mode
* (87-108MHz) and then wait for user commands via the serial port.
* More information on the RDA5807M chip can be found in the datasheet.
*
* HARDWARE SETUP:
* This sketch assumes you are using the RRD-102 (V2.0) breakout board.
*
* The board should be connected to a 3.3V Arduino's I2C interface.
* Alternatively, a 5V Arduino can be used if a proper I2C level translator is
* used (see the README for an example).
* You will need an audio amplifier as the RDA5807M is too weak to directly drive
* a pair of headphones. Immediate candidates would be a pair of active
* (multimedia) speakers that you're probably already using with your computer.
* You will also need a proper antenna connected to breakout board. Luckily
* for you, this is a very forgiving chip when it comes to "proper" antennas,
* so for FM only you will be able to get away with just a 2ft (60cm) length of
* wire connected to the FM antenna pad on the shield. Decent results can
* probably be obtained using a 6" breadboard jumper wire too.
*/

//Due to a bug in Arduino, this needs to be included here first
#include <Wire.h>
//#include <radio.h>
#include <RDA5807M.h>

//Create an instance of the RDA5807M named radio
RDA5807M radio;
char command;
word status;
word frequency;

void setup() {
  Serial.begin(9600);

  //Initialize the radio to the West-FM band. (see RDA5807M_BAND_* constants).
  //radio.begin(RDA5807M_BAND_WEST);
  radio.init();
}

void loop() {
  frequency = radio.getFrequency();
  Serial.print(frequency);
  Serial.print(F("Currently tuned to "));
  Serial.print(frequency / 100);
  Serial.print(".");
  Serial.print(frequency % 100);
  Serial.println(F("MHz FM"));
  Serial.flush();
  radio.setFrequency(151);  //102.1
  delay(5000);
}
