#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

//D1 - 5 -  SCL for the wemos
//D2 - 4 -  SDA for the wemos
//A5 - 19 -  SCL for the Arduino Nano and Pro Mini
//A4 - 18 -  SDA for the Arduino Nano and Pro Mini

// Set the LCD address to 0x27 for a 16 chars and 2 line display

//#define I2C_PORT 0x27
#define I2C_PORT 0x3f

LiquidCrystal_I2C lcd(I2C_PORT, 16, 2);

unsigned long idx = 0;

void setup()
{
  int port = scan_port();
  String port_info = String(port);
  port_info.concat(" i2c port.");
  lcd.begin();

  for( int i = 0; i < 3; i++ ) {
    lcd.backlight();
    delay(200);
    lcd.noBacklight();
    delay(200);
  }
  lcd.backlight(); // finish with backlight on  
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(port_info);
  delay(2000);

  // to 0 - 4,294,967,295 (2^32 - 1).   - 4294967295 
  //String x = String(sizeof(unsigned long));
}

void loop()
{
  idx++;
  idx = (idx % 4294967277);
  String counter = String(idx);
  
  if( IsPrime(idx) == 1 ) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(counter);
    lcd.setCursor(0, 1);
    lcd.print("is prime");
    lcd.blink();
    
    Serial.println(counter);
    delay(2000);
  } else {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(counter);
    lcd.setCursor(0, 1);
    lcd.print("is NOT prime");
    lcd.noBlink();

    Serial.println(counter);
    delay(500);
  }
}

int IsPrime(unsigned long number) {
    long i;
    for ( i=2; i<number; i++ ) {
        if (number % i == 0 && i != number) {
          return 0;
        }
    }
    return 1;
}

int scan_port() {
  Serial.begin (9600);

  // Leonardo: wait for serial port to connect
  while (!Serial)
    {
    }

  Serial.println ("I2C scanner. Scanning ...");

  Wire.begin();
  for (byte i = 1; i < 127; i++) {
    Wire.beginTransmission (i);
    if (Wire.endTransmission () == 0) {
      Serial.print ("Found address: ");
      Serial.print (i, DEC);
      Serial.print (" (0x");
      Serial.print (i, HEX);
      Serial.println (")");
      return i;
      delay(1);
      }
  }
  return 0;
}
