#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN            10

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      16

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_RGB + NEO_KHZ800);

int delayval = 75 * 4; // delay

void setup() {
  pixels.begin(); // This initializes the NeoPixel library.
  pixels.clear();
}

void loop() {
    pixels.setPixelColor(0, pixels.Color(66, 100, 244));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(6, pixels.Color(66, 100, 244));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(10, pixels.Color(66, 100, 244));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(12, pixels.Color(66, 100, 244));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(1, pixels.Color(66, 244, 66));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(5, pixels.Color(66, 244, 66));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(11, pixels.Color(66, 244, 66));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(2, pixels.Color(241, 244, 65));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(4, pixels.Color(241, 244, 65));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(7, pixels.Color(166, 65, 244));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(9, pixels.Color(166, 65, 244));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(13, pixels.Color(166, 65, 244));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(8, pixels.Color(244, 65, 157));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(14, pixels.Color(244, 65, 157));
    pixels.show();
    delay(delayval);

    pixels.clear();

    pixels.setPixelColor(15, pixels.Color(66, 100, 244));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(9, pixels.Color(66, 100, 244));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(5, pixels.Color(66, 100, 244));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(3, pixels.Color(66, 100, 244));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(14, pixels.Color(66, 244, 66));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(10, pixels.Color(66, 244, 66));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(4, pixels.Color(66, 244, 66));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(13, pixels.Color(241, 244, 65));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(11, pixels.Color(241, 244, 65));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(8, pixels.Color(166, 65, 244));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(6, pixels.Color(166, 65, 244));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(2, pixels.Color(166, 65, 244));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(7, pixels.Color(244, 65, 157));
    pixels.show();
    delay(delayval);

    pixels.setPixelColor(1, pixels.Color(244, 65, 157));
    pixels.show();
    delay(delayval);
    
    pixels.clear();

//pixels.setPixelColor(idx_i, pixels.Color(66, 100, 244));
//pixels.setPixelColor(idx_i, pixels.Color(66, 244, 66));
//pixels.setPixelColor(idx_i, pixels.Color(241, 244, 65));
//pixels.setPixelColor(idx_i, pixels.Color(166, 65, 244));
//pixels.setPixelColor(idx_i, pixels.Color(244, 65, 157));

    for(int idx_i = 0; idx_i < NUMPIXELS; idx_i++ ) {  
      pixels.setPixelColor(idx_i, pixels.Color(66, 100, 244));
      pixels.show();
      delay(delayval);
    }
    pixels.clear();

    for(int idx_i = 0; idx_i < NUMPIXELS; idx_i++ ) {  
      pixels.setPixelColor(idx_i, pixels.Color(66, 244, 66));
      pixels.show();
      delay(delayval);
    }
    pixels.clear();

    for(int idx_i = 0; idx_i < NUMPIXELS; idx_i++ ) {  
      pixels.setPixelColor(idx_i, pixels.Color(241, 244, 65));
      pixels.show();
      delay(delayval);
    }
    pixels.clear();

    for(int idx_i = 0; idx_i < NUMPIXELS; idx_i++ ) {  
      pixels.setPixelColor(idx_i, pixels.Color(166, 65, 244));
      pixels.show();
      delay(delayval);
    }
    pixels.clear();

    for(int idx_i = 0; idx_i < NUMPIXELS; idx_i++ ) {  
      pixels.setPixelColor(idx_i, pixels.Color(244, 65, 157));
      pixels.show();
      delay(delayval);
    }
    pixels.clear();
}
