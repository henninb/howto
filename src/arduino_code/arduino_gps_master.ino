#include <TinyGPS++.h>
//#include <SoftwareSerial.h>
#include <AltSoftSerial.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

//#define I2C_PORT 0x27
#define I2C_PORT 0x3f

//gy-neo6mv2

#define ARDUINO_RX_PIN 8
#define ARDUINO_TX_PIN 9

//D1 - 5 -  SCL for the wemos
//D2 - 4 -  SDA for the wemos
//A5 - 19 -  SCL for the Arduino Nano
//A4 - 18 -  SDA for the Arduino Nano
//A5 - SCL for the Arduino Pro Mini
//A4 - SDA for the Arduino Pro Mini

//AltSoftSerial:Setup info: begin
//AltSoftSerial:USE TIMER1
//AltSoftSerial:PIN: 8 RX
//AltSoftSerial:PIN: 9 TX
//AltSoftSerial:PIN:10 (unused PWM)
//AltSoftSerial:Setup info: end


//GPS_Vcc > Arduino 5 V (Red)
//GPS_Ground > BreadBoard_Ground > 10k oHm  > GPS_TX_PIN
//10k oHm > 4.7K oHm >  Pin 8 (ARDUINO_RX_PIN)
//-BreadBoard_Ground > Arduino_Ground (Black)
//-GPS_RX_PIN > Pin 9 (ARDUINO_TX_PIN)

AltSoftSerial ss;
//SoftwareSerial ss(ARDUINO_RX_PIN, ARDUINO_TX_PIN); 
TinyGPSPlus gps;

//LiquidCrystal_I2C lcd(0x27, 16, 2); //39
//LiquidCrystal_I2C lcd1(0x3f, 16, 2);  //63
LiquidCrystal_I2C lcd(I2C_PORT, 16, 2);

void setup()
{
  Serial.begin(9600);
  ss.begin(9600);
  Serial.println("GPS Start...");
  int port = scan_port();
  String port_info = String(port);
  port_info.concat(" i2c port.");
  lcd.begin();
  lcd.noBacklight();  
  lcd.backlight(); // finish with backlight on  
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(port_info);
  smartDelay(1000);
  lcd.clear();
}

void loop()
{
  String latitude;
  String longitude;
  while( ss.available() ) {
    //This feeds the serial NMEA data into the library one char at a time
    gps.encode(ss.read());
  }

  Serial.println("Attempting...");
  smartDelay(1000);
  if( gps.location.isUpdated() ) {
    latitude = "Lat: " + String(gps.location.lat(), 6);
    longitude = "Lng: " + String(gps.location.lng(), 6);
    Serial.print("Satellite Count: ");
    Serial.println(gps.satellites.value());
    Serial.print("Latitude: ");
    Serial.println(gps.location.lat(), 6);
    Serial.print("Longitude: ");
    Serial.println(gps.location.lng(), 6);
    Serial.print("Speed MPH: ");
    Serial.println(gps.speed.mph());
    //Serial.print("Altitude Feet:");
    //Serial.println(gps.altitude.feet());
    Serial.println("*****");

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(latitude);
    lcd.setCursor(0, 1);
    lcd.print(longitude);
    
    if (gps.date.isValid()) {
      Serial.print("Date: ");
      Serial.print(gps.date.month());
      Serial.print("/");
      Serial.print(gps.date.day());
      Serial.print("/");
      Serial.println(gps.date.year());
    }

    if (gps.time.isValid()) {
      Serial.print("Time: ");
      if (gps.time.hour() < 10) Serial.print("0");
      Serial.print(gps.time.hour());
      Serial.print(":");
      if (gps.time.minute() < 10) Serial.print("0");
      Serial.print(gps.time.minute());
      Serial.print(":");
      if (gps.time.second() < 10) Serial.print("0");
      Serial.print(gps.time.second());
    }

    Serial.println("");
  }
}

static void smartDelay(unsigned long ms)
{
  unsigned long start = millis();
  do 
  {
    while (ss.available()) {
      gps.encode(ss.read());
    }
  } while (millis() - start < ms);
}

int scan_port() {
  Wire.begin();
  for( byte idx_i = 1; idx_i < 127; idx_i++ ) {
    Wire.beginTransmission(idx_i);
    if( Wire.endTransmission () == 0 ) {
      return idx_i;
      delay(1);
      }
  }
  return 0;
}
