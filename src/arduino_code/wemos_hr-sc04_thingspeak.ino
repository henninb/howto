#include <ESP8266WiFi.h>
#include <NewPing.h>
#include <TimeLib.h>
#include <WiFiUdp.h>
#include <PubSubClient.h>
#include <aJSON.h>

//GPIO0 and GPIO2 are used for ECHO and TRIG on the ESP-01
//for Wemos deepSleep -- connect D0 to the reset
//Reprogram as the ESP is sleeping, disconnect D0 - RST and try again
//D1 - 05 for SCL (i2c)
//D2 - 04 for SDA (i2c)
//#define PIN_SCL 5
//#define PIN_SDA 4

#define  TRIGGER_PIN  12 //D6 12 for wemos
#define  ECHO_PIN     13 //D7 13 for wemos
#define WIFI_DELAY 300
#define USE_SERIAL_PRINT_FLAG 1
#define USE_DEEP_SLEEP_FLAG 1
#define USE_SENSOR_FLAG 1
#define UDP_LOCAL_PORT 2390
#define MQTT_PORT 1883
#define NTP_PACKET_SIZE 48
#define ESP_DEEP_SLEEP 600  //10 minutes
//#define ESP_DEEP_SLEEP 10 //10 seconds
#define MAX_DISTANCE 400 // Maximum distance we want to ping for (in centimeters). (Maximum distance is rated at 400-500cm)

const char* ssid = "";
const char* password = "";
const char* thingspeak_host = "api.thingspeak.com";
const char* sparkfun_host = "data.sparkfun.com";
const char* mqttServerName = "192.168.100.134";
const char* ntpServerName = "time.nist.gov";

//mosquitto_sub -d -t home/lower_bathroom/temperature

String sparkfun_publicKey = "";
String sparkfun_privateKey = "";

String mqtt_channel = "default";
String THINGSPEAK_API_KEY_DISTANCE = ""; //Write API Key
String THINGSPEAK_API_KEY = String(THINGSPEAK_API_KEY_DISTANCE);

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

void setup() {
  //Wemos Pins, need to test
  //pinMode(D0, OUTPUT);  //Save Power by writing all Digital IO LOW
  //pinMode(D1, OUTPUT);  //Save Power by writing all Digital IO LOW
  //pinMode(D2, OUTPUT);  //Save Power by writing all Digital IO LOW
  //pinMode(D3, OUTPUT);  //Save Power by writing all Digital IO LOW
  //pinMode(D4, OUTPUT);  //Save Power by writing all Digital IO LOW
  //pinMode(D5, OUTPUT);  //Save Power by writing all Digital IO LOW
  //pinMode(D6, OUTPUT);  //Save Power by writing all Digital IO LOW
  //pinMode(D7, OUTPUT);  //Save Power by writing all Digital IO LOW
  //pinMode(D8, OUTPUT);  //Save Power by writing all Digital IO LOW
#ifdef USE_DEEP_SLEEP_FLAG
  pinMode(D0, WAKEUP_PULLUP);
#endif

#ifdef USE_SERIAL_PRINT_FLAG
  Serial.begin(115200);
#endif

#ifdef USE_SERIAL_PRINT_FLAG
  Serial.println("\nINFO: ESP8266 Connecting to " + String(ssid));
#endif
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  
  while( WiFi.status() != WL_CONNECTED ) {
    delay(WIFI_DELAY);
#ifdef USE_SERIAL_PRINT_FLAG
    Serial.print(".");
#endif
  }

  while (WiFi.localIP() == INADDR_NONE) {
    delay(300);
  }
  
#ifdef USE_SERIAL_PRINT_FLAG
  Serial.println("\nWiFi connected");  
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
#endif
}

void callback( char* topic, byte* payload, unsigned int length ) {
  Serial.print("Received message for topic ");
  Serial.print(topic);
  Serial.print("with length ");
  Serial.println(length);
  Serial.println("Message:");
  Serial.write(payload, length);
  Serial.println();
}

aJsonObject *createJson( String inches, String centimeters ) {
  aJsonObject *msg = aJson.createObject();
  
  aJson.addItemToObject(msg, "inches", aJson.createItem(inches));
  aJson.addItemToObject(msg, "centimeters", aJson.createItem(centimeters));

  return msg;
}

void publishMQTT(const char *hostname, String inches, String centimeters, String location) {
  WiFiClient client1;
  aJsonObject *json;
  char *jsonStr;
  String json_str;
  PubSubClient client(hostname, MQTT_PORT, callback, client1);

  json = createJson(inches, centimeters);
  //jsonStr = aJson.print(json);
  json_str = "{\"inches\":" + inches + ",\"centimeters\":" + centimeters + "}";
  
  // Reconnect if the connection was lost
  if (!client.connected()) {
    Serial.println("WARN: publishMQTT() disconnected, reconnecting to publish.");
    if (!client.connect(mqtt_channel.c_str())) {
      Serial.println("WARN: publishMQTT() connection failed");
    }
  }

  if (client.publish(mqtt_channel.c_str(), json_str.c_str())) {
  //if (client.publish(mqtt_channel.c_str(), jsonStr)) {
    Serial.println("INFO: publishMQTT() Publish success");
  } else {
    Serial.println("WARN: publishMQTT() publish failed");
  }
}

void postThingspeakMetrics(const char *hostname, String api_key, String inches, String centimeters) {
  WiFiClient client1;
  String url;
  unsigned long timeout;
  String line;

  if( !client1.connect(hostname, 80) ) {
#ifdef USE_SERIAL_PRINT_FLAG
    Serial.println("WARN: postThingspeakMetrics() - connection failed.");
#endif
    return;
  }

  url = "/update?api_key=";
  url += api_key;
  url += "&field1=";
  url += String(inches);
  url += "&field2=";
  url += String(centimeters);

  client1.print(String("GET ") + url + " HTTP/1.1\r\n" + "Host: " + hostname + "\r\n" +   "Connection: close\r\n\r\n");
  delay(10);

  timeout = millis();
  while ( client1.available() == 0 ) {
    if (millis() - timeout > 5000) {
#ifdef USE_SERIAL_PRINT_FLAG
      Serial.println("WARN: postThingspeakMetrics() - client timeout.");
#endif
      client1.stop();
      return;
    }
  }
  
  // Read all the lines of the reply from server and print them to Serial
  while( client1.available() ) {
    line = client1.readStringUntil('\r');
  }
#ifdef USE_SERIAL_PRINT_FLAG
    Serial.print("INFO: postThingspeakMetrics() - server responded successfully.");
#endif
}

void loop() {
  long inches = 0;
  long centimeters = 0;

  inches = sonar.ping_in();
  centimeters = sonar.ping_cm();
  
#ifdef USE_SERIAL_PRINT_FLAG
  Serial.println("INFO: Inches: " + String(inches) + "\t" + "Centimeters: " + String(centimeters));
  Serial.println("INFO: host: " + String(thingspeak_host));
#endif

if( inches == 0 ) {
#ifdef USE_SERIAL_PRINT_FLAG
  Serial.println("INFO: zero inches.");
#endif
  postThingspeakMetrics(thingspeak_host, THINGSPEAK_API_KEY, String(inches), String(centimeters));
} else {
  postThingspeakMetrics(thingspeak_host, THINGSPEAK_API_KEY, String(inches), String(centimeters));
}
#ifdef USE_SERIAL_PRINT_FLAG  
    Serial.println("\nINFO: postThingspeakMetrics() - closing connection.");
#endif

#ifdef USE_DEEP_SLEEP_FLAG
  ESP.deepSleep(ESP_DEEP_SLEEP * 1000000);
#endif
}

unsigned long fetch_epoch() {
  int cb = 0;
  unsigned long highWord;
  unsigned long lowWord;
  unsigned long secsSince1900;
  unsigned long epoch;
  unsigned long seventyYears;
  
  udp.begin(UDP_LOCAL_PORT);
  
  WiFi.hostByName(ntpServerName, timeServerIP); 

  do {
    sendNTPpacket(timeServerIP);
    delay(4000);
  } while ((cb = udp.parsePacket()) == 0 );

  udp.read(packetBuffer, NTP_PACKET_SIZE);

  highWord = word(packetBuffer[40], packetBuffer[41]);
  lowWord = word(packetBuffer[42], packetBuffer[43]);
  secsSince1900 = highWord << 16 | lowWord;
  seventyYears = 2208988800UL;
  epoch = secsSince1900 - seventyYears;
  return epoch;
}

unsigned long sendNTPpacket(IPAddress& address) {
  //Serial.println("sending NTP packet...");
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}
