#include <Wire.h>
#include <ds3231.h>
#include <RTClib.h>
#include <TimeLib.h>
#include <DS1302RTC.h>

#define DS1307_ADDRESS 0x68
//#define DS1307_ADDRESS 0x57

RTC_DS1307 RTC;

//#define TIME_MSG_LEN  11   // time sync to PC is HEADER followed by unix time_t as ten ascii digits
//#define TIME_HEADER  255   // Header tag for serial time sync message
//#define BUFF_MAX 128

//RTC_DS1307 RTC;

//uint8_t time[8];
//char recv[BUFF_MAX];
//unsigned int recv_size = 0;
//unsigned long prev, interval = 5000;
//struct ts t;
//RTC_DS1307 RTC;

void setup() {
  Serial.begin(9600);
  scan_port();
  printDate();
  delay(1000);
  Serial.println("was the previous time that was set.");
  Wire.begin();
  RTC.begin();

  if (! RTC.isrunning()) {
    Serial.println("RTC is NOT running!");
  }
// following line sets the RTC to the date & time this sketch was compiled
// uncomment it & upload to set the time, date and start run the RTC!
   RTC.adjust(DateTime(__DATE__, __TIME__));
  //setDateTime();
}

//void setup1()
//{
//    Serial.begin(9600);
//    Wire.begin();
//    DS3231_init(DS3231_INTCN);
//    memset(recv, 0, BUFF_MAX);
//    Serial.println("GET time");

   // t.year = 2016;
   // t.mday = 17;
   // t.mon = 6;
   // t.hour = 17;
   // t.min = 8;
    
    //DS3231_set(t);

//setTime(hr,min,sec,day,month,yr);
//RTC.set(now());
    
//}

void loop() {
  printDate();
  delay(1000);
}

byte decToBcd(byte val){
// Convert normal decimal numbers to binary coded decimal
  return ( (val/10*16) + (val%10) );
}

byte bcdToDec(byte val)  {
// Convert binary coded decimal to normal decimal numbers
  return ( (val/16*10) + (val%16) );
}

void setDateTime(){
  byte zero = 0x00; //workaround for issue #527
  byte second =      00; //0-59
  byte minute =      56; //0-59
  byte hour =        9; //0-23
  byte weekDay =     1; //1-7
  byte monthDay =    24; //1-31
  byte month =       7; //1-12
  byte year  =       16; //0-99

  Wire.beginTransmission(DS1307_ADDRESS);
  Wire.write(zero); //stop Oscillator
  Wire.write(decToBcd(second));
  Wire.write(decToBcd(minute));
  Wire.write(decToBcd(hour));
  Wire.write(decToBcd(weekDay));
  Wire.write(decToBcd(monthDay));
  Wire.write(decToBcd(month));
  Wire.write(decToBcd(year));
  Wire.write(zero); //start 
  Wire.endTransmission();
}

void printDate(){
  byte zero = 0x00; //workaround for issue #527
  // Reset the register pointer
  Wire.beginTransmission(DS1307_ADDRESS);
  Wire.write(zero);
  Wire.endTransmission();

  Wire.requestFrom(DS1307_ADDRESS, 7);

  int second = bcdToDec(Wire.read());
  int minute = bcdToDec(Wire.read());
  int hour = bcdToDec(Wire.read() & 0b111111); //24 hour time
  int weekDay = bcdToDec(Wire.read()); //0-6 -> sunday - Saturday
  int monthDay = bcdToDec(Wire.read());
  int month = bcdToDec(Wire.read());
  int year = bcdToDec(Wire.read());

  //print the date EG   3/1/11 23:59:59
  Serial.print("date - ");
  Serial.print(month);
  Serial.print("/");
  Serial.print(monthDay);
  Serial.print("/");
  Serial.print(year);
  Serial.print(" ");
  Serial.print(hour);
  Serial.print(":");
  Serial.print(minute);
  Serial.print(":");
  Serial.println(second);
}

int scan_port() {
  Serial.begin (9600);

  // Leonardo: wait for serial port to connect
  while (!Serial)
    {
    }

  Serial.println ();
  Serial.println ("I2C scanner. Scanning ...");
  byte count = 0;
 
  Wire.begin();
  for (byte i = 1; i < 120; i++)
  {
    Wire.beginTransmission (i);
    if (Wire.endTransmission () == 0) {
      Serial.print ("Found address: ");
      Serial.print (i, DEC);
      Serial.print (" (0x");
      Serial.print (i, HEX);
      Serial.println (")");
      return i;
      count++;
      delay (1);  // maybe unneeded?
      } // end of good response
  } // end of for loop
  Serial.println ("Done.");
  Serial.print ("Found ");
  Serial.print (count, DEC);
  Serial.println (" device(s).");
  return 0;
}
