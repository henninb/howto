10K ohm resistor should be used on the i2c bus unless the breakout board has a (pull up resistor)

An I2C interface MUST have pull-up resistors. The value of those depend on the voltage and the speed you plan to run at.
The Raspberry-Pi already has pull-up resistor on the board so you don't have to add any more.
That is if you connect to the Pi! 

But!! If you ever make your own I2C bus you must add pull-up resistors.
e.g. from the Atmega on the Gertboard to a I2c device other then the Pi.

Note that some controllers have internal pull-up resistors on the I/O pins which you can program to be on.
Those are not suited to be used as I2C pull up resistors as they are often too weak.


For many reasons (like noise resistance) it is wise not to leave any pin floated. For another reasons (like current limiting in case of unintended shortcuts or in/out circuit failure) it is wise to use pull-up/down resistors. In order to bypass unstable conditions at power-up it is good to add a capacitor to RST.

It may be that your circuit without any of the above works correct, but the probability of such event in noisy environment is rather low :) Please read here about problems with even simple relay switching... - See more at: http://www.esp8266.com/viewtopic.php?f=13&t=2533#sthash.W0sWshrO.dpuf
