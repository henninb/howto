package example.processor;

import example.pojo.Message;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class MessageProcessor implements Processor {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Override
    public void process(Exchange exchange) throws Exception {
        //String messageType = (String) exchange.getIn().getHeader("messageType");
        //Customer response = exchange.getOut().getBody(Customer.class);
        Message message = exchange.getIn().getBody(Message.class);
        LOGGER.info("queueName=" + message.getQueueName());
        exchange.getIn().setHeader("queueName", message.getQueueName());
        LOGGER.info("sequenceNumber=" + message.getSequenceNumber());
        exchange.getIn().setHeader("sequenceNumber", message.getSequenceNumber());
        LOGGER.info("message=" + message.getQueueMessage());
        //exchange.getProperty("endpoint1.body");
        exchange.setProperty("queueName", message.getQueueName());
        exchange.getIn().setBody(message.getQueueMessage());
    }
}
