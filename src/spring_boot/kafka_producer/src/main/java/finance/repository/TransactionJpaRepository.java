package finance.repository;

import finance.model.Transaction;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionJpaRepository  extends JpaRepository<Transaction, Long> {
}
