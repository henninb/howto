package finance.controller;

import finance.components.KafkaProducer;
import finance.model.Transaction;
import finance.repository.TransactionJpaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//@Controller
@RestController
@RequestMapping("/transaction")
public class TransactionController {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TransactionJpaRepository transactionJpaRepository;

    //http://localhost:8080/transaction/all
    @GetMapping(value = "/all")
    public List<Transaction> findAll() {

        Transaction transaction = new Transaction();

        transaction.setAccountNameOwner("chase_brian");
        transaction.setAccountType("credit");
        transaction.setAmount("10.0");
        transaction.setCategory("restaurant");
        transaction.setDescription("Subway");
        transaction.setNotes("super secret");
        transaction.setDateAdded("8/18/2017");
        transactionJpaRepository.saveAndFlush(transaction);

        return transactionJpaRepository.findAll();
    }

    //http://localhost:8080/transaction/kafka
    @GetMapping(value = "/kafka")
    public void callKafka() {
        KafkaProducer kafkaProducer = new KafkaProducer();
        kafkaProducer.send("test", "my message.");
    }
}
