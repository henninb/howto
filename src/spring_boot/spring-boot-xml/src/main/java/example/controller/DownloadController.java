package example.controller;

import example.services.XmlService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@RestController
public class DownloadController {

    @Autowired
    XmlService xmlService;

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(path = "/download", method = {RequestMethod.GET})
    private String download(String param) throws IOException {

        return "{download}";
    }

    @RequestMapping(value = "/upload", method={RequestMethod.POST,RequestMethod.GET})
    @ResponseBody
    private String upload(@RequestParam("xml") String xml) throws IOException {
        xmlService.readXml(xml);

        return "{upload}, xml=" + xml;
        //return new ResponseEntity("Successfully uploaded", new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/uploadXml", method = {RequestMethod.GET})
    private String uploadXml(String parm) throws SOAPException, IOException, JAXBException, ParserConfigurationException {

        xmlService.readSoap();

        return "<html>\n" +
                "<head><title>Upload Xml</title></head>\n" +
                "<body>\n" +
                "  <h2 align=\"left\">Upload Xml</h2>\n" +
                "  <form method=\"post\" action=\"/upload\">\n" +
                "    <textarea name=\"xml\" rows=\"20\" cols=\"30\"><SimpleBean><x>1</x><y>2</y></SimpleBean></textarea><br />\n" +
                "    <input type=\"submit\" value=\"Send\" />\n" +
                "    <input type=\"reset\" value=\"Clear\" />\n" +
                "  </form>\n" +
                "</body>\n" +
                "</html>";
    }
}
