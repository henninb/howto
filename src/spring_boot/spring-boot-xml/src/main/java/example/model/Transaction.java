package example.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.sql.Date;




@Entity
//@Entity(name="TransactionEntity")
@Table(name = "t_transaction")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //@GeneratedValue(strategy= GenerationType.TABLE)
    //@Column(name="transaction_id", columnDefinition = "char")
    private int transactionId;
    //@GeneratedValue(strategy= GenerationType.TABLE)
    private String guid;

    //@ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    //@JoinColumn(name = "DBPARENTPLANOGRAMKEY", referencedColumnName="DBKEY", insertable = false, updatable = false)

    private Integer accountId;
    private String accountType;
    private String accountNameOwner;
    private java.sql.Date transactionDate;
    private String description;
    private String category;
    private double amount;
    private int isCleared;
    private String notes;
    private java.sql.Date dateUpdated;
    private java.sql.Date dateAdded;

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountNameOwner() {
        return accountNameOwner;
    }

    public void setAccountNameOwner(String accountNameOwner) {
        this.accountNameOwner = accountNameOwner;
    }

    public java.sql.Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(java.sql.Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getIsCleared() {
        return isCleared;
    }

    public void setIsCleared(int isCleared) {
        this.isCleared = isCleared;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public java.sql.Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(java.sql.Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }


    public java.sql.Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(java.sql.Date dateAdded) {
        this.dateAdded = dateAdded;
    }
}
