package example.pojo;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ErrorHeader")
@XmlType(name = "", propOrder = {"exceptionNumber", "timeStamp", "serverName"})
public class ErrorHeader {

    @XmlElement(name = "ExceptionNumber", required = true)
    private String exceptionNumber;
    @XmlElement(name = "TimeStamp", required = true)
    private String timeStamp;
    @XmlElement(name = "ServerName", required = true)
    private String serverName;

    public String getExceptionNumber() {
        return exceptionNumber;
    }

    public void setExceptionNumber(String value) {
        this.exceptionNumber = value;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String value) {
        this.timeStamp = value;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String value) {
        this.serverName = value;
    }
}