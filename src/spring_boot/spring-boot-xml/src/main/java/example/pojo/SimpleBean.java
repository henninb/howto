package example.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

//<SimpleBean><x>1</x><y>2</y></SimpleBean>

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonXmlRootElement(localName = "SimpleBean")
public class SimpleBean {
    @JacksonXmlProperty(localName = "x")
    private int x = 1;

    //@JacksonXmlText
    //@JacksonXmlProperty(isAttribute = true)
    @JacksonXmlProperty(localName = "y")
    private int y = 2;
        //standard setters and getters

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
