package example.pojo;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ErrorMessage")
@XmlType(name = "", propOrder = {"errorHeader", "errorDetails"})
public class ErrorMessage {

    @XmlElement(name = "ErrorHeader", required = true)
    private ErrorHeader errorHeader;
    @XmlElement(name = "ErrorDetails", required = true)
    private String errorDetails;

    public ErrorHeader getErrorHeader() {
        return errorHeader;
    }

    public void setErrorHeader(ErrorHeader value) {
        this.errorHeader = value;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(String value) {
        this.errorDetails = value;
    }
}
