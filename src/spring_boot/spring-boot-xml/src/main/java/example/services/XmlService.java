package example.services;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import example.pojo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
public class XmlService {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    XmlMapper xmlMapper = new XmlMapper();

    public void readXml(String xml) throws IOException {
        SimpleBean value = xmlMapper.readValue(xml, SimpleBean.class);
        System.out.println("X = " + value.getX());
        System.out.println("Y = " + value.getY());
    }


    public void readSoap() throws SOAPException, IOException, JAXBException, ParserConfigurationException {
        ErrorHeader errorHeader = new ErrorHeader();
        errorHeader.setExceptionNumber("1");
        errorHeader.setServerName("server");
        errorHeader.setTimeStamp("4/2/2018");
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setErrorDetails("details");
        errorMessage.setErrorHeader(errorHeader);

        String xml = "";
        JAXBContext context = JAXBContext.newInstance(ErrorMessage.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(errorMessage, System.out);
        //marshaller.marshal(user, new File("user.xml"));

        TestClass testClass = new TestClass();
        testClass.setX(5);
        testClass.setY(6);

        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        Marshaller marshaller1 = JAXBContext.newInstance(TestClass.class).createMarshaller();
        marshaller1.marshal(testClass, document);
        SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
        soapMessage.getSOAPBody().addDocument(document);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        soapMessage.writeTo(outputStream);
        String output = new String(outputStream.toByteArray());
        System.out.println(output);

        //DatatypeFactory datatypes = DatatypeFactory.newInstance();
        //booking.setCourseDate(datatypes.newXMLGregorianCalendarDate(2006,06,15,0));

        String soapEnvelope =
                "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header/><SOAP-ENV:Body><testClass><x>7</x><y>8</y></testClass></SOAP-ENV:Body></SOAP-ENV:Envelope>";

        SOAPMessage message = MessageFactory.newInstance().createMessage(null,  new ByteArrayInputStream(soapEnvelope.getBytes()));
        Unmarshaller unmarshaller = JAXBContext.newInstance(TestClass.class).createUnmarshaller();
        testClass = (TestClass) unmarshaller.unmarshal(message.getSOAPBody().extractContentAsDocument());
        System.out.println(testClass.getX());
        System.out.println(testClass.getY());
    }
}
