package example.services;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

@Component
public class ConsumerService {
    @JmsListener(destination = "DSH.SORTER.INPUT")
    //public void receiveQueue(String message) {
    public void receiveQueue(TextMessage message) {
        System.out.println("INFO: consumer text is <" + message.toString() + ">");
    }
}
