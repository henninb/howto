package example.service

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.ibm.msg.client.jms.DetailedInvalidDestinationException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jms.connection.CachingConnectionFactory
import org.springframework.stereotype.Service

import javax.jms.*
import java.util.HashMap

@Service
class QueueDepthService {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    internal var cachingConnectionFactory: CachingConnectionFactory? = null

    @Throws(JsonProcessingException::class, JMSException::class)
    fun getQueueDepths(queueNameList: List<String>): String {
        var counter: Int? = 0
        val session: Session

        try {
            val queueConnection = cachingConnectionFactory!!.createQueueConnection()
            LOGGER.info("cachingConnectionFactory.createQueueConnection()")
            session = queueConnection.createSession(false, Session.AUTO_ACKNOWLEDGE)
            LOGGER.info("queueConnection.createSession(false, Session.AUTO_ACKNOWLEDGE)")
        } catch (e: JMSException) {
            LOGGER.info(e.message)
            return "[]"
        }

        val map = HashMap<String, Int>()

        LOGGER.info("new QueueDepthMessageList()")

        for (queueName in queueNameList) {
            LOGGER.info("queueDepthMessage.setQueueName(queueName)")
            try {
                val queue = session.createQueue(queueName)
                LOGGER.info("session.createQueue(queueName)")
                val queueBrowser = session.createBrowser(queue)
                LOGGER.info("session.createBrowser(queue)")
                val enumeration = queueBrowser.enumeration
                LOGGER.info("queueBrowser.getEnumeration()")
                counter = 0
                while (enumeration.hasMoreElements()) {
                    enumeration.nextElement()
                    counter += 1
                }
                queueBrowser.close()
                LOGGER.info("queueBrowser.close()")
            } catch (ide: DetailedInvalidDestinationException) {
                LOGGER.info(ide.explanation)
                LOGGER.info(ide.toString())
                counter = -1
            } catch (e: Exception) {
                LOGGER.info(e.message)
                LOGGER.info(e.toString())
            }

            map[queueName] = counter!!
            LOGGER.info("$queueName count= $counter")
        }

        session.close()
        LOGGER.info("session.close()")
        return objectMapper.writeValueAsString(map)
    }

    companion object {
        private val objectMapper = ObjectMapper()
    }
}
