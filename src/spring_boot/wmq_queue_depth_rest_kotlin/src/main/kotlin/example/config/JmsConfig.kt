package example.config


import com.ibm.mq.jms.MQQueueConnectionFactory
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.jms.connection.CachingConnectionFactory
import org.springframework.jms.connection.JmsTransactionManager
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement
import com.ibm.msg.client.wmq.WMQConstants

@Configuration
@EnableTransactionManagement
open class JmsConfig {
    @Value("\${project.mq.host}")
    private val host: String? = null
    @Value("\${project.mq.port}")
    private val port: Int? = null
    @Value("\${project.mq.queue-manager}")
    private val queueManager: String? = null
    @Value("\${project.mq.channel}")
    private val channel: String? = null
    @Value("\${project.mq.receive-timeout}")
    private val receiveTimeout: Long = 0

    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    @Bean
    open fun mqQueueConnectionFactory(): MQQueueConnectionFactory {
        val mqQueueConnectionFactory = MQQueueConnectionFactory()
        mqQueueConnectionFactory.hostName = host
        try {
            mqQueueConnectionFactory.transportType = WMQConstants.WMQ_CM_CLIENT
            mqQueueConnectionFactory.ccsid = 1208
            mqQueueConnectionFactory.channel = channel
            mqQueueConnectionFactory.port = port!!
            mqQueueConnectionFactory.queueManager = queueManager
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return mqQueueConnectionFactory
    }

    @Bean
    @Primary
    open fun cachingConnectionFactory(mqQueueConnectionFactory: MQQueueConnectionFactory): CachingConnectionFactory {
        val cachingConnectionFactory = CachingConnectionFactory()
        cachingConnectionFactory.targetConnectionFactory = mqQueueConnectionFactory
        cachingConnectionFactory.sessionCacheSize = 20
        cachingConnectionFactory.setReconnectOnException(true)
        return cachingConnectionFactory
    }

    @Bean
    open fun jmsTransactionManager(cachingConnectionFactory: CachingConnectionFactory): PlatformTransactionManager {
        val jmsTransactionManager = JmsTransactionManager()
        jmsTransactionManager.connectionFactory = cachingConnectionFactory
        return jmsTransactionManager
    }
}
