package example.controller

import com.fasterxml.jackson.core.JsonProcessingException
import example.service.QueueDepthService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.jms.JMSException

@RestController
class QueueDepthController {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    internal var queueDepthService: QueueDepthService? = null

    @Value("#{'\${project.mq.queue.list}'.split(',')}")
    private val queueNameList: List<String>? = null

    //http://localhost:8080/queue_list_count
    @RequestMapping("/queue_list_count")
    @Throws(JsonProcessingException::class, JMSException::class)
    fun queue_list_count(): String {
        LOGGER.info("" + "")
        return queueDepthService!!.getQueueDepths(queueNameList!!)
    }
}
