#!/bin/sh

if [ $# -ne 0 ]; then
    echo "Usage: $0 <noargs>"
    exit 1
fi

find . \( -name "*.java" -o -name "build.gradle" -o -name "*.kt" -o -name "*.groovy" -o -name "*.sh" -o -name "*.md" -o -name "pom.xml" \) -print | xargs sed -i 's/\r//g' > /dev/null 2>&1
find . \( -name "*.java" -o -name "build.gradle" -o -name "*.kt" -o -name "*.groovy" -o -name "*.sh" -o -name "*.md" -o -name "pom.xml" \) -print | xargs sed -i 's/\t/    /g' > /dev/null 2>&1
#find . \( -name "*.java" -o -name "build.gradle" -o -name "*.kt" -o -name "*.groovy" -o -name "*.sh" -o -name "*.md" \) -print | xargs sed -i 's/ +$//' > /dev/null 2>&1
find . \( -name "*.java" -o -name "build.gradle" -o -name "*.kt" -o -name "*.groovy" -o -name "*.sh" -o -name "*.md" -o -name "pom.xml" \) -print | xargs sed -i 's/\s*$//g' > /dev/null 2>&1

exit 0
