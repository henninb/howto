CREATE TABLE t_account (
  account_id INTEGER NOT NULL,
  account_name_owner CHAR(40) NOT NULL,
  account_type CHAR(10) NOT NULL,
  active_status CHAR(1) NOT NULL,
  moniker CHAR(5)
);
