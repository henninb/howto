gradle assemble
gradle tasks
gradle -q projects

test.onlyIf { ! Boolean.getBoolean('skip.tests') }
Run the build with -Dskip.tests=true and all test tasks will be skipped.