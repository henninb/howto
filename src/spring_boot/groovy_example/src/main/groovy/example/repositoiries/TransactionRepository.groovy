package example.repositoiries

import example.model.Transaction
import org.springframework.data.jpa.repository.JpaRepository

interface TransactionRepository extends JpaRepository<Transaction, Long> {
}
