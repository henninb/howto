package example.model

import ch.qos.logback.classic.db.names.ColumnName

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "t_transaction")
class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="transaction_id")
    Long transactionId
    String accountType
    String accountNameOwner
    String transactionDate
    String description
    String category
    String amount
    boolean isCleared
    String notes
    String dateUpdated
    String dateAdded

    Long getTransactionId() {
        return transactionId
    }

    void setTransactionId(Long transactionId) {
        this.transactionId = transactionId
    }

    String getAccountType() {
        return accountType
    }

    void setAccountType(String accountType) {
        this.accountType = accountType
    }

    String getAccountNameOwner() {
        return accountNameOwner
    }

    void setAccountNameOwner(String accountNameOwner) {
        this.accountNameOwner = accountNameOwner
    }

    String getTransactionDate() {
        return transactionDate
    }

    void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate
    }

    String getDescription() {
        return description
    }

    void setDescription(String description) {
        this.description = description
    }

    String getCategory() {
        return category
    }

    void setCategory(String category) {
        this.category = category
    }

    String getAmount() {
        return amount
    }

    void setAmount(String amount) {
        this.amount = amount
    }

    boolean getIsCleared() {
        return isCleared
    }

    void setIsCleared(boolean isCleared) {
        this.isCleared = isCleared
    }

    String getNotes() {
        return notes
    }

    void setNotes(String notes) {
        this.notes = notes
    }

    String getDateUpdated() {
        return dateUpdated
    }

    void setDateUpdated(String dateUpdated) {
        this.dateUpdated = dateUpdated
    }

    String getDateAdded() {
        return dateAdded
    }

    void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded
    }
}
