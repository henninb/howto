package example.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "t_account")
class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="account_id")
    Long accountId
    String name
    String owner
    String accountType
    String activeStatus
    String moniker
    String dateCloseed
    String dateUpdated
    String dateAdded

    Long getAccountId() {
        return accountId
    }

    void setAccountId(Long accountId) {
        this.accountId = accountId
    }

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }

    String getOwner() {
        return owner
    }

    void setOwner(String owner) {
        this.owner = owner
    }

    String getAccountType() {
        return accountType
    }

    void setAccountType(String accountType) {
        this.accountType = accountType
    }

    String getActiveStatus() {
        return activeStatus
    }

    void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus
    }

    String getMoniker() {
        return moniker
    }

    void setMoniker(String moniker) {
        this.moniker = moniker
    }

    String getDateCloseed() {
        return dateCloseed
    }

    void setDateCloseed(String dateCloseed) {
        this.dateCloseed = dateCloseed
    }

    String getDateUpdated() {
        return dateUpdated
    }

    void setDateUpdated(String dateUpdated) {
        this.dateUpdated = dateUpdated
    }

    String getDateAdded() {
        return dateAdded
    }

    void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded
    }
}
