package example.controller

import example.model.Transaction
import example.repositoiries.TransactionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/transactions")
class TransactionController {
    @Autowired
    TransactionRepository transactionRepository;

    //INSERT INTO t_transaction(account_type, account_name_owner, transaction_date, description, category, amount, is_cleared, notes, date_updated, date_added) VALUES('credit', 'usbankcash_brian', '8/2/2017', 'Logitech Keyboard', 'electronics', '29.99', false, '', '8/2/2017', '8/2/2017');
    //INSERT INTO t_transaction(account_type, account_name_owner, transaction_date, description, category, amount, is_cleared, notes, date_updated, date_added) VALUES('credit', 'usbankcash_brian', '8/2/2017', 'Mario Kart', 'toys', '49.99', false, '', '8/2/2017', '8/2/2017');
    //INSERT INTO t_transaction(account_type, account_name_owner, transaction_date, description, category, amount, is_cleared, notes, date_updated, date_added) VALUES('credit', 'usbankcash_brian', '8/2/2017', 'Barbie Doll', 'toys', '9.99', false, '', '8/2/2017', '8/2/2017');

    @GetMapping(value = "/all")
    public List<Transaction> findAll() {
        return transactionRepository.findAll()
    }
}
