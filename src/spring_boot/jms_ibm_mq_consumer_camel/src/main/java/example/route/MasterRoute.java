package example.route;

import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.io.File;

@Component
public class MasterRoute extends RouteBuilder {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    //@Autowired
    //JsonTransactionProcessor jsonTransactionProcessor;

    //@Autowired
    //InsertTransactionProcessor insertTransactionProcessor;

    @Override
    public void configure() throws Exception {
        onException(UnrecognizedPropertyException.class)
                .handled(false)
                .log("UnrecognizedPropertyException")
                //.to("log:noconnect")
                .logExhaustedMessageBody(true)
                .end();

        onException( org.springframework.jms.IllegalStateException.class)
                .handled(true)
                .maximumRedeliveries(3)
                .log("IllegalStateException")
                .to("file:in" + File.separator + ".failedFiles")
                .end();

        //read from queue and write to file
        from("wmq:queue:queue_backout")
                .autoStartup(true)
                .log("INFO read from queue")
                .to("file:in" + File.separator + ".processedFiles")
                .log("$simple{file:onlyname} processed")
                .end();

        //write from queue and write to file
        from("file:in?delete=true&moveFailed=.failed")
                .autoStartup(true)
                .log("INFO: write to queue then to file")
                .to("wmq:queue:queue_name1")
                .log("wmq completed.")
                .to("file:in" + File.separator + ".processedFiles")
                .log("$simple{file:onlyname} processed")
                .to("activemq:queue:queue_name2")
                .log("activemq completed.")
                .end();
    }
}
