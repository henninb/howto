package example.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "example")
public class SimpleMongoConfig {
/*
    @Bean
    public Mongo mongo() throws Exception {
        System.out.println("config - localhost");
        return new MongoClient("localhost");
    }

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        System.out.println("config -test");
        return new MongoTemplate(mongo(), "test");
    }
*/
}
