package example.services;

import javax.jms.JMSException;
import javax.jms.Queue;

import example.util.Tool;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
//public class Producer implements CommandLineRunner {
public class ProducerService {

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Autowired
    private Queue queue;

    @Bean
    public Queue queue() throws JMSException {
        //need to fix
        System.out.println("INFO: test_queue is the queue name");
        Queue queue = new ActiveMQQueue("test_queue");
        Tool tool = new Tool();
        tool.tool();
        //queue.send("Sample message");
        //String s = queue.getQueueName();
        return queue;
    }

    public void send(String msg) {
        //System.out.println("in here");
        this.jmsMessagingTemplate.convertAndSend(this.queue, msg);
    }
}
