package example.services;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

@Component
public class ConsumerService {
    @JmsListener(destination = "test_queue")
    //public void receiveQueue(String message) {
    public void receiveQueue(TextMessage message) {
        System.out.println("INFO: consumer text is <" + message.toString() + ">");
    }

    //@JmsListener(destination = "test_queue")
    //public void processOrder(Message message) throws JMSException {
    //    String convertedMessage = ((TextMessage) message).getText();
    //    System.out.println("INFO: consumer text is <" + convertedMessage + ">");
    //}
}
