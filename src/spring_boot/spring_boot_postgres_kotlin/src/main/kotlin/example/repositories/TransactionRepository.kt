package example.repositories

import example.model.Summary
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import example.model.Transaction
import org.springframework.transaction.annotation.Transactional

//import org.springframework.test.web.servlet.result.MockMvcResultMatchers.model

interface TransactionRepository : JpaRepository<Transaction, Long> {

    //@NamedNativeQuery(name="showSummaryResults", query="SELECT  account_name_owner, SUM(amount) AS totals FROM t_transaction GROUP BY account_name_owner ORDER BY account_name_owner", resultSetMapping="showSummaryResults")
    @get:Query(value = "SELECT  account_name_owner as accountNameOwner, SUM(amount) AS totals FROM t_transaction GROUP BY account_name_owner ORDER BY account_name_owner", nativeQuery = true)
    val summary: List<Summary>

    //function name must match name on database table
    fun findByDescriptionIgnoreCase(description: String): Transaction

    fun findByTransactionId(transactionId: Long?): Transaction

    //public List<Transaction> findByAccountNameOwner(String accountNameOwner);
    //public List<Transaction> findByAccountNameOwnerIgnoreCase(String accountNameOwner);
    fun findByAccountNameOwnerIgnoreCaseOrderByTransactionDate(accountNameOwner: String): List<Transaction>

    fun findByGuid(guid: String): Transaction

    // Using SpEL expression
    @Query("SELECT SUM(amount) FROM #{#entityName} WHERE isCleared = 1 AND accountNameOwner=?1")
    //public double find(@Param("accountNameOwner") String accountNameOwner);
    fun fetchAccoutTotals(accountNameOwner: String): Double

    // Using SpEL expression
    @Query("SELECT SUM(amount) AS accountTotal FROM #{#entityName} WHERE isCleared=1 AND accountNameOwner=?1")
    //public double find(@Param("accountNameOwner") String accountNameOwner);
    fun fetchAccoutClearedTotals(accountNameOwner: String): Double

    @Modifying
    @Transactional
    @Query(value = "DELETE from t_transaction WHERE guid = ?1", nativeQuery = true)
    fun deleteByGuid(guid: String)

    //@NamedNativeQuery(name="summaryResults", query="SELECT accountNameOwner, SUM(amount) AS totals FROM #{#entityName} GROUP BY accountNameOwner ORDER BY accountNameOwner", resultSetMapping="summaryResults")
    //public List<Summary> fetchSummary();

    // Using SpEL expressions
    //@Query("SELECT accountNameOwner, SUM(amount) AS totals FROM #{#entityName} GROUP BY accountNameOwner ORDER BY accountNameOwner")
    //public List<Summary> fetchSummary();

    //@NamedQuery(name="findWhatever", query="SELECT new path.to.dto.MyDto(e.id, e.otherProperty) FROM Student e WHERE e.id = ?1")
}