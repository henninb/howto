package example.repositories


import example.model.Summary

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

//import org.springframework.test.web.servlet.result.MockMvcResultMatchers.model

interface SummaryRepository : JpaRepository<Summary, Long> {

    // Using SpEL expression
    @Query(value = "FROM SummaryEntity WHERE guid = :guid ORDER BY accountNameOwner")
    fun fetchSummary1(@Param("guid") guid: String): List<Summary>

    @Query(value = "SELECT guid FROM t_summary ORDER BY date_added DESC LIMIT 1", nativeQuery = true)
    fun fetchGuid(): String

    @Query(value = "SELECT * FROM t_summary WHERE guid = :guid ORDER BY account_name_owner", nativeQuery = true)
    fun fetchSummary(@Param("guid") guid: String): List<Summary>

    // Using SpEL expression
    //should only return 1 value
    @Query(value = "FROM SummaryEntity WHERE guid = :guid AND accountNameOwner = :accountNameOwner ORDER BY accountNameOwner")
    fun fetchSummaryByAccountNameOwner(@Param("guid") guid: String, @Param("accountNameOwner") accountNameOwner: String): List<Summary>
}
