package example.controller

import example.model.Account
import example.model.AccountTotal
import example.model.Summary
import example.repositories.SummaryRepository
import example.model.Transaction
import example.service.AccountService
import example.service.TransactionService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation .Autowired
import org.springframework.web.bind.annotation .*

//https://stackoverflow.com/questions/43533691/required-a-bean-of-type-org-hibernate-sessionfactory-that-could-not-be-found

@CrossOrigin(origins = arrayOf("http://localhost:3000"))
@RestController
@RequestMapping("/transactions")
//@EnableTransactionManagement
//@RequestMapping("/transactions", method=RequestMethod.GET)
class TransactionController {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    internal var transactionService: TransactionService? = null

    @Autowired
    private val accountService: AccountService? = null

    @Autowired
    private val summaryRepository: SummaryRepository? = null

    //http://localhost:8080/transactions/getSummary
    //return summaryRepository.findAll();
    val summary: List<Summary>
        @GetMapping(value = "/getSummary")
        get() {
            val guid = summaryRepository!!.fetchGuid()
            return summaryRepository.fetchSummary(guid)
        }

    //insert into t_transaction(account_type, account_name_owner, transaction_date, description, category, amount, is_cleared, notes, date_updated, date_added) VALUES('credit', 'usbankcash_brian', '8/1/2017', 'Mario Kart', 'toys', '49.99', false, '', '8/1/2017', '8/1/2017')
    //http://localhost:8080/transactions/transactionFindAll
    @GetMapping(value = "/transactionFindAll")
    fun transactionFindAll(): List<Transaction> {
        return transactionService!!.findAll()
    }

    //http://localhost:8080/transactions/getTransaction/340c315d-39ad-4a02-a294-84a74c1c7ddc
    @GetMapping(value = "/getTransaction/{guid}")
    fun getTransaction(@PathVariable guid: String): Transaction {
        val transaction: Transaction
        LOGGER.info(guid)
        //Transaction transaction = transactionRepository.findByGuid(guid);
        transaction = transactionService!!.findByGuid(guid)
        return transaction
    }

    //http://localhost:8080/transactions/fetchAccoutTotals/chase_kari
    @GetMapping(value = "/fetchAccoutTotals/{accountNameOwner}")
    fun fetchAccoutTotals(@PathVariable accountNameOwner: String): AccountTotal {
        val results = ""
        var totals: Double? = 0.0
        val accountTotal = AccountTotal()
        totals = transactionService!!.fetchAccoutTotals(accountNameOwner)
        accountTotal.accountTotal = totals
        return accountTotal
    }

    //http://localhost:8080/transactions/deleteTransaction/340c315d-39ad-4a02-a294-84a74c1c7ddc
    @GetMapping(value = "/deleteTransaction/{guid}")
    fun deleteTransaction(@PathVariable guid: String): String {
        LOGGER.info(guid)
        try {
            val transaction = transactionService!!.findByGuid(guid)
            LOGGER.info(transaction.description)
            LOGGER.info(transaction.accountNameOwner)
            LOGGER.info(transaction.guid)
            LOGGER.info("transactions.contains(transaction) is found.")

            transactionService!!.deleteByGuid(guid)
        } catch (ex: Exception) {
            println(ex)
            return "delete failure."
        } finally {
            return guid
        }
    }

    //http://localhost:8080/transactions/getActiveAccounts
    @GetMapping(value = "/findActiveAccounts")
    fun findActiveAccounts(): List<Account> {
        return accountService!!.findActiveAccounts()
    }

    //http://localhost:8080/transactions/getAccounts
    @GetMapping(value = "/accountfindAll")
    fun accountfindAll(): List<Account> {
        return accountService!!.findAll()
    }

    //http://localhost:8080/transactions/getTransactionsByAccountNameOwner/chase_kari
    @GetMapping(value = "/getTransactionsByAccountNameOwner/{accountNameOwner}")
    fun getTransactionsByAccountNameOwner(@PathVariable accountNameOwner: String): List<Transaction> {
        //return transactionRepository.findByAccountNameOwner(accountNameOwner);
        //return transactionRepository.findByAccountNameOwnerIgnoreCase(accountNameOwner);
        return transactionService!!.findByAccountNameOwnerIgnoreCaseOrderByTransactionDate(accountNameOwner)
    }

    //http://localhost:8080/transactions/getSummaryByAccountNameOwner/chase_kari
    @GetMapping(value = "/getSummaryByAccountNameOwner/{accountNameOwner}")
    fun getSummaryByAccountNameOwner(@PathVariable accountNameOwner: String): List<Summary> {
        val guid = summaryRepository!!.fetchGuid()
        return summaryRepository.fetchSummaryByAccountNameOwner(guid, accountNameOwner)
    }
}
