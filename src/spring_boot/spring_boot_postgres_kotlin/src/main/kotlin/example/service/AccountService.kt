package example.service

import example.model.Account
import example.repositories.AccountRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AccountService {

    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    internal var accountRepository: AccountRepository? = null

    fun findActiveAccounts(): List<Account> {
        return accountRepository!!.findActiveAccounts()
    }

    fun findAll(): List<Account> {
        return accountRepository!!.findAll()
    }
}
