package example.service

import example.model.Transaction
import example.repositories.TransactionRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.transaction.Transactional
import java.util.ArrayList
import java.util.function.Consumer

/*
 Annotation | Meaning                                             |
 +------------+-----------------------------------------------------+
 | @Component | generic stereotype for any Spring-managed component |
 | @Repository| stereotype for persistence layer                    |
 | @Service   | stereotype for service layer                        |
 | @Controller| stereotype for presentation layer (spring-mvc)      |
 +------------+-----------------------------------------------------+
*/
@Service
open class TransactionService {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    internal var transactionRepository: TransactionRepository? = null

    fun findAll(): List<Transaction> {
        val transactions = ArrayList<Transaction>()
        this.transactionRepository!!.findAll().forEach(Consumer<Transaction> { transactions.add(it) }) //fun with Java 8
        return transactions
    }

    fun fetchAccoutTotals(accountNameOwner: String): Double {
        return transactionRepository!!.fetchAccoutTotals(accountNameOwner)
    }

    fun deleteByGuid(guid: String) {
        try {
            transactionRepository!!.deleteByGuid(guid)
        } catch (ex: Exception) {
            println(ex)
        }

    }

    fun findByGuid(guid: String): Transaction {
        return transactionRepository!!.findByGuid(guid)
    }

    fun findByAccountNameOwnerIgnoreCaseOrderByTransactionDate(accountNameOwner: String): List<Transaction> {
        return transactionRepository!!.findByAccountNameOwnerIgnoreCaseOrderByTransactionDate(accountNameOwner)
    }

    @Transactional
    fun save(transaction: Transaction): Transaction {
        return transactionRepository!!.save(transaction)
    }
}
