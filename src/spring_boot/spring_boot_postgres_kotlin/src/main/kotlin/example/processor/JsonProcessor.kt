package example.processor

import com.fasterxml.jackson.databind.ObjectMapper
import com.google.common.hash.Hashing
import example.model.ExcelTransaction
import example.model.Transaction
import example.repositories.TransactionRepository
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.orm.jpa.JpaSystemException
import org.springframework.stereotype.Component

import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.sql.Date
import java.text.*
import java.util.Arrays

@Component
class JsonProcessor : Processor {
    //private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private var preString: String? = null
    private var digest: MessageDigest? = null

    @Autowired
    private val transactionRepository: TransactionRepository? = null

    //@Autowired
    //private AccountRepository accountRepository;

    @Throws(Exception::class)
    override fun process(exchange: Exchange) {
        var bodyStr = exchange.`in`.getBody(String::class.java)

        bodyStr = bodyStr.replace("\\n".toRegex(), "")
        bodyStr = bodyStr.replace("\\r".toRegex(), "")
        bodyStr = bodyStr.replace("\u0000".toRegex(), "")

        //byte[] body = bodyStr.getBytes();

        //List<Transaction> transactionList = mapper.readValue(exchange.getIn().getBody(String.class), new TypeReference<List<Transaction>>() { });
        //TypeReference<HashMap<ExcelTransaction,String>> typeRef = new TypeReference<HashMap<ExcelTransaction, String>>() {};

        //POJO = Plain Old Java Object
        try {
            val excelTransactions = mapper.readValue(bodyStr, Array<ExcelTransaction>::class.java)
            val transactionList = Arrays.asList(*excelTransactions)
            digest = MessageDigest.getInstance("SHA-256")

            //accounts.
            println("size: " + transactionList.size)

            for (t in transactionList) {
                var transaction: Transaction? = null
                if (t.guid != null)
                    transaction = transactionRepository!!.findByGuid(t.guid.toString())
                else
                    transaction = transactionRepository!!.findByGuid("")

                if (transaction == null) {
                    transaction = Transaction()
                    transaction.guid = t.guid
                    //System.out.println("we have a problem with nulls.");
                }

                transaction.transactionDate = toEpoch(t.transactionDate)
                transaction.description = t.description
                transaction.category = t.category
                transaction.amount = java.lang.Double.parseDouble(t.amount!!)
                transaction.notes = t.notes
                transaction.cleared = Integer.parseInt(t.cleared!!)
                transaction.dateAdded = toEpoch(t.dateAdded)
                transaction.dateUpdated = toEpoch(t.dateUpdated)
                transaction.accountType = t.accountType
                transaction.accountNameOwner = t.accountNameOwner
                transaction.sha256 = t.sha256
                println(t.sha256)

                preString = cleanField(t.transactionDate!!.toString()) + "||" + cleanField(t.description!!.toString()) + "||" + cleanField(t.category!!.toString()) + "||" + toFormatedDouble(cleanField(t.amount!!.toString())) + "||" + t.cleared + "||" + cleanField(t.notes!!.toString() + "||" + t.accountNameOwner)
                println("INFO:<$preString>")
                val sha256hex = Hashing.sha256().hashString(preString!!, StandardCharsets.UTF_8).toString()
                println("sha256hex:$sha256hex")
                try {
                    transactionRepository.save(transaction)
                } catch (pae: JpaSystemException) {
                    pae.printStackTrace()
                    println("WARN: issue with account_id with primative type")
                    //continue;
                } catch (ex: DataIntegrityViolationException) {
                    ex.printStackTrace()
                    println("WARN: duplicate key.")
                    //continue;
                }

            }

        } catch (e: Exception) {
            e.printStackTrace()
            exchange.setException(e)
        }

        //exchange.getIn().setBody(transactionList);
    }

    private fun toEpoch(epochString: String?): Date {

        try {
            val epoch = java.lang.Long.parseLong(epochString!!)
            return Date(epoch * 1000)
        } catch (ex: Exception) {
            ex.stackTrace
        }

        return Date(0L)
        //return new SimpleDateFormat("YYYY-MM-dd-HH:mm:ss").format(new Date(0"0"));
    }

    private fun toFormatedDouble(field: String): String {
        val numberFormat = DecimalFormat("#0.00")
        return numberFormat.format(java.lang.Double.parseDouble(field))
    }

    private fun cleanField(field: String): String {
        return field.replace("^\\s+".toRegex(), "").replace("\\s+$".toRegex(), "")
    }

    companion object {
        private val mapper = ObjectMapper()
    }
}
