package example.route

import example.processor.JsonProcessor
import org.apache.camel.builder.RouteBuilder
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.io.File

@Component
class FileConsumerRoute : RouteBuilder() {

    @Autowired
    internal var jsonProcessor: JsonProcessor? = null
    val x: Int = 1
    //http://camel.apache.org/file-language.html
    @Throws(Exception::class)
    override fun configure() {
        //works
        //from("file:" + File.separator + "camel" + File.separator + "input?fileName=file.txt").log("got here").multicast().to("file:" + File.separator + "camel" + File.separator + "output?fileName=output.txt").end();

        from("file:" + File.separator + "usr" + File.separator + "finance_data" + File.separator + "json_in?delete=true&moveFailed=.failedWithErrors")
                .choice()
                .`when`(header("CamelFileName").endsWith(".json")).log("\$simple{file:onlyname.noext}_\$simple{date:now:yyyyMMdd}.json").process(jsonProcessor).to("file:C:\\usr\\finance_data\\json_out")
                .`when`(header("CamelFileName").endsWith(".txt")).to("file:" + File.separator + "usr" + File.separator + "finance_data" + File.separator + "json_in" + File.separator + ".notProcessed")
                //.when(header("CamelFileName").endsWith(".txt")).log("$simple{file:onlyname.noext}_$simple{date:now:yyyyMMdd}.txt").to("file:C:\\usr\\finance_data\\json_in\\.notProcessed?output?fileName=file.out.txt")
                .otherwise().to("file:" + File.separator + "usr" + File.separator + "finance_data" + File.separator + "json_in" + File.separator + ".notProcessed")
                .end()

        //works
        //from("file:C:\\camel\\input?recursive=true&delete=true").to("file:C:\\camel\\output").end();

        //works
        //from("file:C:\\camel\\input?recursive=true&delete=true").transform(method("fetchGuid", "fetchGuid")).to("file:C:\\camel\\output").end();
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(FileConsumerRoute::class.java)
    }
}
