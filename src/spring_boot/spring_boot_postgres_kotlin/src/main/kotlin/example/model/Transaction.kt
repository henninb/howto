package example.model

import javax.persistence.*

@Entity(name = "TransactionEntity")
@Table(name = "t_transaction")
class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var transactionId: Int = 0
    var guid: String? = null
    var accountId: Int? = null
    var accountType: String? = null
    var accountNameOwner: String? = null
    var transactionDate: java.sql.Date? = null
    var description: String? = null
    var category: String? = null
    var amount: Double = 0.toDouble()
    @Column(name = "cleared")
    var cleared: Int = 0
    var notes: String? = null
    var dateUpdated: java.sql.Date? = null
    var dateAdded: java.sql.Date? = null
    var sha256: String? = null
}



