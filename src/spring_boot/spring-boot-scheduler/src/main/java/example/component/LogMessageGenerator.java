package example.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class LogMessageGenerator {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Scheduled(fixedDelay = 1000L)
    void logSomeStuff(){
        LOGGER.info("Log message generated");
    }
}
