package example.service;

import example.model.Transaction;
import example.repository.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionService {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    TransactionRepository transactionRepository;

    public void setRepositiory( TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public List<Transaction> findAll() {
        return transactionRepository.findAll();
    }

    public void insertTransaction(Transaction transaction) {
        Transaction result = transactionRepository.saveAndFlush(transaction);
        if( transaction.getGuid().equals(result.getGuid()) ) {
            LOGGER.info("INFO: transactionRepository.saveAndFlush success.");
        } else {
            LOGGER.info("WARN: transactionRepository.saveAndFlush failure.");
        }
    }

    public void deleteAll() {
        transactionRepository.deleteAll();
    }

    public Transaction findByGuid( String guid) {
        Transaction transaction = transactionRepository.findByGuid(guid);
        return transaction;
    }

    public void deleteByGuid( String guid) {
        transactionRepository.delete(findByGuid(guid));
    }
}
