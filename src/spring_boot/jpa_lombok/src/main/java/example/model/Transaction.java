package example.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import example.util.TransactionDeserializer;
import example.util.TransactionSerializer;
import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.text.DecimalFormat;

@Entity
@Builder
@AllArgsConstructor //goes with builder constructor
@Getter
@Setter
@Table(name = "TransactionEntity")
@JsonDeserialize(using = TransactionDeserializer.class)
@JsonSerialize(using = TransactionSerializer.class)
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="transaction_id")
    private Long transactionId;
    private String guid;
    private String accountType;
    private String accountNameOwner;
    private Date transactionDate;
    private String description;
    @Column(name="category")
    private String category;
    private Double amount;
    @Column(name="cleared")
    private int cleared;
    @Column(name="notes")
    private String notes;
    private Date dateUpdated;
    private Date dateAdded;
    private String sha256;

    @JsonIgnore
    @Transient  //persistance ignore
    private String extra;

    public Transaction() {
    }

    public Transaction(String guid, String accountType, String accountNameOwner, Date transactionDate, String description, String category, Double amount, int cleared, String notes, Date dateUpdated, Date dateAdded, String sha256) {
        this.transactionId = transactionId;
        this.guid = guid;
        this.accountType = accountType;
        this.accountNameOwner = accountNameOwner;
        this.transactionDate = transactionDate;
        this.description = description;
        this.category = category;
        this.amount = amount;
        this.cleared = cleared;
        this.notes = notes;
        this.dateUpdated = dateUpdated;
        this.dateAdded = dateAdded;
        this.sha256 = sha256;
    }
/*
    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountNameOwner() {
        return accountNameOwner;
    }

    public void setAccountNameOwner(String accountNameOwner) {
        this.accountNameOwner = accountNameOwner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSha256() {
        return sha256;
    }

    public void setSha256(String sha256) {
        this.sha256 = sha256;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public int getCleared() {
        return cleared;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public void setCleared(int cleared) {
        this.cleared = cleared;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }
    */
}
