package example.processor;

import com.fasterxml.jackson.databind.ObjectMapper;
import example.model.Transaction;
import example.service.TransactionService;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.ResourceBundle;
import java.util.PropertyResourceBundle;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ComponentScan(basePackages = "example")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JsonTransactionProcessorTest {
    private ResourceBundle resourceBundle;
    private static ObjectMapper mapper = new ObjectMapper();

    //@Autowired
    @MockBean
    TransactionService transactionService;

    private Transaction transactionDummy;
    private Transaction[] transactionsDummy;

    @Before
    public void setup() {
        this.resourceBundle = PropertyResourceBundle.getBundle("test_inputs");
        MockitoAnnotations.initMocks(this);
        Mockito.doNothing().when(transactionService).insertTransaction(transactionDummy);
    }

    @Test
    public void testJsonToTransaction() throws Exception {
        String json_string = this.resourceBundle.getString("json_string");
        transactionDummy = mapper.readValue(json_string, Transaction.class);
        System.out.println(transactionDummy.getTransactionDate());
        assertEquals(transactionDummy.getGuid(), "0e0a01e2-06b0-4e3f-a15c-5afa86d968ce");
    }

    @Test
    public void testJsonToTransactionList() throws Exception {
        String json_string_array = this.resourceBundle.getString("json_string_array");
        transactionsDummy = mapper.readValue(json_string_array, Transaction[].class);
        assertEquals(transactionsDummy.length, 2);
        for (Transaction transaction : transactionsDummy) {
            transactionService.insertTransaction(transaction);
        }
        assert(true);
    }

}