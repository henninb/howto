package example.exception;

public class XPathNotFoundException extends Exception {
    // Parameterless Constructor
    public XPathNotFoundException() {}

    // Constructor that accepts a message
    public XPathNotFoundException(String message)
    {
        super(message);
    }
}
