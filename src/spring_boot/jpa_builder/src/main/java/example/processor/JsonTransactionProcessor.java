package example.processor;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import example.model.Transaction;
import example.service.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.logging.Logger;

@Component
@Slf4j
public class JsonTransactionProcessor implements Processor {
    //private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private static ObjectMapper mapper = new ObjectMapper();

    @Override
    public void process(Exchange exchange) throws Exception {
        Transaction[] transactions;

        try {
            String jsonString = exchange.getIn().getBody(String.class);
            log.info("");
            transactions = mapper.readValue(jsonString, Transaction[].class);
            exchange.getIn().setBody(transactions);
        }

        catch( UnrecognizedPropertyException upe) {
            System.out.println("UnrecognizedPropertyException");
            upe.printStackTrace();
            throw upe;
        }
        catch( IOException ioe ) {
            System.out.println("IOException");
            ioe.printStackTrace();
        }
        catch(Exception e) {
            System.out.println("UnrecognizedPropertyException");
            e.printStackTrace();
        }
    }
}
