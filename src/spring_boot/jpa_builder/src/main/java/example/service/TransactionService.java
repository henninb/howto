package example.service;

import example.model.Transaction;
import example.repositories.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TransactionService {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    TransactionRepository transactionRepository;

    public void setRepositiory( TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public List<Transaction> findAll() {
        List<Transaction> transactions = transactionRepository.findAll();
        if( transactions.isEmpty()) {
            //return new List<Transaction>();
            //TODO: return null?
        }
        return transactions;
    }

    public Boolean insertTransaction(Transaction transaction) {
        Optional<Transaction> optionalTransaction = transactionRepository.findByGuid(transaction.getGuid());
        if( optionalTransaction.isPresent() ) {
            transactionRepository.saveAndFlush(transaction);
            return true;
        }
        return false;
    }

    public Boolean deleteAll() {
        transactionRepository.deleteAll();
        return true;
    }

    public Transaction findByGuid( String guid ) {
        Optional<Transaction> optionalTransaction = transactionRepository.findByGuid(guid);
        if( optionalTransaction.isPresent() ) {
            return optionalTransaction.get();
        }
        return new Transaction();
    }

    public Boolean deleteByGuid( String guid ) {
        Optional<Transaction> optionalTransaction = transactionRepository.findByGuid(guid);
        if( optionalTransaction.isPresent() ) {
            transactionRepository.delete(optionalTransaction.get());
            return true;
        }
        return false;
    }
}
