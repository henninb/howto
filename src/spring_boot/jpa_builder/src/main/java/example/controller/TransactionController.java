package example.controller;

import example.model.Transaction;
import example.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/transaction")
public class TransactionController {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    TransactionService transactionService;

    //curl http://localhost:8080/transaction/all
    @GetMapping(value = "/all")
    public ResponseEntity<List<Transaction>> listTransactionsAll() {
        return ResponseEntity.ok(transactionService.findAll());
    }

    //http://localhost:8080/transaction/delete/0e0a01e2-06b0-4e3f-a15c-5afa86d968ce
    @DeleteMapping(value = "/delete/{guid}")
    public ResponseEntity<String> deleteTransactionByGuid(@PathVariable String guid) {
        if( transactionService.deleteByGuid(guid) ) {
            return ResponseEntity.ok("element deleted");
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    //http://localhost:8080/transaction/select/0e0a01e2-06b0-4e3f-a15c-5afa86d968ce
    @GetMapping(value = "/select/{guid}")
    public ResponseEntity<Transaction> listTransactionByGuid(@PathVariable String guid) {
        Transaction transaction = transactionService.findByGuid(guid);
        return ResponseEntity.ok(transaction);
    }
}
