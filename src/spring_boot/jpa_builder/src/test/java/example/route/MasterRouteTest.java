package example.route;

import example.model.Transaction;
import example.processor.JsonTransactionProcessor;
import org.apache.camel.*;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

//@Component
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
//@TestPropertySource(locations = "classpath:application_test.properties")
@RunWith(SpringRunner.class)
public class MasterRouteTest extends CamelTestSupport {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    //RouteBuilder routeBuilder = new MasterRoute();

    private ResourceBundle resourceBundle;

    @EndpointInject(uri = "mock:result")
    protected MockEndpoint resultEndpoint;

    @Produce(uri = "direct:start")
    protected ProducerTemplate template;


    @Autowired
    JsonTransactionProcessor jsonTransactionProcessor;

    @Override
    protected RouteBuilder createRouteBuilder() {
        return new RouteBuilder() {
            public void configure() {
                from("direct:start").log("starting")
                        .autoStartup(true)
                        .process(jsonTransactionProcessor)
                        .log("post processing")
                        .to("mock:result");
            }
        };
    }

    @Test
    public void testRoute() throws Exception {
        LOGGER.info("Entering test");
        this.resourceBundle = PropertyResourceBundle.getBundle("test_inputs");
        String json_string_array = this.resourceBundle.getString("json_string_array");
        System.out.println(json_string_array);
        resultEndpoint.expectedMessageCount(1);
        template.sendBody(json_string_array);
        resultEndpoint.assertIsSatisfied();

        Exchange exchange = resultEndpoint.getReceivedExchanges().get(0);
        Transaction[] x = (Transaction[]) exchange.getIn().getBody();
        assertEquals(x.length, 2);
    }
}