package example.service;

import example.TestTransactionData;
import example.model.Transaction;
import example.repository.TransactionRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import org.springframework.test.context.junit4.SpringRunner;

//import static org.junit.jupiter.api.Assertions.*;
import java.sql.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;


//@Service
//@ComponentScan(basePackages = "example")
//@TestPropertySource(locations = "classpath:application_test.properties")
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class TransactionServiceTest {
    @Mock
    TransactionRepository transactionRepository;

    @Autowired
    //@MockBean
    TransactionService transactionService;

    private Transaction transactionDummy;
    private List<Transaction> transactionsDummy;

    private String guid = "f7846484-7cdc-4ed1-b665-c5fb96e9aa64";

    @Before
    public void setup() {
        TestTransactionData testTransactionData = new TestTransactionData();
        transactionDummy = testTransactionData.setOneTransaction();
        transactionsDummy = testTransactionData.getTransactionList();
        transactionService.setRepositiory(transactionRepository);
        Mockito.when(transactionRepository.findAll()).thenReturn(transactionsDummy);
        Mockito.when(transactionRepository.findByGuid(guid)).thenReturn(transactionDummy);
        Mockito.when(transactionRepository.saveAndFlush(transactionDummy)).thenReturn(transactionDummy);
    }

    @Test
    public void testFindAll() {
        List<Transaction> transactions = transactionRepository.findAll();
        assertEquals(transactions.size(),3);
    }

    @Test
    public void testInsertTransaction() {
        transactionService.insertTransaction(transactionDummy);
        assert(true);
    }

    @Test
    public void testDeleteAllTransactions() {
        Mockito.doNothing().when(transactionRepository).deleteAll();
        transactionService.deleteAll();
        assert(true);
    }

    @Test
    public void testFindByGuid() {
        Transaction transaction =  transactionService.findByGuid(guid);
        assertEquals(transaction.getGuid(), guid);
    }

    @Test
    public void testUnfoundGuid() {
        Transaction transaction =  transactionService.findByGuid("notfound");
        assert(transaction == null);
    }
}