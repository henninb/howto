package example.service


import example.model.Transaction
import example.repository.TransactionRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TransactionService {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    //@Autowired
    //internal var transactionRepository: TransactionRepository

    @Autowired
    internal var transactionRepository: TransactionRepository? = null

    fun setRepositiory(transactionRepository: TransactionRepository) {
        this.transactionRepository = transactionRepository
    }

    fun findAll(): List<Transaction> {
        return transactionRepository!!.findAll()
    }

    fun insertTransaction(transaction: Transaction) {
        val result = transactionRepository!!.saveAndFlush(transaction)
        if (transaction.guid == result.guid) {
            LOGGER.info("INFO: transactionRepository.saveAndFlush success.")
        } else {
            LOGGER.info("WARN: transactionRepository.saveAndFlush failure.")
        }
    }

    fun deleteAll() {
        transactionRepository!!.deleteAll()
    }

    fun findByGuid(guid: String): Transaction {
        return transactionRepository!!.findByGuid(guid)
    }

    fun deleteByGuid(guid: String) {
        transactionRepository!!.delete(findByGuid(guid))
    }
}
