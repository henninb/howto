package example.controller


import example.model.Transaction
import example.service.TransactionService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/transaction")
class TransactionController {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    internal var transactionService: TransactionService? = null

    //http://localhost:8080/transaction/all
    //curl http://172.17.0.2:8080/transaction/all
    //curl http://localhost:8080/transaction/all
    @GetMapping(path = arrayOf("/all"))
    fun listTransactionsAll(): List<Transaction> {
        return transactionService!!.findAll()
    }

    //http://localhost:8080/transaction/deleteTransaction/0e0a01e2-06b0-4e3f-a15c-5afa86d968ce
    @DeleteMapping(path = arrayOf("/deleteTransaction/{guid}"))
    fun deleteTransactionByGuid(@PathVariable guid: String): String {
        LOGGER.info(guid)
        try {
            val transaction = transactionService!!.findByGuid(guid)
            //LOGGER.info(transaction.getDescription())
            //LOGGER.info(transaction.getAccountNameOwner())
            //LOGGER.info(transaction.getGuid())
            LOGGER.info("transactions.contains(transaction) is found.")

            transactionService!!.deleteByGuid(guid)
            return guid
        } catch (npe: NullPointerException) {
            //npe.printStackTrace();
            println("NullPointerException, delete failure.")
            return "delete failure."
        } catch (ex: Exception) {
            //System.out.println(ex);
            ex.printStackTrace()
            return "delete failure."
        }

    }

    //http://localhost:8080/transaction/getTransaction/0e0a01e2-06b0-4e3f-a15c-5afa86d968ce
    @GetMapping(path = arrayOf("/getTransaction/{guid}"))
    fun listTransactionByGuid(@PathVariable guid: String): String {
        val transaction = transactionService!!.findByGuid(guid)
        //LOGGER.info(guid);
        //transaction = transactionService.findByGuid(guid);
        return "{\"key\"=\"value\"}"
    }
}
