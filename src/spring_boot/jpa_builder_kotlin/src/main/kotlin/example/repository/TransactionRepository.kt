package example.repository

import example.model.Transaction
import org.springframework.data.jpa.repository.JpaRepository

interface TransactionRepository : JpaRepository<Transaction, Long> {
    //function name must match name on database table
    fun findByDescriptionIgnoreCase(description: String): Transaction

    fun findByTransactionId(transactionId: Int): Transaction

    fun findByAccountNameOwnerIgnoreCase(accountNameOwner: String): List<Transaction>
    fun findByAccountNameOwnerIgnoreCaseOrderByTransactionDate(accountNameOwner: String): List<Transaction>

    fun findByGuid(guid: String): Transaction
}
