package example.processor

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException
import example.model.Transaction
import jdk.nashorn.internal.runtime.regexp.joni.Config.log
import lombok.extern.slf4j.Slf4j
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.springframework.stereotype.Component
import java.io.IOException

@Component
//@Slf4j
class JsonTransactionProcessor : Processor {

    @Throws(Exception::class)
    override fun process(exchange: Exchange) {
        val transactions: Array<Transaction>

        try {
            val jsonString = exchange.`in`.getBody(String::class.java)

            transactions = mapper.readValue(jsonString, Array<Transaction>::class.java)
            exchange.`in`.body = transactions
        } catch (upe: UnrecognizedPropertyException) {
            println("UnrecognizedPropertyException")
            upe.printStackTrace()
            throw upe
        } catch (ioe: IOException) {
            println("IOException")
            ioe.printStackTrace()
        } catch (e: Exception) {
            println("UnrecognizedPropertyException")
            e.printStackTrace()
        }
    }

    companion object {
        private val mapper = ObjectMapper()
    }
}
