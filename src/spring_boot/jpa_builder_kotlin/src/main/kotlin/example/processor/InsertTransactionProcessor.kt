package example.processor

import com.fasterxml.jackson.databind.ObjectMapper
import example.model.Transaction
import example.service.TransactionService
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import java.io.ByteArrayOutputStream

@Component
class InsertTransactionProcessor : Processor {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)
    internal val jsonOutputBytes = ByteArrayOutputStream()

    @Autowired
    private val transactionService: TransactionService? = null

    @Throws(Exception::class)
    override fun process(exchange: Exchange) {
        var jsonString = ""
        try {
            val transactions = exchange.`in`.getBody(Array<Transaction>::class.java)
            for (transaction in transactions) {
                transactionService!!.insertTransaction(transaction)
            }

            mapper.writeValue(jsonOutputBytes, transactions)
            jsonString = String(jsonOutputBytes.toByteArray())
            //exchange.getIn().setBody(exchange.getIn().getHeader("jsonString"));
            exchange.`in`.body = jsonString
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    companion object {
        private val mapper = ObjectMapper()
    }
}
