package example.util

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import example.model.Transaction

import java.io.IOException

class TransactionSerializer @JvmOverloads constructor(transactionClass: Class<Transaction>? = null) : StdSerializer<Transaction>(transactionClass) {

    @Throws(IOException::class, JsonProcessingException::class)
    override fun serialize(transaction: Transaction, jgen: JsonGenerator, provider: SerializerProvider) {

        jgen.writeStartObject()
        jgen.writeStringField("guid", transaction.guid)
        jgen.writeStringField("sha256", transaction.sha256)
        jgen.writeStringField("accountType", transaction.accountType)
        jgen.writeStringField("accountNameOwner", transaction.accountNameOwner)
        jgen.writeStringField("description", transaction.description)
        jgen.writeStringField("category", transaction.category)
        jgen.writeStringField("notes", transaction.notes)
        jgen.writeNumberField("cleared", transaction.cleared)
        jgen.writeStringField("amount", transaction.amount.toString())
        jgen.writeNumberField("transactionDate", transaction.transactionDate?.getTime()!! / 1000)
        jgen.writeNumberField("dateUpdated", transaction.dateUpdated?.getTime()!! / 1000)
        jgen.writeNumberField("dateAdded", transaction.dateAdded?.getTime()!! / 1000)
        jgen.writeEndObject()
    }
}
