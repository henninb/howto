package example.controller;

import java.util.Enumeration;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.BrowserCallback;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.jms.JMSException;
import javax.jms.QueueBrowser;
import javax.jms.Session;

@RestController
public class QueueSizeController {
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    CachingConnectionFactory cachingConnectionFactory;

    private BrowserCallback<Integer> callback = new BrowserCallback<Integer>() {
        public Integer doInJms(final Session session, final QueueBrowser browser) throws JMSException {
            Enumeration enumeration = browser.getEnumeration();
            int counter = 0;
            while (enumeration.hasMoreElements()) {
                enumeration.nextElement();
                counter += 1;
            }
            return counter;
        }
    };

//    //http://localhost:8080/greeting
//    @RequestMapping("/greeting")
//    public String greeting() {
//        return "ok";
//    }

    //http://localhost:8080/queue_count
    @RequestMapping("/queue_count")
    public String greeting_simple() {
        JmsTemplate jmsTemplate = new JmsTemplate(cachingConnectionFactory);
        jmsTemplate.setReceiveTimeout(2000);

        String queue_name = "RVPCK.WMSTODSB.ORD.REQ.BACKOUT";
        int count = jmsTemplate.browse("RVPCK.WMSTODSB.ORD.REQ.BACKOUT", callback);
        LOGGER.info(queue_name + " Count: " + count);
        return "<b>" + queue_name + " Count: </b>" + count;
    }
}
