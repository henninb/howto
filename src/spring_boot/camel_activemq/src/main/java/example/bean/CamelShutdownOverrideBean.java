package example.bean;

import org.apache.camel.impl.DefaultShutdownStrategy;
import org.apache.camel.spi.ShutdownStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class CamelShutdownOverrideBean {

    @Bean
    public ShutdownStrategy shutdownStrategy() {
        DefaultShutdownStrategy defaultShutdownStrategy = new DefaultShutdownStrategy();
        defaultShutdownStrategy.setTimeout(30);
        return defaultShutdownStrategy;
    }
}
