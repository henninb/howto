package example.repository;

import example.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    //function name must match name on database table
    Transaction findByDescriptionIgnoreCase(String description);
    public Transaction findByTransactionId(int transactionId);

    public List<Transaction> findByAccountNameOwnerIgnoreCase(String accountNameOwner);
    public List<Transaction> findByAccountNameOwnerIgnoreCaseOrderByTransactionDate(String accountNameOwner);

    public Transaction findByGuid(String guid);
}
