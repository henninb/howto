package example.config;

import javax.jms.*;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.component.jms.JmsComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class JmsConfig {
    private long receiveTimeout = 1000;

    @Value("${activemq.broker-url}")
    private String brokerUrl;

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private ActiveMQConnectionFactory activeMQConnectionFactory() {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(brokerUrl);
        return activeMQConnectionFactory;
    }

    //activeMQ JMS endpoint for camel
    @Bean(name="activemq")
    public JmsComponent activeMQJmsComponent() {
        JmsComponent jmsComponent = new JmsComponent();
        jmsComponent.setConnectionFactory(this.activeMQConnectionFactory());

        jmsComponent.setTransacted(true);
        jmsComponent.setReceiveTimeout(receiveTimeout);
        return jmsComponent;
    }
}
