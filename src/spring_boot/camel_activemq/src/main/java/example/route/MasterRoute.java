package example.route;

import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import example.model.Transaction;
import example.processor.InsertTransactionProcessor;
import example.processor.JsonTransactionProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class MasterRoute extends RouteBuilder {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    JsonTransactionProcessor jsonTransactionProcessor;

    @Autowired
    InsertTransactionProcessor insertTransactionProcessor;

    @Override
    public void configure() throws Exception {
        onException(UnrecognizedPropertyException.class)
                .handled(true).log("UnrecognizedPropertyException").
        end();

        from("file:in?delete=true&moveFailed=.failed")
                .autoStartup(true)
                .choice()
                .when(header("CamelFileName").endsWith(".json"))
                    //.process(exchange -> exchange.getIn().setHeader("jsonString", exchange.getIn().getBody()))
                    //.process(exchange -> exchange.getIn().setHeader("jsonString", exchange.getIn().getBody()))
                    .log("pre - json Transaction processor")
                    .process(jsonTransactionProcessor)
                    .process(insertTransactionProcessor)
                    //.convertBodyTo(String.class)
                    .log("$simple{file:onlyname} processed")
                    .to("file:in" + File.separator + ".processedFiles")
                .when(header("CamelFileName").endsWith(".txt"))
                    .log("$simple{file:onlyname} moved")
                    .to("file:in" + File.separator + ".textFiles")
                .otherwise()
                    .log("$simple{file:onlyname} unprocessed")
                    .to("file:in" + File.separator + ".otherFiles")
                .end();
    }
}