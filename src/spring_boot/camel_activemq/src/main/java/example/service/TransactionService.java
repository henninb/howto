package example.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import example.model.Transaction;
import example.repository.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import javax.jms.*;
import java.util.List;

@Service
public class TransactionService {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private static ObjectMapper mapper = new ObjectMapper();

    @Autowired
    TransactionRepository transactionRepository;

    public void setRepositiory( TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public List<Transaction> findAll() {
        return transactionRepository.findAll();
    }

    public Boolean insertTransaction(Transaction transaction) {
        Transaction result = transactionRepository.saveAndFlush(transaction);
        if( transaction.getGuid().equals(result.getGuid()) ) {
            LOGGER.info("INFO: transactionRepository.saveAndFlush success.");
        } else {
            LOGGER.info("WARN: transactionRepository.saveAndFlush failure.");
        }
        return true;
    }

    public Boolean deleteAll() {
        transactionRepository.deleteAll();
        return true;
    }

    public Transaction findByGuid( String guid) {
        Transaction transaction = transactionRepository.findByGuid(guid);
        return transaction;
    }

    public Boolean postActiveMQ(Transaction transaction) throws JMSException, JsonProcessingException {
        // that JMS server is on localhost
        String url = ActiveMQConnection.DEFAULT_BROKER_URL;
        // default broker URL is : tcp://localhost:61616"

        String jsonResult = mapper.writeValueAsString(transaction);

        String queueName = "test_queue"; //Queue Name
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
        Connection connection = connectionFactory.createConnection();
        connection.start();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Destination destination = session.createQueue(queueName);
        MessageProducer producer = session.createProducer(destination);
        TextMessage message = session.createTextMessage(jsonResult);
        producer.send(message);
        connection.close();
        return true;
    }

    public Boolean deleteByGuid( String guid ) {
        transactionRepository.delete(findByGuid(guid));
        return true;
    }
}
