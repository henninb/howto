package example.controller;

import example.model.Transaction;
import example.service.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/transaction")
public class TransactionController {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    TransactionService transactionService;

    //mongoexport --host localhost --db finance_db --collection TransactionEntity --type=csv --out TransactionEntity.csv --fields guid,accountType,accountNameOwner,transactionDate,description,category,amount,cleared,notes,dateUpdated,dateAdded,sha256
    //mongoexport --host localhost --db finance_db --collection TransactionEntity --type=json --out TransactionEntity.json
    //http://localhost:8080/transaction/all
    //curl http://172.17.0.2:8080/transaction/all
    //curl http://localhost:8080/transaction/all
    //
    @GetMapping(value = "/all")
    public List<Transaction> listTransactionsAll() {
      return transactionService.findAll();
    }

    //http://localhost:8080/transaction/delete/0e0a01e2-06b0-4e3f-a15c-5afa86d968ce
    @GetMapping(value = "/delete/{guid}")
    public String deleteTransactionByGuid(@PathVariable String guid) {
        LOGGER.info(guid);
        //log("");
        try {
            Transaction transaction = transactionService.findByGuid(guid);
            LOGGER.info(transaction.getDescription());

            LOGGER.info(transaction.getAccountNameOwner());
            LOGGER.info(transaction.getGuid());
            LOGGER.info("transactions.contains(transaction) is found.");

            transactionService.deleteByGuid(guid);
            return guid;
        } catch (NullPointerException npe) {
            //npe.printStackTrace();
            System.out.println("NullPointerException, delete failure.");
            return "delete failure.";
        } catch (Exception ex ) {
            //System.out.println(ex);
            ex.printStackTrace();
            return "delete failure.";
        }
    }

    //http://localhost:8080/transaction/select/0e0a01e2-06b0-4e3f-a15c-5afa86d968ce
    @GetMapping(value = "/select/{guid}")
    public Transaction listTransactionByGuid(@PathVariable String guid) {
        Transaction transaction = transactionService.findByGuid(guid);
        //LOGGER.info(guid);
        //transaction = transactionService.findByGuid(guid);
        if( transaction != null ) {
          return transaction;
        } else {
            return new Transaction();
        }
    }
}
