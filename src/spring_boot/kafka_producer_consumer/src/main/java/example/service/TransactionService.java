package example.service;

import example.config.KafkaProducerProperties;
import example.model.Transaction;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;

@Service
public class TransactionService {

    @Autowired
    private KafkaTemplate<String, Transaction> kafkaTemplate;

    @Autowired
    private KafkaProducerProperties kafkaProducerProperties;

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    public boolean dispatch( Transaction transaction ) {
        try {
            kafkaTemplate.sendDefault(transaction.getId(), transaction);
            SendResult<String, Transaction> sendResult = kafkaTemplate.sendDefault(transaction.getId(), transaction).get();
            RecordMetadata recordMetadata = sendResult.getRecordMetadata();
            LOGGER.info("topic = {}, partition = {}, offset = {}, transaction = {}", recordMetadata.topic(), recordMetadata.partition(), recordMetadata.offset(), transaction);
            return true;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
