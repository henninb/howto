package example.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
//import com.google.common.base.MoreObjects;

public class Transaction {
    private String id = "1";
    private String definition = "some definitions";

    //public Transaction() {
    //    //for Spring-Web binding
    //}
    //@JsonCreator
    //public Transaction(@JsonProperty("id") String id, @JsonProperty("definition") String definition) {
    //    this.id = id;
    //    this.definition = definition;
    //}

    public String getId() {
        return id;
    }


    public String getDefinition() {
        return definition;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    //@Override
    //public String toString() {
    //    return MoreObjects.toStringHelper(this)
    //            .add("id", id)
    //            .add("definition", definition)
    //            .toString();
    //}
}
