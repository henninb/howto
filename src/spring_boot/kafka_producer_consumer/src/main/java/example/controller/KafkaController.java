package example.controller;

import example.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import example.service.TransactionService;

@RestController
public class KafkaController {

    @Autowired
    private TransactionService transactionService;

    @GetMapping("/generateWork")
    public boolean sendMessage(Transaction transaction) {
        return this.transactionService.dispatch(transaction);
    }
}
