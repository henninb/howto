package example.util

class HelloWorld {
    private var message: String? = null

    fun setMessage(message: String) {
        this.message = message
    }

    fun printHello() {
        println("Message: " + message!!)
    }
}
