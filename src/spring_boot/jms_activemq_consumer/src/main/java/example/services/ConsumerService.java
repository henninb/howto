package example.services;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.stereotype.Component;

import javax.jms.BytesMessage;
import javax.jms.TextMessage;

@Component
public class ConsumerService {
    @JmsListener(destination = "test_queue")
    //public void receiveQueue(String message) {

 //TextMessages, BytesMessages, MapMessages, and ObjectMessages

    public void receiveQueue(BytesMessage message) throws Exception {
        try {
            System.out.println("INFO: consumer text is <" + message.toString() + ">");
            //long x = message.getBodyLength();
            System.out.println(message.getJMSTimestamp());
            System.out.println(message.getJMSDeliveryMode());
        }
        //catch (J m) {

        //}
        catch (MessageConversionException m) {
            //jmsMessageType=Text
            System.out.println("Cannot convert from [[B] to [javax.jms.TextMessage] for GenericMessage");
        }
    }

    //@JmsListener(destination = "test_queue")
    //public void processOrder(Message message) throws JMSException {
    //    String convertedMessage = ((TextMessage) message).getText();
    //    System.out.println("INFO: consumer text is <" + convertedMessage + ">");
    //}
}
