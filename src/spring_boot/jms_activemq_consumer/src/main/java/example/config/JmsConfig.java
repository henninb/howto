package example.config;

import javax.jms.*;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.connection.JmsTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.ibm.msg.client.wmq.WMQConstants;

@Configuration
@EnableTransactionManagement
public class JmsConfig {
    @Value("${project.mq.host}")
    private String ibmHost;
    @Value("${project.mq.port}")
    private Integer port;
    @Value("${project.mq.queue-manager}")
    private String queueManager;
    @Value("${project.mq.channel}")
    private String channel;
    //@Value("${project.mq.queue}")
    //private String queue;
    @Value("${project.mq.receive-timeout}")
    private long receiveTimeout;

    @Value("${activemq.broker-url}")
    private String brokerUrl;

    public static final Logger LOGGER = LogManager.getLogger(JmsConfig.class);

    @Bean
    public ActiveMQConnectionFactory activeMQConnectionFactory() {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(brokerUrl);
        return activeMQConnectionFactory;
    }

    //activeMQ JMS endpoint for camel
    @Bean(name="activemq")
    public JmsComponent activeMQJmsComponent(ActiveMQConnectionFactory cachingConnectionFactory) {
        JmsComponent jmsComponent = new JmsComponent();
        jmsComponent.setConnectionFactory(cachingConnectionFactory);
        //TODO, investigate if setTransacted functional for activeMQ
        jmsComponent.setTransacted(true);
        jmsComponent.setReceiveTimeout(receiveTimeout);
        return jmsComponent;
    }

    @Bean
    public ConnectionFactory activeMQConnectionFactoryBean(ActiveMQConnectionFactory connectionFactory) {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(brokerUrl);

        return activeMQConnectionFactory;
    }

    //IBM MQ JMS endpoint for camel
    @Bean(name="wmq")
    public JmsComponent mqJmsComponent(CachingConnectionFactory cachingConnectionFactory) {
        JmsComponent jmsComponent = new JmsComponent();
        JmsTransactionManager jmsTransactionManager = new JmsTransactionManager(cachingConnectionFactory);
        jmsComponent.setConnectionFactory(cachingConnectionFactory);
        jmsComponent.setTransacted(true);
        jmsComponent.setReceiveTimeout(receiveTimeout);
        //TODO, investigate if setMaxConcurrentConsumers is required for IBM MQ
        //jmsComponent.setMaxConcurrentConsumers(10);
        return jmsComponent;
    }

    //IBM MQ
    @Bean
    public MQQueueConnectionFactory mqQueueConnectionFactory() {
        MQQueueConnectionFactory mqQueueConnectionFactory = new MQQueueConnectionFactory();
        mqQueueConnectionFactory.setHostName(ibmHost);
        try {
            mqQueueConnectionFactory.setTransportType(WMQConstants.WMQ_CM_CLIENT);
            mqQueueConnectionFactory.setCCSID(1208);
            mqQueueConnectionFactory.setChannel(channel);
            mqQueueConnectionFactory.setPort(port);
            //TODO: investigate if setClientReconnectTimeout is required
            //mqQueueConnectionFactory.setClientReconnectTimeout();
            mqQueueConnectionFactory.setQueueManager(queueManager);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mqQueueConnectionFactory;
    }

    //IBM MQ
    @Bean
    @Primary
    public CachingConnectionFactory cachingConnectionFactory(MQQueueConnectionFactory mqQueueConnectionFactory) {
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory();
        cachingConnectionFactory.setTargetConnectionFactory(mqQueueConnectionFactory);
        //TODO: investigate the setting from the web for setCacheConsumers and setSessionCacheSize
        //cachingConnectionFactory.setCacheConsumers(true);
        //cachingConnectionFactory.setSessionCacheSize(5);
        cachingConnectionFactory.setSessionCacheSize(20);
        cachingConnectionFactory.setReconnectOnException(true);
        return cachingConnectionFactory;
    }

    //IBM MQ
    @Bean
    public PlatformTransactionManager jmsTransactionManager(CachingConnectionFactory cachingConnectionFactory) {
        JmsTransactionManager jmsTransactionManager = new JmsTransactionManager();
        jmsTransactionManager.setConnectionFactory(cachingConnectionFactory);
        return jmsTransactionManager;
    }
}
