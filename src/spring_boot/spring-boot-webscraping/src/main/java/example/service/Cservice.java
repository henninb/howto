package example.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service
@Repository
public class Cservice {
    @Value("${message:World}")
    private String msg;

    public String message() {
        return this.msg;
    }
}
