package example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.stereotype.Controller;


//@EnableAutoConfiguration
//@Configuration
//@ComponentScan
@SpringBootApplication
@Controller
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }
}
