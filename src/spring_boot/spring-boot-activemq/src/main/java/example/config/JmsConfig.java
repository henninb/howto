package example.config;

import javax.jms.*;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.connection.JmsTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class JmsConfig {
    @Value("${project.mq.host}")
    private String ibmHost;
    @Value("${project.mq.port}")
    private Integer port;
    @Value("${project.mq.queue-manager}")
    private String queueManager;
    @Value("${project.mq.channel}")
    private String channel;
    //@Value("${project.mq.queue}")
    //private String queue;
    @Value("${project.mq.receive-timeout}")
    private long receiveTimeout;

    @Value("${activemq.broker-url}")
    private String brokerUrl;

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Primary
    @Bean
    public ActiveMQConnectionFactory activeMQConnectionFactory() {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(brokerUrl);
        return activeMQConnectionFactory;
    }


    //activeMQ JMS endpoint for camel
    @Bean(name="activemq")
    public JmsComponent activeMQJmsComponent(ActiveMQConnectionFactory cachingConnectionFactory) {
        JmsComponent jmsComponent = new JmsComponent();
        jmsComponent.setConnectionFactory(cachingConnectionFactory);
        //TODO, investigate if setTransacted functional for activeMQ
        jmsComponent.setTransacted(true);
        jmsComponent.setReceiveTimeout(receiveTimeout);
        return jmsComponent;
    }

//    @Bean
//    public ConnectionFactory activeMQConnectionFactoryBean(ActiveMQConnectionFactory connectionFactory) {
//        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
//        activeMQConnectionFactory.setBrokerURL(brokerUrl);
//
//        return activeMQConnectionFactory;
//    }
}
