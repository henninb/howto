package example.pojo;

import java.util.ArrayList;
import java.util.List;

public class QueueDepthMessageList {
    private List<QueueDepthMessage> queueDepthMessageList = new ArrayList<>();

    public void addQueueDepthMessage(QueueDepthMessage queueDepthMessage) {
        queueDepthMessageList.add(queueDepthMessage);
    }

    public List<QueueDepthMessage> getQueueDepthMessageList() {
        return queueDepthMessageList;
    }

    public void setQueueDepthMessageList(List<QueueDepthMessage> queueDepthMessageList) {
        this.queueDepthMessageList = queueDepthMessageList;
    }
}
