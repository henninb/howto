package example.controller;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.core.JsonProcessingException;
import example.service.QueueDepthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.jms.JMSException;

@RestController
public class QueueDepthController {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    QueueDepthService queueDepthService;

    //http://localhost:8080/queue_list_count
    @RequestMapping("/queue_list_count")
    public String queue_list_count() throws JsonProcessingException, JMSException {
        List<String> queueList = new ArrayList<>();
        queueList.add("Some_Queue");

        return queueDepthService.getQueueDepths(queueList);
    }
}
