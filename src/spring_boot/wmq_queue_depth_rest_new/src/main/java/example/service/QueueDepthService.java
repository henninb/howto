package example.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import example.pojo.QueueDepthMessage;
import example.pojo.QueueDepthMessageList;
import com.ibm.msg.client.jms.DetailedInvalidDestinationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.stereotype.Service;

import javax.jms.*;
import java.util.Enumeration;
import java.util.List;

@Service
public class QueueDepthService {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private static ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    CachingConnectionFactory cachingConnectionFactory;

    public String getQueueDepths(List<String> queueNameList ) throws JsonProcessingException, JMSException {
        int counter = 0;

        QueueConnection queueConnection = cachingConnectionFactory.createQueueConnection();
        LOGGER.info("cachingConnectionFactory.createQueueConnection()");
        Session session = queueConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        LOGGER.info("queueConnection.createSession(false, Session.AUTO_ACKNOWLEDGE)");

        QueueDepthMessageList queueDepthMessageList = new QueueDepthMessageList();
        LOGGER.info("new QueueDepthMessageList()");

        for (String queueName : queueNameList) {
            QueueDepthMessage queueDepthMessage = new QueueDepthMessage();
            queueDepthMessage.setQueueName(queueName);
            LOGGER.info("queueDepthMessage.setQueueName(queueName)");
            try {
                Queue queue = session.createQueue(queueName);
                LOGGER.info("session.createQueue(queueName)");
                QueueBrowser queueBrowser = session.createBrowser(queue);
                LOGGER.info("session.createBrowser(queue)");
                Enumeration enumeration = queueBrowser.getEnumeration();
                LOGGER.info("queueBrowser.getEnumeration()");
                counter = 0;
                while (enumeration.hasMoreElements()) {
                    enumeration.nextElement();
                    counter += 1;
                }
                queueBrowser.close();
                LOGGER.info("queueBrowser.close()");
            } catch ( DetailedInvalidDestinationException ide ) {
                LOGGER.info(ide.getExplanation());
                LOGGER.info(ide.toString());
                counter = -1;
            } catch ( Exception e ) {
                LOGGER.info(e.getMessage());
                LOGGER.info(e.toString());
            }
            queueDepthMessage.setQueueCount(counter);
            queueDepthMessageList.addQueueDepthMessage(queueDepthMessage);
            LOGGER.info(queueName + " count= " + counter);
        }

        session.close();
        LOGGER.info("session.close()");
        return objectMapper.writeValueAsString(queueDepthMessageList);
    }
}
