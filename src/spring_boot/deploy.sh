#!/bin/sh

if [ $# -ne 0 ]; then
    echo "Usage: $0 <noargs>"
    exit 1
fi

PROGRAM=$(basename $(pwd))
WD=$(pwd)

#find . -type f -perm /u=x,g=x,o=x | xargs rm

for ELEMENT in $(find . -mindepth 1 -maxdepth 1 -type d -print | sort); do
  #echo "${ELEMENT}"
  #if [ ".mvn" = "${ELEMENT}" ]; then
  #  break
  #fi
  rm -rf ${ELEMENT}/src/main/resources/application_properties_master
  cp build.gradle_master ${ELEMENT}/build.gradle_master
  cp application.properties_master ${ELEMENT}/src/main/resources/application.properties_master
  cp logback.xml_master ${ELEMENT}/src/main/resources/logback.xml_master
  cp gitignore_master ${ELEMENT}/.gitignore
  #cp mvnw.cmd ${ELEMENT}/mvnw.cmd
  #cp mvnw ${ELEMENT}/mvnw
  #cp gradlew.bat ${ELEMENT}/gradlew.bat
  #cp gradlew ${ELEMENT}/gradlew
  #cp -r .mvn ${ELEMENT}/.mvn
  #rm -f ${ELEMENT}/src/main/resources/.gitignore

  #git add ${ELEMENT}/gradlew*
  #git add ${ELEMENT}/mvnw*
  git add -f ${ELEMENT}/.mvn/wrapper/maven-wrapper.jar
  git add -f ${ELEMENT}/.mvn/wrapper/maven-wrapper.properties
  #git add ${ELEMENT}/src/main/resources/logback.xml
  #git add ${ELEMENT}/copy_programs_here.sh
  cd "${ELEMENT}"
  gradle writePom > /dev/null 2>&1
  #git add pom.xml
  gradle wrapper
  #make > /dev/null 2>&1
  #gradle build > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "${ELEMENT} failed."
  else
    echo "${ELEMENT} success."
  fi
  rm -rf out
  rm -rf build
  rm -rf .idea
  rm -rf .gradle
  rm -rf LOGS_IS_UNDEFINED
  rm -rf *.bak
  echo "gradle bootRun"
  cd "$WD"
done

exit 0
