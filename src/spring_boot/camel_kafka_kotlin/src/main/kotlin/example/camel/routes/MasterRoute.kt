package example.camel.routes

import example.processor.JsonProcessor
import org.apache.camel.builder.RouteBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Component
class MasterRoute : RouteBuilder() {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    internal var jsonProcessor: JsonProcessor? = null

    @Throws(Exception::class)
    override fun configure() {

        onException(org.apache.kafka.common.errors.TimeoutException::class.java)
                .handled(true).log("TimeoutException has occured.")
                .end()

        onException(Exception::class.java)
                .handled(true).log("Exception has occured.")
                .end()

        //from("sqlComponent:SELECT guid AS GUID, description AS DESRIPTION FROM t_transaction WHERE kafka_flag = 'N'"
        //from("sql:SELECT guid AS GUID, description AS DESRIPTION FROM transaction WHERE kafkaFlag = 'N'"

        from("jpa://example.model.Transaction?consumer.nativeQuery=SELECT transaction_id AS transactionID, account_name_owner AS accountNameOwner, guid AS guid, account_type AS accountType, description AS description, transaction_date AS transactionDate, description AS description, category AS category, amount AS amount, is_cleared AS isCleared, notes as notes, date_updated AS dateUpdated, date_added AS dateAdded, kafka_flag AS kafkaFlag FROM t_transaction WHERE kafka_flag = 'N'"
                //from("sql:SELECT guid AS GUID, description AS DESRIPTION FROM t_transaction WHERE kafka_flag != 'Y'"
                + "&consumer.initialDelay=3000"
                + "&consumer.delay=3000"
                + "&consumeDelete=true"
                //+"&outputType=SelectOne"
                //+"&outputType=SelectList"
                + "&consumer.resultClass=example.model.Transaction"
        )
                .autoStartup(true)
                .log("Select query executed.")
                .process(jsonProcessor)
                .log("json Processed")
                .to("direct:publishToKafkaRoute")
                .log("kafka published.")
                .to("file:results?fileName=to_kafka.txt&autoCreate=true&fileExist=Append")
                .log("INFO: Publish success. \${header.guid} :#guid")
                //.log("UPDATE t_transaction SET kafka_flag = 'Y' WHERE guid = '${header.guid}'")
                //.to("sql:UPDATE t_transaction SET kafka_flag = 'Y' WHERE guid = ':#guid'")
                .end()
    }
}
