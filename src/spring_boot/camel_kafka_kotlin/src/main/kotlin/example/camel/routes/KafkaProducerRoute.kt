package example.camel.routes

import example.configuration.KafkaProperties
import org.apache.camel.builder.RouteBuilder
//import org.apache.camel.component.kafka.KafkaConstants
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class KafkaProducerRoute : RouteBuilder() {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    //@Autowired
    //var kafkaProperties: KafkaProperties? = null

    @Autowired
    lateinit var kafkaProperties: KafkaProperties

    @Throws(Exception::class)
    override fun configure() {
        onException(org.apache.kafka.common.errors.TimeoutException::class.java)
                .handled(true)
                //.maximumRedeliveries(1)
                .log("ABORT: unable to connect to Kafka.")
                .end()

        onException(Exception::class.java)
                .handled(true)
                .log("ABORT: another Exception while producing for Kafka.")
                .end()

        from("direct:publishToKafkaRoute")
                .log("INFO: publishToKafkaRoute(), Message prior to publishing.")
                .log("INFO: to publish: " + kafkaProperties.kafkaTopic)
                //.process { exchange -> exchange.`in`.setHeader(KafkaConstants.PARTITION_KEY, 0) }
                //.process { exchange -> exchange.`in`.setHeader(KafkaConstants.KEY, "1") }
                .to("kafka:" + kafkaProperties.kafkaConnect + "?topic=" + kafkaProperties.kafkaTopic
                        + "&serializerClass=org.apache.kafka.common.serialization.StringSerializer&keySerializerClass=org.apache.kafka.common.serialization.StringSerializer"
                        + "&partitioner=org.apache.kafka.clients.producer.internals.DefaultPartitioner&sslKeymanagerAlgorithm=PKIX"
                        + "&brokers=" + kafkaProperties.kafkaConnect
                        //                    +"&securityProtocol=SSL" + "&sslKeystoreLocation="+kafkaProperties.getSslKeystoreLocation()+"&sslKeystorePassword="+ kafkaProperties.getSslKeystorePassword() +"&sslTruststoreLocation="+kafkaProperties.getSslTruststoreLocation()+"&sslTruststorePassword=" +kafkaProperties.getSslTruststorePassword()
                )
                .end()
    }
}
