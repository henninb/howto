package example.repository

import example.model.Transaction
import org.springframework.data.jpa.repository.JpaRepository

interface TransactionRepository<T : Transaction> : JpaRepository<T, Long> {

}
