package example.services

import example.model.Transaction
import example.repository.TransactionRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation .Autowired
import org.springframework.stereotype.Service

@Service
class TransactionService {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    lateinit private var transactionRepository: TransactionRepository<Transaction>

    val gransactions: List<Transaction>
        get() = transactionRepository.findAll()
}
