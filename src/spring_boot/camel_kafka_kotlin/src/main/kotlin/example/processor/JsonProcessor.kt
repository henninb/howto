package example.processor

import com.fasterxml.jackson.databind.ObjectMapper
import example.model.Transaction
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class JsonProcessor : Processor {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    @Throws(Exception::class)
    override fun process(exchange: Exchange) {
        try {
            val o = exchange.`in`.body
            LOGGER.info("class: " + o.javaClass.name)
            LOGGER.info("Body: " + exchange.`in`.body)
            val transaction = exchange.`in`.body as Transaction
            exchange.`in`.body = mapper.writeValueAsString(transaction)
            exchange.`in`.setHeader("guid", transaction.guid)
        } catch (ex: Exception) {
            LOGGER.info("Failure in JsonProcessor.")
            println(ex)
        } finally {

        }
        LOGGER.info("INFO: JsonProcessor completed.")
    }

    companion object {
        private val mapper = ObjectMapper()
    }
}