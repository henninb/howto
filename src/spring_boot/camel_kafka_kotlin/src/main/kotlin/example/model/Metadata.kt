package example.model

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.UUID

class Metadata {
    var id = UUID.randomUUID().toString()
    var version = "1.0"
    var messageSourceId = "3D"
    var messageTimestampStr: String? = null

    init {
        val messageTimestampZoned = ZonedDateTime.now()
        this.messageTimestampStr = messageTimestampZoned.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
    }
}
