package example.repository

import example.model.Transaction
import org.springframework.data .jpa.repository.JpaRepository
import org.springframework.stereotype.Component

@Component
interface TransactionRepository : JpaRepository<Transaction, Long>
