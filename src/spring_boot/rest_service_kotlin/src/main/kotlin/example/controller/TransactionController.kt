package example.controller

import example.service.TransactionService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class TransactionController {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    internal var transactionService: TransactionService? = null

    val transactions: String
        @CrossOrigin(origins = arrayOf("http://localhost:3000"))
        @RequestMapping("/getTransactions")
        get() = "[\n" +
                "     {\n" +
                "        \"accountType\": \"credit\",\n" +
                "        \"accountNameOwner\": \"usbank_brian\",\n" +
                "        \"transactionDate\": \"March 18, 2016\",\n" +
                "        \"transactionId\": 1,\n" +
                "        \"description\": \"Garden Cart\",\n" +
                "        \"category\": \"Home Improvement\",\n" +
                "        \"amount\": 32.99,\n" +
                "        \"isCleared\": true,\n" +
                "        \"notes\": \"\",\n" +
                "        \"dateUpdated\": \"March 18, 2016\",\n" +
                "        \"dateAdded\": \"March 18, 2016\",\n" +
                "        \"imageUrl\": \"app/assets/images/edit.png\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"accountType\": \"credit\",\n" +
                "        \"accountNameOwner\": \"usbank_kari\",\n" +
                "        \"transactionDate\": \"May 21, 2016\",\n" +
                "        \"transactionId\": 2,\n" +
                "        \"description\": \"Hammer\",\n" +
                "        \"category\": \"Home Improvement\",\n" +
                "        \"amount\": 8.9,\n" +
                "        \"isCleared\": false,\n" +
                "        \"notes\": \"\",\n" +
                "        \"dateUpdated\": \"May 21, 2016\",\n" +
                "        \"dateAdded\": \"May 21, 2016\",\n" +
                "        \"imageUrl\": \"app/assets/images/edit.png\"\n" +
                "    },\n" +
                "    \n" +
                "        {\n" +
                "        \"accountType\": \"credit\",\n" +
                "        \"accountNameOwner\": \"usbank_brian\",\n" +
                "        \"transactionDate\": \"July 30, 2017\",\n" +
                "        \"transactionId\": 3,\n" +
                "        \"description\": \"Mario Kart\",\n" +
                "        \"category\": \"Electronics\",\n" +
                "        \"amount\": 144.99,\n" +
                "        \"isCleared\": false,\n" +
                "        \"notes\": \"\",\n" +
                "        \"dateUpdated\": \"July 30, 2017\",\n" +
                "        \"dateAdded\": \"July 30, 2017\",\n" +
                "        \"imageUrl\": \"app/assets/images/edit.png\"\n" +
                "    },\n" +
                "\n" +
                "        {\n" +
                "        \"accountType\": \"credit\",\n" +
                "        \"accountNameOwner\": \"usbank_brian\",\n" +
                "        \"transactionDate\": \"July 30, 2017\",\n" +
                "        \"transactionId\": 4,\n" +
                "        \"description\": \"Barbie Doll\",\n" +
                "        \"category\": \"Toy\",\n" +
                "        \"amount\": 24.99,\n" +
                "        \"isCleared\": false,\n" +
                "        \"notes\": \"\",\n" +
                "        \"dateUpdated\": \"July 30, 2017\",\n" +
                "        \"dateAdded\": \"July 30, 2017\",\n" +
                "        \"imageUrl\": \"app/assets/images/edit.png\"\n" +
                "    }\n" +
                "]"

    @RequestMapping("/deleteTransaction")
    fun deleteTransaction(): String {
        return "one two three"
    }

    //@RequestMapping(method = RequestMethod.GET)
    @RequestMapping("/updateTransaction")
    fun updateTransaction(): String {
        return "one two three"
    }

    @RequestMapping("/addTransaction")
    fun addTransaction(): String {
        return "one two three"
    }
}
