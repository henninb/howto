package example.services;

import example.model.Transaction;
import example.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    public void setTransaction() {
        Transaction transaction = new Transaction();
        transaction.setAccountNameOwner("chase_brian");
        transaction.setAccountType("credit");
        transaction.setAmount("10.0");
        transaction.setCategory("restaurant");
        transaction.setDescription("Subway");
        transaction.setNotes("super secret");
        transaction.setDateAdded("8/16/2017");
        transactionRepository.save(transaction);
    }

}
