package example.repository;

import example.model.Transaction;
import example.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TransactionRepository extends MongoRepository<Transaction, String> {
}
