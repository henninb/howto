package example.config;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
//import org.apache.commons.lang3.StringUtils;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

import java.util.Arrays;

/**
 * Mongo Configuration information.
 *
 * @author Dataportal Engineering Team
 * @CopyRight (C) All rights reserved to Target Inc. It's Illegal to reproduce this code.
 */
@Configuration
public class MongoConfig {

    @Value("${spring.data.mongodb.host}")
    private String host;

    @Value("${spring.data.mongodb.port}")
    private int port;

    @Value("${spring.data.mongodb.database}")
    private String databaseName;

    //@Value("${spring.data.mongodb.username}")
    private String userName = "";

    //@Value("${spring.data.mongodb.password}")
    private String password = "";

    @Bean
    public MongoDbFactory mongoDbFactory() throws Exception {

        MongoCredential mongoCredential;
        MongoClient mongoClient;
        ServerAddress serverAddress = new ServerAddress(host, port);

        if (StringUtils.isNotBlank(userName) && StringUtils.isNotBlank(password)) {
            mongoCredential = MongoCredential.createCredential(userName, databaseName, password.toCharArray());
            mongoClient = new MongoClient(serverAddress, Arrays.asList(mongoCredential));
        } else {
            mongoClient = new MongoClient(serverAddress);
        }
        return new SimpleMongoDbFactory(mongoClient, databaseName);
    }

    @Bean
    @Autowired
    public MongoTemplate mongoTemplate() throws Exception {
        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory(), getDefaultMongoConverter());
        return mongoTemplate;
    }

    @Bean
    public MappingMongoConverter getDefaultMongoConverter() throws Exception {
        MappingMongoConverter converter = new MappingMongoConverter(
                new DefaultDbRefResolver(mongoDbFactory()), new MongoMappingContext());
        return converter;
    }

}