package example.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import example.util.TransactionDeserializer
import example.util.TransactionSerializer
import javax.persistence.*
import java.sql.Date

@Entity
@Table(name = "TransactionEntity")
@JsonDeserialize(using = TransactionDeserializer::class)
@JsonSerialize(using = TransactionSerializer::class)
class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transaction_id")
    var transactionId: Long? = null
    var guid: String? = null
    var accountType: String? = null
    var accountNameOwner: String? = null
    var transactionDate: Date? = null
    @Column(name = "description")
    var description: String? = null
    @Column(name = "category")
    var category: String? = null
    var amount: Double? = null

    @Column(name = "cleared")
    var cleared: Int = 0

    @Column(name = "notes")
    var notes: String? = null

    var dateUpdated: Date? = null
    var dateAdded: Date? = null
    var sha256: String? = null

    @JsonIgnore
    var extra: String? = null

    constructor() {}

    constructor(guid: String, accountType: String, accountNameOwner: String, transactionDate: Date, description: String, category: String, amount: Double?, cleared: Int, notes: String, dateUpdated: Date, dateAdded: Date, sha256: String) {
        this.transactionId = transactionId
        this.guid = guid
        this.accountType = accountType
        this.accountNameOwner = accountNameOwner
        this.transactionDate = transactionDate
        this.description = description
        this.category = category
        this.amount = amount
        this.cleared = cleared
        this.notes = notes
        this.dateUpdated = dateUpdated
        this.dateAdded = dateAdded
        this.sha256 = sha256
    }
}
