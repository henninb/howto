package example.processor

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException
import example.model.Transaction
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.io.IOException

@Component
class JsonTransactionProcessor : Processor {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    @Throws(Exception::class)
    override fun process(exchange: Exchange) {
        val transactions: Array<Transaction>

        try {
            val jsonString = exchange.`in`.getBody(String::class.java)
            transactions = mapper.readValue(jsonString, Array<Transaction>::class.java)
            exchange.`in`.body = transactions
        } catch (upe: UnrecognizedPropertyException) {
            println("UnrecognizedPropertyException")
            upe.printStackTrace()
            throw upe
        } catch (ioe: IOException) {
            println("IOException")
            ioe.printStackTrace()
        } catch (e: Exception) {
            println("UnrecognizedPropertyException")
            e.printStackTrace()
        }
    }

    companion object {
        private val mapper = ObjectMapper()
    }
}
