package example.controller

import example.model.Transaction
import example.service.TransactionService
import lombok.extern.slf4j.Slf4j
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@Slf4j
@RestController
@RequestMapping("/transaction")
class TransactionController {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    //internal var transactionService: TransactionService? = null
    lateinit var transactionService: TransactionService

    //mongoexport --host localhost --db finance_db --collection TransactionEntity --type=csv --out TransactionEntity.csv --fields guid,accountType,accountNameOwner,transactionDate,description,category,amount,cleared,notes,dateUpdated,dateAdded,sha256
    //mongoexport --host localhost --db finance_db --collection TransactionEntity --type=json --out TransactionEntity.json
    //http://localhost:8080/transaction/all
    //curl http://172.17.0.2:8080/transaction/all
    //curl http://localhost:8080/transaction/all
    //
    @GetMapping(path = arrayOf("/all"))
    fun listTransactionsAll(): ResponseEntity<List<Transaction>> {
        return ResponseEntity.ok(transactionService.findAll())
        //return transactionService.findAll()
    }

    //http://localhost:8080/transaction/deleteTransaction/0e0a01e2-06b0-4e3f-a15c-5afa86d968ce
    @DeleteMapping(path = arrayOf("/deleteTransaction/{guid}"))
    fun deleteTransactionByGuid(@PathVariable guid: String): ResponseEntity<String> {
        try {
            val transaction = transactionService.findByGuid(guid)
            transactionService.deleteByGuid(guid)
            return ResponseEntity.ok("successfully deleted.")
        } catch (npe: NullPointerException) {
            println("NullPointerException, delete failure.")
            return ResponseEntity.notFound().build()
        } catch (ex: Exception) {
            ex.printStackTrace()
            return ResponseEntity.notFound().build()
        }
    }

    //http://localhost:8080/transaction/getTransaction/0e0a01e2-06b0-4e3f-a15c-5afa86d968ce
    @GetMapping(path = arrayOf("/getTransaction/{guid}"))
    fun listTransactionByGuid(@PathVariable guid: String): ResponseEntity<Transaction> {
        val transaction = transactionService.findByGuid(guid)
        //transaction = transactionService.findByGuid(guid);
        return ResponseEntity.ok(transaction)
    }
}
