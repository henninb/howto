package example.service

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import example.model.Transaction
import example.repository.TransactionRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.apache.activemq.ActiveMQConnection
import org.apache.activemq.ActiveMQConnectionFactory
import java.util.*
import javax.jms.*

@Service
class TransactionService {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    lateinit private var transactionRepository: TransactionRepository<Transaction>

//    @Autowired
//    internal var transactionRepository: TransactionRepository? = null

//    fun setRepositiory(transactionRepository: TransactionRepository) {
//        this.transactionRepository = transactionRepository
//    }

    fun findAll(): List<Transaction> {
        return transactionRepository.findAll()
    }

    fun insertTransaction(transaction: Transaction) {
        val result = transactionRepository.saveAndFlush(transaction)
        if (transaction.guid == result.guid) {
            LOGGER.info("INFO: transactionRepository.saveAndFlush success.")
        } else {
            LOGGER.info("WARN: transactionRepository.saveAndFlush failure.")
        }
    }

    fun deleteAll() {
        transactionRepository.deleteAll()
    }

    fun findByGuid(guid: String): Transaction {
        val optionalTransaction : Optional<Transaction> =  transactionRepository.findByGuid(guid)
        if( optionalTransaction.isPresent) {
            return optionalTransaction.get()
        }
        return Transaction()
    }

    @Throws(JMSException::class, JsonProcessingException::class)
    fun postActiveMQ(transaction: Transaction) {
        // that JMS server is on localhost
        val url = ActiveMQConnection.DEFAULT_BROKER_URL
        // default broker URL is : tcp://localhost:61616"

        val jsonResult = mapper.writeValueAsString(transaction)

        val queueName = "test_queue" //Queue Name
        val connectionFactory = ActiveMQConnectionFactory(url)
        val connection = connectionFactory.createConnection()
        connection.start()
        val session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE)
        val destination = session.createQueue(queueName)
        val producer = session.createProducer(destination)
        val message = session.createTextMessage(jsonResult)
        producer.send(message)
        connection.close()
    }

    fun deleteByGuid(guid: String) :Boolean {
        transactionRepository.delete(findByGuid(guid))
        return true
    }

    companion object {
        private val mapper = ObjectMapper()
    }
}