package example.repository

import example.model.Transaction
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface TransactionRepository<T : Transaction> : JpaRepository<T, Long> {
//interface TransactionRepository : JpaRepository<Transaction, Long> {
    //function name must match name on database table
    fun findByDescriptionIgnoreCase(description: String): Optional<Transaction>

    fun findByTransactionId(transactionId: Int): Optional<Transaction>

    fun findByAccountNameOwnerIgnoreCase(accountNameOwner: String): List<Transaction>
    fun findByAccountNameOwnerIgnoreCaseOrderByTransactionDate(accountNameOwner: String): List<Transaction>

    fun findByGuid(guid: String): Optional<Transaction>
}