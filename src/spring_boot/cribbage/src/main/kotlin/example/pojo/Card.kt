package example.pojo

class Card {
    val rank1: Int = 0
    val suit1: Char? = null;

    val cardRank: Int = 0
    val cardSuit: Int = 0
    val rank: String? = null
    val suit: String? = null
    val suitsArray = arrayOf("♠", "♥", "♦", "♣")
    val ranksArray = arrayOf("A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K")
    val ordinal: Int = 0
}