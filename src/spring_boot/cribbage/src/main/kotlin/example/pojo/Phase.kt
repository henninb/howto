package example.pojo

enum class Phase {
    cuttingPhase,
    peggingPhase,
    handCountingPhase,
    countingPhase
}