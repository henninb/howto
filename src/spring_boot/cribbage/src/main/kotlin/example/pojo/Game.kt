package example.pojo

import javax.persistence.*
import java.io.Serializable

@Entity
class Game : Serializable {
    var action : String = ""
    var activePlayer: String = ""
    var scorePlayer1: String = ""
    var scorePlayer2: String = ""

    var deck : String = ""
    var gameState : String = ""
    var phaseOfGame: String = ""
    var peggingPhase: Boolean = false;
    var handCountingPhase: Boolean = false
    var countingPhase: Boolean = false
}