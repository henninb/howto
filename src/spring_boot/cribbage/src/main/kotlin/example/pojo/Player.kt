package example.pojo

class Player {
    var hand: MutableList<Card> = ArrayList()
    //var color: String = ""
    var id: Int = 0
    var name: String = ""
    var hasPlayedThisRound: Boolean = false
}
