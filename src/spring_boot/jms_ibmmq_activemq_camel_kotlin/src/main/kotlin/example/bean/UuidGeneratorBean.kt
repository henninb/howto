package example.bean

import org.apache.camel.impl.DefaultShutdownStrategy
import org.apache.camel.spi.ShutdownStrategy
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

import java.util.UUID

@Component
class UuidGeneratorBean {

    @Bean
    fun generateId(): String {
        return UUID.randomUUID().toString()
    }
}