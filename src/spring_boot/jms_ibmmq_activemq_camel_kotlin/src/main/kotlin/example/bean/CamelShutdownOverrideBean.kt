package example.bean

import org.apache.camel.impl.DefaultShutdownStrategy
import org.apache.camel.spi.ShutdownStrategy
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

@Component
class CamelShutdownOverrideBean {

    @Bean
    fun shutdownStrategy(): ShutdownStrategy {
        val defaultShutdownStrategy = DefaultShutdownStrategy()
        defaultShutdownStrategy.timeout = 30
        return defaultShutdownStrategy
    }
}