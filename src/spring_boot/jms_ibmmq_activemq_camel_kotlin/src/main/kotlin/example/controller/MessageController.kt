package example.controller

import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

//for testing only
@RestController
class MessageController {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    //@RequestMapping(method = RequestMethod.GET)
    @RequestMapping("/updateTransaction")
    fun updateTransaction(): String {
        return "got here"
    }
}
