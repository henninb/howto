package example.exception

class XPathNotFoundException : Exception {
    // Parameterless Constructor
    constructor() {}

    // Constructor that accepts a message
    constructor(message: String) : super(message) {}
}
