package example.route


import example.exception.XPathNotFoundException
import org.apache.camel.LoggingLevel
import org.apache.camel.builder.RouteBuilder
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component

@Configuration
@Component
open class MasterRoute : RouteBuilder() {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    @Value("\${project.message.technology}")
    private val messageTechnology: String? = null
    //private String messageTechnology = "wmq";

    @Value("\${project.http.maximum.redeliveries}")
    private val httpMaximumRedeliveries: Int? = null
    //private Integer httpMaximumRedeliveries = 3;

    @Value("\${project.http.redelivery.delay}")
    private val httpRedeliveryDelay: Int? = null
    //private Integer httpRedeliveryDelay = 30000;

    @Value("\${project.mq.maximum.redeliveries}")
    private val mqMaximumRedeliveries: Int? = null
    //private Integer mqMaximumRedeliveries = 3;

    @Value("\${project.http.redelivery.delay}")
    private val mqRedeliveryDelay: Int? = null
    //private Integer mqRedeliveryDelay = 30000;

    @Value("\${project.mq.request.queue}")
    private val mqRequestQueueName: String? = null
    //private String mqRequestQueueName = "queue_name";

    @Value("\${project.mq.backout.queue}")
    private val mqBackoutQueueName: String? = null
    //private String mqBackoutQueueName = "queue_backout";

    @Value("\${project.mq.error.queue}")
    private val mqErrorQueueName: String? = null
    //private String mqErrorQueueName = "queue_error";

    @Value("\${project.mq.concurrent.consumers}")
    private val mqConcurrentConsumers: Int? = null
    //private Integer mqConcurrentConsumers=8;

    @Value("\${project.mq.max.concurrent.consumers}")
    private val mqMaxConcurrentConsumers: Int? = null
    //private Integer mqMaxConcurrentConsumers=16;

    @Throws(Exception::class)
    override fun configure() {
        //TODO: for MQException or IllegalStateException what if any code changees need to be made in the onException blocks below.
        //        //onException(com.ibm.mq.MQException.class)
        //        onException(org.springframework.jms.IllegalStateException.class)

        //data errors
        //org.apache.camel.TypeConversionException: Error during type conversion from type: java.lang.String to the required type: org.w3c.dom.Document with value
        //org.apache.camel.processor.validation.SchemaValidationException: Validation failed for: com.sun.org.apache.xerces.internal.jaxp.validation.SimpleXMLSchema
        onException(org.apache.camel.processor.validation.SchemaValidationException::class.java)
                .onException(org.apache.camel.TypeConversionException::class.java)
                //.onException(MessageType2Exception.class)
                //.onException(MessageType1Exception.class)
                .onException(XPathNotFoundException::class.java)
                .handled(true)
                .log(LoggingLevel.ERROR, "\${exception.class} :: \${exception.message}")
                .doTry()
                .to("$messageTechnology:queue:$mqErrorQueueName")
                .doCatch(Exception::class.java)
                .log(LoggingLevel.ERROR, "\${exception.class} :: \${exception.message}")
                .doTry()
                //.to("file:in" + File.separator + ".failedFiles")
                .process { System.exit(253) }
                .stop()
                .doCatch(Exception::class.java)
                .log(LoggingLevel.ERROR, "\${exception.class} :: \${exception.message}")
                .log(LoggingLevel.ERROR, "ERROR: really bad scenario, we may need to abort.")
                .end()

        //connection errors
        //java.net.ConnectException) caught when processing request: Connection refused
        //org.apache.camel.http.common.HttpOperationFailedException :: HTTP operation failed invoking http://localhost:8080 with statusCode: 500
        //onException(org.apache.camel.http.common.HttpOperationFailedException.class)
        onException(java.net.ConnectException::class.java)
                .maximumRedeliveries(httpMaximumRedeliveries!!)
                .redeliveryDelay(httpRedeliveryDelay!!.toLong())
                .retryAttemptedLogLevel(LoggingLevel.INFO)
                .maximumRedeliveries(httpMaximumRedeliveries).handled(true)
                .log(LoggingLevel.ERROR, "\${exception.class} :: \${exception.message}")
                .doTry()
                .to("$messageTechnology:queue:$mqBackoutQueueName")
                .doCatch(Exception::class.java)
                .log(LoggingLevel.ERROR, "\${exception.class} :: \${exception.message}")
                .doTry()
                //.to("file:in" + File.separator + ".failedFiles")
                .process { System.exit(254) }
                .stop()
                .doCatch(Exception::class.java)
                .log(LoggingLevel.ERROR, "\${exception.class} :: \${exception.message}")
                .log(LoggingLevel.ERROR, "ERROR: really bad scenario, we may need to abort.")
                .end()

        //default execption
        onException(Exception::class.java)
                .maximumRedeliveries(1)
                .redeliveryDelay(1000)
                .retryAttemptedLogLevel(LoggingLevel.INFO)
                .maximumRedeliveries(1).handled(true)
                .log(LoggingLevel.ERROR, "\${exception.class} :: \${exception.message}")
                .doTry()
                .to("$messageTechnology:queue:$mqBackoutQueueName")
                .doCatch(Exception::class.java)
                .log(LoggingLevel.ERROR, "\${exception.class} :: \${exception.message}")
                .doTry()
                //.to("file:in" + File.separator + ".failedFiles")
                .process { System.exit(255) }
                .stop()
                .doCatch(Exception::class.java)
                .log(LoggingLevel.ERROR, "\${exception.class} :: \${exception.message}")
                .log(LoggingLevel.ERROR, "ERROR: Unhandled exception, application needs to abort.")
                .end()

        //queue route for golive
        from("$messageTechnology:queue:$mqRequestQueueName?concurrentConsumers=$mqConcurrentConsumers&maxConcurrentConsumers=$mqMaxConcurrentConsumers")
        //file route for testing
        //from("file:in?delete=true&moveFailed=.failed") //used for testing only (file input)
                .routeId("WMQRoute")
                .autoStartup(true)

                .log(LoggingLevel.INFO, "INFO: read from mqRequestQueueName:$mqRequestQueueName messageTechnology=$messageTechnology")
                .log(LoggingLevel.INFO, "INFO: body=\${body}")
                // No longer required
                //.to("file:in" + File.separator + ".originalMessage?fileName=file_${bean:uuidGenerator.generateId}.txt")
                .process { exchange -> exchange.`in`.setHeader("originalMessage", exchange.`in`.getBody(String::class.java)) }
                //.setHeader("messageName").xpath("/root/header/@messageName", String.class)
                .choice()
                .`when`()
                .xpath("/root/header[messageName='MessageType1']")
                //.to("validator:xsd/MessageType1.xsd")
                //.to("xslt:xslt/MessageType1.xslt")
                //.to("file:in" + File.separator + ".SoapRequestMessageType1")
                .log(LoggingLevel.INFO, "INFO: requested MessageType1")
                .to("http://localhost:8080/updateTransaction")
                .log(LoggingLevel.INFO, "INFO: response MessageType1")
                //.process(MessageType1SoapProcessor)
                .log(LoggingLevel.INFO, "INFO: soapResponseBody=\${body}")
                //.to("file:in" + File.separator + ".SoapResponseMessageType1")
                .`when`()
                .xpath("/root/header[messageName='MessageType2']")
                //.to("validator:xsd/MessageType2.xsd")
                //.to("xslt:xslt/MessageType2.xslt")
                //.to("file:in" + File.separator + ".SoapRequestMessageType2")
                .log(LoggingLevel.INFO, "INFO: requested MessageType2")
                .to("http://localhost:8080/updateTransaction")
                .log(LoggingLevel.INFO, "INFO: response MessageType2")
                //.process(MessageType2SoapProcessor)
                .log(LoggingLevel.INFO, "INFO: soapResponseBody=\${body}")
                //.to("file:in" + File.separator + ".SoapResponseMessageType2")
                .otherwise()
                //.to("file:in" + File.separator + ".xpathNotFound")
                .log(LoggingLevel.WARN, "WARN: xpath not found for the message.")
                .throwException(XPathNotFoundException::class.java, "xpath not found for the message.")
                .end()
    }
}
