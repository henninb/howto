package example.config


import javax.jms.*
import org.apache.activemq.ActiveMQConnectionFactory
import org.apache.camel.component.jms.JmsComponent
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.jms.connection.CachingConnectionFactory
import org.springframework.jms.connection.JmsTransactionManager
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement
import com.ibm.mq.jms.MQQueueConnectionFactory
import com.ibm.msg.client.wmq.WMQConstants

@Configuration
@EnableTransactionManagement
open class JmsConfig {
    @Value("\${project.mq.host}")
    private val ibmHost: String? = null
    @Value("\${project.mq.port}")
    private val port: Int? = null
    @Value("\${project.mq.queue-manager}")
    private val queueManager: String? = null
    @Value("\${project.mq.channel}")
    private val channel: String? = null
    //@Value("${project.mq.queue}")
    //private String queue;
    @Value("\${project.mq.receive-timeout}")
    private val receiveTimeout: Long = 0

    @Value("\${activemq.broker-url}")
    private val brokerUrl: String? = null

    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    @Bean
    open fun activeMQConnectionFactory(): ActiveMQConnectionFactory {
        val activeMQConnectionFactory = ActiveMQConnectionFactory()
        activeMQConnectionFactory.brokerURL = brokerUrl
        return activeMQConnectionFactory
    }

    //activeMQ JMS endpoint for camel
    @Bean(name = arrayOf("activemq"))
    open fun activeMQJmsComponent(cachingConnectionFactory: ActiveMQConnectionFactory): JmsComponent {
        val jmsComponent = JmsComponent()
        jmsComponent.setConnectionFactory(cachingConnectionFactory)
        //TODO, investigate if setTransacted functional for activeMQ
        jmsComponent.setTransacted(true)
        jmsComponent.setReceiveTimeout(receiveTimeout)
        return jmsComponent
    }

    @Bean
    open fun activeMQConnectionFactoryBean(connectionFactory: ActiveMQConnectionFactory): ConnectionFactory {
        val activeMQConnectionFactory = ActiveMQConnectionFactory()
        activeMQConnectionFactory.brokerURL = brokerUrl

        return activeMQConnectionFactory
    }

    //IBM MQ JMS endpoint for camel
    @Bean(name = arrayOf("wmq"))
    open fun mqJmsComponent(cachingConnectionFactory: CachingConnectionFactory): JmsComponent {
        val jmsComponent = JmsComponent()
        val jmsTransactionManager = JmsTransactionManager(cachingConnectionFactory)
        jmsComponent.setConnectionFactory(cachingConnectionFactory)
        jmsComponent.setTransacted(true)
        jmsComponent.setReceiveTimeout(receiveTimeout)
        //TODO, investigate if setMaxConcurrentConsumers is required for IBM MQ
        //jmsComponent.setMaxConcurrentConsumers(10);
        return jmsComponent
    }

    //IBM MQ
    @Bean
    open fun mqQueueConnectionFactory(): MQQueueConnectionFactory {
        val mqQueueConnectionFactory = MQQueueConnectionFactory()
        mqQueueConnectionFactory.hostName = ibmHost
        try {
            mqQueueConnectionFactory.transportType = WMQConstants.WMQ_CM_CLIENT
            mqQueueConnectionFactory.ccsid = 1208
            mqQueueConnectionFactory.channel = channel
            mqQueueConnectionFactory.port = port!!
            //TODO: investigate if setClientReconnectTimeout is required
            //mqQueueConnectionFactory.setClientReconnectTimeout();
            mqQueueConnectionFactory.queueManager = queueManager
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return mqQueueConnectionFactory
    }

    //IBM MQ
    @Bean
    @Primary
    open fun cachingConnectionFactory(mqQueueConnectionFactory: MQQueueConnectionFactory): CachingConnectionFactory {
        val cachingConnectionFactory = CachingConnectionFactory()
        cachingConnectionFactory.targetConnectionFactory = mqQueueConnectionFactory
        //TODO: investigate the setting from the web for setCacheConsumers and setSessionCacheSize
        //cachingConnectionFactory.setCacheConsumers(true);
        //cachingConnectionFactory.setSessionCacheSize(5);
        cachingConnectionFactory.sessionCacheSize = 20
        cachingConnectionFactory.setReconnectOnException(true)
        return cachingConnectionFactory
    }

    //IBM MQ
    @Bean
    open fun jmsTransactionManager(cachingConnectionFactory: CachingConnectionFactory): PlatformTransactionManager {
        val jmsTransactionManager = JmsTransactionManager()
        jmsTransactionManager.connectionFactory = cachingConnectionFactory
        return jmsTransactionManager
    }
}
