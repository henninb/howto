--drop table t_transaction;
CREATE TABLE t_transaction_work (
  account_id INTEGER auto_increment,
  account_type CHAR(10),
  account_name_owner CHAR(40) NOT NULL,
  transaction_id INTEGER,
  guid CHAR(40) NOT NULL,
  transaction_date VARCHAR(25) NOT NULL,
  description VARCHAR(75) NOT NULL,
  category VARCHAR(50),
  amount DECIMAL(12,2) NOT NULL,
  is_cleared INTEGER,
  notes VARCHAR(100),
  date_updated VARCHAR(25),
  date_added VARCHAR(25),
  kafka_flag CHAR(2) DEFAULT 'N'
);

CREATE TABLE t_transaction (
  account_id INTEGER,
  account_type CHAR(10),
  account_name_owner CHAR(40) NOT NULL,
  transaction_id INTEGER auto_increment,
  guid CHAR(40) NOT NULL,
  transaction_date VARCHAR(25) NOT NULL,
  description VARCHAR(75) NOT NULL,
  category VARCHAR(50),
  amount DECIMAL(12,2) NOT NULL,
  is_cleared INTEGER,
  notes VARCHAR(100),
  date_updated VARCHAR(25),
  date_added VARCHAR(25),
  kafka_flag CHAR(2) DEFAULT 'N'
);
