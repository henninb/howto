package example.camel.routes;


import example.configuration.KafkaProperties;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeoutException;


@Component
public class KafkaProducerRoute extends RouteBuilder {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private KafkaProperties kafkaProperties;

    @Override
    public void configure() throws Exception {

        LOGGER.info("INFO: topic:" + kafkaProperties.getKafkaTopic());

        onException(org.apache.kafka.common.errors.TimeoutException.class)
                .handled(true)
                //.maximumRedeliveries(1)
                .log("ABORT: unable to connect to Kafka.")
                //.to("sqlComponent:UPDATE some_table SET pub_f = 'Y' WHERE something = :#failedPickTask")
                //.to("sqlComponent:update some_table set pub_retry_i=pub_retry_i+1 where something = :#failedPickTask")
                //.to("sqlComponent:UPDATE some_table SET PUB_RETRY_I = PUB_RETRY_I + 1 WHERE something = 1116")
                .end();

        onException(Exception.class)
                .handled(true)
                .log("ABORT: another Exception while producing for Kafka.")
                //.to("sqlComponent:UPDATE some_table SET pub_f = 'F' WHERE something = :#failedPickTask")
                .end();

        from("direct:publishToKafkaRoute")
                .log("INFO: publishToKafkaRoute(), Message prior to publishing.")
                .log("INFO: to publish: " + kafkaProperties.getKafkaTopic())
                .process(exchange -> exchange.getIn().setHeader(KafkaConstants.PARTITION_KEY, 0))
                .process(exchange -> exchange.getIn().setHeader(KafkaConstants.KEY, "1"))
                .to("kafka:"+ kafkaProperties.getKafkaConnect() +"?topic=" + kafkaProperties.getKafkaTopic()
                        +"&serializerClass=org.apache.kafka.common.serialization.StringSerializer&keySerializerClass=org.apache.kafka.common.serialization.StringSerializer"
                        +"&partitioner=org.apache.kafka.clients.producer.internals.DefaultPartitioner&sslKeymanagerAlgorithm=PKIX"
                        +"&brokers="+ kafkaProperties.getKafkaConnect()
    //                    +"&securityProtocol=SSL" + "&sslKeystoreLocation="+kafkaProperties.getSslKeystoreLocation()+"&sslKeystorePassword="+ kafkaProperties.getSslKeystorePassword() +"&sslTruststoreLocation="+kafkaProperties.getSslTruststoreLocation()+"&sslTruststorePassword=" +kafkaProperties.getSslTruststorePassword()
                )

                .end();
    }
}
