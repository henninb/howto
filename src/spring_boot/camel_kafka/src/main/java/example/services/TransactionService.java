package example.services;

import example.model.Transaction;
import example.repository.TransactionJpaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionService {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    TransactionJpaRepository transactionJpaRepository;

    public List<Transaction> getGransactions() {
        return transactionJpaRepository.findAll();
    }
}
