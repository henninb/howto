package example.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Component
@ConfigurationProperties(prefix="project.kafka", ignoreUnknownFields = false)
public class KafkaProperties {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private String kafkaConnect;
    private String zookeeperConnect;
    private String kafkaTopic;
    private String sslKeystoreLocation;
    private String sslKeystorePassword;
    private String sslTruststoreLocation;
    private String sslTruststorePassword;

    public String getKafkaConnect() {
        return kafkaConnect;
    }
    public void setKafkaConnect(String kafkaConnect) {
        this.kafkaConnect = kafkaConnect;
    }
    public String getZookeeperConnect() {
        return zookeeperConnect;
    }
    public void setZookeeperConnect(String zookeeperConnect) {
        this.zookeeperConnect = zookeeperConnect;
    }
    public String getKafkaTopic() {
        return kafkaTopic;
    }
    public void setKafkaTopic(String kafkaTopic) {
        this.kafkaTopic = kafkaTopic;
    }
    public String getSslKeystoreLocation() {
        return sslKeystoreLocation;
    }
    public void setSslKeystoreLocation(String sslKeystoreLocation) {
        this.sslKeystoreLocation = sslKeystoreLocation;
    }
    public String getSslKeystorePassword() {
        return sslKeystorePassword;
    }
    public void setSslKeystorePassword(String sslKeystorePassword) {
        this.sslKeystorePassword = sslKeystorePassword;
    }
    public String getSslTruststoreLocation() {
        return sslTruststoreLocation;
    }
    public void setSslTruststoreLocation(String sslTruststoreLocation) {
        this.sslTruststoreLocation = sslTruststoreLocation;
    }
    public String getSslTruststorePassword() {
        return sslTruststorePassword;
    }
    public void setSslTruststorePassword(String sslTruststorePassword) {
        this.sslTruststorePassword = sslTruststorePassword;
    }
}
