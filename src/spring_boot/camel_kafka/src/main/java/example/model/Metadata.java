package example.model;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

public class Metadata {
    private String Id = UUID.randomUUID().toString();
    private String version ="1.0";
    private String messageSourceId ="3D";
    private String messageTimestampStr;

    public Metadata(){
        ZonedDateTime messageTimestampZoned = ZonedDateTime.now();
        this.messageTimestampStr = messageTimestampZoned.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    public String getId() {
        return this.Id;
    }

    public void setId(String id) {
        this.Id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getMessageSourceId() {
        return messageSourceId;
    }

    public void setMessageSourceId(String messageSourceId) {
        this.messageSourceId = messageSourceId;
    }

    public String getMessageTimestampStr() {
        return messageTimestampStr;
    }

    public void setMessageTimestampStr(String messageTimestampStr) {
        this.messageTimestampStr = messageTimestampStr;
    }
}
