package example.processor;

import com.fasterxml.jackson.databind.ObjectMapper;
import example.model.Transaction;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class JsonProcessor implements Processor {
    private static ObjectMapper mapper = new ObjectMapper();
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Override
    public void process(Exchange exchange) throws Exception {
        try {
            Object o = exchange.getIn().getBody();
           //instance.getClass().getName();
            LOGGER.info("class: " + o.getClass().getName());
            LOGGER.info("Body: " + exchange.getIn().getBody());
            Transaction transaction = (Transaction) exchange.getIn().getBody();
            exchange.getIn().setBody(mapper.writeValueAsString(transaction));
            exchange.getIn().setHeader("guid", transaction.getGuid());
            //Transaction transaction = new Transaction();
            //exchange.getIn().setHeader("guid", "123");
            //exchange.getIn().setBody(transaction);
        } catch (Exception ex){
            LOGGER.info("Failure in JsonProcessor.");
            System.out.println(ex);
            //throw ex;
        } finally {

        }
        LOGGER.info("INFO: JsonProcessor completed.");
    }
}
