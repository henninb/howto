package example.repository;

import example.model.Transaction;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.JpaRepository;

//@EntityScan(basePackages = {"finance.model"})
public interface TransactionJpaRepository  extends JpaRepository<Transaction, Long> {
}
