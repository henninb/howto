package example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;


@SpringBootApplication
@EnableAutoConfiguration
@EnableConfigurationProperties
@ComponentScan
public class Application {

    //@Autowired
    //private Environment env;
    //https://dzone.com/articles/gradle-goodness-passing-environment-variable-via-d

    public static void main(String[] args) {
        //Application app = new Application();
        //System.getProperties().put( "server.port", 8181);
        SpringApplication.run(Application.class, args);
        //ApplicationContext applicationContext = SpringApplication.run(Application.class, args);
        //env.getProperty("key.something")
    }
}
