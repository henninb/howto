package example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/*
@SpringBootApplication
//@EnableMongoRepositories("com.tests4geeks.tutorials.repository")
public class Application {


    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    public void run() {
    }
}
*/

@SpringBootApplication
@EnableAutoConfiguration
@EnableConfigurationProperties
public class Application {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
