package example.repository;

import org.springframework.data.repository.CrudRepository;

import example.model.Car;

public interface CarMongoRepository extends CrudRepository<Car, String>{}