package example.controller;

import example.model.Transaction;
import example.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/transaction")
public class TransactionController {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    TransactionService transactionService;

    //http://localhost:8080/transaction/all
    @GetMapping(value = "/all")
    public List<Transaction> listAllTransactions() {
      return transactionService.findAll();
    }
}
