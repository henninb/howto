package example.service;

import example.model.Transaction;
import example.repository.TransactionJpaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionService {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    TransactionJpaRepository transactionJpaRepository;

    public List<Transaction> findAll() {

        Transaction transaction = new Transaction();

        transaction.setAccountNameOwner("chase_brian");
        transaction.setAccountType("credit");
        transaction.setAmount("10.0");
        transaction.setCategory("restaurant");
        transaction.setDescription("Subway");
        transaction.setNotes("super secret");
        transaction.setDateAdded("8/16/2017");
        transactionJpaRepository.saveAndFlush(transaction);

        return transactionJpaRepository.findAll();
    }
}
