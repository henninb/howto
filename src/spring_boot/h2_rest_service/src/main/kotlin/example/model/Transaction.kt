package example.model

import javax.persistence.*

@Entity
@Table(name = "t_transaction")
class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transaction_id")
    var transactionId: Long? = null
    var guid: String? = null
    var accountType: String? = null
    var accountNameOwner: String? = null
    var transactionDate: String? = null
    var description: String? = null
    var category: String? = null
    var amount: String? = null
    var isCleared: String? = null
    var notes: String? = null
    var dateUpdated: String? = null
    var dateAdded: String? = null
}
