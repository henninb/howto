package example.controllers;

import example.dao.TransactionDAOImpl;
import example.model.Transaction;
import example.services.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/transaction")
public class TransactionController {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    TransactionService transactionService;


    @Autowired
    TransactionDAOImpl transactionDAO;

    //curl --header "Content-Type: application/json" --request POST --data '{"guid":"a064b942-1e78-4913-adb3-b992fc1b4dd3","sha256":"","accountType":"credit","accountNameOwner":"discover_brian","description":"mydescription","category":"","notes":"","cleared":0,"reoccurring":false,"amount":"0.00","transactionDate":1512730594,"dateUpdated":1487332021,"dateAdded":1487332021}' http://localhost:8080/transaction/insert
    @PostMapping("/insert")
    private ResponseEntity<String> insertTransaction(@RequestBody Transaction transaction )  {
        if (transactionService.insertTransaction(transaction) ) {
            return ResponseEntity.ok("transaction inserted");
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    //http://localhost:8080/transaction/all
    //curl http://172.17.0.2:8080/transaction/all
    //curl http://localhost:8080/transaction/all
    @GetMapping(value = "/all")
    public ResponseEntity<List<Transaction>> listTransactionsAll() {
        return ResponseEntity.ok(transactionService.findAll());
    }

    //http://localhost:8080/transaction/delete/0e0a01e2-06b0-4e3f-a15c-5afa86d968ce
    @DeleteMapping(value = "/delete/{guid}")
    public ResponseEntity<String> deleteTransactionByGuid(@PathVariable String guid) {
            Optional<Transaction> optionalTransaction = transactionService.findByGuid(guid);
            if( optionalTransaction.isPresent()) {
                Boolean is_deleted = transactionService.deleteByGuid(guid);
                return ResponseEntity.ok("transaction deleted.");
            } else {
                return ResponseEntity.noContent().build();
            }
    }

    //http://localhost:8080/transaction/select/0e0a01e2-06b0-4e3f-a15c-5afa86d968ce
    @GetMapping(value = "/select_guid/{guid}")
    public ResponseEntity<Transaction> listTransactionByGuid(@PathVariable String guid) {
        Optional<Transaction> optionalTransaction = transactionService.findByGuid(guid);
        if( optionalTransaction.isPresent() ) {
            return ResponseEntity.ok(optionalTransaction.get());
        }
        return ResponseEntity.noContent().build();
    }

    //http://localhost:8080/transaction/select/chase_brian
    @GetMapping(value = "/select/{accountNameOwner}")
    public ResponseEntity<List<Transaction>> listTransactionByDescription(@PathVariable String accountNameOwner) {
        List<Transaction> transactions = transactionDAO.getTransactionByAccountNameOwner(accountNameOwner);
        return ResponseEntity.ok(transactions);
    }
}
