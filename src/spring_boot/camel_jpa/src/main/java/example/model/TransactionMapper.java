package example.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TransactionMapper implements RowMapper<Transaction> {

    public Transaction mapRow(ResultSet resultSet, int i) throws SQLException {

        Transaction transaction = new Transaction();

        transaction.setTransactionId(resultSet.getLong("transaction_id"));
        transaction.setAccountNameOwner(resultSet.getString("account_name_owner"));
        transaction.setGuid(resultSet.getString("guid"));
        transaction.setDescription(resultSet.getString("description"));
        transaction.setCategory(resultSet.getString("category"));
        transaction.setNotes(resultSet.getString("notes"));
        transaction.setAccountType(resultSet.getString("account_type"));
        transaction.setAmount(resultSet.getDouble("amount"));
        transaction.setCleared(resultSet.getInt("cleared"));
        transaction.setReoccurring(resultSet.getBoolean("reoccurring"));

        Date date = resultSet.getDate("transaction_date");

        return transaction;
    }
}
