package example.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import example.util.TransactionDeserializer;
import example.util.TransactionSerializer;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;

@Entity
@Table(name = "t_transaction")
@SequenceGenerator(name="t_transaction_transaction_id_seq")
public class Transaction implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="transaction_id")
    private Long transactionId;
    private String guid;
    private String accountType;
    private String accountNameOwner;
    private Date transactionDate;
    private String description;
    @Column(name="category")
    private String category;
    private Double amount;
    @Column(name="cleared")
    private int cleared;
    @Column(name="notes")
    private String notes;
    private Boolean reoccurring;
    private Timestamp dateUpdated;
    private Timestamp dateAdded;
    private String sha256;

    public Transaction() {
    }

    public Transaction(String guid, String accountType, String accountNameOwner, Date transactionDate, String description, String category, Double amount, int cleared, String notes, Timestamp dateUpdated, Timestamp dateAdded, String sha256, Boolean reoccurring) {
        this.guid = guid;
        this.accountType = accountType;
        this.accountNameOwner = accountNameOwner;
        this.transactionDate = transactionDate;
        this.description = description;
        this.category = category;
        this.amount = amount;
        this.cleared = cleared;
        this.notes = notes;
        this.dateUpdated = dateUpdated;
        this.dateAdded = dateAdded;
        this.sha256 = sha256;
        this.reoccurring = reoccurring;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountNameOwner() {
        return accountNameOwner;
    }

    public void setAccountNameOwner(String accountNameOwner) {
        this.accountNameOwner = accountNameOwner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSha256() {
        return sha256;
    }

    public void setSha256(String sha256) {
        this.sha256 = sha256;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public int getCleared() {
        return cleared;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public void setCleared(int cleared) {
        this.cleared = cleared;
    }

    public Boolean getReoccurring() {
        return reoccurring;
    }

    public void setReoccurring(Boolean reoccurring) {
        this.reoccurring = reoccurring;
    }

    public Timestamp getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Timestamp dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Timestamp getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Timestamp dateAdded) {
        this.dateAdded = dateAdded;
    }
}
