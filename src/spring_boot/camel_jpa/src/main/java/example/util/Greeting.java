package example.util;

interface Greeting {
    String processName(String str);
}