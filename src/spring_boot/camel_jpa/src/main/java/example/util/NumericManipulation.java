package example.util;

public interface NumericManipulation {
    int mathOperations(int x, int y);
}
