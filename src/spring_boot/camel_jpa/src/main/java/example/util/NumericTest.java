package example.util;

public interface NumericTest {
    boolean computeTest(int n);
}
