package example.dao;

import example.model.Transaction;

public interface TransactionDAO {
    Transaction getTraansactionById( Long transactionId );
    Boolean deleteTransactionByGuid( String guid );
}
