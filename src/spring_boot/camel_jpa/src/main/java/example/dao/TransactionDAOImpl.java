package example.dao;

import example.model.Transaction;
import example.model.TransactionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
public class TransactionDAOImpl implements TransactionDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String SQL_FIND_TRANSACTION = "select * from t_transaction where transaction_id = ?";
    private final String SQL_FIND_TRANSACTION_BY_ACCOUNT_NAME_OWNER = "SELECT * FROM t_transaction WHERE account_name_owner = ?";
    private final String SQL_DELETE_TRANSACTION = "delete from t_transaction where transaction_id = ?";
    private final String SQL_UPDATE_PERSON = "update people set first_name = ?, last_name = ?, age  = ? where id = ?";
    private final String SQL_GET_ALL = "select * from people";
    private final String SQL_INSERT_PERSON = "insert into people(id, first_name, last_name, age) values(?,?,?,?)";

    public Transaction getTraansactionById(Long transaction_id) {
        return jdbcTemplate.queryForObject(SQL_FIND_TRANSACTION, new Object[] { transaction_id }, new TransactionMapper());
        //return new Transaction();
    }

    public List<Transaction> getTransactionByAccountNameOwner(String accountNameOwner) {
        return jdbcTemplate.query(SQL_FIND_TRANSACTION_BY_ACCOUNT_NAME_OWNER, new Object[] { accountNameOwner }, new TransactionMapper());
    }

    public Boolean deleteTransactionByGuid( String guid ) {
        //jdbcTemplate.query(SQL_DELETE_TRANSACTION);

        return true;
    }

    @Transactional
    public List<Transaction> findAll() {
        return jdbcTemplate.query("select * from t_transaction", new TransactionMapper());
    }
}
