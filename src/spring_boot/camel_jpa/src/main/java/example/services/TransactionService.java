package example.services;

import example.model.Transaction;
import example.repositories.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TransactionService {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    TransactionRepository transactionRepository;

    public List<Transaction> findAll() {
        return transactionRepository.findAll();
    }

    public Boolean insertTransaction(Transaction transaction) {
        Optional<Transaction> optionalTransaction = findByGuid(transaction.getGuid());
        if( optionalTransaction.isPresent()) {
            return false;
        } else {
            transactionRepository.saveAndFlush(transaction);
            return true;
        }
    }

    public Boolean deleteAll() {
        transactionRepository.deleteAll();
        return true;
    }

    public Optional<Transaction> findByGuid(String guid) {
        Optional<Transaction> optionalTransaction = transactionRepository.findByGuid(guid);
        return optionalTransaction;
    }

    public Boolean deleteByGuid( String guid) {
        Optional<Transaction> optionalTransaction = findByGuid(guid);
        if( optionalTransaction.isPresent()) {
            transactionRepository.delete(optionalTransaction.get());
            return true;
        }
        return false;
    }
}
