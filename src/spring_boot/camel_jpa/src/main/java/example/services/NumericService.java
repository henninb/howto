package example.services;

import example.util.NumericManipulation;
import example.util.NumericTest;
import org.springframework.stereotype.Service;

@Service
public class NumericService {
    NumericTest isEven = (n) -> (n % 2) == 0;
    NumericTest isNegative = (n) -> (n < 0);

    NumericManipulation addIntegers = (x,y) -> x + y;
    NumericManipulation multIntegers = (x,y) -> x * y;

    public void NumericExecute() {
        System.out.println(isEven.computeTest(5));
        System.out.println(isNegative.computeTest(-5));
        System.out.println(addIntegers.mathOperations(3,4));
        System.out.println(multIntegers.mathOperations(3,4));
    }
}
