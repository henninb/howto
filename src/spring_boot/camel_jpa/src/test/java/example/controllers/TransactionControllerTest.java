package example.controllers;

import example.TestTransactionData;
import example.model.Transaction;
import example.services.TransactionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
//@TestPropertySource(locations = "classpath:application_test.properties")
public class TransactionControllerTest {

    @Mock
    TransactionService transactionService;

    private Transaction transactionDummy;
    private List<Transaction> transactionsDummy;

    @Before
    public void setup() {
        TestTransactionData testTransactionData = new TestTransactionData();
        transactionDummy = testTransactionData.setOneTransaction();
        transactionsDummy = testTransactionData.getTransactionList();

        Mockito.when(transactionService.findAll()).thenReturn(transactionsDummy);
        Mockito.doNothing().when(transactionService).insertTransaction(transactionDummy);
    }

    @Test
    public void testListAllTransactions() throws Exception {
        transactionsDummy = transactionService.findAll();
        assertEquals(transactionsDummy.size(), 2);
    }


}