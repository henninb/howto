package example;

import example.model.Transaction;
import org.jetbrains.annotations.NotNull;

import java.sql.Date;
import java.util.*;

public class TestTransactionData {

    public Transaction setOneTransaction() {

        Transaction transactionDummy = new Transaction();


        transactionDummy.setSha256("123348");
        transactionDummy.setTransactionDate(new Date(1513820227));
        transactionDummy.setGuid("f7846484-7cdc-4ed1-b665-c5fb96e9aa64");
        transactionDummy.setAccountNameOwner("chase_brian");
        transactionDummy.setAccountType("credit");
        transactionDummy.setAmount(10.15);
        transactionDummy.setCleared(1);
        transactionDummy.setCategory("restaurant");
        transactionDummy.setDescription("Subway");
        transactionDummy.setNotes("super secret");
        transactionDummy.setDateAdded(new Date(1513820227));
        transactionDummy.setDateUpdated(new Date(1513821227));

       return transactionDummy;
    }

    public List<Transaction> getTransactionList() {
        //List<Transaction> transactionsDummy = new List<Transaction>[2];
        ArrayList<Transaction> transactionsDummy = new ArrayList<Transaction>();
        //Transaction[] transactionsDummy = new Transaction[2];
        //transactionsDummy[0] = setOneTransaction();
        //transactionsDummy[1] = setOneTransaction();
        transactionsDummy.add(setOneTransaction());
        transactionsDummy.add(setOneTransaction());
        return transactionsDummy;
    }
}
