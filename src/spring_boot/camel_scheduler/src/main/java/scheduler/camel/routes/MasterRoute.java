package scheduler.camel.routes;

import scheduler.processor.MasterProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Component
public class MasterRoute extends RouteBuilder {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    MasterProcessor masterProcessor;

    @Override
    public void configure() throws Exception {

        //from("jpa://scheduler.model.Server?consumer.nativeQuery=SELECT server_id AS serverId, server_name AS serverName, server_port as serverPort, active_flag as activeFlag FROM t_server"
        from("jpa://scheduler.model.Schedule?consumer.nativeQuery=SELECT schedule_id as scheduleId, schedule_name as scheduleName, job_id as jobId, server_id as serverId, cyclic_flag as cyclicFlag, status_flag as statusFlag FROM t_schedule where status_flag = 'U'"
        //from("sql://SELECT schedule_id as scheduleId, schedule_name as scheduleName, job_id as jobId, server_id as serverId, cyclic_flag as cyclicFlag, status_flag as statusFlag FROM t_schedule where status_flag = 'U'"
        //from("sql://SELECT scheduleId, scheduleName, jobId, serverId, cyclicFlag, statusFlag FROM t_schedule"
        //from("jdbc://SELECT scheduleId, scheduleName, jobId, serverId, cyclicFlag, statusFlag FROM t_schedule"
                        +"&consumer.initialDelay=1000"
                        +"&consumer.delay=3000"
                        +"&consumeDelete=true"
                        //+"&outputType=SelectOne"
                        //+"&outputType=SelectList"
                        +"&consumer.resultClass=scheduler.model.Schedule"
                )
                //.split().body()

                //java DSL
                        .autoStartup(true)
                //.stopOnException(true)
                //.dynamicRouter()
                        .log("Select query executed.")
                        .process(masterProcessor)
                        //.to("direct:publishJobToFile")
                //.to("jpa://usePassedInEntityManager=true&consumer.nativeQuery=UPDATE t_schedule set status_flag='S' WHERE schedule_id=1")
                //.to("sql://UPDATE t_schedule set status_flag='S' WHERE schedule_id=1")
                .log("masterProcessor called")
                //.to("direct:publishJobToFile")
                .setBody(constant("features:list"))
                .to("direct:exampleSshProducer")
                .log("body=${body}")
                .log("exampleSshProducer called")
                //.setBody(constant("token1;token2;token3"))
                //.to("direct:start")
                //.sendBody("direct:start", "token1;token2;token3")
                .log("start called")
                //.setBody(constant("SELECT scheduleId, scheduleName, jobId, serverId, cyclicFlag, statusFlag FROM t_schedule"))
                //.to("jdbc:h2:mem:scheduler_db")
                .log("setBody")
                        //.exchange.getIn().setBody(list, ArrayList.class);
                        //.to("direct:publishToKafkaRoute")
                        //.to("file:logs?fileName=KafkaSimulator.txt&autoCreate=true&fileExist=Append")
                        //.log("INFO: Publish success. ${header.guid} :#guid")
                        //.log("UPDATE t_transaction SET kafka_flag = 'Y' WHERE guid = '${header.guid}'")
                        //.to("sql:UPDATE t_transaction SET kafka_flag = 'Y' WHERE guid = ':#guid'")
                 .end()
                 ;
    }
}


/*
    <bean id="resourceKeyPairProvider"
          class="org.example.ResourceKeyPairProviderFactory"
          factory-method="createResourceKeyPairProvider">
        <constructor-arg>
            <array value-type="java.lang.String">
                <value>test_rsa</value>
            </array>
        </constructor-arg>
    </bean>
            <route id="camel-example-ssh-consumer-secure" autoStartup="true">
            <from uri="ssh://scott@localhost:8101?keyPairProvider=#resourceKeyPairProvider&amp;useFixedDelay=true&amp;delay=5000&amp;pollCommand=features:list%0A"/>
            <log message="${body}"/>
        </route>
 */
