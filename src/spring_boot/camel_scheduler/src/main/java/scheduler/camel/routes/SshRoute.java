package scheduler.camel.routes;

import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
//import org.springframework.beans.factory.annotation.Autowired;
//import scheduler.processor.MasterProcessor;

@Component
public class SshRoute extends RouteBuilder {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Override
    public void configure() throws Exception {

        String userName = "henninb";
        String serverName = "localhost";
        String serverPort = "22222";
        String serverCommand = "date;uptime;uname";

        from ("direct:exampleSshProducer")
                //.to("ssh://" + userName + "@" + serverName + ":" + serverPort + "?certResource=classpath:id_rsa_home&useFixedDelay=true&delay=5000&pollCommand=uptime")
                .to("ssh://henninb@localhost:22222?certResource=classpath:id_rsa_home&useFixedDelay=true&delay=5000&pollCommand=uptime%0A")
                .process(exchange -> {
                    System.out.println("info: " + exchange.getIn().getBody(String.class));
                })
                //.onCompletion()
                .autoStartup(true)
                .log("body = ${body}")
                .end()
        ;

        //from("ssh://henninb@localhost:22222?certResource=classpath:id_rsa_home&useFixedDelay=true&delay=5000&pollCommand=date;uptime;uname")
        //        .log("${body}");


    }
}
