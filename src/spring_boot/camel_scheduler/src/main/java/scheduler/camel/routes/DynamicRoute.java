package scheduler.camel.routes;

import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scheduler.model.Message;
import scheduler.processor.MasterProcessor;

@Component
public class DynamicRoute extends RouteBuilder {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());


    @Override
    public void configure() throws Exception {
        from("direct:start")
                .log("got here")
                .split(body().tokenize(";"))
                .recipientList()
                .method(Message.class);

        from("direct:token1")
                .log("Token1: body = ${body}");

        from("direct:token2")
                .log("Token2: body = ${body}");

        from("direct:default")
                .log("Token3: body = ${body}");
    }
}
