package scheduler.camel.routes;


import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scheduler.processor.MasterProcessor;

import java.io.File;

@Component
public class FileRoute extends RouteBuilder {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    MasterProcessor jsonProcessor;

    @Override
    public void configure() throws Exception {
        from("direct:publishJobToFile")
        .to("file:" + File.separator + "scheduler_in?delete=true&moveFailed=" + File.separator + "scheduler_in" + File.separator + "camel_errors&include=.*.dat")
                .autoStartup(true)
                .log("processJob")
        ;
    }
}
