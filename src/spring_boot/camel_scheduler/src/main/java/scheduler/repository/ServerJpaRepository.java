package scheduler.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import scheduler.model.Server;

public interface ServerJpaRepository extends JpaRepository<Server, Long> {

    public Server findByServerId(int serverId);

}
