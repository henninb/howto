package scheduler.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import scheduler.model.Schedule;

public interface ScheduleJpaRepository extends JpaRepository<Schedule, Long> {

}
