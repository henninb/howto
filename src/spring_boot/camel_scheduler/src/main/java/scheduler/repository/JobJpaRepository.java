package scheduler.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import scheduler.model.Job;

public interface JobJpaRepository extends JpaRepository<Job, Long> {

    Job findByJobId(int JobId);

}
