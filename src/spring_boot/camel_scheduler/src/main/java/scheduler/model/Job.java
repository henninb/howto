package scheduler.model;

import javax.persistence.*;

@Entity
@Table(name = "t_job")
public class Job {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="job_id")
    int jobId;
    String jobName;
    String jobCmd;
    char activeFlag;

    public int getJob_id() {
        return jobId;
    }

    public void setJob_id(int jobId) {
        this.jobId = jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobCmd() {
        return jobCmd;
    }

    public void setJobCmd(String jobCmd) {
        this.jobCmd = jobCmd;
    }

    public char getActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(char activeFlag) {
        this.activeFlag = activeFlag;
    }
}
