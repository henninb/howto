package scheduler.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Message {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    public String routeTo(final String row) {
        String id;
        if (row.contains("1")) {
            LOGGER.info("sub1");
            id = "sub1";
        } else if (row.contains("2")) {
            LOGGER.info("sub2");
            id = "sub2";
        } else {
            LOGGER.info("default");
            id = "default";
        }
        return "direct:" + id;
    }
}
