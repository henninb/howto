package scheduler.model;

import javax.persistence.*;

@Entity
@Table(name = "t_schedule")
public class Schedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="schedule_id")
    int scheduleId;

    String scheduleName;
    int jobId;
    int serverId;
    char cyclicFlag;
    char statusFlag;

    public int getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(int scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public char getCyclicFlag() {
        return cyclicFlag;
    }

    public void setCyclicFlag(char cyclicFlag) {
        this.cyclicFlag = cyclicFlag;
    }

    public char getStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(char statusFlag) {
        this.statusFlag = statusFlag;
    }
}
