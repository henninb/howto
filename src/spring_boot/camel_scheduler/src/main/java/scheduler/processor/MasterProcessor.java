package scheduler.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import scheduler.model.Schedule;
import scheduler.model.Server;
import scheduler.service.RetrieveSchedules;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class MasterProcessor implements Processor {
    private static ObjectMapper mapper = new ObjectMapper();
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    public void setProducer(ProducerTemplate producer) {
        System.out.println("We got here: setProducer().");
        this.producer = producer;
    }

    ProducerTemplate producer;

    @Autowired
    RetrieveSchedules retrieveSchedules;

    @Override
    public void process(Exchange exchange) throws Exception {
        try {

            retrieveSchedules.findAllServers();
            retrieveSchedules.findAllJobs();
            retrieveSchedules.findUnprocessedSchedules();

            Object o = exchange.getIn().getBody();
           //instance.getClass().getName();
            LOGGER.info("class: " + o.getClass().getName());

            Schedule schedule = (Schedule) exchange.getIn().getBody();
            LOGGER.info("Schedule Body: " + mapper.writeValueAsString(schedule));
            exchange.getIn().setBody(schedule);

            //CamelContext context = new DefaultCamelContext();
            // need to figure this out
            //ProducerTemplate template = context.createProducerTemplate();
            //String reply = producer.requestBody("direct:start", "login:bot", String.class);
            //System.out.println(reply);


            //Exchange exchange = context.getEndpoint("direct:start").createExchange();
            //exchange.getIn().setBody("Hello");

            //exchange.getIn().setBody("<hello>world!</hello>");

           // producerTemplate.sendBodyAndHeader(
           //         "sftp://_target_machine_url_?username=_username_"
           //                 "&privateKeyFile=" + sshKeyPath
           //                 "&knownHostsFile=" + knownHosts
           //                 "&preferredAuthentications=publickey",
           //         new ByteArrayInputStream(new String("Random junk").getBytes()),
           //         "CamelFileName", "/home/_username_/test_file.txt"
           // );

            //exchange.getIn().setHeader("guid", server.getGuid());
            //Server server = new Server();
            //exchange.getIn().setHeader("guid", "123");
            //exchange.getIn().setBody(server);
        } catch (Exception ex){
            LOGGER.info("Failure in MasterProcessor.");
            throw ex;
        }
        LOGGER.info("INFO: MasterProcessor completed.");
    }
}
