package scheduler.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import scheduler.model.Job;
import scheduler.model.Schedule;
import scheduler.model.Server;
import scheduler.repository.JobJpaRepository;
import scheduler.repository.ScheduleJpaRepository;
import scheduler.repository.ServerJpaRepository;

@Service
public class RetrieveSchedules {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ServerJpaRepository serverRepository;

    @Autowired
    JobJpaRepository jobRepository;

    @Autowired
    ScheduleJpaRepository scheduleRepository;

    public void findAllServers(){

        List<Server> servers = serverRepository.findAll();

        for(Server svr : servers) {
            System.out.println("Server Name is " + svr.getServerName());
        }
    }

    public Server findServerById(int serverId){
        Server returnedServer = new Server();
        returnedServer = serverRepository.findByServerId(serverId);
        return returnedServer;
    }

    public void findAllJobs() {

        List<Job> jobs = jobRepository.findAll();

        for(Job job : jobs) {
            System.out.println("Job Name is " + job.getJobName());
        }
    }

    public Job findByJobId(int jobId){
        Job returnedJob = new Job();
        returnedJob = jobRepository.findByJobId(jobId);
        return returnedJob;
    }

    public void findUnprocessedSchedules() {

        List<Schedule> schedules = scheduleRepository.findAll();

        for(Schedule schedule : schedules) {
            if( schedule.getStatusFlag() == 'U') {
                Server server = new Server();
                Job job = new Job();

                job = findByJobId(schedule.getJobId());
                server = findServerById(schedule.getScheduleId());

                System.out.println("The Server is: " + server.getServerName());
                System.out.println("The Server Port is: " + server.getServerPort());
                System.out.println("The job Command is: " + job.getJobCmd());
            }
        }
    }
}
