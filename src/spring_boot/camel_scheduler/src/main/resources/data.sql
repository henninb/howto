INSERT INTO t_server(server_name, server_port, active_flag) VALUES('localhost', 222222, 'Y');
INSERT INTO t_server(server_name, server_port, active_flag) VALUES('some_server', 222222, 'N');
INSERT INTO t_job(job_name, job_cmd, active_flag) VALUES('ServerUptime', 'uptime', 'Y');
INSERT INTO t_job(job_name, job_cmd, active_flag) VALUES('Server time', 'date', 'Y');

--INSERT INTO t_schedule(schedule_name, job_id, server_id, cyclic_flag, status_flag ) VALUES('first_run', 1, 2, 'N', 'U');

INSERT INTO t_schedule (schedule_name, job_id, server_id, cyclic_flag, status_flag)
SELECT 'first_run', A.server_id, B.job_id, 'N', 'U' FROM t_server A, t_job B WHERE A.server_name = 'localhost' AND job_name='ServerUptime';

--select B.serverName from t_schedule A, t_server B where A.status_flag = 'U' and  A.server_id = B.server_id;


--select schedule_id as scheduleId, schedule_name as scheduleName, job_id as jobId, server_id as serverId from t_schedule where status_flag = 'U';
