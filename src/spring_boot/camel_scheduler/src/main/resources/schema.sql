CREATE TABLE t_server (
  server_id INTEGER primary key auto_increment not null,
  server_name VARCHAR(75) NOT NULL,
  server_port INTEGER DEFAULT 22,
  active_flag CHAR(1) DEFAULT 'N'
);

CREATE TABLE t_job (
  job_id INTEGER primary key auto_increment not null,
  job_name VARCHAR(75) NOT NULL,
  job_cmd VARCHAR(150) NOT NULL,
  active_flag CHAR(1) DEFAULT 'N'
);

CREATE TABLE t_schedule (
  schedule_id INTEGER primary key auto_increment not null,
  schedule_name VARCHAR(75) NOT NULL,
  job_id INTEGER,
  server_id INTEGER,
  cyclic_flag char(1) DEFAULT 'N',
  status_flag CHAR(1) DEFAULT 'U'
);

CREATE TABLE t_schedule_w (
  schedule_id INTEGER,
  schedule_name VARCHAR(75),
  job_id INTEGER,
  server_id INTEGER,
  cyclic_flag char(1),
  status_flag CHAR(1)
);
