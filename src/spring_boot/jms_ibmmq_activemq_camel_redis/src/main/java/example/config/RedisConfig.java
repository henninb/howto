package example.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfig {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Bean
    public StringRedisSerializer stringRedisSerializer() {
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        return stringRedisSerializer;
    }

//    @Bean
//    public RedisTemplate<String, String> redisTemplate(RedisConnectionFactory factory) {
//        StringRedisTemplate template = new StringRedisTemplate(factory);
//    }
//
//    @Bean
//    public RedisTemplate<Object, Object> sessionRedisTemplate(
//            RedisConnectionFactory connectionFactory) {
//        RedisTemplate<Object, Object> template = new RedisTemplate<Object, Object>();
//        template.setKeySerializer(new StringRedisSerializer());
//        template.setHashKeySerializer(new StringRedisSerializer());
//        template.setDefaultSerializer(new StringRedisSerializer());
//        template.setConnectionFactory(connectionFactory);
//        return template;
//    }
//
//    @Autowired
//    private StringRedisTemplate stringRedisTemplate;
//
//    @Bean
//    public CacheManager cacheManager() {
//        RedisCacheManager cacheManager = new RedisCacheManager(stringRedisTemplate);
//
//       // RedisCachePrefix cachePrefix = new RedisPrefix("prefix");
//
//        return cacheManager;
//    }
//
//    @Bean
//    public RedisConnectionFactory connectionFactory() {
//        JedisConnectionFactory factory = new JedisConnectionFactory();
//        factory.setHostName("localhost");
//        factory.setPort(6379);
//        factory.setUsePool(true);
//        return factory;
//    }
//
//    @Bean
//    public RedisTemplate<String, ?> createRedisTemplateForEntity() {
//        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<String, Object>();
//        redisTemplate.setConnectionFactory(connectionFactory());
//        redisTemplate.setHashValueSerializer(new StringRedisSerializer());
//        redisTemplate.setKeySerializer(new StringRedisSerializer());
//        redisTemplate.setValueSerializer(new StringRedisSerializer());
//        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
//        redisTemplate.afterPropertiesSet();
//
//        return redisTemplate;
//    }

/*
    @Bean
    public RedisTemplate redisTemplate() {
        JedisConnectionFactory factory = new JedisConnectionFactory();
        RedisTemplate redisTemplate = new RedisTemplate();
        redisTemplate.setConnectionFactory(connectionFactory());
        return redisTemplate;
    }

    @Bean
    public RedisTemplate stringRedisTemplate() {
        RedisTemplate redisTemplate = new StringRedisTemplate();


        redisTemplate.setConnectionFactory(connectionFactory());




        return redisTemplate;
    }
*/
    /*
    @Bean
    public StringRedisTemplate stringRedisTemplate() {
        StringRedisTemplate stringRedisTemplate = new StringRedisTemplate();
        return stringRedisTemplate;
    }
*/
    /*
<bean id="stringRedisTemplate" class="org.springframework.data.redis.core.StringRedisTemplate">
    <property name="connectionFactory" ref="jedisConnectionFactory" />
</bean>

    @Bean
    RedisOperationsSessionRepository sessionRepository() {
        RedisTemplate<Object, Object> template = new RedisTemplate<>();
        template.setKeySerializer(new StringRedisSerializer());
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setConnectionFactory(connectionFactory());
        template.setDefaultSerializer(new CustomRedisSerializer());
        template.afterPropertiesSet();

        RedisOperationsSessionRepository repository = new RedisOperationsSessionRepository(template);
        repository.setDefaultSerializer(new CustomRedisSerializer());
        return repository;
    }

    @Bean
    RedisTemplate<String, Object> redisTemplate() {
        final RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory());
        template.setKeySerializer(new StringRedisSerializer());
        template.setHashValueSerializer(new GenericToStringSerializer<>( Object.class ));
        template.setValueSerializer(new GenericToStringSerializer<>( Object.class ));
        return template;
    }
    */

 //   @Bean
//    RedisSerializer<Object> springSessionDefaultRedisSerializer() {
//        return new CustomRedisSerializer();
//    }


    //    @Bean
//    //@ComponentScan(basePackages = {"example"})
//    JedisConnectionFactory jedisConnectionFactory() {
//        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration("localhost", 6379);
//        //redisStandaloneConfiguration.setPassword(RedisPassword.of("yourRedisPasswordIfAny"));
//        return new JedisConnectionFactory(redisStandaloneConfiguration);
//    }

    /*
    @Bean(name="redisdb")
    public RedisConnectionFactory jedisConnectionFactory() {
        //LOGGER.info("redisConfig ", this.getClass().toString(), redisHost);
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        JedisConnectionFactory ob = new JedisConnectionFactory(poolConfig);
        ob.setUsePool(true);
        ob.setHostName("localhost");
        ob.setPort(6379);
        return ob;
    }
*/
/*
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Bean
    public StringRedisSerializer stringRedisSerializer() {
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        return stringRedisSerializer;
    }


    @Bean
    public CacheManager cacheManager() {
        RedisCacheManager cacheManager = new RedisCacheManager(redisTemplate);
        return cacheManager;
    }
*/




/*


<bean
    id="myStringSerializer"
    class="org.springframework.data.redis.serializer.StringRedisSerializer"/>

RedisCacheManager

private RedisTemplate<String, ?> createRedisTemplateForEntity() {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<String, Object>();
        redisTemplate.setConnectionFactory(getRedisConnectionFactory());
        redisTemplate.setHashValueSerializer(new StringRedisSerializer());
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new StringRedisSerializer());
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.afterPropertiesSet();

    return redisTemplate;
}


    @Bean(name="redisTemplate")
    public RedisTemplate<String, String> redisTemplate(RedisConnectionFactory factory) {

        RedisTemplate<String, String> template = new RedisTemplate<>();
        RedisSerializer<String> redisSerializer = new StringRedisSerializer();

        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);

        template.setConnectionFactory(factory);
        //key
        template.setKeySerializer(redisSerializer);
        //value
        template.setValueSerializer(jackson2JsonRedisSerializer);
        //value hashmap
        template.setHashValueSerializer(jackson2JsonRedisSerializer);

        return template;
    }
*/
}
