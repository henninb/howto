package example.route;

//public class JacksonDataFormat extends org.apache.camel.model.DataFormatDefinition implements org.apache.camel.spi.DataFormat {
//}

import org.apache.camel.Exchange;
import org.apache.camel.model.DataFormatDefinition;
import org.apache.camel.spi.DataFormat;

import java.io.InputStream;
import java.io.OutputStream;

public class JacksonDataFormat extends DataFormatDefinition { //implements DataFormat {

//    @Override
//    public void marshal(Exchange exchange, Object graph, OutputStream stream) throws Exception {
//        System.out.println("Marshal");
//        byte[] bytes = exchange.getContext().getTypeConverter().mandatoryConvertTo(byte[].class, graph);
//        stream.write(bytes);
//    }
//
//    @Override
//    public Object unmarshal(Exchange exchange, InputStream stream)
//            throws Exception {
//        System.out.println("Unmarshal");
//        byte[] bytes = exchange.getContext().getTypeConverter().mandatoryConvertTo(byte[].class, stream);
//        return bytes;
//    }

    public void marshal(Exchange exchange, Object graph, OutputStream stream) throws Exception {
        byte[] bytes = exchange.getContext().getTypeConverter().mandatoryConvertTo(byte[].class, graph);
        String body = "hello world";
        stream.write(body.getBytes());
    }

    public Object unmarshal(Exchange exchange, InputStream stream) throws Exception {
        byte[] bytes = exchange.getContext().getTypeConverter().mandatoryConvertTo(byte[].class, stream);
        String body = "hello";
        return body;
    }
}