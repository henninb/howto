package example.route;

import example.exception.XPathNotFoundException;
import example.processor.RedisDataProcessor;
import example.processor.RedisDataQueueProcessor;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

@Configuration
@Component
public class MasterRoute extends RouteBuilder {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Value("${project.message.technology}")
    private String messageTechnology;
    //private String messageTechnology = "wmq";

    @Value("${project.http.maximum.redeliveries}")
    private Integer httpMaximumRedeliveries;
    //private Integer httpMaximumRedeliveries = 3;

    @Value("${project.http.redelivery.delay}")
    private Integer httpRedeliveryDelay;
    //private Integer httpRedeliveryDelay = 30000;

    @Value("${project.mq.maximum.redeliveries}")
    private Integer mqMaximumRedeliveries;
    //private Integer mqMaximumRedeliveries = 3;

    @Value("${project.http.redelivery.delay}")
    private Integer mqRedeliveryDelay;
    //private Integer mqRedeliveryDelay = 30000;

    @Value("${project.mq.request.queue}")
    private String mqRequestQueueName;
    //private String mqRequestQueueName = "queue_name";

    @Value("${project.mq.backout.queue}")
    private String mqBackoutQueueName;
    //private String mqBackoutQueueName = "queue_backout";

    @Value("${project.mq.error.queue}")
    private String mqErrorQueueName;
    //private String mqErrorQueueName = "queue_error";

    @Value("${project.mq.concurrent.consumers}")
    private Integer mqConcurrentConsumers;
    //private Integer mqConcurrentConsumers=8;

    @Value("${project.mq.max.concurrent.consumers}")
    private Integer mqMaxConcurrentConsumers;
    //private Integer mqMaxConcurrentConsumers=16;


    @Autowired
    private StringRedisTemplate redisTemplate;


    @Autowired
    private RedisDataQueueProcessor redisDataQueueProcessor;

    @Autowired
    private RedisDataProcessor redisDataProcessor;

    public MasterRoute() {
    }

    @Override
    public void configure() throws Exception {


        //Registry.put("serializer", new StringRedisSerializer());
        //TODO: for MQException or IllegalStateException what if any code changees need to be made in the onException blocks below.
        //        //onException(com.ibm.mq.MQException.class)
        //        onException(org.springframework.jms.IllegalStateException.class)

        //data errors
        //org.apache.camel.TypeConversionException: Error during type conversion from type: java.lang.String to the required type: org.w3c.dom.Document with value
        //org.apache.camel.processor.validation.SchemaValidationException: Validation failed for: com.sun.org.apache.xerces.internal.jaxp.validation.SimpleXMLSchema
        onException(org.apache.camel.processor.validation.SchemaValidationException.class)
        .onException(org.apache.camel.TypeConversionException.class)
        //.onException(MessageType2Exception.class)
        //.onException(MessageType1Exception.class)
        .onException(XPathNotFoundException.class).useOriginalMessage()
                .handled(true)
                .log(LoggingLevel.ERROR, "${exception.class} :: ${exception.message}")
                .doTry()
                .to(messageTechnology + ":queue:" + mqErrorQueueName + "")
                .doCatch(Exception.class)
                .log(LoggingLevel.ERROR, "${exception.class} :: ${exception.message}")
                .doTry()
                //.to("file:in" + File.separator + ".failedFiles")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        System.exit(253);
                    }
                })
                .stop()
                .doCatch( Exception.class)
                .log(LoggingLevel.ERROR, "${exception.class} :: ${exception.message}")
                .log(LoggingLevel.ERROR, "ERROR: really bad scenario, we may need to abort.")
                .end();

        //connection errors
        //java.net.ConnectException) caught when processing request: Connection refused
        //org.apache.camel.http.common.HttpOperationFailedException :: HTTP operation failed invoking http://localhost:8080 with statusCode: 500
        //onException(org.apache.camel.http.common.HttpOperationFailedException.class)
        onException(java.net.ConnectException.class).useOriginalMessage()
                .maximumRedeliveries(httpMaximumRedeliveries)
                .redeliveryDelay(httpRedeliveryDelay)
                .retryAttemptedLogLevel(LoggingLevel.INFO)
                .maximumRedeliveries(httpMaximumRedeliveries).handled(true)
                .log(LoggingLevel.ERROR, "${exception.class} :: ${exception.message}")
                .doTry()
                .to(messageTechnology + ":queue:" + mqBackoutQueueName + "")
                .doCatch(Exception.class)
                .log(LoggingLevel.ERROR, "${exception.class} :: ${exception.message}")
                .doTry()
                //.to("file:in" + File.separator + ".failedFiles")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        System.exit(254);
                    }
                })
                .stop()
                .doCatch( Exception.class)
                .log(LoggingLevel.ERROR, "${exception.class} :: ${exception.message}")
                .log(LoggingLevel.ERROR, "ERROR: really bad scenario, we may need to abort.")
                .end();

        //default execption
        onException(Exception.class).useOriginalMessage()
                .maximumRedeliveries(1)
                .redeliveryDelay(1000)
                .retryAttemptedLogLevel(LoggingLevel.INFO)
                .maximumRedeliveries(1).handled(true)
                .log(LoggingLevel.ERROR, "${exception.class} :: ${exception.message}")
                .log("${exception.}")
                .doTry()
                .to(messageTechnology + ":queue:" + mqBackoutQueueName + "")
                .doCatch(Exception.class)
                .log(LoggingLevel.ERROR, "${exception.class} :: ${exception.message}")
                .doTry()
                //.to("file:in" + File.separator + ".failedFiles")
                .process(new Processor() {
                        @Override
                        public void process(Exchange exchange) throws Exception {
                            System.exit(255);
                        }
                })
                .stop()
                .doCatch( Exception.class)
                  .log(LoggingLevel.ERROR, "${exception.class} :: ${exception.message}")
                  .log(LoggingLevel.ERROR, "ERROR: Unhandled exception, application needs to abort.")
                .end();

        //Functional with transactions
        from(messageTechnology + ":queue:" + mqRequestQueueName + "?concurrentConsumers=" + mqConcurrentConsumers + "&maxConcurrentConsumers=" + mqMaxConcurrentConsumers)
        //from("file:in?delete=true&moveFailed=.failed") //used for testing only (file input)
                .routeId("QueueRoute")
                .autoStartup(true)
                .log(LoggingLevel.INFO, "body=${body}")
                .removeHeaders("*")

                //.setHeader("CamelRedis.Channel", constant("mychannel"))
                //.setHeader("CamelRedis.Message", constant("mymessage1"))
                .process(redisDataQueueProcessor)   .id("redisDataQueueProcessor : Send to Redis")
                .to("spring-redis://localhost:6379?command=PUBLISH&serializer=#stringRedisSerializer")
                //does not work for publish
                //.to("spring-redis://localhost:6379?serializer=#stringRedisSerializer")

//                .process(redisDataProcessor)
//                .to("spring-redis://localhost:6379?serializer=#stringRedisSerializer")

                //.setHeader("CamelRedis.Key", constant("name"))
                //.setHeader("CamelRedis.Value", constant("${body}"))
                //.to("spring-redis://localhost:6379?command=SET&serializer=#stringRedisSerializer")

                .log(LoggingLevel.INFO, "body=${body}")
                // No longer required
                //.to("file:in" + File.separator + ".originalMessage?fileName=file_${bean:uuidGenerator.generateId}.txt")
                //.process(exchange -> exchange.getIn().setHeader("originalMessage", exchange.getIn().getBody(String.class)))
                //.setHeader("messageName").xpath("/root/header/@messageName", String.class)
                //.to()
                //.choice()
                //.when()
//                  .xpath("/root/header[messageName='MessageType1']")
//                  //.to("validator:xsd/MessageType1.xsd")
//                  //.to("xslt:xslt/MessageType1.xslt")
//                  //.to("file:in" + File.separator + ".SoapRequestMessageType1")
//                  .log(LoggingLevel.INFO, "INFO: requested MessageType1")
//                  .to("http://localhost:8080")
//                  .log(LoggingLevel.INFO, "INFO: response MessageType1")
//                  //.process(MessageType1SoapProcessor)
//                  .log(LoggingLevel.INFO, "INFO: soapResponseBody=${body}")
//                  //.to("file:in" + File.separator + ".SoapResponseMessageType1")
//                .when()
//                  .xpath("/root/header[messageName='MessageType2']")
//                  //.to("validator:xsd/MessageType2.xsd")
//                  //.to("xslt:xslt/MessageType2.xslt")
//                  //.to("file:in" + File.separator + ".SoapRequestMessageType2")
//                  .log(LoggingLevel.INFO,"INFO: requested MessageType2")
//                  .to("http://localhost:8080")
//                  .log(LoggingLevel.INFO,"INFO: response MessageType2")
//                  //.process(MessageType2SoapProcessor)
//                  .log(LoggingLevel.INFO, "INFO: soapResponseBody=${body}")
//                  //.to("file:in" + File.separator + ".SoapResponseMessageType2")
//                .otherwise()
//                  //.to("file:in" + File.separator + ".xpathNotFound")
//                  .log(LoggingLevel.WARN, "WARN: xpath not found for the message.")
//                  .throwException(XPathNotFoundException.class,"xpath not found for the message.")
                .end();
    }
}
