package example.processor;


//import org.apache.camel.Processor;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RedisDataProcessor implements Processor {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Override
    public void process(Exchange exchange) throws Exception {
        LOGGER.info("starting");
        String payload = exchange.getIn().getBody(String.class);

        LOGGER.info("payload: " + payload);
        exchange.getOut().setHeader("command", "SET");
        exchange.getOut().setHeader("CamelRedis.Key", UUID.randomUUID().toString());
        exchange.getOut().setHeader("CamelRedis.Value", payload);

//        exchange.getOut().setHeader("command", "PUBLISH");
//        exchange.getOut().setHeader("CamelRedis.Channel", "mychannel");
//        exchange.getOut().setHeader("CamelRedis.Message", payload);
        LOGGER.info("ending");
    }
}
