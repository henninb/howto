package example.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RedisDataQueueProcessor implements Processor {
    //Logger logger = LoggerFactory.getLogger(RedisReformatProcessor.class);
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Override
    public void process(Exchange exchange) throws Exception {
        LOGGER.info("starting");
        String payload = exchange.getIn().getBody(String.class);

        LOGGER.info("payload: " + payload);

        //exchange.getOut().setHeader("command", "PUBLISH");
        exchange.getOut().setHeader("CamelRedis.Channel", "channel_brian");
        exchange.getOut().setHeader("CamelRedis.Message", payload);
        //exchange.getOut().setHeader("CamelRedis.Key", payload);
        LOGGER.info("ending");
    }
}
