package example.service

import example.model.Transaction
import example.repository.TransactionJpaRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TransactionService {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    internal var transactionJpaRepository: TransactionJpaRepository? = null

    fun findAll(): List<Transaction> {

        val transaction = Transaction()

        transaction.accountNameOwner = "chase_brian"
        transaction.accountType = "credit"
        transaction.amount = "10.0"
        transaction.category = "restaurant"
        transaction.description = "Subway"
        transaction.notes = "super secret"
        transaction.dateAdded = "8/16/2017"
        transactionJpaRepository!!.saveAndFlush(transaction)

        return transactionJpaRepository!!.findAll()
    }
}
