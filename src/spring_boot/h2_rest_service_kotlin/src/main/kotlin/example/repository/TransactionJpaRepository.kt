package example.repository

import example.model.Transaction
import org.springframework.data.jpa.repository.JpaRepository

interface TransactionJpaRepository : JpaRepository<Transaction, Long>
