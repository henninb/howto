package example.controller

import example.model.Transaction
import example.service.TransactionService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/transaction")
class TransactionController {
    private val LOGGER = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    internal var transactionService: TransactionService? = null

    //http://localhost:8080/transaction/all
    @GetMapping(path = arrayOf("/all"))
    fun listAllTransactions(): List<Transaction> {
        return transactionService!!.findAll()
    }
}
