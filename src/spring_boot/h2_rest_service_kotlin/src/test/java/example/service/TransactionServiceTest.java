package example.service;

import example.model.Transaction;
import example.repository.TransactionJpaRepository;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class TransactionServiceTest {

    @Mock
    TransactionJpaRepository transactionJpaRepository;

    @InjectMocks
    TransactionService transactionService;

    @Before
    public void setUp() throws Exception {
    }

    //mokito
    @Test
    public void findAll() throws Exception {
        given(transactionJpaRepository.findAll()).willReturn(new List<Transaction>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @NotNull
            @Override
            public Iterator<Transaction> iterator() {
                return null;
            }

            @NotNull
            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @NotNull
            @Override
            public <T> T[] toArray(@NotNull T[] a) {
                return null;
            }

            @Override
            public boolean add(Transaction transaction) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(@NotNull Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(@NotNull Collection<? extends Transaction> c) {
                return false;
            }

            @Override
            public boolean addAll(int index, @NotNull Collection<? extends Transaction> c) {
                return false;
            }

            @Override
            public boolean removeAll(@NotNull Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(@NotNull Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }

            @Override
            public Transaction get(int index) {
                return null;
            }

            @Override
            public Transaction set(int index, Transaction element) {
                return null;
            }

            @Override
            public void add(int index, Transaction element) {

            }

            @Override
            public Transaction remove(int index) {
                return null;
            }

            @Override
            public int indexOf(Object o) {
                return 0;
            }

            @Override
            public int lastIndexOf(Object o) {
                return 0;
            }

            @NotNull
            @Override
            public ListIterator<Transaction> listIterator() {
                return null;
            }

            @NotNull
            @Override
            public ListIterator<Transaction> listIterator(int index) {
                return null;
            }

            @NotNull
            @Override
            public List<Transaction> subList(int fromIndex, int toIndex) {
                return null;
            }
        });
        List<Transaction> transactions = transactionService.findAll();
    }

}