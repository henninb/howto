package example.processor;

import com.fasterxml.jackson.databind.ObjectMapper;
import example.model.Transaction;
import example.service.TransactionService;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;

@Component
public class InsertTransactionProcessor implements Processor {
    //private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private static ObjectMapper mapper = new ObjectMapper();
    final ByteArrayOutputStream jsonOutputBytes = new ByteArrayOutputStream();

    @Autowired
    private TransactionService transactionService;

    @Override
    public void process(Exchange exchange) throws Exception {
        String jsonString = "";
        try {
            Transaction[] transactions = exchange.getIn().getBody(Transaction[].class);
            for (Transaction transaction : transactions) {
                transactionService.insertTransaction(transaction);
                transactionService.postActiveMQ(transaction);
            }
            mapper.writeValue(jsonOutputBytes, transactions);
            jsonString = new String(jsonOutputBytes.toByteArray());
            //exchange.getIn().setBody(exchange.getIn().getHeader("jsonString"));
            exchange.getIn().setBody(jsonString);
        }

        catch(Exception e) {
            e.printStackTrace();
        }
    }
}
