package example.bean;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UuidGeneratorBean {

    @Bean
    public String generateId() {
        return UUID.randomUUID().toString();
    }
}
