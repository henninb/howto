#!/bin/sh

FILELIST=$(git status | grep modified: | sed 's/modified://' | sed 's/^\#'//)

for FLIST in $(echo ${FILELIST}); do
  echo "${FLIST}"
  git add ${FLIST}
done

git commit -m updates
echo "git clean -fd"
echo "git clean -fX"

exit 0
