using System;

public class Player {
  private string name = string.Empty;
  private int score = 0;
  private bool dealer = false;
  private bool isHuman = false;
  private Card[] hand = null;
  
  public Player( string name ) {
    this.Name = name;
  }

  public Player( string name, bool dealer, bool isHuman ) {
    this.Name = name;
    this.Dealer = dealer;
    this.IsHuman = isHuman;
    this.Score = 0;
  }
  
  public string PrintHand() {
    string hand1 = string.Empty;
    for( int idx = 0; idx < 4; idx++ ) {
      //hand += (hand[idx].getRank() + hand[idx].getSuit().ToString() + " ");
      hand1 += (hand[idx].ToString() + " ");
    }
    return hand1;
  }
  
  public string Name {
    get {
      return this.name;
    }
  
    set {
      this.name = value;
    }
  }
  
  public int Score {
    get {
      return this.score;
    }
  
    set {
      this.score = value;
    }
  }
  
  public bool Dealer {
    get {
      return this.dealer;
    }
  
    set {
      this.dealer = value;
    }
  }
  
  public bool IsHuman {
    get {
      return this.isHuman;
    }
  
    set {
      this.isHuman = value;
    }
  }
  
  public Card[] Hand {
    get {
      return this.hand;
    }
  
    set {
      this.hand = value;
    }
  }
}
