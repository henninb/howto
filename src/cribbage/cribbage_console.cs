using System;
//using nsClearConsole;
using System.Threading;

public class Game {
  public Game() {
    Player player1 = new Player("Brian");
    Player player2 = new Player("Greg");

    Deck deck = new Deck();
    Card player1_card = deck.getTopCard();
    Card player2_card = deck.getTopCard();
  
    int x = player1_card.getDeckValue();
    int y = player2_card.getDeckValue();

    if( x < y ) {
      player1.Dealer = true;
      player2.Dealer = false;
    } else {
      player2.Dealer = true;
      player1.Dealer = false;
    }
  
    int roundCount = 0;
    while( playRound( player1, player2 ) == true ) {
      roundCount++;
    }
  
    Console.WriteLine(player1.Name + " final score=" + player1.Score);
    Console.WriteLine(player2.Name + " final score=" + player2.Score);
    Console.WriteLine("round: " + roundCount );
  }

  public bool playRound( Player player1, Player player2 ) {
    Deck deck = null;
    //Card card = null;

    Console.Clear();
    //random
    Thread.Sleep(DateTime.Now.Millisecond % 100);
    //FastRandom = new FastRandom();
  
    player1.Hand = new Card[4];
    player2.Hand = new Card[4];
    Card[] cribCards = new Card[4];
    Card cutCard = null;
  
    deck = new Deck();
    for( int idx = 0; idx < 4; idx++ ) {
      player1.Hand[idx] = deck.getTopCard();
      player2.Hand[idx] = deck.getTopCard();
      cribCards[idx] = deck.getTopCard();
    }
    cutCard = deck.getTopCard();

    int p1_score = Hand.countHand( player1.Hand, cutCard, false);
    Console.WriteLine(player1.Name + " Hand = " + player1.PrintHand() + " ** hand score=" + Hand.countHand(player1.Hand, cutCard, false));
    //Console.WriteLine(player1.Name + );
    int p2_score = Hand.countHand(player2.Hand, cutCard, false);
    Console.WriteLine(player2.Name + " Hand = " + player2.PrintHand() + " ** hand score=" + Hand.countHand(player2.Hand, cutCard, false));
    Console.Write("Crib Hand = ");
    for( int idx = 0; idx < 4; idx++ ) {
      Console.Write(cribCards[idx] + " ");
    }
    int crib_score = Hand.countHand(cribCards, cutCard, true);
    Console.WriteLine("\ncutCard: " + cutCard);
    if( player1.Dealer == true ) {
      Console.WriteLine(player1.Name + " crib score = " + crib_score);
    } else {
      Console.WriteLine(player2.Name + " crib score = " + crib_score);
    }
    Console.Write("Press the enter key.");
    Console.ReadLine();
    if( player1.Dealer == true ) {
      player2.Score += p2_score;
      if( player2.Score > 120 ) {
        return false;
      }
      player1.Score += p1_score;
      player1.Score += crib_score;
      if( player1.Score > 120 ) {
        return false;
      }
      player2.Dealer = true;
      player1.Dealer = false;
    } else {
      player1.Score += p1_score;
      if( player1.Score > 120 ) {
        return false;
      }
      player2.Score += p2_score;
      player2.Score += crib_score;
      if( player2.Score > 120 ) {
        return false;
      }
      player1.Dealer = true;
      player2.Dealer = false;
    }
  
    return true;
  
    //build crib
    //build hand1
    //build hand2
    //count pon hand check for winner
    //count hand check for winner
    //count crib check for winner
  }

  public static int peggingCount( Card[] stack, Player player, Card currCard, int currCount ) {
    int thirtyOneCount = 0;
    int fifteenCount = 0;
    //int goCount = 0;
  
    if( currCount == 31 ) {
      thirtyOneCount++;
      // 1 for 31 and 1 for the go
    }
  
    if( currCount == 15 ) {
      fifteenCount += 2;
      //2 for the 15
    }
  
    //pair
    //idx = 1
    // i(i-1)
    //while prev card == currCard count the number {
    //idx++;
    //}
    //pairsCount += (idx * idx - 1));
  
    //}
  
    //if( runLength > 2 ) {
    //runsCount;
    //}
  
    //run
  
    //cut a jack
  
    //if player1 and player2 don't have cards to play
    //1 for a go
  
    return 0;
  }

  public static void Main() {
    new Game();
  }
}
