@echo off

rem need to escape space to work with mingw32-make
set PATH=%WINDIR%\Microsoft.NET\Framework\v2.0.50727
set PATH=%WINDIR%\Microsoft.NET\Framework\v4.0.30319
set PATH=C:\TDM-GCC-64\bin;%PATH%
set OS=win32

mingw32-make win32

pause
