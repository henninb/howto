using System;
using System.Collections;

public class Deck {
  public Deck() {
    deck = new ArrayList();
    //this.createDeck();
    this.shuffleDeck();
  }

  private void createDeck() {
    for( int idx = 0; idx < 52; idx++ ) {
      deck.Add(idx);
    }
  }
  
  private void shuffleDeck() {
    Random random = new Random( DateTime.Now.Millisecond );
    int rand_pos = 0;
    int tmp = 0;
  
    this.createDeck();
    for( int idx = deck.Count - 1; idx > 0; idx-- ) {
      rand_pos = random.Next() % idx;
      tmp = (int)deck[rand_pos];
      deck[rand_pos] = (int)deck[idx];
      deck[idx] = (int)tmp;
    }
  }
  
  public Card getTopCard() {
    int topCard = -1;
    Card card = null;
  
    if( deck.Count > 0 ) {
      topCard = (int)deck[deck.Count - 1];
      card = new Card(topCard, false);
      deck.RemoveAt(deck.Count - 1);
      return card;
    } else {
      throw new Exception("ABORT: deck is empty.");
    }
  }

  public Card getCurrCard() {
    return this.currCutCard;
  }

  private Card topCard;
  private Card currCutCard;
  private ArrayList deck = null;
}
