const AMQPClient = require("amqp10").Client;
const Promise = require("bluebird");

//Fix from: https://github.com/noodlefrenzy/node-amqp10/issues/241
const activeMQPolicy = require("amqp10").Policy;
const client = new AMQPClient(activeMQPolicy.ActiveMQ);

const setUp = () => {
    return Promise.all([
        client.createReceiver("amq.topic"),
        client.createSender("amq.topic")
    ]);
};

client.connect("amqp://localhost")
    .then(setUp)
    .spread(function (receiver, sender) {
        receiver.on("errorReceived", function (err) {

            if (err) {
                console.log(`failed with error: ${err}`);
                return;
            }

            receiver.on("message", message => console.log(`Rx message: ${message.body}`));

            return sender.send({ key: "Value" });
        });
    })
    .error( err => console.log("error: ", err));
