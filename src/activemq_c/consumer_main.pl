use Net::STOMP::Client;

my $stomp = Net::STOMP::Client->new(host => "127.0.0.1", port => 61613);
$stomp->connect(login => "admin", passcode => "admin");
  # declare a callback to be called for each received message frame
$stomp->message_callback(sub {
      my($self, $frame) = @_;
      $self->ack(frame => $frame);
      printf("received: %s\n", $frame->body());
      return($self);
  });
  # subscribe to the given queue
$stomp->subscribe(
      destination => "/queue/test_queue",
      id          => "consumer_main.pl",          # required in STOMP 1.1
      ack         => "client",           # client side acknowledgment
  );

$stomp->wait_for_frames(callback => sub {
      my($self, $frame) = @_;
      # continue to wait for more frames
      #return(0); return zero to wait
      return(1); # return 1 to stop listening
  });
$stomp->unsubscribe(id => "consumer_main.pl");
$stomp->disconnect();
