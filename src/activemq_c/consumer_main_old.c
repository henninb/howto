#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stomp.h"

#define SERVERNAME "localhost"
#define SERVER_PORT 61613
#define USERNAME "admin"
#define PASSWD "admin"
#define QUEUE_NAME "/queue/test_queue"

//mqtt stomp amqp
static void check_status(apr_status_t, const char *);
static void terminate();
int die(int exitCode, const char *, apr_status_t);

int die(int exitCode, const char *message, apr_status_t reason)
{
    char msgbuf[80];
    apr_strerror(reason, msgbuf, sizeof (msgbuf));
    fprintf(stderr, "%s: %s (%d)\n", message, msgbuf, reason);
    exit(exitCode);
    return reason;
}
static void terminate()
{
    apr_terminate();
}
static void check_status(apr_status_t rc, const char *message) {
    if (rc != APR_SUCCESS) {
        die(-2, message, rc);
    }
}
int main(int argc, char *argv[])
{
    apr_status_t rc;
    apr_pool_t *pool;
    stomp_connection *connection;
    setbuf(stdout, NULL);
    rc = apr_initialize();
    check_status(rc, "Could not initialize");
    atexit(terminate);
    rc = apr_pool_create(&pool, NULL);
    check_status(rc, "Could not allocate pool");
    
    fprintf(stdout, "Connecting......");
    rc = stomp_connect(&connection, "localhost", 61613, pool);
    check_status(rc, "Could not connect");
    fprintf(stdout, "OK\n");
    fprintf(stdout, "Sending connect message.");
    {
        stomp_frame frame;
        frame.command = "CONNECT";
        frame.headers = apr_hash_make(pool);
        apr_hash_set(frame.headers, "login", APR_HASH_KEY_STRING, "admin");
        apr_hash_set(frame.headers, "passcode", APR_HASH_KEY_STRING, "admin");
        frame.body = NULL;
        frame.body_length = -1;
        rc = stomp_write(connection, &frame, pool);
        check_status(rc, "Could not send frame");
    }
    fprintf(stdout, "OK\n");
    fprintf(stdout, "Reading Response.");
    {
        stomp_frame *frame;
        rc = stomp_read(connection, &frame, pool);
        check_status(rc, "Could not read frame");
        fprintf(stdout, "Response: %s, %s\n", frame->command, frame->body);
    }
    fprintf(stdout, "OK\n");
    
    fprintf(stdout, "Sending Subscribe.");
    {
        stomp_frame frame;
        frame.command = "SUBSCRIBE";
        frame.headers = apr_hash_make(pool);
        apr_hash_set(frame.headers, "destination", APR_HASH_KEY_STRING, "/queue/test_queue");
        apr_hash_set(frame.headers, "ack", APR_HASH_KEY_STRING, "client");
        apr_hash_set(frame.headers, "id", APR_HASH_KEY_STRING, "test-id");
        apr_hash_set(frame.headers, "activemq.retroactive", APR_HASH_KEY_STRING, "true");
        apr_hash_set(frame.headers, "activemq.dispatchAsync", APR_HASH_KEY_STRING, "false");
        frame.body_length = -1;
        frame.body = NULL;
        rc = stomp_write(connection, &frame, pool);
        check_status(rc, "Could not send frame");
    }
    fprintf(stdout, "OK\n");
    fprintf(stdout, "Reading Response.\n");
    {
        apr_hash_t *headers;
        stomp_frame *frame;
        //frame.headers = apr_hash_make(pool);
        rc = stomp_read(connection, &frame, pool);
        check_status(rc, "Could not read frame");
        fprintf(stdout, "Response: %s, %s\n", frame->command, frame->body);
    }
    fprintf(stdout, "OK\n");

    fprintf(stdout, "Sending UnSubscribe.");
    {
        stomp_frame frame;
        frame.command = "UNSUBSCRIBE";
        frame.headers = apr_hash_make(pool);
        apr_hash_set(frame.headers, "destination", APR_HASH_KEY_STRING, "/queue/test_queue");
        apr_hash_set(frame.headers, "id", APR_HASH_KEY_STRING, "test-id");
        frame.body_length = -1;
        frame.body = NULL;
        rc = stomp_write(connection, &frame, pool);
        check_status(rc, "Could not send frame");
    }


    fprintf(stdout, "OK\n");
    fprintf(stdout, "Sending Disconnect.\n");
    {
        stomp_frame frame;
        frame.command = "DISCONNECT";
        frame.headers = NULL;
        frame.body_length = -1;
        frame.body = NULL;
        rc = stomp_write(connection, &frame, pool);
        check_status(rc, "Could not send frame");
    }
    fprintf(stdout, "OK\n");
    fprintf(stdout, "Disconnecting...\n");
    rc = stomp_disconnect(&connection);
    check_status(rc, "Could not disconnect");
    fprintf(stdout, "OK\n");
    apr_pool_destroy(pool);
    return 0;
}

