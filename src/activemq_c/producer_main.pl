#!/usr/bin/perl

use strict;
use warnings;
use Net::STOMP::Client;

sub date_today {
  my ($sec,$min,$hour,$mday,$month,$year,$wday,$yday,$isdst) = localtime(time);
  $month++;
  $year = $year + 1900;
  return "$year-$month-$mday $hour:$min:$sec";
}

sub date_today_utc {
  my ($sec,$min,$hour,$mday,$month,$year,$wday,$yday,$isdst) = gmtime(time);
  $month++;
  $year = $year + 1900;
  return "$year-$month-$mday $hour:$min:$sec";
}

sub main {
  my $x;

  if( $#ARGV+1 != 0 ) {
    print STDERR "Usage: $0 <noargs>\n";
    exit(1);
  }

  my $message = "local=<" .&date_today() . "> from perl";
  my $stomp = Net::STOMP::Client->new(host => "127.0.0.1", port => 61613);

  $stomp->connect(login => "admin", passcode => "admin");
  $stomp->send(destination => "/queue/test_queue", body => $message);
  print "message: $message\n";
  $stomp->disconnect();


  #print $ARGV[0], "\n";
  exit(0);
}

&main();

