
#include <stdlib.h>
#include "stomp.h"

int die(int exitCode, const char *message, apr_status_t reason) {
    char msgbuf[80];
	apr_strerror(reason, msgbuf, sizeof(msgbuf));
	fprintf(stderr, "%s: %s (%d)\n", message, msgbuf, reason);
	exit(exitCode);
	return reason;
}

static void terminate()
{
   apr_terminate();
}

int main(int argc, char *argv[])
{
   apr_status_t rc;
   apr_pool_t *pool;
   stomp_connection *connection;
   
   setbuf(stdout, NULL);
   
   rc = apr_initialize();
	rc==APR_SUCCESS || die(-2, "Could not initialize", rc);
   atexit(terminate);	
   
   rc = apr_pool_create(&pool, NULL);
	rc==APR_SUCCESS || die(-2, "Could not allocate pool", rc);
   
   fprintf(stdout, "Connecting......");
   rc=stomp_connect( &connection, "localhost", 61613, pool);
	rc==APR_SUCCESS || die(-2, "Could not connect", rc);
   fprintf(stdout, "OK\n");
      
   fprintf(stdout, "Sending connect message.");
   {
      stomp_frame frame;
      frame.command = "CONNECT";
      frame.headers = apr_hash_make(pool);
      apr_hash_set(frame.headers, "login", APR_HASH_KEY_STRING, "admin");          
      apr_hash_set(frame.headers, "passcode", APR_HASH_KEY_STRING, "admin");          
      frame.body = NULL;
	  frame.body_length = -1;
      rc = stomp_write(connection, &frame, pool);
      rc==APR_SUCCESS || die(-2, "Could not send frame", rc);
   }  
   fprintf(stdout, "OK\n");   
   fprintf(stdout, "Reading Response.");
   {
      stomp_frame *frame;
      rc = stomp_read(connection, &frame, pool);
      rc==APR_SUCCESS || die(-2, "Could not read frame", rc);
      fprintf(stdout, "Response: %s, %s\n", frame->command, frame->body);
   }     
   fprintf(stdout, "OK\n");

   fprintf(stdout, "Sending Subscribe.");
   {
      stomp_frame frame;
      frame.command = "SUB";
      frame.headers = apr_hash_make(pool);
      apr_hash_set(frame.headers, "destination", APR_HASH_KEY_STRING, "/queue/test_queue"); 
      apr_hash_set(frame.headers, "ack", APR_HASH_KEY_STRING, "client"); 
      apr_hash_set(frame.headers, "activemq.retroactive", APR_HASH_KEY_STRING, "true");
      apr_hash_set(frame.headers, "activemq.dispatchAsync", APR_HASH_KEY_STRING, "false");
      

	  frame.body_length = -1;
      frame.body = NULL;
      rc = stomp_write(connection, &frame, pool);
      rc==APR_SUCCESS || die(-2, "Could not send frame", rc);
   }  
   fprintf(stdout, "OK\n");
   
   
   
   fprintf(stdout, "Reading Response.");
   while (1)
   {

      stomp_frame frame;
      // begin txn
      frame.command = "BEGIN";
      frame.headers = apr_hash_make(pool);
      apr_hash_set(frame.headers, "transaction", APR_HASH_KEY_STRING, "mytxn"); 
      frame.body_length = -1;
      frame.body = NULL;
      rc = stomp_write(connection, &frame, pool);
     
      // read
      stomp_frame* frame2;
      rc = stomp_read(connection, &frame2, pool);
      rc==APR_SUCCESS || die(-2, "Could not read frame", rc);
      fprintf(stdout, "Response: %s, %s\n", frame2->command, frame2->body);


      //ack
      char* msgid = (char*) apr_hash_get(frame2->headers, "message-id", APR_HASH_KEY_STRING);

      printf("msg-id :%s\n", msgid);
      frame.command = "ACK";
      frame.headers = apr_hash_make(pool);
      apr_hash_set(frame.headers, "message-id", APR_HASH_KEY_STRING, msgid); 
      apr_hash_set(frame.headers, "transaction", APR_HASH_KEY_STRING, "mytxn");
      frame.body_length = -1;
      frame.body = NULL;
      rc = stomp_write(connection, &frame, pool);


      //abort
      frame.command = "ABORT";
      frame.headers = apr_hash_make(pool);
      apr_hash_set(frame.headers, "transaction", APR_HASH_KEY_STRING, "mytxn"); 
      frame.body_length = -1;
      frame.body = NULL;
      rc = stomp_write(connection, &frame, pool);


      printf("sleep\n");
      ///////////////////////////////
      
      // begin txn
      frame.command = "BEGIN";
      frame.headers = apr_hash_make(pool);
      apr_hash_set(frame.headers, "transaction", APR_HASH_KEY_STRING, "mytxn2"); 
      frame.body_length = -1;
      frame.body = NULL;
      rc = stomp_write(connection, &frame, pool);
     
      // read
      rc = stomp_read(connection, &frame2, pool);
      rc==APR_SUCCESS || die(-2, "Could not read frame", rc);
      fprintf(stdout, "Response: %s, %s\n", frame2->command, frame2->body);


      //ack
      msgid = (char*) apr_hash_get(frame2->headers, "message-id",
	APR_HASH_KEY_STRING);

      frame.command = "ACK";
      frame.headers = apr_hash_make(pool);
      apr_hash_set(frame.headers, "message-id", APR_HASH_KEY_STRING, msgid); 
      frame.body_length = -1;
      frame.body = NULL;
      rc = stomp_write(connection, &frame, pool);


      //commit
      frame.command = "COMMIT";
      frame.headers = apr_hash_make(pool);
      apr_hash_set(frame.headers, "transaction", APR_HASH_KEY_STRING, "mytxn2"); 
      frame.body_length = -1;
      frame.body = NULL;
      rc = stomp_write(connection, &frame, pool);


      
      
   }     
   fprintf(stdout, "OK\n");
   

   fprintf(stdout, "Sending Disconnect.");
   {
      stomp_frame frame;
      frame.command = "DISCONNECT";
      frame.headers = NULL;
	  frame.body_length = -1;
      frame.body = NULL;
      rc = stomp_write(connection, &frame, pool);
      rc==APR_SUCCESS || die(-2, "Could not send frame", rc);
   }  
   fprintf(stdout, "OK\n");
   
   fprintf(stdout, "Disconnecting...");
	rc=stomp_disconnect(&connection); 
	rc==APR_SUCCESS || die(-2, "Could not disconnect", rc);
   fprintf(stdout, "OK\n");
   
   apr_pool_destroy(pool);	   
   return 0;
}
