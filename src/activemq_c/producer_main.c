#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pwd.h>
#include <time.h>
#include <assert.h>
#include <uuid/uuid.h>
#include "stomp.h"

#define SERVER_PORT 61613
#define USERNAME "sender_receiver"
#define PASSWD "sender_receiver"

#define FAILURE 1
#define SUCCESS 0


/* structure to encapsulate queue manager, queue and message objects */
typedef struct {
    int _qmgr_connected;                /* ConnQMgr suceeded             */
    int _queue_opened;                  /* InitQueue succeeded           */
    void *amqConnection;                /* connection handle             */
    void *pool;                         /* pool                          */
    long buflen;                        /* buffer length                 */
    long mqMsgLen;                      /* message length received       */
    char buffer[500];                   /* message buffer                */
    char queueName[100];                /* Queue Manager Name            */
} MQDEF;

#define MQDEF_DEFAULT   0,       \
                        0,       \
                        NULL,    \
                        NULL,    \
                        0,       \
                        0,       \
                      { '\0' },  \
                      { '\0' }

int amq_connect( MQDEF *, char * );
int amq_disconnect( MQDEF * );
//void check_status( apr_status_t, const char * );
int send_to_queue(  MQDEF *, char * );
int send_to_queue_trans(  MQDEF *, char *, int );
int receive_from_queue( MQDEF *, int );
int browse_queue( MQDEF *, int );
void date_today( char * );
int receive_from_queue_no_commit( MQDEF * );
int receive_from_queue_ack( MQDEF *, int  );

//MQDEF mqdef = {MQDEF_DEFAULT};

int main( int argc, char *argv[] ) {
    char mydate[100];
    int idx = 0;
    MQDEF mqdef = {MQDEF_DEFAULT};
    int rc = 0;
    int i =0;
    //char list[i];
    
    if( argc != 1 ) {
      fprintf(stderr, "Usage: %s <noargs>\n", argv[0]);
      exit(1);
    }
    //while( i < 10 ) {
    //    printf("%d\n", i++); 
    //}
    //exit(1);
    
    //printf("mqdef._qmgr_connected=<%d>\n", mqdef._qmgr_connected);
    //assert(mqdef._qmgr_connected != 0);

    rc = amq_connect(&mqdef, "localhost");
    printf("mqdef._qmgr_connected=<%d>\n", mqdef._qmgr_connected);
    if( rc != SUCCESS ) {
        printf("rc=<%d>\n", rc);
        exit(1);
    }
    //assert(mqdef._qmgr_connected != 0);
    char *queueName = "/queue/queueName";
    strncpy(mqdef.queueName, queueName, strlen(queueName));
    printf("%s\n", mqdef.queueName);
    for( idx = 0; idx < 4; idx++ ) {
      memset(mydate, '\0', 100);
      //date_today(mydate);
      sprintf(mydate, "message %d", idx);
      //fprintf(stdout, "message=<%s>\n", mydate);
      //send_to_queue_trans(&mqdef, mydate, idx);
      send_to_queue(&mqdef, mydate);
      //sleep(1);
    }
    //exit(1);
    for( idx = 0; idx < 100; idx++ ) {
      //browse_queue(&mqdef, idx);
      //sleep(1);
    }
    sleep(1);
    printf("time to receive from queue\n");
    for( idx = 0; idx < 100; idx++ ) {
      //receive_from_queue(&mqdef, idx);
      //receive_from_queue_ack(&mqdef, idx);
      //exit(1);
      //receive_from_queue_no_commit(&mqdef);
      //sleep(1);
      //exit(1);
      //sleep(1);
    }
    amq_disconnect(&mqdef);
    
    return 0;
}

void date_today( char *mydate ) {
    struct tm *local;
    struct tm *utc;
    time_t timeinfo;
    
    timeinfo = time(NULL);
    local = localtime(&timeinfo);
    sprintf(mydate, "local=<%d/%d/%d %d:%d:%d>\n", local->tm_year + 1900, local->tm_mon + 1, local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
    utc = gmtime(&timeinfo);
}

int amq_connect( MQDEF *mqdef, char *serverName ) {
    apr_status_t rc;
    stomp_frame frame_write;
    stomp_frame *frame_read = NULL;
    char *version;
    char *server;
    
    rc = apr_initialize();
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    rc = apr_pool_create((struct apr_pool_t **)&mqdef->pool, NULL);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    rc = stomp_connect((struct stomp_connection **)&mqdef->amqConnection, serverName, SERVER_PORT, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    frame_write.command = "CONNECT";
    frame_write.headers = apr_hash_make((apr_pool_t *)mqdef->pool);
    apr_hash_set(frame_write.headers, "login", APR_HASH_KEY_STRING, "admin");
    apr_hash_set(frame_write.headers, "passcode", APR_HASH_KEY_STRING, "password");
    apr_hash_set(frame_write.headers, "accept-version", APR_HASH_KEY_STRING, "1.2");
    //apr_hash_set(frame_write.headers, "accept-version", APR_HASH_KEY_STRING, "1.0");
    //apr_hash_set(frame_write.headers, "accept-version", APR_HASH_KEY_STRING, "1.2");
    //apr_hash_set(frame_write.headers, "accept-version", APR_HASH_KEY_STRING, "1.0,1.1,1.2");
    //apr_hash_set(frame_write.headers, "receipt", APR_HASH_KEY_STRING, "receipt-123");
    frame_write.body = NULL;
    frame_write.body_length = -1;
    rc = stomp_write(mqdef->amqConnection, &frame_write, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        mqdef->_qmgr_connected = 0;
        return FAILURE;
    }
    frame_read = NULL;
    rc = stomp_read(mqdef->amqConnection, &frame_read, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        printf("ERROR: error occured during connect.\n");
        mqdef->_qmgr_connected = 0;
        return FAILURE;
    }
    if( frame_read != NULL && strncmp(frame_read->command, "CONNECTED", 9) == 0 ) {
         version = (char*) apr_hash_get(frame_read->headers, "version", APR_HASH_KEY_STRING);
         printf("version=<%s>\n", version);
         server = (char*) apr_hash_get(frame_read->headers, "server", APR_HASH_KEY_STRING);
         printf("server=<%s>\n", server);
//heart-beat:0,0
//session:ID:centos7-41108-1529422050852-5:39
    } else if( frame_read != NULL && strncmp(frame_read->command, "ERROR", 5) == 0 ) {
        mqdef->_qmgr_connected = 0;
        fprintf(stdout, "cmd=<%s>, body=<%s> len=<%d>\n", frame_read->command, frame_read->body, frame_read->body_length);
        exit(1);
        //return FAILURE;
    } else {
        fprintf(stdout, "cmd=<%s>, body=<%s> len=<%d>\n", frame_read->command, frame_read->body, frame_read->body_length);
        exit(1);
    }

    mqdef->_qmgr_connected = 1;
    return SUCCESS;
}

int amq_disconnect( MQDEF *mqdef ) {
    apr_status_t rc;
    //stomp_frame frame_write;
    //stomp_frame *frame_read;
    
    fprintf(stdout, "Disconnecting.\n");
    rc = stomp_disconnect((struct stomp_connection **)&mqdef->amqConnection);
    if ( rc != APR_SUCCESS ) {
        mqdef->_qmgr_connected = 0;
        return FAILURE;
    }
    fprintf(stdout, "Disconnected\n");
    apr_pool_destroy((apr_pool_t *)mqdef->pool);
    fprintf(stdout, "pool destroyed.\n");
    mqdef->_qmgr_connected = 0;
    return SUCCESS;
}

int send_to_queue(  MQDEF *mqdef, char *message ) {
    apr_status_t rc;
    stomp_frame frame_write;
    stomp_frame* frame_read = NULL;
    char content_length[50];
    char *receipt_id;
    uuid_t uuid;
    char receipt_id_uuid[37];
    int receipt_flag = 1;

    memset(content_length, '\0', sizeof(content_length));
    sprintf(content_length, "%d", strlen(message));

    //uuid_generate_time_safe(uuid);
    uuid_generate_random(uuid);
    uuid_unparse_lower(uuid, receipt_id_uuid);

    fprintf(stdout, "Sending Message %s.\n", mqdef->queueName);
    frame_write.command = "SEND";
    frame_write.headers = apr_hash_make((apr_pool_t *)mqdef->pool);
    apr_hash_set(frame_write.headers, "destination", APR_HASH_KEY_STRING, mqdef->queueName);
    apr_hash_set(frame_write.headers, "persistent", APR_HASH_KEY_STRING, "true");
    apr_hash_set(frame_write.headers, "content-length", APR_HASH_KEY_STRING, content_length);
    //apr_hash_set(frame_write.headers, "include-seq", APR_HASH_KEY_STRING, "seq");
    if( receipt_flag == 1 ) {
        apr_hash_set(frame_write.headers, "receipt", APR_HASH_KEY_STRING, receipt_id_uuid);
    }
    //apr_hash_set(frame.headers, "content-type", APR_HASH_KEY_STRING, "text/UTF-8");
    frame_write.body_length = -1;
    frame_write.body = message;
    rc = stomp_write(mqdef->amqConnection, &frame_write, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    if( receipt_flag == 1 ) {
    frame_read = NULL;
    rc = stomp_read(mqdef->amqConnection, &frame_read, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    if( frame_read != NULL && strncmp(frame_read->command, "RECEIPT", 7) == 0 ) {
        receipt_id = (char*) apr_hash_get(frame_read->headers, "receipt-id", APR_HASH_KEY_STRING);
        printf("receipt_id=<%s>\n", receipt_id);
    } else if( frame_read != NULL && strncmp(frame_read->command, "ERROR", 5) == 0 ) {
        printf("FAILURE: <%s>\n", frame_read->body);
        return FAILURE;
    } else {
        printf("message_body: %s\n", frame_read->body);
    }
    }

    fprintf(stdout, "sent messsage<%s>\n", message);
    return SUCCESS;
}

//https://github.com/stompgem/stomp/issues/124

int receive_from_queue( MQDEF *mqdef, int transaction ) {
    apr_status_t rc;
    stomp_frame frame_write;
    stomp_frame* frame_read = NULL;
    char transaction_str[37];
    char* ack = NULL;
    char* protocol = NULL;
    char* receipt_id = NULL;
    char* message_id = NULL;
    uuid_t uuid;
    char receipt_id_begin_uuid[37];
    char receipt_id_commit_uuid[37];
    char id_uuid[37];

    uuid_generate_random(uuid);
    uuid_unparse_lower(uuid, transaction_str);

    uuid_generate_random(uuid);
    uuid_unparse_lower(uuid, receipt_id_begin_uuid);

    uuid_generate_random(uuid);
    uuid_unparse_lower(uuid, id_uuid);

    uuid_generate_random(uuid);
    uuid_unparse_lower(uuid, receipt_id_commit_uuid);

    fprintf(stdout, "1) *** Transaction BEGIN - (receive_from_queue)\n");
    frame_write.command = "BEGIN";
    frame_write.headers = apr_hash_make((apr_pool_t *)mqdef->pool);
    apr_hash_set(frame_write.headers, "transaction", APR_HASH_KEY_STRING, transaction_str); 
    frame_write.body_length = -1;
    frame_write.body = NULL;
    rc = stomp_write(mqdef->amqConnection, &frame_write, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
   
    printf("3) Subscribe begin\n");
    frame_write.command = "SUBSCRIBE";
    frame_write.headers = apr_hash_make((apr_pool_t *)mqdef->pool);
    apr_hash_set(frame_write.headers, "destination", APR_HASH_KEY_STRING, mqdef->queueName);
    //apr_hash_set(frame_write.headers, "ack", APR_HASH_KEY_STRING, "client");
    apr_hash_set(frame_write.headers, "id", APR_HASH_KEY_STRING, id_uuid);
    //apr_hash_set(frame_write.headers, "activemq.prefetchSize", APR_HASH_KEY_STRING, "1");
    //apr_hash_set(frame_write.headers, "activemq.priority", APR_HASH_KEY_STRING, "0");
    frame_write.body_length = -1;
    frame_write.body = NULL;
    rc = stomp_write(mqdef->amqConnection, &frame_write, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    frame_read = NULL;
    rc = stomp_read(mqdef->amqConnection, &frame_read, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    ack = NULL;
    if( frame_read != NULL && strncmp(frame_read->command, "MESSAGE", 7) == 0 ) {
        message_id = (char*) apr_hash_get(frame_read->headers, "message-id", APR_HASH_KEY_STRING);
        ack = (char*) apr_hash_get(frame_read->headers, "ack", APR_HASH_KEY_STRING);
        printf("5) message_id=<%s>\n", message_id);
        printf("5) ack=<%s>\n", ack);
        printf("5) message_body=<%s>\n", frame_read->body);
        //printf("5) message_protocol=<%s>\n", protocol);
    } else if( frame_read != NULL && strncmp(frame_read->command, "ERROR", 5) == 0 ) {
        fprintf(stdout, "Error: command=<%s>, body=<%s>\n", frame_read->command, frame_read->body);
        exit(1);
    } else {
        fprintf(stdout, "Error: command=<%s>, body=<%s>\n", frame_read->command, frame_read->body);
        printf("should never get here.\n");
        exit(1);
    }
    printf("9) commit begin\n");
    frame_write.command = "COMMIT";
    frame_write.headers = apr_hash_make((apr_pool_t *)mqdef->pool);
    apr_hash_set(frame_write.headers, "transaction", APR_HASH_KEY_STRING, transaction_str);
    frame_write.body_length = -1;
    frame_write.body = NULL;
    rc = stomp_write(mqdef->amqConnection, &frame_write, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    fprintf(stdout, "12) *** Transaction Complete - (receive_from_queue)\n\n");
}


int receive_from_queue_ack( MQDEF *mqdef, int transaction ) {
    apr_status_t rc;
    stomp_frame frame_write;
    stomp_frame *frame_read = NULL;
    char transaction_str[37];
    char* ack = NULL;
    char* protocol = NULL;
    char* receipt_id = NULL;
    char* message_id = NULL;
    char* message = NULL;
    char* seq = NULL;
    uuid_t uuid;
    char receipt_id_begin_uuid[37];
    char receipt_id_commit_uuid[37];
    char receipt_id_ack_uuid[37];
    char id_uuid[37];
    int receipt_flag = 1;

    uuid_generate_random(uuid);
    uuid_unparse_lower(uuid, transaction_str);

    uuid_generate_random(uuid);
    uuid_unparse_lower(uuid, receipt_id_begin_uuid);

    uuid_generate_random(uuid);
    uuid_unparse_lower(uuid, id_uuid);

    uuid_generate_random(uuid);
    uuid_unparse_lower(uuid, receipt_id_commit_uuid);


    uuid_generate_random(uuid);
    uuid_unparse_lower(uuid, receipt_id_ack_uuid);

    fprintf(stdout, "1) *** Transaction BEGIN - (receive_from_queue)\n");
    frame_write.command = "BEGIN";
    frame_write.headers = apr_hash_make((apr_pool_t *)mqdef->pool);
    apr_hash_set(frame_write.headers, "transaction", APR_HASH_KEY_STRING, transaction_str); 
    frame_write.body_length = -1;
    frame_write.body = NULL;
    rc = stomp_write(mqdef->amqConnection, &frame_write, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
   
    printf("3) Subscribe begin\n");
    frame_write.command = "SUBSCRIBE";
    frame_write.headers = apr_hash_make((apr_pool_t *)mqdef->pool);
    apr_hash_set(frame_write.headers, "destination", APR_HASH_KEY_STRING, mqdef->queueName);
    apr_hash_set(frame_write.headers, "ack", APR_HASH_KEY_STRING, "client");
    apr_hash_set(frame_write.headers, "id", APR_HASH_KEY_STRING, "client-123");
    apr_hash_set(frame_write.headers, "activemq.prefetchSize", APR_HASH_KEY_STRING, "1");
    //apr_hash_set(frame_write.headers, "activemq.dispatchAsync", APR_HASH_KEY_STRING, "false");
    //apr_hash_set(frame_write.headers, "activemq.priority", APR_HASH_KEY_STRING, "0");
    frame_write.body_length = -1;
    frame_write.body = NULL;
    rc = stomp_write(mqdef->amqConnection, &frame_write, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    frame_read = NULL;
    rc = stomp_read(mqdef->amqConnection, &frame_read, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    ack = NULL;
    if( frame_read != NULL && strncmp(frame_read->command, "MESSAGE", 7) == 0 ) {
        ack = (char*) apr_hash_get(frame_read->headers, "ack", APR_HASH_KEY_STRING);
        printf("5) ack=<%s>\n", ack);
        if( ack == NULL ) {
            printf("ack being null is a problem\n");
            exit(1);
        }
        //message = (char*) apr_hash_get(frame_read->headers, "message", APR_HASH_KEY_STRING);
        //seq = (char*) apr_hash_get(frame_read->headers, "seq", APR_HASH_KEY_STRING);
        //printf("5) seq=<%s>\n", seq);
        //message = (char*) apr_hash_get(frame_read->headers, "message", APR_HASH_KEY_STRING);
        message_id = (char*) apr_hash_get(frame_read->headers, "message-id", APR_HASH_KEY_STRING);
        printf("5) message_id=<%s>\n", message_id);
        printf("5) command=<%s>, body=<%s>\n", frame_read->command, frame_read->body);
    } else if( frame_read != NULL && strncmp(frame_read->command, "ERROR", 5) == 0 ) {
        message_id = (char*) apr_hash_get(frame_read->headers, "message-id", APR_HASH_KEY_STRING);
        printf("5) message_id=<%s>\n", message_id);
        printf("5) command=<%s>, body=<%s>\n", frame_read->command, frame_read->body);
        exit(1);
    } else {
        //message = (char*) apr_hash_get(frame_read->headers, "message", APR_HASH_KEY_STRING);
        message_id = (char*) apr_hash_get(frame_read->headers, "message-id", APR_HASH_KEY_STRING);
        printf("5) message_id=<%s>\n", message_id);
        printf("5) command=<%s>, body=<%s>\n", frame_read->command, frame_read->body);
        printf("5) should never get here.\n");
        exit(1);
    }
    frame_write.command = "ACK";
    frame_write.headers = apr_hash_make((apr_pool_t *)mqdef->pool);
    apr_hash_set(frame_write.headers, "transaction", APR_HASH_KEY_STRING, transaction_str);
    apr_hash_set(frame_write.headers, "id", APR_HASH_KEY_STRING, ack);
    apr_hash_set(frame_write.headers, "receipt-id", APR_HASH_KEY_STRING, receipt_id_ack_uuid);
    //apr_hash_set(frame_write.headers, "message-id", APR_HASH_KEY_STRING, message_id);
    printf("ack=<%s>\n", ack);
    frame_write.body_length = -1;
    frame_write.body = NULL;
    rc = stomp_write(mqdef->amqConnection, &frame_write, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    frame_read = NULL;
    rc = stomp_read(mqdef->amqConnection, &frame_read, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    if( frame_read != NULL && strncmp(frame_read->command, "RECEIPT", 7) == 0 ) {
        message_id = (char*) apr_hash_get(frame_read->headers, "message-id", APR_HASH_KEY_STRING);
        receipt_id = (char*) apr_hash_get(frame_read->headers, "receipt-id", APR_HASH_KEY_STRING);
        printf("7) receipt-id=<%s>\n", receipt_id);
        printf("7) message-id=<%s>\n", message_id);
        printf("7) command=<%s>, body=<%s>\n", frame_read->command, frame_read->body);
    } else if( frame_read != NULL && strncmp(frame_read->command, "ERROR", 5) == 0 ) {
        message_id = (char*) apr_hash_get(frame_read->headers, "message_id", APR_HASH_KEY_STRING);
        receipt_id = (char*) apr_hash_get(frame_read->headers, "receipt-id", APR_HASH_KEY_STRING);
        printf("7) receipt-id=<%s>\n", receipt_id);
        printf("7) message-id=<%s>\n", message_id);
        printf("7) command=<%s>, body=<%s>\n", frame_read->command, frame_read->body);
        exit(1);
    } else {
        message_id = (char*) apr_hash_get(frame_read->headers, "message_id", APR_HASH_KEY_STRING);
        printf("7) message-id=<%s>\n", message_id);
        printf("7) command=<%s>, body=<%s>\n", frame_read->command, frame_read->body);
        ack = (char*) apr_hash_get(frame_read->headers, "ack", APR_HASH_KEY_STRING);
        printf("5) ack=<%s>\n", ack);
        printf("this is a failure\n");
        frame_write.command = "ACK";
        frame_write.headers = apr_hash_make((apr_pool_t *)mqdef->pool);
        apr_hash_set(frame_write.headers, "transaction", APR_HASH_KEY_STRING, transaction_str);
        apr_hash_set(frame_write.headers, "id", APR_HASH_KEY_STRING, ack);
        if( receipt_flag == 1 ) {
            //apr_hash_set(frame_write.headers, "receipt-id", APR_HASH_KEY_STRING, receipt_id_ack_uuid);
        }
        rc = stomp_write(mqdef->amqConnection, &frame_write, (apr_pool_t *)mqdef->pool);
        if ( rc != APR_SUCCESS ) {
            return FAILURE;
        }
    }
    printf("9) commit begin\n");
    frame_write.command = "COMMIT";
    frame_write.headers = apr_hash_make((apr_pool_t *)mqdef->pool);
    apr_hash_set(frame_write.headers, "transaction", APR_HASH_KEY_STRING, transaction_str);
    frame_write.body_length = -1;
    frame_write.body = NULL;
    rc = stomp_write(mqdef->amqConnection, &frame_write, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    fprintf(stdout, "12) *** Transaction Complete - (receive_from_queue)\n\n");
}

int receive_from_queue_not_functional( MQDEF *mqdef, int transaction ) {
    apr_status_t rc;
    stomp_frame frame_write;
    stomp_frame* frame_read = NULL;
    char transaction_str[37];
    char* ack = NULL;
    char* protocol = NULL;
    char* receipt_id = NULL;
    char* message_id = NULL;
    uuid_t uuid;
    char receipt_id_begin_uuid[37];
    char receipt_id_commit_uuid[37];
    char receipt_id_ack_uuid[37];

    uuid_generate_random(uuid);
    uuid_unparse_lower(uuid, transaction_str);

    uuid_generate_random(uuid);
    uuid_unparse_lower(uuid, receipt_id_begin_uuid);

    uuid_generate_random(uuid);
    uuid_unparse_lower(uuid, receipt_id_ack_uuid);

    uuid_generate_random(uuid);
    uuid_unparse_lower(uuid, receipt_id_commit_uuid);

    fprintf(stdout, "1) *** Transaction BEGIN - (receive_from_queue)\n");
    frame_write.command = "BEGIN";
    frame_write.headers = apr_hash_make((apr_pool_t *)mqdef->pool);
    apr_hash_set(frame_write.headers, "transaction", APR_HASH_KEY_STRING, transaction_str); 
    //apr_hash_set(frame_write.headers, "receipt", APR_HASH_KEY_STRING, receipt_id_begin_uuid);
    frame_write.body_length = -1;
    frame_write.body = NULL;
    rc = stomp_write(mqdef->amqConnection, &frame_write, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    frame_read = NULL;
    //rc = stomp_read(mqdef->amqConnection, &frame_read, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    if( frame_read != NULL && strncmp(frame_read->command, "RECEIPT", 7) == 0 ) {
        receipt_id = (char*) apr_hash_get(frame_read->headers, "receipt-id", APR_HASH_KEY_STRING);
        printf("receipt_id_begin=<%s>\n", receipt_id);
    } else if( frame_read != NULL && strncmp(frame_read->command, "ERROR", 5) == 0 ) {
        fprintf(stdout, "Error: command=<%s>, body=<%s>\n", frame_read->command, frame_read->body);
        exit(1);
    } else {
        printf("2) begin will not need a receipt\n");
        //exit(1);
    }
   
    printf("3) Subscribe begin\n");
    frame_write.command = "SUBSCRIBE";
    frame_write.headers = apr_hash_make((apr_pool_t *)mqdef->pool);
    apr_hash_set(frame_write.headers, "destination", APR_HASH_KEY_STRING, mqdef->queueName);
    apr_hash_set(frame_write.headers, "ack", APR_HASH_KEY_STRING, "client");
    apr_hash_set(frame_write.headers, "id", APR_HASH_KEY_STRING, "12345");
    apr_hash_set(frame_write.headers, "activemq.prefetchSize", APR_HASH_KEY_STRING, "1");
    //apr_hash_set(frame_write.headers, "activemq.priority", APR_HASH_KEY_STRING, "0");
    //apr_hash_set(frame_write.headers, "receipt", APR_HASH_KEY_STRING, "receipt-123");
    frame_write.body_length = -1;
    frame_write.body = NULL;
    rc = stomp_write(mqdef->amqConnection, &frame_write, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    frame_read = NULL;
    rc = stomp_read(mqdef->amqConnection, &frame_read, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    ack = NULL;
    if( frame_read != NULL && strncmp(frame_read->command, "MESSAGE", 7) == 0 ) {
        ack = (char*) apr_hash_get(frame_read->headers, "ack", APR_HASH_KEY_STRING);
        printf("4) message_ack=<%s>\n", ack);
        message_id = (char*) apr_hash_get(frame_read->headers, "message-id", APR_HASH_KEY_STRING);
        //protocol = (char*) apr_hash_get(frame_read->headers, "protocol", APR_HASH_KEY_STRING);
        //printf("ack=<%s>\n", ack);
        printf("5) message_id=<%s>\n", message_id);
        printf("5) message_body=<%s>\n", frame_read->body);
        //printf("5) message_protocol=<%s>\n", protocol);
    } else {
        printf("^^^ should receive a message response\n");
        printf("%s\n", frame_read->command);
    }
    if( frame_read != NULL && strncmp(frame_read->command, "ERROR", 5) == 0 ) {
        fprintf(stdout, "Error: command=<%s>, body=<%s>\n", frame_read->command, frame_read->body);
        exit(1);
    }
    //return SUCCESS;
    if( ack == NULL ) {
      printf("^^^ why is the message ack NULL\n");
      exit(1);
    }
    printf("6) Acknoledge begin\n");
    frame_write.command = "ACK";
    frame_write.headers = apr_hash_make((apr_pool_t *)mqdef->pool);
    apr_hash_set(frame_write.headers, "transaction", APR_HASH_KEY_STRING, transaction_str);
    apr_hash_set(frame_write.headers, "id", APR_HASH_KEY_STRING, ack);
    apr_hash_set(frame_write.headers, "receipt", APR_HASH_KEY_STRING, receipt_id_ack_uuid);
    //apr_hash_set(frame_write.headers, "message-id", APR_HASH_KEY_STRING, message_id);
    printf("set id =<%s>\n", ack);
    frame_write.body_length = -1;
    frame_write.body = NULL;
    rc = stomp_write(mqdef->amqConnection, &frame_write, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    frame_read = NULL;
    rc = stomp_read(mqdef->amqConnection, &frame_read, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    if( frame_read != NULL && strncmp(frame_read->command, "RECEIPT", 7) == 0 ) {
        receipt_id = (char*) apr_hash_get(frame_read->headers, "receipt-id", APR_HASH_KEY_STRING);
        printf("7) ack_receipt_id=<%s>\n", receipt_id);
    } else if( frame_read != NULL && strncmp(frame_read->command, "ERROR", 5) == 0 ) {
        fprintf(stdout, "ACK Error: command=<%s>, body=<%s>\n", frame_read->command, frame_read->body);
        exit(1);
    } else {
        printf("8) message->body %s\n", frame_read->body);
    }
    printf("9) commit begin\n");
    frame_write.command = "COMMIT";
    frame_write.headers = apr_hash_make((apr_pool_t *)mqdef->pool);
    apr_hash_set(frame_write.headers, "transaction", APR_HASH_KEY_STRING, transaction_str);
    //apr_hash_set(frame_write.headers, "receipt", APR_HASH_KEY_STRING, receipt_id_commit_uuid);
    frame_write.body_length = -1;
    frame_write.body = NULL;
    rc = stomp_write(mqdef->amqConnection, &frame_write, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    frame_read = NULL;
    //rc = stomp_read(mqdef->amqConnection, &frame_read, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
        return FAILURE;
    }
    if( frame_read != NULL && strncmp(frame_read->command, "RECEIPT", 7) == 0 ) {
        receipt_id = (char*) apr_hash_get(frame_read->headers, "receipt-id", APR_HASH_KEY_STRING);
        printf("receipt_id_commit=<%s>\n", receipt_id);
    } else if( frame_read != NULL && strncmp(frame_read->command, "ERROR", 5) == 0 ) {
        fprintf(stdout, "Error: command=<%s>, body=<%s>\n", frame_read->command, frame_read->body);
        exit(1);
    } else {
        printf("11) commit not requireing a receipt\n");
    }
    fprintf(stdout, "12) *** Transaction Complete - (receive_from_queue)\n\n");
}

int send_to_queue_trans( MQDEF *mqdef, char *message, int transaction ) {
    apr_status_t rc;
    stomp_frame frame;
    stomp_frame *frame_read = NULL;
    char transaction_str[50];
    char content_length[50];

    memset(content_length, '\0', sizeof(content_length));
    sprintf(content_length, "%d", strlen(message));

    memset(transaction_str, '\0', sizeof(transaction_str));
    sprintf(transaction_str, "trxn%d", transaction);

    frame.command = "BEGIN";
    frame.headers = apr_hash_make((apr_pool_t *)mqdef->pool);
    apr_hash_set(frame.headers, "transaction", APR_HASH_KEY_STRING, transaction_str);
    frame.body_length = -1;
    frame.body = NULL;
    rc = stomp_write(mqdef->amqConnection, &frame, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
      printf("ERROR: apr failure during begin.\n");
      return FAILURE;
    }
    if( strncmp(frame.command, "ERROR", 5) == 0 ) {
      printf("ERROR: error occured during begin.\n");
    }

    frame.command = "SEND";
    frame.headers = apr_hash_make((apr_pool_t *)mqdef->pool);
    printf("queueName <%s>\n",  mqdef->queueName);
    apr_hash_set(frame.headers, "destination", APR_HASH_KEY_STRING, mqdef->queueName);
    apr_hash_set(frame.headers, "transaction", APR_HASH_KEY_STRING, transaction_str);
    apr_hash_set(frame.headers, "persistent", APR_HASH_KEY_STRING, "true");
    apr_hash_set(frame.headers, "content-length", APR_HASH_KEY_STRING, content_length);
    apr_hash_set(frame.headers, "id", APR_HASH_KEY_STRING, "12345");
    apr_hash_set(frame.headers, "receipt", APR_HASH_KEY_STRING, "message-12345");
    //apr_hash_set(frame.headers, "ttl", APR_HASH_KEY_STRING, "2000");
    //apr_hash_set(frame.headers, "content-type", APR_HASH_KEY_STRING, "text/UTF-8");
    //apr_hash_set(frame.headers, "content-type", APR_HASH_KEY_STRING, "text/plain; charset=UTF-8");
    frame.body_length = -1;
    frame.body = message;
    rc = stomp_write(mqdef->amqConnection, &frame, (apr_pool_t *)mqdef->pool);
    if ( rc != APR_SUCCESS ) {
      printf("ERROR: apr failure during send.\n");
      return FAILURE;
    }

    //frame_read->command = "ERROR";
    //frame_read->body = "";
    //frame_read->body_length = -1;
    //printf("%s\n", frame.headers.key);
    //only needed if queue dne
    rc = stomp_read(mqdef->amqConnection, &frame_read, (apr_pool_t *)mqdef->pool);
    //apr_hash_get (frame.headers, "key", 1);
    //int x = apr_hash_this_key_len (frame.headers);
    int x = apr_hash_count(frame.headers);
    printf("apr_hash_count = %d\n", x);
    if( frame_read !=NULL && strncmp(frame_read->command, "RECEIPT", 7) == 0 ) {
      printf("frame_body=<%s>\n", frame_read->body);
    }
    if( frame_read !=NULL && strncmp(frame_read->command, "ERROR", 5) == 0 ) {
      printf("ERROR: error occured during send.\n");
      //rc = stomp_read(mqdef->amqConnection, &frame_read, (apr_pool_t *)mqdef->pool);
      printf("%s\n", frame_read->body);
      exit(1);
    }

    if( transaction % 2 == 0 ) {
        printf("ABORT = <%d>\n", transaction);
        frame.command = "ABORT";
        frame.headers = apr_hash_make((apr_pool_t *)mqdef->pool);
        apr_hash_set(frame.headers, "transaction", APR_HASH_KEY_STRING, transaction_str);
        frame.body_length = -1;
        frame.body = NULL;
        rc = stomp_write(mqdef->amqConnection, &frame, (apr_pool_t *)mqdef->pool);
        if ( rc != APR_SUCCESS ) {
          printf("ERROR: apr failure during commit.\n");
          return FAILURE;
        }
        if( strncmp(frame.command, "ERROR", 5) == 0 ) {
          printf("ERROR: error occured during abort.\n");
          //rc = stomp_read(mqdef->amqConnection, &frame_read, (apr_pool_t *)mqdef->pool);
          //printf("%s\n", frame_read->body);
          //exit(1);
        }
    } else {
        frame.command = "COMMIT";
        frame.headers = apr_hash_make((apr_pool_t *)mqdef->pool);
        apr_hash_set(frame.headers, "transaction", APR_HASH_KEY_STRING, transaction_str);
        frame.body_length = -1;
        frame.body = NULL;
        rc = stomp_write(mqdef->amqConnection, &frame, (apr_pool_t *)mqdef->pool);
        if ( rc != APR_SUCCESS ) {
          printf("ERROR: apr failure during commit.\n");
          return FAILURE;
        }
        if( strncmp(frame.command, "ERROR", 5) == 0 ) {
          printf("ERROR: error occured during abort.\n");
        }
        //rc = stomp_read(mqdef->amqConnection, &frame_read, (apr_pool_t *)mqdef->pool);
    }
    fprintf(stdout, "Transaction Completed: send_to_queue_trans\n");
}
