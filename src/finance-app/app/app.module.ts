import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule} from '@angular/forms';
import { AppComponent }  from './app.component';
import {HttpModule} from '@angular/http';
import { MdDialog, MdDialogRef } from '@angular/material';
import {RouterModule} from '@angular/router';
import { BsDropdownModule } from 'ngx-bootstrap';
import { AlertModule } from 'ngx-bootstrap';
import { BsModalService } from 'ngx-bootstrap';
//import { Ng2BootstrapModule } from 'ng2-bootstrap/ng2-bootstrap';
//import { BsDropdownModule } from 'ng2-bootstrap';
//import { NgbdDropdownBasic } from './dropdown-basic';
//import { BsDropdownModule } from 'ngx-bootstrap';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//http://valor-software.com/ngx-bootstrap/#/dropdowns


import {ConfirmationDialog} from './transactions/confirmation-dialog';
import {TransactionListComponent} from './transactions/transaction-list.component';
import {AccountListComponent} from './transactions/account-list.component';
import {TransactionDetailComponent} from './transactions/transaction-detail.component';
import {TransactionFilterPipe} from './transactions/transaction-filter.pipe';
import {WelcomeComponent} from './home/welcome.component';
import {RemoveSpacesPipe} from './transactions/remove-spaces.pipe';
//import {ProductService} from './transactions/product.service';

@NgModule({
  imports: [ BrowserModule, FormsModule, HttpModule, //AlertModule, //BsDropdownModule.forRoot(),
  RouterModule.forRoot([ 
    {path: 'transactions', component: TransactionListComponent},
    {path: 'accounts', component: AccountListComponent},
    {path: 'transactions/:guid', component: TransactionDetailComponent},
    {path: 'welcome', component: WelcomeComponent},
    {path: '', redirectTo: 'welcome', pathMatch:'full'},
    {path: '**', component:WelcomeComponent}
    ], { useHash: true })
   ],
  declarations: [ AppComponent, AccountListComponent, TransactionListComponent, TransactionFilterPipe, TransactionDetailComponent, WelcomeComponent, RemoveSpacesPipe ],
  bootstrap: [ AppComponent ]
  //entryComponents: [ConfirmationDialog]
})
export class AppModule {

 }
