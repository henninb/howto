import {Pipe, PipeTransform} from '@angular/core';
import {ITransaction} from './transaction';

@Pipe({
    name:'transactionFilter'
})

export class TransactionFilterPipe implements PipeTransform{

  transform(value: ITransaction[], filterBy: string ): ITransaction[] {
    filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;

    return filterBy ? value.filter((product: ITransaction) =>
            product.description.toLocaleLowerCase().indexOf(filterBy) !== -1) : value;
  }
}