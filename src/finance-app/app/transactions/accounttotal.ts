export interface IAccountTotal {
    accountTotal: string;
    accountTotalCleared: string;
}