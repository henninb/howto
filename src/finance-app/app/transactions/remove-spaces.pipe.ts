import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name:'removeSpacesPipe'
})

//export class TransactionFilterPipe implements PipeTransform{
export class RemoveSpacesPipe implements PipeTransform {
  transform(value: string): string {
    return value.replace(/ /g, '');
  }
}
