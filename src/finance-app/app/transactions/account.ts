export interface IAccount {
    accountId: string;
    accountNameOwner: string;
    accountType: string;
    activeStatus: string;
    moniker: string;
    totals: string;
    totalsBalanced: number;
    dateClosed: boolean;
    dateUpdated: string;
    dateAdded: string;
}
