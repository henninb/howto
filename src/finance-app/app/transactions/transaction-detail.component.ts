import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ITransaction} from './transaction';
import { TransactionService } from './transaction.service';

@Component({
  selector: 'pm-details',
  moduleId: module.id,
  templateUrl: './transaction-detail.component.html',
  styleUrls: ['./transaction-detail.component.css']
})

export class TransactionDetailComponent implements OnInit {
  pageTitle: string = 'Transaction Detail';
  errorMessage: string;
  //transaction: ITransaction;
  guid: string = 'guid';
  transaction: ITransaction[] = null;


  constructor(private _route: ActivatedRoute, private _router: Router, private transactionService: TransactionService) {
    console.log('guid: ' + this._route.snapshot.paramMap.get('guid'));
    this.guid = this._route.snapshot.paramMap.get('guid');
  }

  ngOnInit() {
      console.log('TransactionDetailComponent - ngOnInit() - start');
      this.transactionService.getTransaction(this.guid).subscribe(transactions => this.transaction = transactions,
      error => this.errorMessage = <any>error);
      console.log('TransactionDetailComponent - ngOnInit() - end');
  }

}
