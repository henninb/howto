export interface ITransaction {
    transactionId: string;
    guid: string;
    accountType: string;
    accountNameOwner: string;
    transactionDate: string;    
    description: string;
    category: string;
    amount: number;
    cleared: number;
    reoccurring:boolean;
    notes: string;
    dateUpdated: string;
    dateAdded: string;
}
