import { Component, OnInit } from '@angular/core';
//import { ActivatedRoute, Router } from '@angular/router';

import { IAccount} from './account';
import { TransactionService } from './transaction.service';

@Component({
    selector: 'account-details',
    moduleId: module.id,
    templateUrl: 'account-list.component.html',
    styleUrls: ['account-list.component.css'],
   //template: '<account-details>wow</account-details>'
})


export class AccountListComponent implements OnInit {
    pageTitle: string = 'Account List';
    errorMessage: string;
    guid: string = 'guid';
    accounts: IAccount[] = null;


    constructor( private transactionService: TransactionService ) {
        this.transactionService = transactionService;
    }

  ngOnInit() {
    console.log('AccountListComponent - ngOnInit() - start');

    this.transactionService.getAccounts().subscribe(accounts => this.accounts = accounts,
        error => this.errorMessage = <any>error
    );
    console.log('AccountListComponent - ngOnInit() - end');
  }

}