import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';

//import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {ITransaction} from './transaction';
import {IAccount} from './account';
import {IAccountTotal} from './accounttotal'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

interface Message {
  type: string;
  payload: any;
}

type MessageCallback = (payload: any) => void;

//https://embed.plnkr.co/quI0fX/

@Injectable()
export class TransactionService {
  private selectTransactionsUrl:string = 'http://localhost:8080/get_by_account_name_owner';
  private selectAccountsUrl:string = "http://localhost:8080/select_accounts"
  private deleteTransactionUrl:string = 'http://localhost:8080/delete/';
  private selectTransactionUrl:string = 'http://localhost:8080/select/';
  private selectAccountTotalUrl:string = 'http://localhost:8080/fetchAccoutTotals/';

  constructor(private _http: Http) {
   }

   public setSelectAccountTotalUrl( accountNameOwner: string ) {
     this.selectAccountTotalUrl = 'http://localhost:8080/fetchAccoutTotals/' + accountNameOwner;
   }

  public setDeleteTransactionUrl(guid : string ) {
    this.deleteTransactionUrl = 'http://localhost:8080/delete/' + guid;
    console.log('url: ' + this.deleteTransactionUrl);
  }

  public setSelectTransactionsUrl(accountNameOwner : string ) {
    this.selectTransactionsUrl = 'http://localhost:8080/get_by_account_name_owner/' + accountNameOwner;
  }

  public setSelectTransactionUrl(guid: string) {
    this.selectTransactionUrl = 'http://localhost:8080/select/' + guid.replace(/ /g, '');
    console.log('url: ' + this.selectTransactionUrl);
  }

  public getAccountTotal() : Observable <IAccountTotal[]> {
    console.log('url for account_total' + this.selectAccountTotalUrl);
    return this._http.get(this.selectAccountTotalUrl)
                 .map((response: Response) => <IAccountTotal[]>response.json())
                 .do(data => console.log('All: ' + JSON.stringify(data)))
                 .catch(this.handleError)
                 ;
  }

  public getTransaction( guid: string ): Observable <ITransaction[]> {
    console.log('guid = "' + guid + '"');
    guid = guid.replace(/ /g, '');
    console.log('guid = "' + guid + '"');
    this.setSelectTransactionUrl(guid.replace(/ /g, ''));
    return this._http.get(this.selectTransactionUrl)
                 .map((response: Response) => <ITransaction[]>response.json())
                 .do(data => console.log('All: ' + JSON.stringify(data)))
                 .catch(this.handleError)
                 ;
  }


  public getTransactions(): Observable <ITransaction[]> {
    return this._http.get(this.selectTransactionsUrl)
                 .map((response: Response) => <ITransaction[]>response.json())
                 .do(data => console.log('All: ' + JSON.stringify(data)))
                 .catch(this.handleError)
                 ;
  }

  public getAccounts(): Observable <IAccount[]> {
    return this._http.get(this.selectAccountsUrl)
                 .map((response: Response) => <IAccount[]>response.json())
                 .do(data => console.log('All: ' + JSON.stringify(data)))
                 .catch(this.handleError)
                 ;
  }

  public deleteTransaction(): Observable <string[]> {
    console.log(this.deleteTransaction);
    return this._http.get(this.deleteTransactionUrl)
                 .map((response: Response) => <string[]>response.json())
                 .do(data => console.log('All: ' + JSON.stringify(data)))
                 .catch(this.handleError)
                 ;
  }

  private handleError(errorResponse: Response) {
    console.error(errorResponse);
    return Observable.throw(errorResponse.json().error || 'ABORT: server error occured.');
  }
}
