import { Component, OnInit } from '@angular/core';
import {ITransaction} from './transaction';
import {IAccount} from './account';
import {TransactionService} from './transaction.service';
import {IAccountTotal} from './accounttotal';
import {TransactionDetailComponent} from './transaction-detail.component';
import {RemoveSpacesPipe} from './remove-spaces.pipe';

@Component({
    //selector: 'pm-transactions',
    moduleId: module.id,
    templateUrl: 'transaction-list.component.html',
    styleUrls: ['transaction-list.component.css']
})

export class TransactionListComponent implements OnInit {
    pageTitle: string = 'Transaction List';
    imageWidth: number = 45;
    imageMargin: number = 2;
    showImagesButton: boolean = false;
    listFilter: string = "";
    errorMessage: string;
    selectedAccount : string = 'chase_kari'; 

   transactions: ITransaction[] = null;
   accounts: IAccount[] = null;
   results: string[];
   accountTotal: IAccountTotal[] = null;
   x: number = 0;
   
    constructor( private transactionService: TransactionService ) {
        this.transactionService = transactionService;
    }

    onDeleteButtonClick(event: any) {
        let target: any = event.target || event.srcElement || event.currentTarget;
        this.transactionService.setDeleteTransactionUrl(target.title);
        console.log(target.title);
        this.transactionService.deleteTransaction().subscribe();
        this.transactionService.getTransactions().subscribe(transactions => this.transactions = transactions,
                                                      error => this.errorMessage = <any>error
        );
    }

   onEditButtonClick(event: any) {
       
       let elementId: string = (event.target as Element).id;
       let target: any = event.target || event.srcElement || event.currentTarget;

       console.log(target.title);
       console.log('onEditButtonClick called.');
       //console.log(event);
   }
   
    onChange(accountNameOwner: string) {
      console.log('onChange - start');
      this.transactionService.setSelectAccountTotalUrl(accountNameOwner);
      //console.log();
      this.transactionService.getAccountTotal().subscribe(accountTotal => this.accountTotal = accountTotal, 
       error => this.errorMessage = <any>error);
       ;
      
      // console.log(id);
      this.transactionService.setSelectTransactionsUrl(accountNameOwner);
      this.transactionService.getTransactions().subscribe(transactions => this.transactions = transactions,
      error => this.errorMessage = <any>error);

      console.log('onChange - end');
    }

    ngOnInit(): void {
      console.log('ngOnInit - start');
/*
      this.transactionService.getTransactions().subscribe(transactions => this.transactions = transactions,
                                                      error => this.errorMessage = <any>error
      );
*/
      this.transactionService.getAccounts().subscribe(accounts => this.accounts = accounts,
                                                      error => this.errorMessage = <any>error
      );
      console.log('ngOnInit - end');
      
    }
}
