fn main() {
    struct Thing {
        id: i8,
        extra: i8,
    }

    impl Thing {
        pub fn new() -> Thing {
            Thing { id: 3, extra: 2 }
        }

       pub fn get_total(&self) -> i8 {
           self.id + self.extra
       }
    }

    let my_thing = Thing::new();
    println!("the thing's total is {}", my_thing.get_total());
}

