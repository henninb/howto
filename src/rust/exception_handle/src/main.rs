use std::error::Error;

fn main() {

    //println!("the thing's total is ");
}

fn halves_if_even(i: i32) {
    //if i % 2 == 0 { Ok(i/2) } else { Err(/* something */) }
    return i/2;
}

fn do_the_thing(i: i32) -> Result<i32, Error> {
    let i = halves_if_even(i)?;
}
