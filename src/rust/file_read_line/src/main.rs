extern crate oracle;
use std::io::{BufReader,BufRead};
use std::fs::File;
use std::process;
//use std::io;
//use oracle::{Connection, Error};
use oracle::{Connection};

fn main() {
    let mut commit_idx = 0;
    let username = "none";
    let password = "none";
    let database = "dbserver:1521/service";
    //let _value = conn.set_autocommit(true);
    let conn = Connection::connect(username, password, database, &[]).unwrap();

    let file = File::open("input.txt").unwrap();
    for line in BufReader::new(file).lines() {
      let str_line = line.unwrap();
      if str_line == "" {
        println!("invalid line handled below");
      }
      //println!("{}", str_line);
      let mut columns = str_line.split_whitespace();
      let mut idx = 0;

      for column in columns {
        idx = idx + 1; 
      }

      let prep_stmt = "UPDATE blah SET name='myname',modf_ts=sysdate";
        println!("<{}>", prep_stmt);
        let _execute = conn.execute(&prep_stmt, &[]).unwrap();
        commit_idx = commit_idx + 1;
        if (commit_idx % 300) == 0 {
            println!("commit at 300");
            conn.commit();
            //process::exit(1);
        }

    }
    println!("final commit at 300 or less <{}>.", commit_idx % 300);
    println!("records processed <{}>.", commit_idx);
    conn.commit();
    conn.close();
    //close connection
}
