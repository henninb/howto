struct Kitchen {
    plate_count :i8 
}

struct House {
    //Kitchen //anonymous field
    room_count :i8 
}

impl Kitchen {
    pub fn get_plate_count(&self) -> i8 {
        self.plate_count
    }
}

impl House {
    pub fn get_room_count(&self) -> i8 {
        self.room_count
    }
}

pub fn main() {
    let s1 = String();
    let t1 = String::with_capacity(30);

    let s = String::new();
    let t = String::with_capacity(30);

    let house = House { room_count: 7 };
    let kitchen = Kitchen { plate_count: 6 };
    println!("House house has this many rooms: {}", house.get_room_count()); 
    println!("Kitchen kitchen has this many plates: {}", kitchen.get_plate_count()); 
/*
    h := House{Kitchen{10}, 3} 
    //to initialize you have to use composed type name.
    fmt.Println("House h has this many rooms:", h.numOfRooms) 
    //numOfRooms is a field of House
    fmt.Println("House h has this many plates:", h.numOfPlates) 
    //numOfPlates is a field of anonymous field Kitchen, so it can be referred to like a field of House
    fmt.Println("The Kitchen contents of this house are:", h.Kitchen) 
    //we can refer to the embedded struct in its entirety by referring to the name of the struct type
*/
}

