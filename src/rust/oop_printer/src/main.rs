//i8, i16, i32, i64: signed integer with 8/16/32/64 bits
//u8, u16, u32, u64: unsigned integer with 8/16/32/64 bits
//isize, usize: signed/unsigned integer with pointer size (64 bit on 64 bit systems)
trait ResultPrinter {
    fn get_the_result(&self) -> i8;

    fn print_the_result(&self) {
        println!("The result is: {}", self.get_the_result());
    }
}

struct MyResultPrinter {
    result: i8,
}

impl ResultPrinter for MyResultPrinter {
    fn get_the_result(&self) -> i8 {
        self.result
    }
}

pub fn main() {
    let rp = MyResultPrinter {
        result: 4 // Chosen by a fair dice roll
    };
    rp.print_the_result();
}

