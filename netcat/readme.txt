sudo yum install netcat

sudo pacman -S netcat

# tcp server
nc -l 1234
nc -lv 4489
nc -l -p 5555

# tcp client - connection
nc localhost 1234

# udp server
nc -l -u -p 4172

# server a file
nc -q 0 -lvnp 31000 < file.txt
cat file.txt | nc -u -q 0 127.0.0.1 5144

# receive a file
nc -v -w 2 192.168.100.25 31000 > file.txt

# port scan
nc -v -w 2 -z 192.168.100.25 10-100

#proxy server
nc -l 12345 | nc www.freebsd.org 80
nc -l 12345 | nc 192.168.100.254 80


mkfifo tmp
mkfifo tmp2
nc -l 8080 -k > tmp < tmp2 &
while true; do
 openssl s_client -connect www.google.com:443 -quiet < tmp > tmp2
done

# http request
echo "GET /hypertext/WWW/index.html HTTP/1.0\r\nHost: www.freebsd.org\r\n\r\n" | nc www.freebsd.org 80
