sudo eopkg up		Solus - Upgrade/Update
sudo eopkg ur		Solus - Update Repo(s)
sudo eopkg ln		Solus - List newest packages in repo
sudo eopkg dc		Solus - delete cache files (clean)
sudo bleachbit		Bleachbit - bb - clean
bleachbit		Bleachbit - bb - clean
eopkg history		HISTORY - upgrade-install package pkg
dmesg > dmesg.log		HISTORY - text output home
cat .bash_history		HISTORY -terminal - list commands
sudo eopkg la		Solus - List all available packages in repo
sudo eopkg lr		Solus - REPO - List repos
sudo eopkg er ?	<repo>	Solus - REPO - Enable repository
sudo eopkg dr ?	<repo>	Solus - REPO - Disable repo
sudo eopkg it ?	<pkg>	Solus - Install package
sudo eopkg ar ?	<repo>	Solus - REPO - Add a Repository
sudo eopkg hs		Solus - History of eopkg/pisi operations
sudo eopkg up -x ?	<pkg>	Solus - EXCLUDE package from Upgrade
sudo eopkg la | grep ?	<pkg type>	Solus - Search for pacakge type
sudo eopkg bl		Solus - Info on Pkg owner and release
sudo eopkg bi		Solus - Build Pisi/eopkg packages
sudo eopkg check		Solus - Verify installation
sudo eopkg clean		Solus - Clean stale locks
sudo eopkg cp		Solus - Configure pending packages
sudo eopkg dt		Solus - Creates delta packages
sudo eopkg em ?	<pkg>	Solus - Build install source packages
sudo eopkg fc ?	<pkg>	Solus - Fetch a package
sudo eopkg help		Solus - Command Help
sudo eopkg info ?	<pkg>	Solus - Display package Info.
sudo eopkg lc		Solus - List available components
sudo eopkg li		Solus - Print list of installed packages
sudo eopkg lp		Solus - List pending packages
sudo eopkg ls		Solus - List sources
sudo eopkg lu		Solus - List packages to be upgraded
sudo eopkg rbd		Solus - Rebuild database
sudo eopkg rm ?	<pkg>	Solus - remove package
sudo eopkg rr ?	<repo>	Solus - Remove repository
sudo eopkg sr ?	<pkg>	Solus - Search packages
sudo eopkg sf	<file>	Solus - Seach for a file
