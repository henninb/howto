# chars 2 through 5
echo a0667-ww | cut -c2-5

# cut the second column from the record
echo 1,2,3,4,5,6,7,8 | cut -d "," -f 2

# the forth char
echo 0667 | cut -c4-4

# cut date command
echo + echo Script started on Thu Feb 26 00:11:18 CST 2009 | cut -d " " -f 7-
echo + echo Store tpp was successful on Thu Feb 26 02:25:33 CST 2009 | cut -d " " -f 9-

# sort command
cat md5.txt | sort -f -t "," -k2 -k1
cat mp3.txt | sort -f > mp3res.txt

# paste is used to combine files
paste -d "," 2.txt 1.txt > combine.txt

# example
md5sum run.cmd
echo f0f29b76cdf204abad08cfa4b2281694 *run.cmd | cut -d " " -f 1

# datetime manip
echo Jun  8 01:16:38 CDT 2009 | tr -s " " | cut -d " " -f1,2
echo "Jun  8 02:02:40 CDT 2009" | tr -s " " | cut -d " " -f 3


echo "Wednesday 07/01/2009 2354" | cut -d " " -f 3
