/etc/yum.repos.d/rkt.repo


[virt7-rkt-common-candidate]
name=virt7-rkt-common-candidate
baseurl=http://cbs.centos.org/repos/virt7-rkt-common-candidate/x86_64/os/
enabled=1
gpgcheck=0

Install rkt
$ sudo yum install rkt

cat /etc/fedora-release
fedora
    1  uname -a
    2  dnf install rkt
    3  rkt
    4  history

[root@8bae0263d8bf /]# rkt list
UUID            APP             IMAGE NAME                                      STATE   CREATED         STARTED NETWORKS
3b26f797        alpine-sh       quay.io/coreos/alpine-sh:latest                 exited  7 minutes ago
455fb72c        alpine-sh       quay.io/coreos/alpine-sh:latest                 exited  8 minutes ago
8605b78e        nginx           registry-1.docker.io/library/nginx:latest       exited  2 minutes ago
d67fe5f6        nginx           registry-1.docker.io/library/nginx:latest       exited  5 minutes ago

rkt run --net=host --insecure-options=image docker://alpine --interactive
cat /etc/alpine-release
apk add <pkg>
apk add <pkg>


 rkt run --net=host --insecure-options=image docker://alpine --interactive --no-overlay

sudo rkt run  --insecure-options=image docker://centos --interactive
sudo rkt run  --insecure-options=image docker://coreos --interactive
 sudo rkt image rm registry-1.docker.io/library/nginx:latest
 
rkt run quay.io/coreos/alpine-sh --exec /bin/bash
 
 rkt run  --insecure-options=image docker://nginx --interactive --exec /bin/bash
 
[root@8bae0263d8bf /]# rkt run quay.io/coreos/alpine-sh --no-overlay --net=none
[root@8bae0263d8bf /]# rkt run quay.io/coreos/alpine-sh --no-overlay
[root@8bae0263d8bf /]# rkt run quay.io/coreos/etcd --no-overlay
stage1: failed to setup network: failed to create namespace: operation not permitted

 sudo rkt run quay.io/coreos/etcd --interactive

rkt run quay.io/coreos/alpine-sh --no-overlay --net=host
rkt version


sudo rkt run --dns 8.8.8.8 --insecure-options image \
    docker://alpine:edge \
    --seccomp mode=retain,@rkt/default-whitelist,mount,umount2 \
    --exec /bin/sh -- -c "apk update" 

sudo rkt run --dns 8.8.8.8 --insecure-options image \
    docker://alpine:edge \
    --seccomp mode=retain,@rkt/default-whitelist,mount,umount2 \
    --interactve 
	
 sudo rkt image rm registry-1.docker.io/library/nginx:latest