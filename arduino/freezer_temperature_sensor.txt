https://www.youtube.com/watch?v=z59XS2wHOB8

Arduino Nano: http://amzn.to/2b9vi4h
USB Cord: http://amzn.to/2b9jA6G
Power Brick: http://amzn.to/2buHgHS
Temperature Sensor: http://bit.ly/2bDODIW
Project Box: http://amzn.to/2buHcI9
Breadboard: http://amzn.to/2buHjUe
LED light: http://bit.ly/2bLS6sB
Laser Temperature Gun: http://amzn.to/2bDMhtQ
Double Sided Tape: http://amzn.to/2b9jrQC


/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int led = 13;
 
// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT);     
}
 
// the loop routine runs over and over again forever:
void loop() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);               // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);               // wait for a second
}
