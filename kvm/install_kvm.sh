#!/bin/sh

if [ "$OS" = "Linux Mint" ]; then
  sudo apt install cpu-checker
  
  kvm-ok
  
  sudo apt install ebtables dnsmasq firewalld
  # may need a reboot
  
  sudo apt install qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils
  sudo adduser $(id -un) kvm
  sudo adduser $(id -un) libvirt
  
  sudo apt-get install virt-manager
  sudo apt-get install qemu-utils
  
  sudo virsh -c qemu:///system list
elif [ "$OS" = "Linux Mint" ]; then
  sudo pacman -Syu libvirt qemu virt-manager spice-client-gtk virt-viewer gir1.2-spiceclientgtk-3.0
  
  sudo pacman -Syu ebtables dnsmasq firewalld
  
  sudo systemctl start libvirtd
  sudo systemctl enable libvirtd
  sudo systemctl status libvirtd
  
  sudo usermod -a -G libvirt $(id -un)
  
  sudo virsh list --all
  
  sudo virsh net-autostart default
  sudo virsh net-list --all
else
  echo "OS=$OS not found."
fi
exit 0
