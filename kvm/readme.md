# virsh or virt-manager
sudo yum install -y virt-manager kvm libvirt seabios.x86_64 seabios-bin libvirt-daemon-kvm

#sudo service libvirtd start
sudo systemctl start libvirtd
sudo systemctl enable libvirtd

sudo usermod -a -G libvirt $(whoami)

Verify that the 'libvirtd' daemon is running.

Libvirt URI is: qemu:///system

Traceback (most recent call last):
  File "/usr/share/virt-manager/virtManager/connection.py", line 1005, in _do_open
    self._backend.open(self._do_creds_password)
  File "/usr/share/virt-manager/virtinst/connection.py", line 150, in open
    open_flags)
  File "/usr/lib64/python2.7/site-packages/libvirt.py", line 105, in openAuth
    if ret is None:raise libvirtError('virConnectOpenAuth() failed')
libvirtError: Failed to connect socket to '/var/run/libvirt/libvirt-sock': No such file or directory

sudo yum install -y seabios.x86_64 seabios-bin.noarch

[henninb@centos7 qemu]$ qemu-system-x86_64 -L /usr/share/qemu/

(process:4658): GLib-WARNING **: gmem.c:483: custom memory allocation vtable not supported

(qemu-system-x86_64:4658): Gtk-WARNING **: cannot open display:

# for vagrant
sudo yum install -y qemu libvirt libvirt-devel ruby-devel gcc qemu-kvm
vagrant plugin install vagrant-libvirt
