#!/bin/sh

sudo apt install cpu-checker

kvm-ok

sudo apt install ebtables dnsmasq firewalld
# may need a reboot

sudo apt install qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils
sudo adduser $(id -un) kvm
sudo adduser $(id -un) libvirt

sudo apt-get install virt-manager
sudo apt-get install qemu-utils

sudo virsh -c qemu:///system list

exit 0

sudo virsh net-start default
error: Failed to start network default
error: internal error: Failed to initialize a valid firewall backend


$ kvm-ok

If the output is:

INFO: /dev/kvm exists
KVM acceleration can be used

Hardware virtualisation is supported.

Install KVM

$ sudo apt install qemu-kvm libvirt-bin bridge-utils

If you wish to install GUI tool run command:

$ sudo apt-get install virt-manager

I don't recommend you to install qtemu.

Add user to libvirtd group, so that the user can launch VMs without root privilege.
$ sudo adduser username libvirt

Log out and log back in as the user to make the group membership change effective.

Run the following command.

$ virsh -c qemu:///system list