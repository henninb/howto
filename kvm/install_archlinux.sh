#!/bin/sh

sudo pacman -S libvirt qemu virt-manager

sudo pacman -Syu ebtables dnsmasq firewalld

sudo systemctl start libvirtd
sudo systemctl enable libvirtd
sudo systemctl status libvirtd

sudo usermod -a -G libvirt $(whoami)


sudo virsh list --all

sudo virsh net-autostart default
sudo virsh net-list --all

# setup the serial service
sudo systemctl enable serial-getty@ttyS0.service
sudo systemctl start serial-getty@ttyS0.service

sudo virsh console <virtual_name>

exit 0
