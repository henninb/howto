#!/bin/sh

# virsh or virt-manager
sudo yum install -y virt-manager kvm libvirt seabios.x86_64 seabios-bin virt-install

#sudo service libvirtd start
sudo systemctl start libvirtd
sudo systemctl enable libvirtd
sudo systemctl status libvirtd

sudo usermod -a -G libvirt $(whoami)

#fallocate -l 2G /var/kvm/images/centos7.img

sudo mkdir -p /var/kvm/images/

virt-install \
--name centos7 \
--ram 4096 \
--disk path=/var/kvm/images/centos7.img,size=30 \
--vcpus 2 \
--os-type linux \
--os-variant rhel7 \
--network bridge=br0 \
--graphics none \
--console pty,target_type=serial \
--location 'http://mirrors.usinternet.com/centos/7.5.1804/os/x86_64/' \
--extra-args 'console=ttyS0,115200n8 serial'

exit 0
