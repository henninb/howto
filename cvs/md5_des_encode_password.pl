######
# DES/MD5 password generator
######
# Usage: make_pass.pl [<password>]
# - if no password supplied, a random password will be picked up.
#########
# ppm install Crypt-PasswdMD5

use strict;
use Crypt::PasswdMD5;

my $password = defined $ARGV[0]?$ARGV[0]:random_hash(8,32);
print "CLEAR:\t", $password,
   $/, "DES:\t", crypt($password, random_hash(2,2)),
   $/, "MD5:\t", unix_md5_crypt($password, random_hash(8,8)),
   $/;

sub random_hash {
   my ($ash,$ml) = ('', ((defined$_[0]&&$_[0]>0)?$_[0]:4));
   my $Ml = (defined$_[1]&&$_[1]>=$ml)?$_[1]:$ml+5;
   $ash .= ('.','/',0..9,'A'..'Z','a'..'z')[rand 64]
   foreach(1..($ml+int(rand $Ml-$ml)));
   $ash;
}
