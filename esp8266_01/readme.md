# Uart Programming ESP8266-01
VCC (ESP8266-01)    -> 3.3V (POWER) (red)
CH_PD (ESP8266-01)  -> 3.3V (POWER) (red)
RST (ESP8266-01)    -> 3.3V (POWER) (red)
GND (ESP8266-01)    -> GND (POWER) (black)
GND (ESP8266-01)    -> GND (FTDI) (black)
TX (ESP8266-01)     -> RX (FTDI) (green)
RX (ESP8266-01)     -> TX (FTDI) (yellow)
GPIO_0 (ESP8266-01) -> GND (POWER) (black) (Sets the module in boot mode for programming)
