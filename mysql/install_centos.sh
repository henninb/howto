#!/bin/sh

sudo yum install -y net-tools psmisc

# wget https://dev.mysql.com/get/mysql57-community-release-el7-9.noarch.rpm
echo MariaDB is the mysql fork
sudo yum -y install -y mariadb-server mariadb

#sudo service mariadb start
sudo systemctl start mariadb
sudo systemctl status mariadb

mysqladmin -u root password monday1

sudo firewall-cmd --zone=public --add-port=3306/tcp
sudo firewall-cmd --list-ports
sudo firewall-cmd --zone=public --permanent --add-port=3306/tcp

netstat -na | grep LISTEN | grep tcp | grep 3306
sudo fuser 3306/tcp

exit 0
