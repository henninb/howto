ntp time sync

```
sudo VBoxService --timesync-set-start
```

VBoxManage guestproperty set {d3cfd81e-f57a-4e46-a292-5c05246d0f09} "/VirtualBox/GuestAdd/VBoxService/--timesync-set-threshold" 10000
VBoxManage guestproperty set centos7 "/VirtualBox/GuestAdd/VBoxService/--timesync-set-threshold" 10000


list VMs
```
cd C:\Program Files\Oracle\VirtualBox
VBoxManage list runningvms
VBoxManage list vms
```