@echo off

set PATH=C:\Program Files\Oracle\VirtualBox

echo VBoxManage startvm "centos7" --type emergencystop
VBoxManage controlvm  "centos7" savestate
VBoxManage sharedfolder add "centos7" --name projects_windows --hostpath "c:\Users\z037640\projects" --automount
VBoxManage startvm "centos7" --type headless

pause
