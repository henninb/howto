Show VM IP
```
VBoxManage guestproperty get freebsd10 "/VirtualBox/GuestInfo/Net/0/V4/IP"
VBoxManage guestproperty get freebsd64 "/VirtualBox/GuestInfo/Net/0/V4/IP"
VBoxManage guestproperty get windows81 "/VirtualBox/GuestInfo/Net/0/V4/IP"
```


start a VM
```
VBoxManage startvm win81
```

save state VM
```
VBoxManage controlvm windows81 savestate
VBoxManage controlvm <vmname> pause|resume|reset|poweroff|savestate
```