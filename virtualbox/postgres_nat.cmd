@echo off

set PATH=C:\Program Files\Oracle\VirtualBox

VBoxManage controlvm  "centos7" savestate
VBoxManage modifyvm "centos7" --natpf1 "Postgres,tcp,127.0.0.1,5432,,5432"
VBoxManage startvm "centos7" --type headless

pause
