I've enabled Cache Dynamic Content (Youtube, Windows Update, Symantec, Avira, avast refresh pattern selected)

my reference so far, basically my method are the combinations from these sites:
http://wiki.squid-cache.org/Features/StoreID
http://wiki.squid-cache.org/Features/StoreID/DB
http://blog.thelifeofkenneth.com/2014/08/using-squid-storeids-to-optimize-steams.html (tried this to, installed bash on pfsense, but same result)
http://wiki.sebeka.k12.mn.us/web_services:squid_update_cache (same result)


edit: i tink i might have found a solution, im going to have to read through it for a little bit and try to understand how it works completely. http://wiki.squid-cache.org/Features/StoreUrlRewrite 

i gave up and use pfsense + lancache (http://blog.multiplay.co.uk/2014/04/lancache-dynamically-caching-game-installs-at-lans-using-nginx/)


https://github.com/rudiservo/pfsense_storeid

https://doc.pfsense.org/index.php/Setup_Squid_as_a_Transparent_Proxy

web cache with squid

http://whatismyipaddress.com/proxy-check

https://www.youtube.com/watch?v=KwmNcPlR5mk

https://forum.pfsense.org/index.php?topic=102070.0

acl Windows_Update dstdomain windowsupdate.microsoft.com
acl Windows_Update dstdomain .update.microsoft.com
acl Windows_Update dstdomain download.windowsupdate.com
acl Windows_Update dstdomain www.download.windowsupdate.com

range_offset_limit -1 Windows_Update

refresh_pattern -i microsoft.com/.*\.(cab|exe|ms[i|u|f]|asf|wm[v|a]|dat|zip) 4320 80% 43200 reload-into-ims
refresh_pattern -i windowsupdate.com/.*\.(cab|exe|ms[i|u|f]|asf|wm[v|a]|dat|zip) 4320 80% 43200 reload-into-ims


opt1 pfsense

https://www.youtube.com/watch?v=W4ZjYWvtFjw