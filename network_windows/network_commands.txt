netsh interface ip set address name="Local Area Connection" static 192.168.100.150 255.255.255.0 192.168.100.1
netsh interface ip set dns "Local Area Connection" static 192.168.100.1
netsh interface ip set dns "Local Area Connection" static 192.168.100.5
netsh interface ip set dns "Local Area Connection" static 8.8.8.8

netsh interface ip set address "Local Area Connection" dhcp
netsh interface ip set dns "Local Area Connection" dhcp
