default['java']['install_flavor'] = 'openjdk'
default['java']['jdk_version'] = '8'

#default['activemq']['version'] = '5.12.0'
default['activemq']['home'] = '/opt/activemq'
default['activemq']['mirror'] = "https://archive.apache.org/dist/"
default['ipsc-activemq']['user'] = 'activemq'
default['ipsc-activemq']['group'] = 'activemq'
