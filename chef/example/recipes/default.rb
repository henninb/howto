#
# Cookbook:: myexample
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

group 'ActiveMQ Group' do
  group_name node['ipsc-activemq']['group']
  action :create
end

user 'ActiveMQ User' do
  comment 'ActiveMQ Message Broker'
  gid lazy { node['ipsc-activemq']['group'] }
  home node['activemq']['home']
  manage_home false
  shell '/bin/false'
  system true
  username node['ipsc-activemq']['user']
  action :create
end

directory 'ActiveMQ Home' do
  path node['activemq']['home']
  owner node['ipsc-activemq']['user']
  group node['ipsc-activemq']['group']
  mode '775'
  recursive true
end

include_recipe 'activemq'

#include_recipe 'chef-vault'

service 'ipsc-activemq service' do
  service_name 'activemq'
  action :nothing
end

#https://archive.apache.org/dist/activemq/apache-activemq/5.8.0/
