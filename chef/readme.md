#install chef client
curl -L https://omnitruck.chef.io/install.sh | sudo bash

#install the chefdk ubuntu
sudo pkg install chef

#install the chefdk centos
sudo rpm -Uvh https://packages.chef.io/files/stable/chefdk/2.4.17/el/7/chefdk-2.4.17-1.el7.x86_64.rpm

mkdir $HOME/cookbooks
cd $HOME/cookbooks
chef generate cookbook activemq_wrapper
chef generate cookbook postgres_wrapper

gem install vagrant


#add to my Berksfile
cookbook 'nginx', '~> 8.1.2'
cookbook 'java'
cookbook 'postgresql'
cookbook 'activemq'
cookbook 'vagrant'

#cookbook 'java',       '= 1.29.0'
#cookbook 'postgresql', '= 3.4.12'
#cookbook 'mysql',      '= 5.6.3'
#cookbook 'sqlite',     '= 1.1.0'
#cookbook 'maven',      '= 1.2.0'
#cookbook 'groovy',     '= 0.0.1'

#chef-apply name_of_recipe.rb

# -z is for local; -o runlist
sudo chef-client -z -o mycookbookname


#not required
#centos
#```
#wget https://packages.chef.io/files/stable/chef-server/12.17.15/el/7/chef-server-core-12.17.15-1.el7.x86_64.rpm
#wget https://packages.chef.io/stable/el/7/chef-server-core-12.10.0-1.el7.x86_64.rpm
#sudo yum install chef-server-core-12.17.15-1.el7.x86_64.rpm
#```
#sudo chef-server-ctl reconfigure


mkdir $HOME/.chef/cookbooks
knife cookbook site download activemq
knife cookbook site download apache2


sudo su
echo "cookbook_path [ '/root/.chef/cookbooks' ]" > /root/.chef/knife.rb

# depreciated knife cookbook create phpapp
chef generate cookbook phpapp


/etc/chef/solo.rb


[henninb@centos7 cookbooks]$ knife cookbook site install activemq
WARNING: No knife configuration file found
Installing activemq to /home/henninb/.chef/cookbooks
ERROR: The cookbook repo path /home/henninb/.chef/cookbooks does not exist or is not a directory
[henninb@centos7 cookbooks]$ knife cookbook site install getting-started
WARNING: No knife configuration file found
Installing getting-started to /home/henninb/.chef/cookbooks
ERROR: The cookbook repo path /home/henninb/.chef/cookbooks does not exist or is not a directory
[henninb@centos7 cookbooks]$
