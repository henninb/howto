#!/bin/sh

sudo yum install -y mosquitto mosquitto-clients

sudo firewall-cmd --zone=public --add-port=1883/tcp
sudo firewall-cmd --list-ports

sudo firewall-cmd --zone=public --permanent --add-port=1883/tcp

netstat -an | grep LISTEN| grep 1883

echo publish
echo mosquitto_pub -d -t home/lower_bathroom/temperature -m "hello world"

exit 0
