Solder ESP32
https://www.youtube.com/watch?v=un_39GOAn8o

flux - Amtech
https://www.youtube.com/watch?v=7wJhi3UGf8E

# Uart Programming ESP32 
VCC (ESP32)    -> 3.3V (POWER) (red)
GND (ESP32)    -> GND (POWER) (black)
GND (ESP32)    -> GND (FTDI) (black)
TX (ESP32)     -> RX (FTDI) (green)
RX (ESP32)     -> TX (FTDI) (yellow)
GPIO_0 (ESP32) -> GND (POWER) (black) (Sets the module in boot mode for programming)

Sanity Check
Open a terminal emulator on the FTDI port with a BAUD rate of 115200
Hit the RESET button
