#!/bin/sh

sudo yum install --enablerepo=epel-testing npm nodejs
sudo npm install -g react
sudo npm install -g create-react-app
sudo npm install -g gatsby

exit 0
