@echo off

rem wget https://github.com/coreybutler/nvm-windows/releases/download/1.1.6/nvm-setup.zip
wget https://github.com/coreybutler/nvm-windows/releases/download/1.1.7/nvm-setup.zip

nvm install 8.10.0
nvm use 8.10.0
nvm install 4.4.0
nvm use 4.4.0
nvm list

pause
