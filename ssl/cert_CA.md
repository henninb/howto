cd $HOME

# Generate private key - pick 4096 for the size and potentially alogorithm
#openssl genrsa -aes256 -out /root/ca/private/ca.key.pem 4096
openssl genrsa -out ca.key.pem 4096

# 10 year cert
#openssl req -key ca.key.pem -new -x509 -days 3650 -sha256 -extensions v3_ca -out ca.crt.pem
#openssl req -key ca.key.pem -new -x509 -days 3650 -out ca.crt.pem
openssl req -key ca.key.pem -new -x509 -days 3650 -extensions v3_ca -out ca.crt.pem -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=centos7/CN=centos7"
openssl req -key ca.key.pem -new -x509 -days 3650 -extensions v3_ca -out ca.crt.pem -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=freebsd11/CN=freebsd11"

# Generate CSR (certificate signing request)
openssl req -key ca.key.pem -new -out centos_kafka.csr.pem -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=centos7/CN=centos7"
openssl req -key ca.key.pem -new -out centos_apache.csr.pem -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=centos7/CN=centos7"
openssl req -key ca.key.pem -new -out freebsd_apache.csr.pem -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=freebsd11/CN=freebsd11"

openssl x509 -req -days 365 -in centos_kafka.csr.pem -signkey ca.key.pem -out centos_kafka.crt.pem
openssl x509 -req -days 365 -in centos_apache.csr.pem -signkey ca.key.pem -out centos_apache.crt.pem
openssl x509 -req -days 365 -in freebsd_apache.csr.pem -signkey ca.key.pem -out freebsd_apache.crt.pem

openssl x509 -pubkey -noout -in centos_kafka.crt.pem > centos_kafka.crt.pub.pem
openssl x509 -pubkey -noout -in centos_apache.crt.pem > centos_apache.crt.pub.pem
openssl x509 -pubkey -noout -in freebsd_apache.crt.pem > freebsd_apache.crt.pub.pem

#curl --cacert ca.crt.pem https://localhost
