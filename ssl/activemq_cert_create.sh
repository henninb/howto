#!/bin/sh

#Using keytool, create a certificate for the broker:

keytool -genkey -noprompt -keyalg RSA -alias broker -dname "CN=archlinux, OU=archlinux, O=Target, L=Minneapolis, ST=MN, C=US" -keystore broker.ks -storepass monday1 -keypass monday1

#Export the broker's certificate so it can be shared with clients:

echo keytool -export -alias broker -keystore broker.ks -file broker_cert
keytool -export -alias broker -keystore broker.ks -file broker_cert
echo $?

#Create a certificate/keystore for the client:
keytool -genkey -noprompt -alias client -keyalg RSA  -dname "CN=archlinux, OU=archlinux, O=Target, L=Minneapolis, ST=MN, C=US" -keystore client.ks -storepass monday1 -keypass monday1

#Create a truststore for the client, and import the broker's certificate. This establishes that the client "trusts" the broker:

#echo keytool -import -alias broker -keystore client.ts -file broker_cert
keytool -import -alias broker -keystore client.ts -file broker_cert

keytool -importkeystore -srckeystore broker.ks -destkeystore myapp.p12 -srcalias broker -srcstoretype jks -deststoretype pkcs12
openssl pkcs12 -in myapp.p12 -out myapp.pem

sudo cp broker.ks client.ks /opt/activemq/conf
sudo systemctl restart activemq

openssl s_client -connect archlinux:61617 -CAfile myapp.pem
openssl s_client -connect archlinux:61617 -CAfile myapp.pem -state -debug

keytool -list -storepass monday1 -keystore client.ks

#keytool -exportcert -alias tomcat -keystore .keystore > tomcat.cert
#keytool -importcert -alias tomcat -keystore LOCATION_OF_JDK\jre\lib\security\cacerts -file tomcat.cert

exit 0


#https://github.com/apache/activemq/blob/master/activemq-unit-tests/src/test/java/org/apache/activemq/transport/tcp/SslBrokerServiceTest.java#L59
#https://stackoverflow.com/questions/12420354/ssl-handshake-with-activemq-server-gives-nullpointerexception-when-connecting-vi

in the activemq.xml
replace the transportConnectors entry with the following lines (to enable SSL)

        <transportConnectors>
            <transportConnector name="ssl" uri="ssl://0.0.0.0:61617?trace=true&amp;needClientAuth=true"/>
            <transportConnector name="stomp+ssl" uri="stomp+ssl://0.0.0.0:61612"/>
        </transportConnectors>
        <sslContext>
            <sslContext 
            keyStore="broker.ks" keyStorePassword="monday1"
            trustStore="client.ks" trustStorePassword="monday1"/>
        </sslContext>

<sslContext>
     <sslContext keyStore="file:${activemq.base}/conf/server.ks" keyStorePassword="monday1" 
       trustStore="file:${activemq.base}/conf/server.ts" trustStorePassword="monday1" />
  </sslContext>

<beans
  <amq:broker useJmx="false" persistent="false">
 
    <amq:sslContext>
      <amq:sslContext
            keyStore="broker.ks" keyStorePassword="monday1"
            trustStore="client.ks" trustStorePassword="monday1"/>
    </amq:sslContext>
 
    <amq:transportConnectors>
      <amq:transportConnector uri="ssl://localhost:61616" />
    </amq:transportConnectors>
 
  </amq:broker>
</beans>