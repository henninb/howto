cd $HOME
echo subject=/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=centos7/CN=centos7
echo no password

# Generate private key - pick 4096 for the size and potentially alogorithm
#openssl genrsa -aes256 -out /root/ca/private/ca.key.pem 4096
#openssl genrsa -out ca.key.pem 2048
openssl genrsa -out ca.key.pem 4096

# Generate CSR certificate signing request
openssl req -new -key ca.key.pem -out ca.csr.pem -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=centos7/CN=centos7"
openssl req -new -key ca.key.pem -out ca.csr.pem -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=freebsd11/CN=freebsd11"

# Generate Self Signed certificate authority cert - expires in 365 days
openssl x509 -req -days 365 -in ca.csr.pem -signkey ca.key.pem -out ca.crt.pem

# Generate the public key
openssl x509 -pubkey -noout -in ca.crt.pem  > pubkey.pem


openssl s_client -debug -connect centos7:443
openssl s_client -debug -connect centos7:9093
