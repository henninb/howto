Transfer the DD-WRT dd-wrt.v24_std_generic.bin image on the router. It is important that you put the DD-WRT dd-wrt.v24_std_generic.bin firmware image into the ramdisk (/tmp) before you start flashing. 
Rename the dd-wrt.v24_std_generic.bin image dd-wrt.v24_std_generic.trx image and flash via mtd
Code:
cd /tmp 
mv dd-wrt.v24_std_generic.bin dd-wrt.v24_std_generic.trx 
mtd write dd-wrt.v24_std_generic.trx linux && reboot

Wait 2 minutes. The router will reboot itself automatically after the upgrade is complete. 
Do a hard reset and clear your browsers cache 
You are done! Access the router via DD-WRT web GUI at http://192.168.1.1/ (username: root/password: admin)
