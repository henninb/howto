export PATH=$PATH:/usr/java/jdk1.8.0_121/jre

keytool -keystore server.keystore.jks -alias centos7 -validity 365 -genkey -keyalg RSA

in kafka
ssl.endpoint.identification.algorithm=HTTPS


keytool -keystore server.keystore.jks -alias freebsd11 -validity 365 -genkey -keyalg RSA -ext SAN=DNS:freebsd11
openssl req -new -x509 -keyout ca.key.pem -out ca.cert.pem -days 365
keytool -keystore client.truststore.jks -alias CARoot -import -file ca.cert.pem
keytool -keystore server.truststore.jks -alias CARoot -import -file ca.cert.pem


#keytool -keystore freebsd11-keystore.jks -genkey -alias client

keytool -genkey -keyalg RSA -alias endeca -keystore freebsd11-truststore.jks
keytool -delete -alias endeca -keystore freebsd11-truststore.jks
keytool -import -v -trustcacerts -alias rootCA -file cacert.pem -keystore freebsd11-truststore.jks


openssl pkcs12 -export -name freebsd11-kafka -in freebsd11-kafka.cert.pem -inkey freebsd11-kafka.key.pem -out freebsd11-kafka.p12
openssl pkcs12 -export -name freebsd11-kafka -in ca/certs/freebsd11-kafka.cert.pem -inkey freebsd11-kafka.key.pem -out freebsd11-kafka.p12

keytool -importkeystore -destkeystore freebsd11-keystore.jks -srckeystore freebsd11-kafka.p12 -srcstoretype pkcs12 -alias freebsd11-kafka
keytool -importkeystore -destkeystore freebsd11-keystore.jks -srckeystore freebsd11-kafka.p12 -srcstoretype pkcs12 -alias freebsd11-kafka

keytool -genkey -keyalg RSA -alias endeca -keystore freebsd11-keystore.jks
keytool -delete -alias endeca -keystore freebsd11-keystore.jks

#keytool -v -importkeystore -srckeystore eneCert.pkcs12 -srcstoretype PKCS12 -destkeystore keystore.ks -deststoretype JKS

server.properties
-----------------
ssl.client.auth = requested
security.inter.broker.protocol=SSL
listeners=PLAINTEXT://:9092,SSL://:9093
advertised.listeners=PLAINTEXT://:9092,SSL://:9093
ssl.keystore.location=/home/henninb/server-keystore.jks
ssl.keystore.password=monday1
ssl.key.password=monday1
ssl.truststore.location = /home/henninb/server-truststore.jks
ssl.truststore.password=monday1

ssl.client.auth = none ("required" => client authentication is required, "requested"
ssl.client.auth = required ("required" => client authentication is required, "requested"
ssl.client.auth = requested ("required" => client authentication is required, "requested"


If you want to enable SSL for inter-broker communication, add the following to the broker properties file (it defaults to PLAINTEXT):

security.inter.broker.protocol = SSL

openssl s_client -debug -connect centos7:9093
openssl s_client -connect freebsd11:9093 -CAfile $HOME/centos_apache.crt.pem


#!/bin/bash

#https://security.stackexchange.com/questions/73156/whats-the-difference-between-x-509-and-pkcs7-certificate

#keytool -keystore server.keystore.jks -alias centos7 -validity 365 -keyalg RSA -genkey -ext SAN=DNS:centos7 -dname "CN=centos7, OU=centos7, O=Brian LLC, L=Denton, ST=Texas, C=US"
keytool -keystore server.keystore.jks -alias centos7 -validity 365 -keyalg RSA -genkey -dname "CN=centos7, OU=centos7, O=Brian LLC, L=Denton, ST=Texas, C=US"
openssl req -new -x509 -keyout ca.key.pem -out ca.crt.pem -days 365 -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=centos7/CN=centos7"
#openssl genrsa -out ca.key.pem 4096
keytool -keystore server.truststore.jks -alias CARoot -import -file ca.crt.pem
keytool -keystore client.truststore.jks -alias CARoot -import -file ca.crt.pem
keytool -keystore server.keystore.jks -alias centos7 -certreq -file cert-file
openssl x509 -req -CA ca.crt.pem -CAkey ca.key.pem -in cert-file -out cert-signed -days 365 -CAcreateserial -passin pass:monday1
keytool -keystore server.keystore.jks -alias CARoot -import -file ca.crt.pem -storepass monday1
keytool -keystore server.keystore.jks -alias centos7 -import -file cert-signed -storepass monday1

#sudo cp $HOME/ca.crt.pem /etc/pki/ca-trust/source/anchors/
#sudo update-ca-trust extract


openssl s_client -debug -connect centos7:9093
openssl s_client -debug -connect centos7:9093 -CAfile $HOME/ca.crt.pem
openssl s_client -connect centos7:9093 -cert $HOME/ca.crt.pem -key $HOME/ca.key.pem

keytool -list -v -keystore server.keystore.jks
keytool -printcert -v -file ca.crt.pem


If SSL is not enabled for inter-broker communication (see below for how to enable it), both PLAINTEXT and SSL ports will be necessary.

server.properties
listeners=PLAINTEXT://centos7:9092,SSL://centos7:9093
ssl.keystore.location=/home/henninb/server.keystore.jks
ssl.keystore.password=monday1
ssl.key.password=monday1
ssl.truststore.location=/home/henninb/server.truststore.jks
ssl.truststore.password=monday1
ssl.ca.location=/home/henninb/ca.crt.pem
ssl.keystore.type=JKS
ssl.truststore.type=JKS


kafka-console-producer.sh --broker-list centos7:9093 --topic test --producer.config client-ssl.properties
kafka-console-consumer.sh --bootstrap-server centos7:9093 --topic test --new-consumer --consumer.config client-ssl.properties








consumer.properties
-------------------
bootstrap.servers=centos7:9093
serializer.class=kafka.serializer.StringEncoder
request.required.acks=1
value.serializer=org.apache.kafka.common.serialization.ByteArraySerializer
key.serializer=org.apache.kafka.common.serialization.StringSerializer
security.protocol=SSL
ssl.protocol=TLSv1.2
ssl.enabled.protocols=TLSv1.2
ssl.client.auth=required
ssl.truststore.type=JKS
ssl.truststore.location=/home/henninb/server.truststore.jks
ssl.truststore.password=monday1
ssl.keystore.location=/home/henninb/server.keystore.jks
ssl.keystore.password=monday1
