Why is advertised listeners needed?

When a client connect to a Kafka cluster by specifying "bootstrap.servers=...", it do not have to be a full list of all Kafka brokers, because client doesn't use it to send data to Kafka directly. Rather, client will somehow find zookeeper, and get the whole list of Kafka brokers(may be part of them that are needed for certain topic) from zookeeper. So "ADVERTISED_LISTENER" is used for client to connect to Kafka.

I used to thought that "ADVERTISED_LISTENER" is just used for zookeeper to communicate with all Kafka brokers. If so, it is really confused why the intranet address(which is the default value) do not work.


listeners=PLAINTEXT://host.name:port,SSL://host.name:port
# The following is only needed if the value is different from 'listeners', but it should contain
# the same security protocols as 'listeners'
advertised.listeners=PLAINTEXT://host.name:port,SSL://host.name:port

# start manually foreground
sudo su kafka -c "/opt/kafka/bin/kafka-server-start.sh /opt/kafka/config/server.properties"
sudo su kafka -c "/opt/kafka/bin/zookeeper-server-start.sh /opt/kafka/config/zookeeper.properties"

# start manually background
sudo su kafka -c "nohup /opt/kafka/bin/zookeeper-server-start.sh /opt/kafka/config/zookeeper.properties &"
sudo su kafka -c "nohup /opt/kafka/bin/kafka-server-start.sh /opt/kafka/config/server.properties &"
