#!/bin/sh

echo export PATH=/opt/kafka/bin:$PATH

curl https://archive.apache.org/dist/kafka/1.0.0/kafka_2.11-1.0.0.tgz --output  kafka_2.11-1.0.0.tgz
tar xvf kafka_2.11-1.0.0.tgz
sudo mv kafka_2.11-1.0.0 /opt/kafka

sudo useradd kafka -m
sudo chown -R kafka:kafka /opt/kafka

sudo sed -i "s/#listeners=PLAINTEXT:\/\/:9092/listeners=PLAINTEXT:\/\/:9092,SSL:\/\/:9093/g" /opt/kafka/config/server.properties
sudo sed -i "s/#advertised.listeners=PLAINTEXT:\/\/your.host.name:9092/advertised.listeners=PLAINTEXT:\/\/:9092,SSL:\/\/:9093/g" /opt/kafka/config/server.properties

cat > zookeeper.service <<'EOF'
[Unit]
Description=Apache Zookeeper server (Kafka)
Documentation=http://zookeeper.apache.org
Requires=network.target remote-fs.target
After=network.target remote-fs.target

[Service]
Type=simple
User=kafka
Group=kafka
Environment=JAVA_HOME=/etc/alternatives/jre
ExecStart=/opt/kafka/bin/zookeeper-server-start.sh /opt/kafka/config/zookeeper.properties
ExecStop=/opt/kafka/bin/zookeeper-server-stop.sh

[Install]
WantedBy=multi-user.target
EOF


cat > kafka.service <<'EOF'
[Unit]
Description=Apache Kafka server (broker)
Documentation=http://kafka.apache.org/documentation.html
Requires=network.target remote-fs.target 
After=network.target remote-fs.target zookeeper.service

[Service]
Type=simple
User=kafka
Group=kafka
Environment=JAVA_HOME=/etc/alternatives/jre
ExecStart=/opt/kafka/bin/kafka-server-start.sh /opt/kafka/config/server.properties
ExecStop=/opt/kafka/bin/kafka-server-stop.sh

[Install]
WantedBy=multi-user.target
EOF

#/etc/init
#initctl reload-configuration
#/etc/init.d/
#initctl list

sudo cp kafka.service /etc/systemd/system/kafka.service
sudo cp zookeeper.service /etc/systemd/system/zookeeper.service

sudo systemctl enable zookeeper
sudo systemctl enable kafka

sudo systemctl daemon-reload
sudo systemctl start zookeeper.service
sudo systemctl start kafka.service

# manually start
sudo su kafka -c "nohup /opt/kafka/bin/zookeeper-server-start.sh /opt/kafka/config/zookeeper.properties &"
sudo su kafka -c "nohup /opt/kafka/bin/kafka-server-start.sh /opt/kafka/config/server.properties &"

sleep 10

netstat -na | grep LISTEN | grep tcp | grep 9093
netstat -na | grep LISTEN | grep tcp | grep 9092
netstat -na | grep LISTEN | grep tcp | grep 2181

kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic ynot
kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic 3d-whse-events
kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test

sudo fuser 2181/tcp
sudo fuser 9092/tcp
sudo fuser 9093/tcp

exit 0
