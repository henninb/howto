#!/bin/sh

export PATH=/opt/kafka/bin:$PATH

sudo pacman -S net-tools psmisc wget curl java-1.8.0-openjdk

if [ ! -f kafka_2.11-1.0.0.tgz ]; then
  #curl http://apache.cs.utah.edu/kafka/0.11.0.2/kafka_2.12-0.11.0.2.tgz --output 
  curl https://archive.apache.org/dist/kafka/1.0.0/kafka_2.11-1.0.0.tgz --output kafka_2.11-1.0.0.tgz
fi

sudo tar -zxvf kafka_2.11-1.0.0.tgz -C /opt
sudo ln -s /opt/kafka_2.11-1.0.0 /opt/kafka
#rm kafka_2.11-1.0.0.tgz
#rm -rf kafka_2.11-0.11.0.0

#tar xvf kafka_2.11-1.0.0.tgz
#sudo mv kafka_2.11-1.0.0 /opt/kafka

sudo useradd kafka -m
sudo chown -R kafka:kafka /opt/kafka/
sudo chown -R kafka:kafka /opt/kafka_2.11-1.0.0/

sudo sed -i "s/#listeners=PLAINTEXT:\/\/:9092/listeners=PLAINTEXT:\/\/:9092,SSL:\/\/:9093/g" /opt/kafka/config/server.properties
sudo sed -i "s/#advertised.listeners=PLAINTEXT:\/\/your.host.name:9092/advertised.listeners=PLAINTEXT:\/\/:9092,SSL:\/\/:9093/g" /opt/kafka/config/server.properties

cat > zookeeper.service <<'EOF'
[Unit]
Description=Apache Zookeeper server (Kafka)
Documentation=http://zookeeper.apache.org
Requires=network.target remote-fs.target
After=network.target remote-fs.target

[Service]
Type=simple
User=kafka
Group=kafka
Environment=JAVA_HOME=/usr/lib/jvm/default
ExecStart=/opt/kafka/bin/zookeeper-server-start.sh /opt/kafka/config/zookeeper.properties
ExecStop=/opt/kafka/bin/zookeeper-server-stop.sh

[Install]
WantedBy=multi-user.target
EOF

cat > kafka.service <<'EOF'
[Unit]
Description=Apache Kafka server (broker)
Documentation=http://kafka.apache.org/documentation.html
Requires=network.target remote-fs.target 
After=network.target remote-fs.target zookeeper.service

[Service]
Type=simple
User=kafka
Group=kafka
Environment=JAVA_HOME=/usr/lib/jvm/default
ExecStart=/opt/kafka/bin/kafka-server-start.sh /opt/kafka/config/server.properties
ExecStop=/opt/kafka/bin/kafka-server-stop.sh

[Install]
WantedBy=multi-user.target
EOF

sudo cp -v kafka.service /etc/systemd/system/kafka.service
sudo cp -v zookeeper.service /etc/systemd/system/zookeeper.service


sudo systemctl enable zookeeper
sudo systemctl enable kafka

sudo systemctl daemon-reload
sudo systemctl start zookeeper
sudo systemctl start kafka
sudo systemctl status kafka



sleep 10

netstat -na | grep LISTEN | grep tcp | grep 9093
netstat -na | grep LISTEN | grep tcp | grep 9092
netstat -na | grep LISTEN | grep tcp | grep 2181

sudo fuser 2181/tcp
sudo fuser 9092/tcp
sudo fuser 9093/tcp

echo security.inter.broker.protocol=SSL
echo ssl.keystore.location=/home/henninb/server.keystore.jks
echo ssl.keystore.password=monday1
echo ssl.key.password=monday1

echo delete.topic.enable = true

#kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic ynot
#kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic 3d-whse-events
#kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test

exit 0
