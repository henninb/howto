#!/bin/sh

echo kills processes
sudo fuser -k 2181/tcp
sudo fuser -k 9092/tcp
sudo fuser -k 9093/tcp
sudo service zookeeper restart
sleep 10
sudo service kafka restart

#sudo systemctl restart zookeeper
#sudo systemctl restart kafka

sleep 10

echo "kafka on 9092"
netstat -na | grep LISTEN | grep tcp | grep 9092

echo "zookeeper on 2181" 
netstat -na | grep LISTEN | grep tcp | grep 2181

echo "kafka on 9093"
netstat -na | grep LISTEN | grep tcp | grep 9093

exit 0
