@echo off

set PATH=C:\Program Files\Oracle\VirtualBox

rem netsh advfirewall firewall add rule name="Kafka TCP Port 9092" dir=in action=allow protocol=TCP localport=9092
rem netsh advfirewall firewall add rule name="Kafka TCP Port 9092" dir=out action=allow protocol=TCP localport=9092

rem netsh advfirewall firewall add rule name="Zookeeper TCP Port 2181" dir=in action=allow protocol=TCP localport=2181
rem netsh advfirewall firewall add rule name="Zookeeper TCP Port 2181" dir=out action=allow protocol=TCP localport=2181

VBoxManage controlvm  "centos7" savestate
VBoxManage modifyvm "centos7" --natpf1 "zookeeper,tcp,127.0.0.1,2181,,2181"
VBoxManage modifyvm "centos7" --natpf1 "kafka,tcp,127.0.0.1,9092,,9092"
VBoxManage startvm "centos7" --type headless

pause
