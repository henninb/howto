show ports
```
netstat -na | grep LIST | grep TCP | grep 9092
netstat -na | grep LIST | grep TCP |grep 2181
```

Start the servers
```
c:\kafka\bin\windows\zookeeper-server-start.bat c:\kafka\config\zookeeper.properties
c:\kafka\bin\windows\kafka-server-start.bat c:\kafka\config\server.properties
```

create a topic
```
C:\kafka\bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic ynot
C:\kafka\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --list
```

start a consumer
```
C:\kafka\bin\windows\kafka-console-consumer.bat --zookeeper localhost:2181 --topic ynot --from-beginning
```

start a producer
```
C:\kafka\bin\windows\kafka-console-producer.bat --broker-list localhost:9092 --topic ynot
```