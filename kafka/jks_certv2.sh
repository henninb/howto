#!/bin/sh

#https://security.stackexchange.com/questions/73156/whats-the-difference-between-x-509-and-pkcs7-certificate

#keytool -keystore server.keystore.jks -alias centos7 -validity 365 -keyalg RSA -genkey -ext SAN=DNS:centos7 -dname "CN=centos7, OU=centos7, O=Brian LLC, L=Denton, ST=Texas, C=US"

rm *.jks *.pem

echo '00 - Generating Keys and Certificates for Kafka Brokers'
#keytool -keystore server.keystore.jks -alias centos7 -validity 365 -genkey  
keytool -keystore server.keystore.jks -alias centos7 -validity 365 -keyalg RSA -genkey -dname "CN=centos7, OU=centos7, O=Brian LLC, L=Denton, ST=Texas, C=US" -storepass monday1
echo '11 - Creating Your Own Certificate Authority'
openssl req -new -x509 -keyout ca.key.pem -out ca.crt.pem -days 365 -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=centos7/CN=centos7" -passin pass:monday1 -passout pass:monday1
#openssl req -new -x509 -keyout ca-key -out ca-cert -days 365
echo '12'
#openssl genrsa -out ca.key.pem 4096
keytool -keystore server.truststore.jks -alias CARoot -import -file ca.crt.pem -storepass monday1
echo '13 - Add the generated CA to the client truststores so that clients can trust this CA'
keytool -keystore client.truststore.jks -alias CARoot -import -file ca.crt.pem -storepass monday1
#keytool -keystore client.truststore.jks -alias CARoot -import -file {ca-cert}
echo 14
keytool -keystore server.keystore.jks -alias centos7 -certreq -file crt.file.pem -storepass monday1
echo 15
openssl x509 -req -CA ca.crt.pem -CAkey ca.key.pem -in crt.file.pem -out crt.signed.pem -days 365 -CAcreateserial -passin pass:monday1
echo 16
keytool -keystore server.keystore.jks -alias CARoot -import -file ca.crt.pem -storepass monday1
echo 17
keytool -keystore server.keystore.jks -alias centos7 -import -file crt.signed.pem -storepass monday1
echo 18

#sudo cp $HOME/ca.crt.pem /etc/pki/ca-trust/source/anchors/
#sudo update-ca-trust extract


#openssl s_client -debug -connect centos7:9093
#openssl s_client -debug -connect centos7:9093 -CAfile $HOME/ca.crt.pem
#openssl s_client -connect centos7:9093 -cert $HOME/ca.crt.pem -key $HOME/ca.key.pem

exit 0
