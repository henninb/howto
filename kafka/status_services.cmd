@echo off

set PATH=C:\opscode\chefdk\embedded\bin;%PATH%

rem netsh advfirewall firewall add rule name="Kafka TCP Port 9092" dir=in action=allow protocol=TCP localport=9092
rem netsh advfirewall firewall add rule name="Kafka TCP Port 9092" dir=out action=allow protocol=TCP localport=9092

rem netsh advfirewall firewall add rule name="Zookeeper TCP Port 2181" dir=in action=allow protocol=TCP localport=2181
rem netsh advfirewall firewall add rule name="Zookeeper TCP Port 2181" dir=out action=allow protocol=TCP localport=2181

echo "kafka on 9092"
netstat -na | grep LISTEN | grep TCP | grep 9092

echo "zookeeper on 2181" 
netstat -na | grep LISTEN | grep TCP | grep 2181

pause
