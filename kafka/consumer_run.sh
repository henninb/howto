#!/bin/sh

export PATH=/opt/kafka/bin:$PATH
#kafka-console-consumer.sh --zookeeper localhost:2181 --topic 3d-whse-events --from-beginning
#kafka-console-consumer.sh --topic 3d-whse-events --from-beginning --bootstrap-server localhost:9093 --consumer.config /opt/kafka/config/client-ssl.properties
kafka-console-consumer.sh --topic 3d-whse-events --from-beginning --bootstrap-server localhost:9092

exit 0
