#!/bin/sh

#set PATH=c:\kafka\bin\windows;C:\Java\jre\bin;C:\Program^ Files\7-Zip;C:\ProgramData\chocolatey\bin

# curl https://archive.apache.org/dist/kafka/1.0.0/kafka_2.11-1.0.0.tgz --output kafka_2.11-1.0.0.tgz
curl --version

# curl --cert-type DER --cert tgt-ca-bundle.crt https://archive.apache.org/dist/kafka/1.0.0/kafka_2.11-1.0.0.tgz --output kafka_2.11-1.0.0.tgz
# curl --cert-type PEM --cert tgt-ca-bundle.crt https://archive.apache.org/dist/kafka/1.0.0/kafka_2.11-1.0.0.tgz --output kafka_2.11-1.0.0.tgz
if [ ! -f kafka_2.11-1.0.0.tgz ]; then
  curl https://archive.apache.org/dist/kafka/1.0.0/kafka_2.11-1.0.0.tgz --output kafka_2.11-1.0.0.tgz
fi

mkdir -p /c/opt
tar -zxvf kafka_2.11-1.0.0.tgz -C /c/opt

# curl --insecure https://archive.apache.org/dist/kafka/1.0.0/kafka_2.11-1.0.0.tgz --output kafka_2.11-1.0.0.tgz
# curl -X GET --cert mycertandkey.pem:mypassphrase -H 'Accept-Encoding: gzip,deflate' -H 'Content-Type: application/json' https://api.URL.com

#  Use the specified HTTP proxy. 
#  If the port number is not specified, it is assumed at port 1080.

# --proxy <[protocol://][user:password@]proxyhost[:port]>


# tar xvf kafka_2.11-1.0.0.tgz
# sudo mv kafka_2.11-1.0.0 /opt/kafka

# echo "cd /opt/kafka && bin/zookeeper-server-start.sh config/zookeeper.properties"
# echo "cd /opt/kafka && sudo bin/kafka-server-start.sh config/server.properties"
# kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic ynot
# kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic 3d-whse-events
# kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test

exit 0
