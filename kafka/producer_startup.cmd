@echo off

set PATH=c:\kafka\bin\windows;C:\Java\jre\bin

rem netsh advfirewall firewall add rule name="Kafka TCP Port 9092" dir=in action=allow protocol=TCP localport=9092
rem netsh advfirewall firewall add rule name="Kafka TCP Port 9092" dir=out action=allow protocol=TCP localport=9092

rem netsh advfirewall firewall add rule name="Zookeeper TCP Port 2181" dir=in action=allow protocol=TCP localport=2181
rem netsh advfirewall firewall add rule name="Zookeeper TCP Port 2181" dir=out action=allow protocol=TCP localport=2181

call kafka-console-producer.bat --broker-list centos7:9092 --topic 3d-whse-events

call kafka-topics.bat --list --zookeeper localhost:2181
call kafka-console-producer.bat --broker-list localhost:9092 --topic 3d-whse-events

pause
