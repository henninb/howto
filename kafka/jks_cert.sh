## 1. Create certificate authority (CA)
openssl req -new -x509 -keyout ca.key.pem -out ca.crt.pem -days 365 -passin pass:monday1 -passout pass:monday1 -subj "/CN=<domain>/OU=<unit>/O=<org>/L=<loc>/ST=<state>/C=<country>"
## 2. Create client keystore
keytool -noprompt -keystore client.keystore.jks -genkey -alias centos7 -dname "CN=centos7, OU=centos7, O=Brian LLC, L=Denton, ST=Texas, C=US" -storepass monday1 -keypass monday1
## 3. Sign client certificate
keytool -noprompt -keystore client.keystore.jks -alias centos7 -certreq -file crt.unsigned.pem -storepass monday1
openssl x509 -req -CA ca.crt.pem -CAkey ca.key.pem -in crt.unsigned.pem -out crt.signed.pem -days 365 -CAcreateserial -passin pass:monday1
## 4. Import CA and signed client certificate into client keystore
keytool -noprompt -keystore client.keystore.jks -alias CARoot -import -file ca.crt.pem -storepass monday1
keytool -noprompt -keystore client.keystore.jks -alias centos7 -import -file crt.signed.pem -storepass monday1
## 5. Import CA into client truststore (only for debugging with Java consumer)
keytool -noprompt -keystore client.truststore.jks -alias CARoot -import -file ca.crt.pem -storepass monday1
## 6. Import CA into server truststore
keytool -noprompt -keystore server.truststore.jks -alias CARoot -import -file ca.crt.pem -storepass monday1
## 7. Create PEM files for Ruby client
### 7.1 Extract signed client certificate
keytool -noprompt -keystore client.keystore.jks -exportcert -alias centos7 -rfc -storepass monday1 -file client_cert.pem
### 7.2 Extract client key
keytool -noprompt -srckeystore client.keystore.jks -importkeystore -srcalias centos7 -destkeystore cert_and_key.p12 -deststoretype PKCS12 -srcstorepass monday1 -storepass monday1
openssl pkcs12 -in cert_and_key.p12 -nocerts -nodes -passin pass:monday1 -out client.crt.key.pem
### 7.3 Extract CA certificate
keytool -noprompt -keystore client.keystore.jks -exportcert -alias CARoot -rfc -file ca.crt.pem -storepass monday1

## 1. Create server keystore
keytool -noprompt -keystore server.keystore.jks -genkey -alias centos7 -dname "CN=<hostname>, OU=<unit>, O=<org>, L=<loc>, ST=<state>, C=<country>" -storepass monday1 -keypass monday1

## 2. Sign server certificate
keytool -noprompt -keystore server.keystore.jks -alias centos7 -certreq -file crt.unsigned.pem -storepass monday1
openssl x509 -req -CA ca.crt.pem -CAkey ca.key.pem -in crt.unsigned.pem -out crt.signed.pem -days 365 -CAcreateserial -passin pass:monday1

## 3. Import CA and signed server certificate into server keystore
keytool -noprompt -keystore server.keystore.jks -alias CARoot -import -file ca.crt.pem -storepass monday1
keytool -noprompt -keystore server.keystore.jks -alias centos7 -import -file crt.signed.pem -storepass monday1



#!/bin/sh

rm *.jks *.pem

echo 0
openssl req -new -x509 -keyout ca.key.pem -out ca.crt.pem -days 365 -passin pass:monday1 -passout pass:monday1 -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=centos7/CN=centos7"
echo 1
keytool -noprompt -keystore client.keystore.jks -genkey -alias centos7 -dname "CN=centos7, OU=centos7, O=Brian LLC, L=Denton, ST=Texas, C=US" -storepass monday1 -keypass monday1
echo 2
keytool -noprompt -keystore client.keystore.jks -alias centos7 -certreq -file crt.unsigned.pem -storepass monday1
echo 3
openssl x509 -req -CA ca.crt.pem -CAkey ca.key.pem -in crt.unsigned.pem -out crt.signed.pem -days 365 -CAcreateserial -passin pass:monday1
echo 4
keytool -noprompt -keystore client.keystore.jks -alias CARoot -import -file ca.crt.pem -storepass monday1
echo 5
keytool -noprompt -keystore client.keystore.jks -alias centos7 -import -file crt.signed.pem -storepass monday1
echo 6
keytool -noprompt -keystore client.truststore.jks -alias CARoot -import -file ca.crt.pem -storepass monday1
echo 7
keytool -noprompt -keystore server.truststore.jks -alias CARoot -import -file ca.crt.pem -storepass monday1
echo 8
keytool -noprompt -keystore client.keystore.jks -exportcert -alias centos7 -rfc -storepass monday1 -file client_cert.pem
echo 9
keytool -noprompt -srckeystore client.keystore.jks -importkeystore -srcalias centos7 -destkeystore cert_and_key.p12 -deststoretype PKCS12 -srcstorepass monday1 -storepass monday1
echo 10
openssl pkcs12 -in cert_and_key.p12 -nocerts -nodes -passin pass:monday1 -out client.crt.key.pem
echo 11
keytool -noprompt -keystore client.keystore.jks -exportcert -alias CARoot -rfc -file ca.crt.pem -storepass monday1
echo 12
keytool -noprompt -keystore server.keystore.jks -genkey -alias centos7 -dname "CN=centos7, OU=centos7, O=Brian LLC, L=Denton, ST=Texas, C=US" -storepass monday1 -keypass monday1
echo 13
keytool -noprompt -keystore server.keystore.jks -alias centos7 -certreq -file crt.unsigned.pem -storepass monday1
echo 14
openssl x509 -req -CA ca.crt.pem -CAkey ca.key.pem -in crt.unsigned.pem -out crt.signed.pem -days 365 -CAcreateserial -passin pass:monday1
echo 15
keytool -noprompt -keystore server.keystore.jks -alias CARoot -import -file ca.crt.pem -storepass monday1
echo 16
keytool -noprompt -keystore server.keystore.jks -alias centos7 -import -file crt.signed.pem -storepass monday1
echo 17

exit 0
