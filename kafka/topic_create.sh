#!/bin/sh

export PATH=/opt/kafka/bin:$PATH
echo "** Before **"
kafka-topics.sh --list --zookeeper localhost:2181
echo "** Before **"
kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic ynot
kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic 3d-whse-events
kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test
echo "** After **"
kafka-topics.sh --list --zookeeper localhost:2181
echo "** After **"
exit 0
