@echo off

set PATH=c:\kafka\bin\windows;C:\Java\jre\bin;C:\Program^ Files\7-Zip;C:\ProgramData\chocolatey\bin

rem curl https://archive.apache.org/dist/kafka/1.0.0/kafka_2.11-1.0.0.tgz --output kafka_2.11-1.0.0.tgz
curl --version

rem curl --cert-type DER --cert tgt-ca-bundle.crt https://archive.apache.org/dist/kafka/1.0.0/kafka_2.11-1.0.0.tgz --output kafka_2.11-1.0.0.tgz
rem curl --cert-type PEM --cert tgt-ca-bundle.crt https://archive.apache.org/dist/kafka/1.0.0/kafka_2.11-1.0.0.tgz --output kafka_2.11-1.0.0.tgz
curl https://archive.apache.org/dist/kafka/1.0.0/kafka_2.11-1.0.0.tgz --output kafka_2.11-1.0.0.tgz

rem curl --insecure https://archive.apache.org/dist/kafka/1.0.0/kafka_2.11-1.0.0.tgz --output kafka_2.11-1.0.0.tgz
rem curl -X GET --cert mycertandkey.pem:mypassphrase -H 'Accept-Encoding: gzip,deflate' -H 'Content-Type: application/json' https://api.URL.com

   rem  Use the specified HTTP proxy. 
   rem  If the port number is not specified, it is assumed at port 1080.

rem --proxy <[protocol://][user:password@]proxyhost[:port]>


rem tar xvf kafka_2.11-1.0.0.tgz
rem sudo mv kafka_2.11-1.0.0 /opt/kafka

rem echo "cd /opt/kafka && bin/zookeeper-server-start.sh config/zookeeper.properties"
rem echo "cd /opt/kafka && sudo bin/kafka-server-start.sh config/server.properties"
rem kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic ynot
rem kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic 3d-whse-events
rem kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test

pause
