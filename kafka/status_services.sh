#!/bin/sh

echo "kafka on 9092"
netstat -na | grep LISTEN | grep tcp | grep 9092

echo "zookeeper on 2181" 
netstat -na | grep LISTEN | grep tcp | grep 2181

echo "kafka on 9093"
netstat -na | grep LISTEN | grep tcp | grep 9093

#sudo fuser -k 2181/tcp
#sudo fuser -k 9092/tcp
#sudo fuser -k 9093/tcp

exit 0
