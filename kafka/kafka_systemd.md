sudo useradd -s /bin/false -r kafka

sudo systemctl is-enabled zookeeper


netstat -na | grep LIST | grep tcp | grep 9092
netstat -na | grep LIST | grep tcp | grep 2181


[henninb@centos7 ~]$ sudo systemctl is-enabled zookeeper
[sudo] password for henninb:
disabled

sudo systemctl enable zookeeper
sudo systemctl enable kafka


sudo vi /etc/systemd/system/zookeeper.service
[Unit]
Description=Apache Zookeeper server (Kafka)
Documentation=http://zookeeper.apache.org
Requires=network.target remote-fs.target
After=network.target remote-fs.target

[Service]
Type=simple
User=henninb
Group=henninb
Environment=JAVA_HOME=/etc/alternatives/jre
ExecStart=/opt/kafka/bin/zookeeper-server-start.sh /opt/kafka/config/zookeeper.properties
ExecStop=/opt/kafka/bin/zookeeper-server-stop.sh

[Install]
WantedBy=multi-user.target



vi /etc/systemd/system/kafka.service

[Unit]
Description=Apache Kafka server (broker)
Documentation=http://kafka.apache.org/documentation.html
Requires=network.target remote-fs.target 
After=network.target remote-fs.target zookeeper.service

[Service]
Type=simple
User=henninb
Group=henninb
Environment=JAVA_HOME=/etc/alternatives/jre
ExecStart=/opt/kafka/bin/kafka-server-start.sh /opt/kafka/config/server.properties
ExecStop=/opt/kafka/bin/kafka-server-stop.sh

[Install]
WantedBy=multi-user.target


sudo systemctl daemon-reload
sudo systemctl start kafka-zookeeper.service
systemctl start kafka.service
