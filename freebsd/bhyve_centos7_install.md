
**download centos iso**

```shell
wget https://buildlogs.centos.org/rolling/7/isos/x86_64/CentOS-7-x86_64-Minimal-1511.iso
```

**install package** 

```shell
sudo pkg install -y vm-bhyve grub2-bhyve
```

**update configuration**

```shell
sudo sysrc vm_enable="YES"
sudo sysrc vm_dir="/vm"
```

**initialize virtual instance**

```shell
sudo vm init
```

**copy templates to the virtual instance**

```shell
sudo cp /usr/local/share/examples/vm-bhyve/* /vm/.templates/
```

**create a switch**

```shell
sudo vm switch create public
```

**add a network interface**

```shell
sudo vm switch add public alc0
sudo vm switch add public re0
```

1. sudo vm iso CentOS-7-x86_64-Minimal-1511.iso
2. sudo vm create -t centos7 -s 15G centos7
3. sudo vm -f install centos7 CentOS-7-x86_64-Minimal-1511.iso
   Note: be sure to use LVM to partitions for install or failures will occur
4. cat /vm/centos/centos7.conf   # to confirm grub paramenters, should be correct
5. sudo vm -f start centos7

sudo vm list

sudo vm start centos7

sudo vm destroy centos7
sudo vm console centos7


reference
https://github.com/churchers/vm-bhyve
https://forums.freebsd.org/threads/62113/

------------------------------------------------------------------------------------

# If not using LVM to partitions for install, these boot issues will occur

[  OK  ] Reached target Basic System.
[    0.812167]  vda: vda1 vda2 vda3
[    1.260077] input: PS/2 Generic Mouse as /devices/platform/i8042/serio1/input/input2
[    1.472683] tsc: Refined TSC clocksource calibration: 3196.809 MHz
[    1.473214] Switching to clocksource tsc


Aug 20 15:36:01 centos7 kernel: Switching to clocksource tsc
Aug 20 15:39:08 centos7 dracut-initqueue[209]: Warning: Could not boot.
Aug 20 15:39:08 centos7 dracut-initqueue[209]: Warning: /dev/mapper/centos-root
Aug 20 15:39:08 centos7 systemd[1]: Starting Dracut Emergency Shell...


Aug 20 15:36:00 centos7 kernel: ACPI Exception: AE_NOT_FOUND, While evaluating S
Aug 20 15:36:00 centos7 kernel: ACPI Exception: AE_NOT_FOUND, While evaluating S
Aug 20 15:36:00 centos7 kernel: ACPI Exception: AE_NOT_FOUND, While evaluating S
Aug 20 15:36:00 centos7 kernel: ACPI Exception: AE_NOT_FOUND, While evaluating S


Aug 20 15:36:01 centos7 kernel: Switching to clocksource tsc
Aug 20 15:39:08 centos7 dracut-initqueue[209]: Warning: Could not boot.
Aug 20 15:39:08 centos7 dracut-initqueue[209]: Warning: /dev/mapper/centos-root
Aug 20 15:39:08 centos7 systemd[1]: Starting Dracut Emergency Shell...


# if using newer version of Centos with XFS as the boot partition, then these issues will occur
grub> ls
(lvm/cl-root) (lvm/cl-swap) (hd0) (hd0,msdos2) (hd0,msdos1) (host) 
grub> ls (lvm/cl-root)/
** error: not a correct XFS inode. **
...
grub> ls (hd0,msdos2)/
error: unknown filesystem.
grub> ls (hd0,msdos1)/
error: not a correct XFS inode.

Note: CentOS-7-x86_64-Minimal-1511.iso works because the /boot partion does not utilize xfs where CentOS-7-x86_64-Minimal-1611.iso does utilize xfs.

------------------------------------------------------------------------------------

example
/vm/centos/centos7.conf

loader="grub"
cpu=1
memory=512M
network0_type="virtio-net"
network0_switch="public"
disk0_type="virtio-blk"
disk0_name="disk0.img"
grub_install0="linux /isolinux/vmlinuz LANG=en_US.UTF-8 KEYTABLE=us SYSFONT=latarcyrheb-sun16 console=ttyS0"
grub_install1="initrd /isolinux/initrd.img"
grub_run0="linux /vmlinuz-3.10.0-327.el7.x86_64 root=/dev/mapper/centos-root LANG=en_US.UTF-8 KEYTABLE=us SYSFONT=latarcyrheb-sun16 console=ttyS0"
grub_run1="initrd /initramfs-3.10.0-327.el7.x86_64.img"
network0_mac="58:9c:fc:00:7f:9b"

------------------------------------------------------------------------------------

https://forums.freebsd.org/threads/52014/

sudo vm stop myguest
sudo vm stop centos7

#sudo vm -f install myguest freebsd.iso
sudo vm -f install centos7 CentOS-7-x86_64-DVD-1611.iso
sudo vm -f install centos7 CentOS-7-x86_64-Minimal-1503-01.iso

sudo  vm switch list


ls (hd0,msdos1)/

kernel=/vmlinuz-3.10.0-229.20.1.el7.x86_64
grub/ grub2/ System.map-3.10.0-229.el7.x86_64 config-3.10.0-229.el7.x86_64 symvers-3.10.0-229.el7.x86_64.gz vmlinuz-3.10.0-229.el7.x86_64 initrd-plymouth.img
initramfs-0-rescue-745f780f827948f5a49cdbad32305b7c.img vmlinuz-0-rescue-745f780f827948f5a49cdbad32305b7c initramfs-3.10.0-229.el7.x86_64.img
grub> ls (hd0,msdos1)/


Post install
----------------

Check the current version of your release.

# cat /etc/redhat-release
CentOS Linux release 7.0.1406 (Core)
From the above, you can see that my machine is running CentOS 7.0

Now, let’s look what the updates available for your system are.

# yum check-update
# yum update
