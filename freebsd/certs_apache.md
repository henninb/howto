openssl genrsa -aes256 -out /root/ca/private/ca.key.pem 4096
openssl req -config openssl.cnf -key /root/ca/private/ca.key.pem -new -x509 -days 3650 -sha256 -extensions v3_ca -out /root/ca/certs/ca.cert.pem
#openssl x509 -noout -text -in /root/ca/certs/ca.cert.pem
openssl genrsa -out private/apache.key.pem 2048
openssl genrsa -aes256 -out private/kafka.key.pem 2048
openssl req -config openssl.cnf -key private/kafka.key.pem -new -sha256 -out csr/kafka.csr.pem
mkdir csr
openssl req -config openssl.cnf -key private/apache.key.pem -new -out csr/apache.csr.pem


openssl ca -config openssl.cnf -extensions server_cert -days 365 -notext -md sha256 -in csr/kafka.csr.pem -out certs/kafka.cert.pem
openssl ca -config openssl.cnf -extensions server_cert -days 365 -notext -md sha256 -in csr/apache.csr.pem -out certs/apache.cert.pem


openssl x509 -pubkey -noout -in cert.pem  > pubkey.pem
openssl rsa  -pubout -in ca.cert.pem > ca_root_pub.cert.pem

curl --cacert /root/certs/ca.cert.pem https://localhost
 
-------------------------------------------------------------------------
 
.csr. No keys
.pem. keys and/or certificates. Look for -----BEGIN PRIVATE KEY---- or -----BEGIN RSA PRIVATE KEY----- or -----BEGIN ENCRYPTED PRIVATE KEY-----
.key keys in pem format
.pkcs12 .pfx .p12 keys and/or certificates. List keys with openssl pkcs12 -info -nocerts -in keystore.p12
.der pem content without base64 encoding. Look for KEY in openssl x509   -inform DER -in cert.der
.cert .cer .crt keys and/or certificates. Content can be pem or der
.p7b. Only certificates
.crl. No keys

------------------------------------------------------------------------
# for windows (change format)

# Create the Root CA private key
openssl genrsa -out ca.key.pem 4096

# Generate the Root CA certificate signed with the private key
openssl req -x509 -new -nodes -key ca.key.pem -days 3650 -out ca.cert.pem


openssl pkcs12 -export -inkey ca.key.pem -in ca.cert.pem -out ca.cert.pfx 

------------------------------------------------------------------------
Create and then delete an empty truststore using the following commands:
keytool -genkey -keyalg RSA -alias endeca -keystore truststore.ks
keytool -delete -alias endeca -keystore truststore.ks

openssl pkcs12 -export -in ca.cert.pem -out ca.cert.pkcs12 
# Import the CA into the truststore, using the following command:
keytool -import -v -trustcacerts -alias endeca-ca -file ca.cert.pem -keystore truststore.ks

keytool -v -importkeystore -srckeystore ca.cert.pfx  -srcstoretype PKCS12 -destkeystore keystore.ks -deststoretype JKS
-----------------------------------------------------------------------

openssl req -x509 -newkey rsa:2048 -keyout private.key -out public.cert -days 365
Optionally, combine the pair into a single file.

openssl pkcs12 -export -inkey private.key -in public.cert -out certificate.pfx
This results in the following files.

private.key
certificate.pfx
public.cert
