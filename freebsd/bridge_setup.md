Manually setup a bridge
***************************************************************************************

/etc/sysctl.conf
net.link.tap.up_on_open=1

/boot/loader.conf
vmm_load="YES"
nmdm_load="YES"
if_bridge_load="YES"
if_tap_load="YES"


/etc/rc.conf
cloned_interfaces="bridge0 tap0"
ifconfig_bridge0="addm alc0 addm tap0"

where alc0 is the physical network interface device
