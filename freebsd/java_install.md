sudo pkg install openjdk8
        mount -t fdescfs fdesc /dev/fd
        mount -t procfs proc /proc

To make it permanent, you need the following lines in /etc/fstab:

fdesc   /dev/fd         fdescfs         rw      0       0
proc    /proc           procfs          rw      0       0
