https://wiki.freebsd.org/Docker

sudo pkg install docker-freebsd ca_root_nss

sudo kldload zfs
# 500 mb
sudo dd if=/dev/zero of=/usr/local/dockerfs bs=1024K count=500
sudo zpool create -f zroot /usr/local/dockerfs

sudo zfs list
sudo zfs create -o mountpoint=/usr/docker zroot/docker

sudo sysrc -f /etc/rc.conf docker_enable="YES"

sudo service docker start

docker pull ubuntu:16.04
docker pull centos:6

docker pull centos


docker run -it centos:6 /bin/bash

# list docker images
docker images


You will need to create a ZFS dataset on /usr/docker

# zfs create -o mountpoint=/usr/docker <zroot>/docker

And lastly enable the docker daemon
# sysrc -f /etc/rc.conf docker_enable="YES"
# service docker start

(WARNING)

Starting the docker service will also add the following PF rule:

nat on ${iface} from 172.17.0.0/16 to any -> (${iface})

Where $iface is the default NIC on the system, or the value
of $docker_nat_iface. This is for network connectivity to docker
containers in this early port. This should not be needed in future
versions of docker.


# 500 mb
# sudo kldload zfs
# sudo dd if=/dev/zero of=/usr/local/dockerfs bs=1024K count=500
# sudo zpool create -f zroot /usr/local/dockerfs


zroot on /zroot (zfs, local, nfsv4acls)
