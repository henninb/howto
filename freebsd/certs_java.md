sudo vi /etc/hosts

10.0.2.15 freebsd11.local
10.0.2.15 freebsd11

# The -key option specifies an existing private key (domain.key) that will be used to generate a new CSR. The -new option indicates that a CSR is being generated.
# trustStore - stores certificates from trusted Certificate authorities(CA) which is used to verify certificate presented by Server in 
# keyStore - used to store private key and own identity certificate which program

#Cert Authority (CA) self-signed cert
openssl req -x509 -days 365 -new  -keyout ca-key -out ca-cert.cer 

sudo openssl req -newkey rsa:2048 -nodes -keyout domain.key -x509 -days 365 -out domain.crt
sudo openssl req -x509 -days 365 -newkey rsa:2048 -keyout /etc/ssl/apache.key -out /etc/ssl/apache.crt

mkdir -p /home/henninb/security/x509/ /home/henninb/security/jks/
cd /home/henninb/security/jks

# Cert Authority (CA) - Generate the key pair and self-signed certificate, and store these in the keystore (freebsd11-keystore.jks). 
# Set -keypass to the same value as -storepass (some apps will not support differ)

# Ensure that common name (CN) matches exactly with the fully qualified domain name (FQDN) of the server. 
The client compares the CN with the DNS domain name to ensure that it is indeed connecting to the desired server, not a malicious one.

keytool -genkeypair -keystore localhost-keystore.jks -keyalg RSA -alias kafka-localhost -dname "CN=localhost,OU=Security,O=home,L=Denton,ST=Texas,C=US" -storepass monday1 -keypass monday1 -validity 365
keytool -genkeypair -keystore freebsd11-keystore.jks -keyalg RSA -alias kafka-freebsd11 -dname "CN=freebsd11,OU=Security,O=home,L=Denton,ST=Texas,C=US" -storepass monday1 -keypass monday1 -validity 365

# Copy the default Java truststore (cacerts) to the alternate system truststore (jssecacerts). 
# Self-signed certificates are appended to jssecacerts without modifying the default cacerts file

sudo cp /usr/local/openjdk8/jre/lib/security/cacerts /usr/local/openjdk8/jre/lib/security/jssecacerts

#alternate command
sudo cp $JAVA_HOME/jre/lib/security/cacerts $JAVA_HOME/jre/lib/security/jssecacerts

#export the cert from the keystore
keytool -export -alias kafka-freebsd11 -keystore freebsd11-keystore.jks -rfc -file selfsigned.crt.pem
keytool -export -alias kafka-localhost -keystore localhost-keystore.jks -rfc -file selfsigned.crt.pem
<password>

# Copy the self-signed certificate (selfsigned.cer) to the /home/henninb/security/x509/ directory.
cp selfsigned.cer /home/henninb/security/x509/freebsd11.pem

# Import the public key into the alternate system truststore (jssecacerts), 
# so that any process that runs with Java on this machine will trust the key. 
# The default password for the Java truststore is changeit
sudo keytool -import -alias freebsd11 -file /home/henninb/security/jks/selfsigned.crt.pem -keystore /usr/local/openjdk8/jre/lib/security/jssecacerts -storepass changeit

# clean up
rm /opt/cloudera/security/selfsigned.cer

# add cert to truststore
keytool -import -v -trustcacerts -alias freebsd11 -file selfsigned.cer -keystore cacerts-truststore.jks -keypass monday1
keytool -import -v -trustcacerts -alias localhost -file selfsigned.cer -keystore cacerts-truststore.jks -keypass monday1

http://docs.confluent.io/current/kafka/ssl.html

for Kafka

export PATH=$PATH:/usr/java/jdk1.8.0_121/jre

#keytool -keystore /root/centos7-keystore.jks -genkey -alias client

keytool -genkey -keyalg RSA -alias endeca -keystore centos7-truststore.jks
keytool -delete -alias endeca -keystore centos7-truststore.jks
keytool -import -v -trustcacerts -alias rootCA -file cacert.pem -keystore centos7-truststore.jks


openssl pkcs12 -export -name centos7-kafka -in centos7-kafka.cert.pem -inkey centos7-kafka.key.pem -out centos7-kafka.p12
openssl pkcs12 -export -name centos7-kafka -in /root/ca/certs/centos7-kafka.cert.pem -inkey /root/ca/private/centos7-kafka.key.pem -out /root/ca/centos7-kafka.p12

keytool -importkeystore -destkeystore centos7-keystore.jks -srckeystore centos7-kafka.p12 -srcstoretype pkcs12 -alias centos7-kafka
keytool -importkeystore -destkeystore /root/ca/centos7-keystore.jks -srckeystore /root/ca/centos7-kafka.p12 -srcstoretype pkcs12 -alias centos7-kafka

keytool -genkey -keyalg RSA -alias endeca -keystore centos7-keystore.jks
keytool -delete -alias endeca -keystore centos7-keystore.jks

#keytool -v -importkeystore -srckeystore eneCert.pkcs12 -srcstoretype PKCS12 -destkeystore keystore.ks -deststoretype JKS

server.properties
-----------------
ssl.client.auth = requested
security.inter.broker.protocol=SSL
listeners=SSL://localhost:9093
ssl.keystore.location=/root/centos7-keystore.jks
ssl.keystore.password=monday1
ssl.key.password=monday1
ssl.truststore.location = /root/centos7-truststore.jks
ssl.truststore.password=monday1


ssl.client.auth = none ("required" => client authentication is required, "requested"
ssl.client.auth = required ("required" => client authentication is required, "requested"
ssl.client.auth = requested ("required" => client authentication is required, "requested"


If you want to enable SSL for inter-broker communication, add the following to the broker properties file (it defaults to PLAINTEXT):

security.inter.broker.protocol = SSL


To verify if the server’s keystore and truststore are setup correctly you can run the following command:

openssl s_client -debug -connect localhost:9093 -tls1



For Appache
-----------

The JKS has certificates in DER and for Apache you want to have PEM (AKA X509) format.

Sample of how to do this:

keytool --list -keystore <mykeystore>
keytool -export -rfc -alias <alias_name> -file <cert.crt> -keystore <mykeystore>


LoadModule ssl_module libexec/apache24/mod_ssl.so



 <VirtualHost 127.0.0.1:443>
  DocumentRoot /usr/local/www/apache24/data
  ServerName localhost
  SSLEngine on
  SSLCertificateFile /etc/ssl/apache.crt
  SSLCertificateKeyFile /etc/ssl/apache.key
</VirtualHost>



[henninb@freebsd11 ~]$ openssl version -d
OPENSSLDIR: "/etc/ssl"

curl --cert /etc/ssl/apache.key https://localhost:443

cd /etc/ssl && ls -al


/usr/local/share/certs/ca-root-nss.crt


[henninb@freebsd11 /usr/local/share/certs]$ ls
ca-root-nss.crt
[henninb@freebsd11 /usr/local/share/certs]$


When I install one by hand it is in /usr/local/share/certs/ca-root-nss.crt, which I symlink to /etc/ssl/cert.pem and thats the main places I search for it. Without luck.

Is there no default root certificate store?

Because StartCom Class 1 is imported to /etc/ssl/cert.pem manually.

If you want to use proper certificates you'll need to request one first (Verisign and all). 
And instead of self-signed certificates why not use LetsEncrypt? That's easily set up using security/acme-client.


There are example scripts in
    /usr/local/etc/acme
that you can use for renewing and deploying multiple certificates

In order to run the script regularly to update
the certificates add this line to /etc/periodic.conf

    weekly_acme_client_enable="YES"

Additionally the following parameters can be added to
/etc/periodic.conf (showing default values):

To specify the domain name(s) to include in the certificate
    weekly_acme_client_domains="$(hostname -f)"

To specify the .well-known/acme-challenge directory (full path)
    weekly_acme_client_challengedir="/usr/local/www/acme"

To set additional acme-client arguments (see acme-client(1))
    weekly_acme_client_args="-b"

To run a specific script for the renewal (ignore previously set variables)
allows generating/renewing multiple keys/certificates
    weekly_acme_client_renewscript="/usr/local/etc/acme/acme-client.sh"

To run a script after the renewal to deploy changed certs
    weekly_acme_client_deployscript="/usr/local/etc/acme/deploy.sh"
[henninb@freebsd11 /etc/ssl]$

certutil -d sql:$HOME/.pki/nssdb -A -t "C,," -n <certificate nickname> -i <certificate filename>

openssl x509 -noout -fingerprint -in cert.pem

# view the openssl file
openssl x509 -in /etc/ssl/apache.crt -text

openssl pkcs12 -in mycert.p12 -out ca.pem -cacerts -nokeys
openssl pkcs12 -in mycert.p12 -out client.pem -clcerts -nokeys 
openssl pkcs12 -in mycert.p12 -out key.pem -nocerts



Firstly, you have a few terminology problems:

the X509 standard defines certificates, and RSA and DSA are two of the public key algorithms that can be used in those certificates;
certificates are used to hold public keys, and never private keys.
PKCS#12 is a standard for a container which can hold an X509 client certificates and the corresponding private keys, 
as well as (optionally) the X509 certificates of the CAs that signed the X509 client certificate(s).
So, if you're examining a PKCS#12 file (typically .p12 extension), then you already know:

It contains at least one X509 client certificate, which contains a public key; and
It contains the corresponding private keys.
All you don't know is whether those certificate & private key are RSA or DSA. You can check this by extracting the certificate(s), and then examine them:

> openssl x509 -text -noout -in apache.crt
> openssl verify -verbose -CAFile ca.crt apache.crt
> openssl rsa -check -in apache.key

> openssl rsa -noout -modulus -in apache.key | openssl md5
> openssl x509 -noout -modulus -in apache.crt | openssl md5

certificate signing request (CSR).

https://www.digitalocean.com/community/tutorials/openssl-essentials-working-with-ssl-certificates-private-keys-and-csrs

# Convert PEM to DER
> openssl x509 -in apache.crt -outform der -out apache.der

> openssl x509 \
       -inform der -in apache.der \
       -out apache.crt

openssl crl2pkcs7 -nocrl \
       -certfile domain.crt \
       -certfile ca-chain.crt \
       -out domain.p7b

openssl pkcs7 \
       -in domain.p7b \
       -print_certs -out domain.crt

openssl pkcs12 \
       -inkey domain.key \
       -in domain.crt \
       -export -out domain.pfx

       openssl pkcs12 \
       -in domain.pfx \
       -nodes -out domain.combined.crt

# test certificate
openssl s_client -connect my.server:443 -servername my.server -CAfile /path/to/your/top-level/ca/certificate
