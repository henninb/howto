wget http://olivier.sessink.nl/jailkit/jailkit-2.11.tar.gz

cd /tmp
wget http://olivier.sessink.nl/jailkit/jailkit-2.11.tar.gz
tar -zxvf jailkit-2.16.tar.gz
cd jailkit-2.16
./configure
make
make install

/usr/ports/shells/jailkit

https://www.freebsd.org/doc/handbook/jails-build.html

# JAIL_PATH=/usr/jail/www
# mkdir -p $JAIL_PATH/www
# cd /usr/src
# make buildworld
# make installworld DESTDIR=$JAIL_PATH
# make distribution DESTDIR=$JAIL_PATH
# mount -t devfs devfs $JAIL_PATH/dev

# jls
  JID  IP Address      Hostname                      Path
    3  192.168.0.10    www                          /usr/jail/www
# jexec 3 /etc/rc.shutdown

# service jail start www
# service jail stop www
