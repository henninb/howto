sudo pkg install linux-c7

sudo kldload linux64
sudo sysrc linux_enable="YES"

FreeBSD Now Has A Port For CentOS 7 Binary Support
Written by Michael Larabel in BSD on 6 September 2016 at 01:11 PM EDT. 10 Comments
BSD -- We've known for a while that FreeBSD has been working on a CentOS 7 compatibility layer while now that work has finally landed in FreeBSD ports. 

As of yesterday, linux_base-c7 landed in ports for installing the CentOS 7 base packages. This will allow running newer Linux binaries built for modern CentOS/RHEL 7 era systems on FreeBSD, assuming the source isn't available or isn't compatible natively with FreeBSD. Previously CentOS 6 was the default port used for this Linux binary compatibility with FreeBSD. 

FreeBSD's binary compatibility layer is good enough for running Linux games but when trying earlier this year atop FreeBSD 11.0 development code it wasn't working as nicely... But now with this CentOS 7 port, I'll try to get around to running some new tests shortly for some FreeBSD vs. Linux gaming benchmarks. 

More details on the emulators/linux_base-c7 addition can be found via FreshPorts.org.


Some programs need linprocfs mounted on /compat/linux/proc.  Add the
following line to /etc/fstab:

linprocfs   /compat/linux/proc	linprocfs	rw	0	0

Then run "mount /compat/linux/proc".

Some programs need tmpfs mounted on /compat/linux/dev/shm.  Add the
following line to /etc/fstab:

tmpfs    /compat/linux/dev/shm	tmpfs	rw,mode=1777	0	0

Then run "mount /compat/linux/dev/shm".


/compat/linux/bin/bash



rpm2cpio google-chrome-stable-60.0.3112.101-1.x86_64.rpm | pax -rv


No RPM database found.  If you wish to use RPM to install
RPM packages the you will need to initialise the database
with the commands:

        mkdir -p /var/lib/rpm
        /usr/local/bin/rpm --initdb

rpm2cpio < /home/dan/Downloads/google-chrome-stable-59.0.3071.115-1.x86_64.rpm | cpio -id
brandelf -t Linux /compat/linux/opt/google/chrome/chrome


[henninb@bsd /compat/linux]$ sudo yum update
There are no enabled repos.
 Run "yum repolist all" to see the repos you have.
 You can enable repos with yum-config-manager --enable <repo>
[henninb@bsd /compat/linux]$
