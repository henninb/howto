sudo pkg install lumina

start-lumina-desktop

An entry for for launching Lumina from a graphical login manager has already been added to the system, but if you with to start Lumina manually
, you will need to do one of the following:
1) Run "start-lumina-desktop" directly from the command line after logging in.
2) Put the line "exec start-lumina-desktop" at the end of your user's "~/.xinitrc"



sudo sysrc slim_enable="YES"
sudo sysrc dbus_enable="YES"
sudo sysrc hald_enable="YES"
