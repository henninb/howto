openssl.cnf
[ san_cert ]
basicConstraints    = CA:FALSE
nsRevocationUrl     = https://XXXXXXX.XXX/otCA.crl
subjectAltName      = DNS:fbsd,DNS:fbsd.local
extendedKeyUsage    = serverAuth

/usr/local/etc/apache24/httpd.conf
# Secure (SSL/TLS) connections
Include etc/apache24/httpd-ssl.conf
LoadModule ssl_module libexec/apache24/mod_ssl.so

touch /usr/local/etc/apache24/httpd-ssl.conf


<VirtualHost *:443>
  SSLEngine on
  SSLCertificateFile /root/ca/certs/fbsd.pem
  SSLCertificateKeyFile /root/ca/private/fbsd.pem
  servername fbsd
  Documentroot /usr/local/www/apache24/data
</VirtualHost>

#export SAN=DNS:fbsd,DNS:fbsd.local
mkdir -p /root/ca/private
mkdir -p /root/ca/certs
mkdir -p /root/ca/newcerts
touch /root/ca/index.txt
touch /root/ca/index.txt.attr
echo 1000 > /root/ca/serial
openssl req -new -newkey rsa:2048 -sha256 -x509 -days 3650 -extensions v3_ca -keyout /root/ca/private/cakey.pem -out /root/ca/cacert.pem -config /etc/ssl/openssl.cnf

openssl req -new -newkey rsa:2048 -sha256 -nodes -out /root/ca/fbsd-req.pem -keyout /root/ca/private/fbsd.key.pem -config /etc/ssl/openssl.cnf
openssl req -new -newkey rsa:2048 -sha256 -nodes -out /root/ca/vbox-fbsd-req.pem -keyout /root/ca/private/vbox-fbsd.key.pem -config /etc/ssl/openssl.cnf
openssl req -new -newkey rsa:2048 -sha256 -nodes -out /root/ca/centos7-kafka-req.pem -keyout /root/ca/private/centos7-kafka.key.pem -config /etc/ssl/openssl.cnf

openssl ca -config /etc/ssl/openssl.cnf -extensions san_cert -md sha256 -out /root/ca/certs/fbsd.cert.pem -infiles /root/ca/fbsd-req.pem
openssl ca -config /etc/ssl/openssl.cnf -extensions san_cert -md sha256 -out /root/ca/certs/vbox-fbsd.cert.pem -infiles /root/ca/vbox-fbsd-req.pem
openssl ca -config /etc/ssl/openssl.cnf -extensions san_cert -md sha256 -out /root/ca/certs/centos7-kafka.cert.pem -infiles /root/ca/centos7-kafka-req.pem

openssl x509 -in /root/ca/certs/fbsd.cert.pem -text

openssl pkcs12 -export -inkey /root/ca/private/cakey.pem -in /root/ca/cacert.pem -out /tmp/cacert.p12

/tmp/cacert.pem

subjectAltName = @alt_names
[alt_names]
DNS.1 = fbsd
DNS.2 = fbsd.local
