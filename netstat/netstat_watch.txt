netstat -an | awk '/tcp/ {print $6}'| sort | uniq -c

root> netstat -an | awk '/tcp/ {print $6}'| sort | uniq -c
      1 CLOSE_WAIT
     13 ESTABLISHED
     18 LISTEN
      2 TIME_WAIT
