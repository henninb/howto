netstat -na | grep EST| grep ":23 "

# uses name
netstat -a | grep EST| grep telnet

(t1212-rx) /> netstat -an | awk '/tcp/ {print $6}'| sort | uniq -c
      1 CLOSE_WAIT
     13 ESTABLISHED
     18 LISTEN
      2 TIME_WAIT


3.3 netstat
This is a utility that prints network connections, routing tables, and interface statistics, masquerade connections, and multicast memberships.

Since the tcpdump displays all exchanged packets, it is overwhelming to count the number of tcp packets, which could be easily retrieved with the netstat command by giving several options. We are interested in obtaining summarized statistics of tcp packets. Learn how to use netstat through "man netstat", and confirm that -st is necessary to display the number of tcp packets sent, received, and retransmitted so far.

Invoke netstat right before and after the execution of "ttcp -t". The actual number of tcp packets exchanged can be calculated as a difference in the statistics between those two invocations of netstat.


[user@uw1-320-01 hw3]$ ttcp -t [-options] uw1-320-02


(t1212-rx) /root> netstat -st | grep segments
    4189387 segments received
    7783470 segments send out
    871 segments retransmited
    0 bad segments received.
