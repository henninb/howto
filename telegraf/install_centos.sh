#!/bin/sh

sudo yum install -y net-tools psmisc

sudo cp influxdata.repo /etc/yum.repos.d/influxdata.repo

sudo yum install -y influxdb
sudo systemctl start influxdb

sudo systemctl status influxdb
sudo chkconfig influxdb on

#echo "CREATE USER 'henninb' WITH PASSWORD 'monday1' WITH ALL PRIVILEGES;"
echo "CREATE USER \"henninb\" WITH PASSWORD 'monday1' WITH ALL PRIVILEGES" | influx 
echo 'SHOW USERS' | influx

sudo sed -i "s/# auth-enabled = false/auth-enabled = true/g" /etc/influxdb/influxdb.conf

sudo systemctl restart influxdb
sudo systemctl enable influxdb.service

sudo yum install -y telegraf

sudo sed -i "s/# username = \"telegraf\"/username = \"henninb\"/g" /etc/telegraf/telegraf.conf
sudo sed -i "s/# password = \"metricsmetricsmetricsmetrics\"/password = \"monday1\"/g" /etc/telegraf/telegraf.conf
  # username = "telegraf"
  # password = "metricsmetricsmetricsmetrics"

sudo systemctl start telegraf
sudo systemctl status telegraf
sudo systemctl enable telegraf.service

echo 'show databases' | influx -username henninb -password monday1

echo use telegraf;
echo SELECT * FROM diskio;

#sudo firewall-cmd --zone=public --add-port=8086/tcp --permanent
#sudo firewall-cmd --reload
#sudo firewall-cmd --list-ports

netstat -na | grep LISTEN | grep tcp | grep 8086
sudo fuser 8086/tcp

exit 0
