C:\Program Files\TortoiseSVN\bin

The TortoiseSVN GUI program is called TortoiseProc.exe.
All commands are specified with the parameter /command:abcd
where abcd is the required command name.
Most of these commands need at least one path argument,
which is given with /path:"some\path". In the following
table the command refers to the /command:abcd parameter
and the path refers to the /path:"some\path" parameter.

TortoiseProc.exe /command:commit
                 /path:"c:\svn_wc\file1.txt*c:\svn_wc\file2.txt"
                 /logmsg:"test log message" /closeonend:0


svn log --verbose
TortoiseProc.exe /command:log /path:"C:\svnfiles\access"
