#!/bin/sh

mkdir howto
cd howto
git init
git remote add -f origin git@github.com:BitExplorer/howto.git
#This creates an empty repository with your remote, and fetches all objects but doesn't check them out. Then do:

git config core.sparseCheckout true
#Now you need to define which files/folders you want to actually check out. This is done by listing them in .git/info/sparse-checkout, eg:

echo "src/common/example" >> .git/info/sparse-checkout
echo "src/common/snakeToCamel.py" >> .git/info/sparse-checkout
#Last but not least, update your empty repo with the state from the remote:

git pull origin master

exit 0
