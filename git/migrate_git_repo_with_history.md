https://developer.atlassian.com/blog/2016/01/totw-copying-a-full-git-repo/

#https://stackoverflow.com/questions/17371150/moving-git-repository-content-to-another-repository-preserving-history

git fetch origin
#git branch -a
git remote add new-origin git@github.com:BitExplorer/NewRepo.git
git push --all new-origin
git push --tags new-origin

git remote rm origin
git remote rename new-origin origin

git remote show origin
git remote show
