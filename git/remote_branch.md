# fetch all remote branches
```
git fetch origin
```

# list all branches including remote branches
```
git branch -v -a
remotes/origin/remote_branch_name1
remotes/origin/remote_branch_name2
remotes/origin/remote_branch_name2
```

# checkout remote branch from master
```
git checkout -b local_branch_name1 origin/remote_branch_name1
git fetch origin local_branch_name1:remote_branch_name1
```

# delete remote branch or local branch
```
git push -d remote_name branch_name
git branch -d branch_name
git branch -D branch_name
```
Note: that in most cases the remote name is origin.
