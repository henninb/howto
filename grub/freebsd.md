# option 1
```
title  FreeBSD
root   (hd0,0,a)
kernel /boot/loader
```

# option 2
```
title FreeBSD
rootnoverify (hd0,0)
makeactive
chainloader +1
```