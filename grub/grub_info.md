centos - set the grub timeout

```shell
sudo vi /etc/default/grub
```

```
GRUB_TIMEOUT=10
sudo update-grub
```

recreate the grub.cfg
```shell
sudo grub2-mkconfig -o /boot/grub2/grub.cfg
sudo grub2-mkconfig -o /boot/efi/EFI/centos/grub.cfg
sudo grub2-mkconfig
```

add a custom grub entry
```shell
sudo vi /etc/grub.d/40_custom
```

```
#!/bin/sh
exec tail -n +3 $0
# This file provides an easy way to add custom menu entries.  Simply type the
# menu entries you want to add after this comment.  Be careful not to change
# the 'exec tail' line above.

menuentry "Windows" {
  set root=(hd0,1)
  chainloader +1
}
```

add a custom grub entry
```
sudo vi /etc/grub.d/40_custom
```

```
#!/bin/sh
exec tail -n +3 $0
# This file provides an easy way to add custom menu entries. Simply type the
# menu entries you want to add after this comment. Be careful not to change
# the 'exec tail' line above.
menuentry "FreeBSD" {
  set root=(hd2)
  chainloader +1
}
```

recreate the grub.cfg (either command)
```shell
sudo grub2-mkconfig -o /boot/grub2/grub.cfg
sudo grub2-mkconfig -o /boot/efi/EFI/centos/grub.cfg
sudo grub2-mkconfig
```

reset mbr
```shell
gpart bootcode -b /boot/boot0 ada0
```

add a custom grub entry
```shell
sudo vi /etc/grub.d/40_custom
```

```
set root=(hd1,gpt6)
linux /vmlinuz-linux root=/dev/nvme0n1p6
initrd /initramfs-linux.img
boot
```

```
  set root='(hd0,gpt5)'
  chainloader /EFI/Microsoft/Boot/bootmgfw.efi
```

```
menuentry "Windows 10 Test entry (on /dev/sdc1)" {
  insmod part_msdos
  insmod ntfs
  set root='(hd0,msdos1)'
  chainloader +1
}
```

recreate the grub.cfg
```shell
sudo grub2-mkconfig -o /boot/grub2/grub.cfg
sudo grub2-mkconfig -o /boot/efi/EFI/centos/grub.cfg
sudo grub2-mkconfig
```
