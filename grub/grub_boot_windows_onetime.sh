#!/bin/sh

# will boot into windows onetime
#sudo grub2-editenv - set next_entry='Windows Boot Manager (on /dev/nvme0n1p2)'
sudo grub2-editenv - set next_entry='Windows 10 (loader) on /dev/sdb1'
#sudo grub2-editenv - set saved_entry='CentOS Linux (3.10.0-862.2.3.el7.x86_64) 7 (Core)'
sudo grub2-editenv - set saved_entry=0

sudo grub2-mkconfig -o /boot/grub2/grub.cfg
#sudo grub2-mkconfig -o /boot/efi/EFI/centos/grub.cfg
sudo grub2-editenv list

#sudo grub2-reboot 'Windows 10 (loader) on /dev/sdb1'
#sudo grub2-reboot 'Windows'

exit 0

#Generating grub configuration file ...
#Found linux image: /boot/vmlinuz-4.16.12-1.el7.elrepo.x86_64
#Found initrd image: /boot/initramfs-4.16.12-1.el7.elrepo.x86_64.img
#Found linux image: /boot/vmlinuz-4.9.86-30.el7.x86_64
#Found initrd image: /boot/initramfs-4.9.86-30.el7.x86_64.img
#Found linux image: /boot/vmlinuz-3.10.0-862.2.3.el7.x86_64
#Found initrd image: /boot/initramfs-3.10.0-862.2.3.el7.x86_64.img
#Found linux image: /boot/vmlinuz-3.10.0-693.21.1.el7.x86_64
#Found initrd image: /boot/initramfs-3.10.0-693.21.1.el7.x86_64.img
#Found linux image: /boot/vmlinuz-0-rescue-3eac68e87d3b4d01b3f4182f9d1ad5b4
#Found initrd image: /boot/initramfs-0-rescue-3eac68e87d3b4d01b3f4182f9d1ad5b4.img
#Found Windows 10 (loader) on /dev/sdb1
#done
#saved_entry=1
#next_entry=Windows 10 (loader) on /dev/sdb1
