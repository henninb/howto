wget https://bintray.com/jfrog/artifactory-rpms/rpm -O bintray-jfrog-artifactory-rpms.repo
sudo mv bintray-jfrog-artifactory-rpms.repo /etc/yum.repos.d/
sudo yum install jfrog-artifactory-oss


The installation of Artifactory has completed successfully.

PLEASE NOTE: It is highly recommended to use Artifactory in conjunction with MySQL. You can easily configure this setup using '/opt/jfrog/artifactory/bin/configure.mysql.sh'.

You can activate artifactory with:
> sudo systemctl start artifactory.service

Then check the status with:
> systemctl status artifactory.service
  Verifying  : jfrog-artifactory-oss-5.4.6-50406900.noarch                                                                                                 1/1

Installed:
  jfrog-artifactory-oss.noarch 0:5.4.6-50406900

Complete!



http://localhost:8081/artifactory


sudo firewall-cmd --zone=public --permanent --add-port=8081/tcp
sudo firewall-cmd --list-ports


jfrog-cli
./jfrog rt u file.txt my-repo


https://www.jfrog.com/getcli/