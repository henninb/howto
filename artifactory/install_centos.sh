#!/bin/sh

sudo yum install -y net-tools psmisc java-1.8.0-openjdk

#wget https://dev.mysql.com/get/mysql57-community-release-el7-9.noarch.rpm
echo MariaDB is the mysql fork
sudo yum install -y mariadb-server mariadb

#sudo service mariadb start
sudo systemctl start mariadb
sudo systemctl enable mariadb

sudo firewall-cmd --zone=public --add-port=3306/tcp --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --list-ports

netstat -na | grep LISTEN | grep tcp | grep 3306
sudo fuser 3306/tcp

mysqladmin -u root password monday1

wget https://bintray.com/jfrog/artifactory-rpms/rpm -O bintray-jfrog-artifactory-rpms.repo
sudo mv bintray-jfrog-artifactory-rpms.repo /etc/yum.repos.d/
sudo yum install -y jfrog-artifactory-oss

sudo /opt/jfrog/artifactory/bin/configure.mysql.sh

sudo systemctl start artifactory

sudo firewall-cmd --zone=public --add-port=8081/tcp --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --list-ports

exit 0
