key servers
```
https://pgp.mit.edu/
```

gpg --edit-key Your-Key-ID-Here
gpg> passwd
gpg> save

export key
```
gpg --export-secret-keys -a 944C9FFA > secret.asc
```

list keys
```
gpg --list-keys
```

encrypt files
```
gpg -e -r Brian file.txt
gpg -e -r henninb@gmail.com file.txt
gpg -e -r 218C9015 file.txt
```

import keys
```
gpg --import private.key
```
