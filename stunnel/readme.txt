sudo yum install stunnel

openssl genrsa -out key.pem 2048
openssl req -new -x509 -key key.pem -out cert.pem -days 1095 -subj "/C=US/ST=Texas/L=Denton/O=Brian LLC/OU=centos7/CN=centos7"
cat key.pem cert.pem | sudo tee /etc/stunnel/stunnel.pem

sudo firewall-cmd --permanent --add-port=5433/tcp --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --list-ports


sudo vi /etc/stunnel/stunnel.conf

client = no
verify = 0
foreground = yes

[postgres-server]
protocol = pgsql
accept = centos7:5433
connect = 5432
cert = /etc/stunnel/stunnel.pem


client = yes
verify = 0
foreground = yes
[postgres-client]
protocol = pgsql
accept = 5431
connect = 192.168.100.135:5433
cert = /etc/stunnel/stunnel.pem
#psql -h centos7 -p 5431 -U henninb -d postgres

psql --set=sslmode=require -h centos7 -p 5433 -U henninb -d postgres
psql -h centos7 -p 5431 -U henninb -d postgres
psql "port=5433 host=centos7 user=henninb sslcert=$HOME/cert.crt sslkey=$HOME/key.pem sslmode=require"
