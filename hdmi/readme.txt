Who Needs HDMI Cables With Ethernet?

First, an important disclaimer. For the Ethernet connection to work properly, you must be connecting two devices which each support Ethernet over HDMI (they’ll be labeled HEC compatible), and not many do just yet. In many ways, you are planning for the future as much as for the present when buying an HDMI cable with Ethernet. Here are the ways these forward-looking cables can work, though.
