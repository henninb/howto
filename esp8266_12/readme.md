https://www.youtube.com/watch?v=O2SSyfP6OM0
adapter plate for ESP8266-12

# Uart Programming ESP8266-12 
VCC (ESP8266-12)    -> 3.3V (POWER) (red)
CH_PD (ESP8266-12)  -> 3.3V (POWER) (red)
GND (ESP8266-12)    -> GND (POWER) (black)
GND (ESP8266-12)    -> GND (FTDI) (black)
TX (ESP8266-12)     -> RX (FTDI) (green)
RX (ESP8266-12)     -> TX (FTDI) (yellow)
GPIO_0 (ESP8266-12) -> GND (POWER) (black) (Sets the module in boot mode for programming)
GPIO15 (ESP8266-12) -> GND (POWER) (black)