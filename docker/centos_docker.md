sudo docker pull centos:7
sudo docker pull centos/httpd-24-centos7

sudo docker run -it centos:7 /bin/bash

# run in daemon mode
sudo docker run -d -p 8080:80 --name container_name --restart unless-stopped image_name
sudo docker exec container_name /bin/sh

# run in interactive mode 
sudo docker run -it --name archlinux -h archlinux --restart unless-stopped base/archlinux
sudo docker attach archlinux

-p 5801:5801


# Build an image using the Dockerfile at current location

FROM centos/httpd-24-centos7

EXPOSE 80

RUN echo "Hello docker!"

# File Author / Maintainer
MAINTAINER Brian

# Example: sudo docker build -t [name] .
sudo docker build -t myhttpd_image .

sudo docker run -d --name myhttp_instance -h myhttp_instance --restart unless-stopped -p 8888:80 myhttpd
sudo docker run -d --name myhttp_instance -h myhttp_instance --restart unless-stopped -p 8888:8080 myhttpd_image
sudo docker ps -l

#sudo docker run -d -p 8888:80 --name myhttpd
#sudo docker run -p 8888:80 --name myhttpd


sudo firewall-cmd --zone=public --add-port=8888/tcp --permanent
sudo firewall-cmd --reload

Configuration
The Apache HTTP Server container image supports the following configuration variable, which can be set by using the -e option with the docker run command:

Variable name	Description
HTTPD_LOG_TO_VOLUME1	By default, httpd logs into standard output, so the logs are accessible by using the docker logs command. When HTTPD_LOG_TO_VOLUME is set, httpd logs into /var/log/httpd24, which can be mounted to host system using the Docker volumes


#!/bin/bash
# Stop all containers
sudo docker stop $(sudo docker ps -a -q)
# Delete all containers
sudo docker rm $(sudo docker ps -a -q)
# Delete all images
sudo docker rmi $(sudo docker images -q)
