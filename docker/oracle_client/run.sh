#!/bin/sh

sudo docker image build -t oracle_client .
docker stop oracle_client
docker rm oracle_client -f
sudo docker run --name=oracle_client -h oracle_client --restart unless-stopped -d oracle_client
#docker exec -it --user root oracle_client /bin/sh

exit 0
