Docker Enterprise Edition (Docker EE) or Docker Community Edition (Docker CE). Learn more about Docker EE and Docker CE.

https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-centos-7
http://blog.thoward37.me/articles/where-are-docker-images-stored/

#clean up docker
docker system prune -a

sudo docker run alpine ip r
sudo docker run alpine ip a

# see the docker network
sudo docker network inspect bridge

prune all docker containers and images
```docker system prune -a```

pull some common containers
```
docker pull postgres:9.6.1
docker pull openjdk:alpine
docker pull openjdk:8
docker pull openjdk:9
docker pull httpd:2.4
```

image build (Dockerfile)
```
sudo docker image build -t image_name .
```

list images
```
sudo docker images
sudo docker images alpine
```

remove image
```
sudo docker rmi b00b15f12050
```

run in daemon mode
```
sudo docker run -d -p 1521:1521 -t 478bf389b75b
sudo docker run -d -p 8080:80 -t container_name
sudo docker run -d -p 5432:5432 -t 478bf389b75b
sudo docker run -d -t 478bf389b75b
```

run in interactive mode
```
sudo docker run -it b7980cfae099 /bin/sh
sudo docker exec -it 478bf389b75b /bin/bash
sudo docker exec -i -t 6b10061620be /bin/bash
```

build the image when you run "docker build".

docker search images
```
docker search centos
docker search java
```

docker files location
```
ls -l /var/lib/docker/
```

simple container
```
docker run -d openjdk:alpine /bin/sh -c "while true; do echo hello world; sleep 1; done"
```




https://hub.docker.com/r/anapsix/alpine-java/~/dockerfile/

docker run -it --rm anapsix/alpine-java java -version
docker run -it anapsix/alpine-java java -version

start a container
```
docker start 9e6396cd7a31
```

docker rm cc8181a0c767

tag a container with a label
```
docker tag 4b795844c7ab /hello-world
```

list running containers
```
sudo docker ps -a
sudo docker container ls
```

container commands
```
docker start starts a container so it is running.
docker stop stops a running container.
docker restart stops and starts a container.
docker pause pauses a running container, "freezing" it in place.
docker unpause will unpause a running container.
docker wait blocks until running container stops.
docker kill sends a SIGKILL to a running container.
docker attach will connect to a running container.
```

docker container ls

Dockerfile 

list of steps

build an image from the docker file

run docker image


create an src directory

<?php
echo "hello world";
?>

FROM php:7.0-apache
COPY src/ /var/www/html
EXPOSE 80

sudo docker build -t my_javaserver . 

docker build -t javaServer .
docker run -p 80:80 -v c:\fullpath\src /var/www/html hello-world
docker info

docker run --name some-app --link some-postgres:postgres -d application-that-uses-postgres

9.6.1, 9.6, 9, latest (9.6/Dockerfile)


FROM postgres:9.6.1
EXPOSE 5432

RUN apt-get update && apt-get install -y vim

#RUN psql --command "CREATE USER docker WITH SUPERUSER PASSWORD 'docker';"

# Adjust PostgreSQL configuration so that remote connections to the database are possible.
RUN echo "host all  all    0.0.0.0/0  md5" >> /var/lib/postgresql/data/pg_hba.conf

docker build -t my_postgres .
docker run -p 5432:5432 my_postgres

sudo docker run --name postgres_localname -t postgres
sudo docker ps -a 
sudo docker exec -i -t 5d2b8d44bc03 /bin/bash

 --publish 5432:5432


docker run --name postgresql -itd --restart always \
  --publish 5432:5432 \
  --volume /srv/docker/postgresql:/var/lib/postgresql \
  sameersbn/postgresql:9.5-3
  
docker create -v /var/lib/postgresql/data --name postgres9.3.6-data busybox
docker run --name local-postgres9.3.6 -e POSTGRES_PASSWORD=asecurepassword -d --volumes-from postgres9.3.6-data postgres:9.3.6
docker run --name local-postgres9.6.1 -p 5432:5432 -e POSTGRES_PASSWORD=asecurepassword -d --volumes-from postgres9.6.1-data postgres:9.6.1



setup arch docker
/etc/docker/daemon.json