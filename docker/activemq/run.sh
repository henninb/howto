#!/bin/sh

mkdir $HOME/activemq_data
sudo docker image build -t activemq .
docker stop activemq
docker rm activemq -f
#sudo sudo docker images
#sudo docker rmi a6bb5d73a5dc --force
#sudo docker stop activemq && docker rm activemq
sudo docker run --name=activemq -h activemq --restart unless-stopped -p 8161:8161 -p 61616:61616 -p 61613:61613 -v $HOME/activemq_data:/opt/activemq/data -d activemq
docker exec -it --user root activemq /bin/sh

exit 0
