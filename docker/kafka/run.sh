#!/bin/sh

sudo docker image build -t kafka .
docker stop kafka
docker rm kafka -f
sudo docker run --name=kafka -h kafka --restart unless-stopped -d kafka
#docker exec -it --user root kafka /bin/sh

exit 0
