#!/bin/sh

echo installing the docker community edition

sudo yum install -y net-tools psmisc
sudo yum remove -y docker docker-common docker-selinux docker-engine

#sudo yum install -y yum-utils device-mapper-persistent-data lvm2

sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
#curl -fsSL https://get.docker.com/ | sh

sudo yum update
sudo yum install -y yum-utils device-mapper-persistent-data lvm2 docker-ce

sudo systemctl start docker
sudo systemctl enable docker

sudo chkconfig docker on

sudo usermod -aG docker $(whoami)

sudo docker run hello-world

#sudo docker search centos
sudo docker search --filter "is-official=true" centos
sudo docker search --filter "is-official=true" alpine
sudo docker search --filter "is-official=true" java
sudo docker search --filter "is-official=true" mongo
sudo docker search --filter "is-official=true" telegraf
sudo docker search --filter "is-official=true" postgres
sudo docker search archlinux

# need to set the config to expose the port
#netstat -na | grep LISTEN | grep tcp | grep 2376
netstat -na | grep LISTEN | grep tcp | grep 2375

#sudo fuser 2376/tcp
sudo fuser 2375/tcp

sudo firewall-cmd --zone=public --add-port=2375/tcp --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --list-ports

echo /etc/docker/daemon.json

exit 0
