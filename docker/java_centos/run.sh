#!/bin/sh

sudo docker image build -t java_centos .
docker stop java_centos
docker rm java_centos -f
sudo docker run --name=java_centos -h java_centos --restart unless-stopped -d java_centos
#docker exec -it --user root java_centos /bin/sh

exit 0
