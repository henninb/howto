#!/bin/sh

sudo cp kubernetes.repo /etc/yum.repos.d/kubernetes.repo
sudo yum install -y docker kubelet kubeadm kubectl kubernetes-cni etcd flannel

#sudo hostnamectl set-hostname 'k8s-master'
#sudo exec bash
sudo setenforce 0
sudo sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux

sudo firewall-cmd --add-port=6443/tcp --permanent
sudo firewall-cmd --add-port=10255/tcp --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --list-ports

#sudo modprobe br_netfilter
#sudo echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables

sudo usermod -aG dockerroot henninb

sudo cp daemon.json /etc/docker/daemon.json
sudo systemctl restart docker

sudo systemctl restart docker
sudo systemctl enable docker
sudo systemctl restart kubelet
sudo systemctl enable kubelet

sudo swapoff -a
echo sudo vi /etc/fstab

sudo kubeadm init

netstat -na | grep LISTEN | grep tcp | grep 6443
netstat -na | grep LISTEN | grep tcp | grep 10255
sudo fuser 6443/tcp
sudo fuser 10255/tcp

#ls -l /etc/kubernetes/manifests/kube-apiserver.yaml
#sudo ls /etc/kubernetes/admin.conf

#sudo vi /etc/kubernetes/apiserver
#KUBE_API_PORT="--port=8080"
#sudo systemctl restart kube-apiserver

sudo cp /etc/kubernetes/admin.conf $HOME/
sudo chown $(id -u):$(id -g) $HOME/admin.conf
export KUBECONFIG=$HOME/admin.conf
echo export KUBECONFIG=$HOME/admin.conf >> $HOME/.bashrc

# run not as root
kubectl get pods

kubectl create -f postgres.yml

#kubectl config set-cluster demo-cluster --server=http://master.example.com:8080
#kubectl config set-context demo-system --cluster=demo-cluster
#kubectl config use-context demo-system
#kubectl get nodes

#my k8s is running on 6443
#defalut port: 10255 kublet

#[ERROR SystemVerification]: failed to get docker info: Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?
#[ERROR Swap]: running with swap on is not supported. Please disable swap

exit 0
