cluster - Master and nodes


pod manifest file

delaritive mode - manifest files tell it what we want it to look like

manifest files are a record of intent (JSON/YAML)
my k8s is running on 6443
default port: 4443 api-server
defalut port: 10255 kublet

Masters - can have many Masters, 
  api server (aka master), exposes REST API, consumes manifest files, brains of the cluster
  cluster store - memory, state, uses etcd (key/value) store by coreos (source of truth)
  controller manger - node controller, endpoint controller, namespace controller, watch for changes, maintains desired state
  scheduler - watches the api server for new pods, assigns work to nodes

Nodes (Minions)
  kubelet (daddy) - is the node, registers the node with the cluster,reports back to the master  /spec, /healthz, /pods
  container runtime - does container management, pulls images, usually docker or rkt
  proxy - network brains, 1 ip per pod, load balance
  
  
  
Pods - 1 or more containers packages together as a single unit
  sandbox to run N containers
  tightly coupled - single pod
  loosely coupled - multiple pod
  scaling - add pod/remove pod replicas
  pods are atomic (up or down)
  

Services - a way of hiding multiple multiple pods behind a single address.
Deployments

replication controller


---

install on centos
1) add repos
2) sudo yum install -y docker kubelet kubeadm kubectl kubernetes-cni etcd flannel

sudo vi /etc/selinux/config
SELINUX=disabled
#sudo setenforce 0
#sudo semodule -l

sudo systemctl enable docker 
sudo systemctl start docker
# network port not open typically
# what network port does docker run on? 2379
# netstat -na| grep 2379 | grep tcp | grep LIST

sudo systemctl enable kubelet 
sudo systemctl start kubelet

---
https://coderleaf.wordpress.com/2017/02/10/run-docker-as-user-on-centos7/

issue: Cannot connect to the Docker daemon. Is the docker daemon running on this host?
ls -lah /var/run/docker.sock
srw-rw----. 1 root root 0 Sep 16 12:24 /var/run/docker.sock

fix: temporary
sudo chgrp dockerroot /var/run/docker.sock

fix: best
sudo usermod -aG dockerroot henninb
sudo vi /etc/docker/daemon.json
{
    "live-restore": true,
    "group": "dockerroot"
}
sudo systemctl restart docker

---

issue:
# may need to remove docker instances
sudo yum remove docker-ce

---

[preflight] WARNING: firewalld is active, please ensure ports [6443 10250] are open or your cluster may not function correctly

---

do not install
#sudo yum install kubernetes

---

#setup kubernetes
sudo kubeadm init

#other options
#sudo kubeadm init --token-ttl 0


kubectl cluster-info

#destroy kubernetes
sudo kubeadm reset

---
issue: hangs at the following
[apiclient] Created API client, waiting for the control plane to become ready
fix: 
sudo vi /etc/selinux/config
SELINUX=disabled
#sudo setenforce 0
sudo reboot
--

sudo kubeadm join --token f0162f.b24fd65f0694dda3 192.168.100.148:6443


Your Kubernetes master has initialized successfully!

To start using your cluster, you need to run (as a regular user):

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  http://kubernetes.io/docs/admin/addons/

You can now join any number of machines by running the following on each node
as root:

sudo kubeadm join --token 7802a8.ee65f1f2b8180c6b 10.0.2.15:6443


Transaction check error:
  file /usr/bin/kubelet from install of kubelet-1.7.5-0.x86_64 conflicts with file from package kubernetes-node-1.5.2-0.7.git269f928.el7.x86_64
  file /usr/bin/kubectl from install of kubectl-1.7.5-0.x86_64 conflicts with file from package kubernetes-client-1.5.2-0.7.git269f928.el7.x86_64

Error Summary
-------------

# curl version
export KUBERNETES_PROVIDER=vagrant; curl -sS https://get.k8s.io | bash
gce - Google Compute Engine [default]
gke - Google Container Engine
aws - Amazon EC2
azure - Microsoft Azure
vagrant - Vagrant (on local virtual machines)
vsphere - VMWare VSphere
rackspace - Rackspace

download
https://storage.googleapis.com/kubernetes-release/release/v1.7.0/bin/windows/amd64/kubectl.exe


kubectl get pods --all-namespaces

kubectl create namespace sock-shop


# sudo snap install kubectl --classic

[henninb@centos7 ~]$ kubectl cluster-info
Kubernetes master is running at https://10.0.2.15:6443
KubeDNS is running at https://10.0.2.15:6443/api/v1/namespaces/kube-system/services/kube-dns/proxy


echo "source <(kubectl completion bash)" >> ~/.bashrc

kubectl run hello-world --replicas=2 --labels="run=load-balancer-example" --image=gcr.io/google-samples/node-hello:1.0  --port=8080


kubectl expose deployment hello-world --type=NodePort --name=example-service
kubectl describe services example-service

kubectl get pods --selector="run=load-balancer-example" --output=wide


kubectl create -f postgres.yml


kubectl  delete pod postgres-qvhrb
kubectl get pods
kubectl get nodes


# to apply plugins
kubectl apply -f https://git.io/weave-kube

# weave is recommended over flannel

sudo yum install libvirt-daemon-kvm qemu-kvm
sudo usermod -a -G libvirt $(whoami)
newgrp libvirt

curl -L https://github.com/dhiltgen/docker-machine-kvm/releases/download/v0.10.0/docker-machine-driver-kvm-centos7 > docker-machine-driver-kvm

sudo mv docker-machine-driver-kvm /usr/local/bin/docker-machine-driver-kvm
 sudo chmod +x /usr/local/bin/docker-machine-driver-kvm


curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/
minikube
minikube start
minikube start --vm-driver=kvm

# windows 7
minikube.exe start --kubernetes-version="v1.5.2" --vm-driver="virtualbox" --v=3



---
To add new nodes to your cluster do the following for each machine:
SSH to the machine.
Become root (e.g. sudo su - )
Run the command that was output by kubeadm init . For example: kubeadm join --token <token> <master-ip>:<master-port>
---



I had this issue on a DigitalOcean droplet running Debian Jessie. No proxy issues etc.

It turns out that in my case the kubelet refused to start containers due to memory accounting cgroup not being enabled.

To fix it I did the old standby: edited /etc/default/grub line GRUB_CMDLINE_LINUX to include cgroup_enable=memory arg and rebooted the VM. Then running kubeadm init from scratch worked.




sudo firewall-cmd --zone=public --add-port=6443/tcp
sudo firewall-cmd --list-ports

sudo firewall-cmd --zone=public --permanent --add-port=6443/tcp


--apiserver-advertise-address  --apiserver-bind-port

cp /etc/kubernetes/kubectl.kubeconfig /root/.kube/config



yum -y install ntp

systemctl stop firewalld


 kubelet --version
 
# get the cluster IP address
kubectl get svc -o wide --namespace=kube-system

# get pods
kubectl get pods --all-namespaces
kubectl delete pod hello-minikube-6fd785d459-dlhhk

# show the routing
sudo iptables -L KUBE-SERVICES

kubectl get endpoints hostnames

kubectl get svc hostnames

kubectl describe pod kube-dns-2425271678-ztrlq --namespace=kube-system

  FirstSeen     LastSeen        Count   From                    SubObjectPath   Type            Reason                  Message
  ---------     --------        -----   ----                    -------------   --------        ------                  -------
  1h            55m             216     default-scheduler                       Warning         FailedScheduling        no nodes available to schedule pods
  53m           10s             187     default-scheduler                       Warning         FailedScheduling        no nodes available to schedule pods

  
[henninb@centos7 systemd]$ kubectl proxy
Starting to serve on 127.0.0.1:8001




kubectl apply -f https://git.io/weave-kube-1.6



kubectl delete pods,services -l name=weave-net-n43v7


# Delete a pod with UID 1234-56-7890-234234-456456.
kubectl delete pod 1234-56-7890-234234-456456

# Delete all pods
kubectl delete pods --all

kubectl apply -f https://git.io/weave-kube
kubectl -n kube-system delete -f https://git.io/weave-kube


kubectl create -f nginx_pod.yaml

kubectl create -f nginx_service.yaml

Now we will delete the nginx pod and service
$ kubectl delete service nginxservice
$ kubectl delete rc nginx



https://kubernetes.io/docs/user-guide/walkthrough/k8s201/


$ export SERVICE_IP=$(kubectl get service nginx-service -o go-template='{{.spec.clusterIP}}')
$ export SERVICE_PORT=$(kubectl get service nginx-service -o go-template='{{(index .spec.ports 0).port}}')
$ echo "$SERVICE_IP:$SERVICE_PORT"
$ kubectl run busybox  --generator=run-pod/v1 --image=busybox --restart=Never --tty -i --env "SERVICE_IP=$SERVICE_IP,SERVICE_PORT=$SERVICE_PORT"
u@busybox$ wget -qO- http://$SERVICE_IP:$SERVICE_PORT # Run in the busybox container
u@busybox$ exit # Exit the busybox container
$ kubectl delete pod busybox # Clean up the pod we created with "kubectl run"




kubectl expose nginx-deployment/nginx_service

#not sure what this does
#service "my-nginx" exposed


# edit a service
kubectl edit svc nginx-service

NodePort to LoadBalancer

kubectl describe service nginx-service


kubectl expose --port=8000 --target-port=8000 --type=LoadBalancer  -f nginx_service.yml
--port 80 --target-port 999 --type="LoadBalancer"
