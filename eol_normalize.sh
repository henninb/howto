#!/bin/sh
#find . -name "*.txt" -print | xargs sed -i 's/\r//g'
#find . -name "*.cmd" -print | xargs sed -i 's/\r//g'
#find . -name "*.sh" -print | xargs sed -i 's/\r//g'


find . \( -name "*.txt" -o -name "*.cmd" -o -name "*.sh" -name "*.bat" -name "*.xml" -o -name "*.c" -o -name "*.clj" -o -name "*.pl" -o -name "*.xml" -o -name "*.ino" -name "*.md" -name "*.java" -name "build.gradle" \) -print | xargs sed -i 's/\r//g'

echo "tr -d '\r' < infile > outfile"
echo cat infile | od -c

exit 0
