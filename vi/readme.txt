# ran in archlinux
echo 'export EDITOR=/usr/bin/vim' | sudo tee /etc/profile.d/editor.sh && sudo chmod a+x /etc/profile.d/editor.sh

cd /usr/bin && sudo rm vi && sudo ln -s /usr/bin/vim vi

```config```
vi ~/.vimrc
#not sure
#autocmd!

set tabstop=4
set shiftwidth=4
set expandtab
set paste
set foldmethod=indent
set tags=tags
set nu  #line numbers

```search and replace, c to confirm each```
:%s/one/two/gc

```visual mode```
v to select and work on blocks

command buffer - search and command mode (history)
```folding```
zc - close
zo - open
zi - toggles all

```macros```
q to record <then a letter a>, should say recording
then action
then q <done recording>
@a
@@ same one
