#!/bin/sh

sudo cp mongodb-org.repo /etc/yum.repos.d/mongodb-org.repo
sudo yum repolist
sudo yum install -y mongodb-org
sudo systemctl enable mongod

sudo systemctl status mongod

sudo systemctl start mongod

sudo chkconfig mongod on

#sudo systemctl reload mongod


sudo firewall-cmd --zone=public --add-port=27017/tcp --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --list-ports

sudo sed -i "s/127.0.0.1/0.0.0.0/g" /etc/mongod.conf
sudo systemctl restart mongod

#  ssl:
#    mode: requireSSL
#    PEMKeyFile: /etc/pki/tls/private/ca.key.pem
#    CAFile: /etc/pki/tls/certs/centos_mongo.crt.pem
#    disabledProtocols: TLS1_0,TLS1_1

netstat -na | grep LIST | grep tcp | grep 27017

sudo fuser 27017/tcp

exit 0
