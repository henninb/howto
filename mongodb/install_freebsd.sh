#!/bin/sh

sudo pkg install -y mongodb
sudo /usr/local/etc/rc.d/mongodb start
sudo sysrc mongod_enable="YES"
sudo service mongod start

cat > mongodb.conf <<'EOF'
net:
  port: 27017
  bindIp : [127.0.0.1,0.0.0.0]
EOF

#net:
#  ssl:
#    mode: requireSSL
#    PEMKeyFile: /etc/ssl/mongodb.pem
#    CAFile: /etc/ssl/ca.pem
#    disabledProtocols: TLS1_0,TLS1_1

sudo mv mongodb.conf /usr/local/etc/mongodb.conf

sudo service mongod restart

netstat -na | grep LIST | grep tcp | grep 27017

exit 0
