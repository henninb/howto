#!/bin/sh

sudo apt-get install -y mongodb libboost-dev libsnappy1 mongodb-dev mongodb-clients mongodb-server mongo
sudo service mongo start
sudo /etc/init.d/mongodb start
sudo update-rc.d mongodb defaults


sudo apt install dirmngr
# Release: The following signatures couldn't be verified because the public key is not available: NO_PUBKEY 58712A2291FA4AD5
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 58712A2291FA4AD5
echo "deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/3.6 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
sudo apt-get update



sudo usermod -a -G mongodb pi

sudo sed -i "s/127.0.0.1/0.0.0.0/g" /etc/mongod.conf
sudo /etc/init.d/mongodb restart

netstat -na | grep LIST| grep tcp | grep 27017

exit 0
