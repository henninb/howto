@echo off

set PATH=C:\Program Files\Oracle\VirtualBox

VBoxManage controlvm  "centos7" savestate
VBoxManage modifyvm "centos7" --natpf1 "mongodb,tcp,127.0.0.1,27017,,27017"
VBoxManage startvm "centos7" --type headless

pause
