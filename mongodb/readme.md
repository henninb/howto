mongo client
```
mongo localhost:27017/admin -u henninb -p monday1
mongo localhost:27017/admin
mongo localhost:27017/local
```

add users
```
db.createUser({user:"henninb",pwd:"monday1", roles:[{role:"userAdminAnyDatabase",db:"admin"}]})
```

database operations
```
show dbs
use finance_db
```

show active db
```
db
```

collection operations (tables)
```
show collections
db.createCollection("master_bedroom")
db.createCollection("lower_bathroom")
db.createCollection("garage")
db.createCollection("laundry_room")
```

selecting data
```
db.getCollection('TransactionEntity').find({cleared:"1"})
db.getCollection('laundry_room').find({sourceLocation:"laundry_room"})
db.collection.find().sort( { age: -1 } )
```

deleting data
```
db.getCollection('laundry_room').deleteMany({sourceLocation:null})
db.getCollection('lower_bathroom').deleteMany({sourceLocation:null})
db.getCollection('master_bedroom').deleteMany({sourceLocation:null})
db.getCollection('office').deleteMany({sourceLocation:null})
db.getCollection('garage').deleteMany({sourceLocation:null})
```

insert data
```
db.garage.insert({"temperature":"80.1"})
db.account.insert({"accountNameOwner":"rcard_brian", "accountType": "credit", "activeStatus": "Y","moniker": "0000", "totals": 0.00, "totalsBalanced": 0.00, "dateClosed": 0, "dateUpdated": 0, "dateAdded": 0})
```

exporting data out of mongo
```
mongoexport --host localhost --db comfort_db --collection lower_bathroom --csv --out lower_bathroom.csv --fields fahrenheit,humidity
mongoexport --host localhost --db comfort_db --collection master_bedroom --csv --out master_bedroom.csv --fields fahrenheit,humidity
mongoexport --host localhost --db comfort_db --collection garage --csv --out garage.csv --fields fahrenheit,humidity
```

misc
https://www.mkyong.com/mongodb/mongodb-allow-remote-access/
https://www.mongodb.com/download-center?jmp=nav#community
https://www.unixmen.com/install-mongodb-centos-7/
client Robo 3T 1.1
